  package utilities;

import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class PDFReader {

	public static String readPDfContent(String pathOfFile) {
		String pdfFileInText = "";
		try {
			PDDocument document = PDDocument.load(new File(pathOfFile));
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				pdfFileInText = tStripper.getText(document);
				System.out.println(pdfFileInText);

			}
			return pdfFileInText;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;

		}
	}
}
