package pages.win;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MercerJobLibraryPage{
	
	@FindBy(xpath="//button[@data-purpose='view-select']")
	public static WebElement selectview;
	
    @FindBy(xpath="//button[contains(text()[normalize-space()],'Automation_JobView')]")
    public static WebElement jobview;
    
	@FindBy(xpath="//input[@data-id='kw']")
	public static WebElement keyword;
	
	@FindBy(xpath="//button[@data-id='mv']")
	public static WebElement marketview;
	
	@FindBy(xpath="(//div[@class='data-grid display-below portal']//div[@class='body']//tbody//tr[@class='odd'])[1]")
	public static WebElement mjltableresult;
	
	@FindBy(xpath="//a[text()='EMEA']")
	public static WebElement emea;		
	
	@FindBy(id="EU.ZA.ac55756c-dd17-e811-900f-8cdcd41d9e58")
	public static WebElement wintest;	
			
	@FindBy(xpath="//button[text()='Apply']")
	public static WebElement apply;
	
	@FindBy(xpath="//button[@class='search-button']")
	public static WebElement search;
	
	@FindBy(xpath="//input[@data-record='-1']")
	public static WebElement selectjobs;
	
	@FindBy(xpath="//button[contains(text()[normalize-space()], 'Continue')]")
	public static WebElement continuebutton;
	
	@FindBy(xpath="//div[text()='Mercer Market Data Results: Library']")
	public WebElement libraryresultpage;
	
	@FindBy(xpath="//span[text()='Mercer Job Library']")
	public WebElement mercerjoblibrarytab;
	
	WebDriver driver;
	
	public MercerJobLibraryPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	public void jobLibrary() throws InterruptedException {
		waitForPageLoad(driver);
		waitForElementToClickable(selectview);
		clickElement(selectview);
		scrollToElement(driver,jobview);
		clickElement(jobview);
		/*clickElement(getEleByXpathContainsNormalizeSpace("button","Market View ",driver));
		clickElement(getEleByXpathContainsNormalizeSpace("a","EMEA",driver));
		clickElement(getEleByXpathContainsNormalizeSpace("label","2017 WIN TEST DE WECBF (SHOWCASE)",driver));*/
		clickElementUsingJavaScript(driver,marketview);
		clickElement(emea);
		clickElement(wintest);
		clickElement(apply);
		setInput(keyword,"Finance");
		clickElement(search);
		waitForElementToClickable(mjltableresult);
		waitForElementToClickable(selectjobs);
		clickElementUsingJavaScript(driver,selectjobs);
		System.out.println("Jobs selected");
		waitForElementToClickable(continuebutton);
		clickElement(continuebutton);
		waitForPageLoad(driver);
	}
	
}
