package pages.win;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToClickable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {
	
	@FindBy(xpath="//a[text()='winqa01 test user1 - Test org 3/4']")
	public static WebElement userdetails;
	
	@FindBy(xpath="//a[contains(text()[normalize-space()], 'Tools')]")
	public static WebElement tools;
	
	@FindBy(xpath="//a[contains(text()[normalize-space()], 'Administration')]")
	public static WebElement administration;
	
	@FindBy(xpath="//a[text()='Home']")
	public WebElement homebreadcrumb;
	
	@FindBy(xpath="//a[text()='Administration']")
	public WebElement administrationbreadcrumb;
	
	@FindBy(xpath="//a[text()='Mercer Market Data']")
	public WebElement mercermarketdatabreadcrumb;
	
	public HeaderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateViaBreadCrumb(WebElement module) throws InterruptedException {
		waitForElementToClickable(module);
		clickElement(module);
	}
	
	public void navigateToAdministrationPage() throws InterruptedException {
		waitForElementToClickable(tools);
		clickElement(tools);
		System.out.println("Tools Dropdown displayed");
		waitForElementToClickable(administration);
		clickElement(administration);
		System.out.println("Naviagted to administration Page");
	}
	
	
}
