package pages.win;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdministrationPage {
	
	@FindBy(xpath="//h1[text()='Mercer WIN - System Administration']")
	public WebElement merceradministartion;
	
	@FindBy(xpath="//b[text()='Manage Job Architecture']")
	public WebElement managejobachitecture;
	
	@FindBy(xpath="//div[contains(text()[normalize-space()], 'Job Architecture:')]")
	public WebElement jobarchitecturepage;
	
	@FindBy(xpath="//b[text()='Import Data']")
	public  WebElement importdata;
	
	@FindBy(xpath="//h1[text()='Import Center']")
	public  WebElement importcenterpage ;
	
	@FindBy(xpath="//h1[text()='MUPCS Catalog']")
	public  WebElement mupcscatalogpage ;
	
	@FindBy(xpath="//b[text()='Search MUPCS Catalog']")
	public WebElement mupcscatalog;
	
	WebDriver driver;
	public AdministrationPage(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver, this);
	}
	
	public void accessAdminModule(WebElement element) throws InterruptedException {
		waitForElementToClickable(element);
		clickElement(element);
		waitForPageLoad(driver);
	}
	
}
