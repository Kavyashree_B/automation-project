package pages.win;

import static driverfactory.Driver.*;
import pages.win.HeaderPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	public HeaderPage headerpage;
	@FindBy(xpath="//h1[text()='My Homepage']")
	public WebElement homepagetext;

	@FindBy(xpath="//a[text()='Mercer Market Data']")
	public WebElement mercermarketdata;
	
	@FindBy(xpath="//h2[contains(text()[normalize-space()], 'Mercer Job Library for Year:')]")
	public WebElement mjlpage;
	
	@FindBy(xpath="//a[text()='Diagnostic Report']")
	public  WebElement diagnosticreport;
	
	@FindBy(xpath="//h1[text()='Diagnostic Report']")
	public  WebElement diagnosticreportpage;
	
	@FindBy(id="feature-menu-button2")
	public  WebElement myjobs;
	
	@FindBy(xpath="//h1[text()='My Jobs']")
	public  WebElement myjobspage;
	
	@FindBy(id="feature-menu-button3")
	public  WebElement myemployees;
	
	@FindBy(xpath="//h1[text()='My Employees']")
	public  WebElement myemployeespage;
	
	@FindBy(xpath="//a[text()='My Library']")
	public  WebElement mylibrary;

	@FindBy(xpath="//a[text()='My Market Library']")
	public  WebElement mymarketlibrary;
	
	@FindBy(xpath="//h1[contains(text()[normalize-space()], 'My Market Library:')]")
	public  WebElement mymarketlibrarypage;
	
	@FindBy(xpath="//a[text()='My Reference Job Library']")
	public  WebElement myreferencejoblibrary;
	
	@FindBy(xpath="//h1[text()='Reference Jobs']")
	public  WebElement myreferencejoblibraryPage;
	
	WebDriver driver;
	
	
	public HomePage(WebDriver driver) {
		headerpage=new HeaderPage(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void accessModule(WebElement module) throws InterruptedException {
		waitForPageLoad(driver);
		waitForElementToDisplay(module);
		waitForElementToClickable(module);
		clickElement(module);
		System.out.println("navigated to module");
	}
	
	public void accessLibrary(WebElement module) throws InterruptedException {
		waitForElementToDisplay(mylibrary);
		clickElement(mylibrary);
		accessModule(module);
		waitForPageLoad(driver);
	}
	
}
