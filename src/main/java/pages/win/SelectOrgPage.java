package pages.win;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.getEleByXpathContains;

public class SelectOrgPage {
	
	@FindBy(xpath="//h1[text()='Select Your Organization']")
	public WebElement selectYourorganization;

	@FindBy(xpath="//*[text()='QA Verification [QA Verification (MMC)]']")
	public static WebElement selectorg;
	
	@FindBy(xpath="//b[text()='Continue']")
	public static WebElement continuebutton;
	
	WebDriver driver;
	
	public SelectOrgPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public static void selectOrg(WebDriver driver,String organization) throws InterruptedException
	{
		//scrollToElement(driver,selectOrg);
		//waitForElementToDisplay(selectorg);
		//clickElement(selectorg);
		
		WebElement eventLink = getEleByXpathContains("div",organization,driver);
		waitForElementToEnable(eventLink);
		eventLink.click();
		clickElement(continuebutton);
		waitForPageLoad(driver);
	}
	
}
