package pages.win;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;

public class LoginPage {

	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement email;
 
	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement passwordinput;
	
	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signin;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username,String password) throws InterruptedException
	{
		waitForElementToDisplay(email);
		setInput(email,username);
		waitForElementToDisplay(passwordinput);
		setInput(passwordinput,password);
		clickElement(signin);
		
	}
}

