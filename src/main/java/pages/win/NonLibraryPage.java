package pages.win;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class NonLibraryPage {
	
	@FindBy(xpath="//a[text()='Global']")
	public static WebElement globaltab;
	
	@FindBy(xpath="//h1[contains(text()[normalize-space()], 'Mercer Market Data for Year: ')]")
	public WebElement globalpage;
	
	@FindBy(xpath="//div[@class='group edit-box select-box view-selector']//div[@class='overlay']")
	public static WebElement selectview;
	
	@FindBy(xpath="//a[text()='Automation_Position Class']")
	public static WebElement positionclass;
	
	@FindBy(xpath="//b[text()='Geography/Market View']")
	public static WebElement marketview;
	
	@FindBy(xpath="//label[text()='2015 China-Guangdong Total Remuneration Survey Manufacturing']")
	public static WebElement citieshightech;
	
	@FindBy(xpath="//div[@class='option-popup GeographySurvey popup container']//child::b[text()='Apply']")
	public static WebElement apply;
	
	@FindBy(xpath="//input[@name='Keyword']")
	public static WebElement keyword;
	
	@FindBy(xpath="//b[text()='Search']")
	public static WebElement search;
	
	@FindBy(xpath="//b[text()='Continue']")
	public static WebElement continuebutton;
	
	@FindBy(xpath="//h1[text()='Mercer Market Data Results']")
	public WebElement globalresultpage;

	@FindBy(xpath="//b[text()='Global']")
	public static WebElement globalnonlibrarytab;
	
	WebDriver driver;
	
	public NonLibraryPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void global() throws InterruptedException {
		waitForElementToDisplay(globaltab);
		clickElement(globaltab);
	}
	
	public void globalSearch() throws InterruptedException {
		waitForPageLoad(driver);
		clickElement(selectview);
		scrollToElement(driver,positionclass);
		clickElement(positionclass);
		clickElement(marketview);
		clickElement(citieshightech);
		clickElement(apply);
		setInput(keyword, "Account");
		clickElement(search);
		clickElement(continuebutton);
		waitForPageLoad(driver);
	}
	
	
}
