package pages.win;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToClickable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignOut {

	@FindBy(xpath="//b[text()='Sign Out']")
	public static WebElement signout;
	
	@FindBy(xpath="//a[contains(text()[normalize-space()], 'Sign back into your account')]")
	public WebElement signback;
	
	WebDriver driver;
	
	public SignOut(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void  signOutUser() throws InterruptedException {
		waitForElementToClickable(signout);
		clickElement(signout);
	}
}
