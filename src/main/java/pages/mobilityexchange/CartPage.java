package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
	WebDriver driver;
	@FindBy (xpath = "//a[contains(text(),'Keep Shopping')]")
	WebElement continueShopping;
	
	@FindBy (xpath = "//input[@value='Secure Checkout']")
	WebElement checkout;
	
	public CartPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void continueShopping() throws InterruptedException {
		clickElement(continueShopping);
	}
	public void navigateTocheckout() throws InterruptedException  {
		clickElement(checkout);
	}
}
