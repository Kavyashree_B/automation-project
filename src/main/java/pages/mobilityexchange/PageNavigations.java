package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class PageNavigations {
	WebDriver driver;


	@FindBy(id = "dnn_ctr513_dnnTITLE_titleLabel")
	public WebElement Title; 
	@FindBy(xpath = "//div[contains(@class,'visible-md')]//span[contains(text(),'CONTACT US')]")
	WebElement ContactUs;
	@FindBy(xpath = "//div[contains(@class,'visible-md')]//span[contains(text(),'My Account')]")
	WebElement MyAccount;
	@FindBy(xpath = "//div[contains(@class,'visible-md')]//a[contains(text(),'My Dashboard')]")
	WebElement Dashboard;
	@FindBy(xpath = "//a[contains(text(),'FEEDBACK')]")
	WebElement Feedback;
	@FindBy(xpath = "//a[contains(text(),'FAQ')]")
	WebElement FAQs;
	@FindBy(xpath = "//a[contains(@href,'Account-Client-selection')]")
	WebElement SwitchClient;
	
	
	
	
	
	public PageNavigations(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void navigate(String tabname,String option) throws InterruptedException {
		hoverOverElement(driver,driver.findElement(By.xpath("//a[contains(@data-toggle,'dropdown') and contains(@href,'mercer') and contains(text(),'"+tabname+"')]")));
		clickElement(driver.findElement(By.xpath("//li[@class]//a[contains(text(),'"+option+"')]")));
	}
	public void contactUs() throws InterruptedException {
		clickElement(ContactUs);
	}
	public void dashboard() throws InterruptedException {
		clickElement(MyAccount);
		clickElement(Dashboard);
	}
	public void feedbackLink() throws InterruptedException {
		dashboard();
		clickElement(Feedback);
	}
	public void faqsLink() throws InterruptedException {
		dashboard();
		clickElement(FAQs);
	}
	public void switchClientLink() throws InterruptedException {
		dashboard();
		clickElement(SwitchClient);
	}
	public void socialShareIcons(String socialIcon) throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//a[contains(@href,'"+socialIcon+"')]")));
		switchToWindowWithURL(socialIcon, driver);
	}
}
