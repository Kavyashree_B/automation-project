package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static utilities.InitTests.dir_path;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class CustomReportTab {
WebDriver driver;
@FindBy(xpath = "//a[contains(text(),'Add folder')]")
WebElement addFolder;
@FindBy(xpath ="//input[@name='folderName']")
WebElement folderName;
@FindBy(xpath="//a[contains(@data-ng-click,'createFolder')]")
WebElement createFolder;
@FindBy(xpath = "//a[contains(text(),'Add file')]")
WebElement addFile;
@FindBy(xpath="//a[contains(@data-ng-click,'uploadFile')]")
WebElement createFile;
@FindBy(xpath = "//a[contains(text(),'Choose file')]")
WebElement chooseFile;
@FindBy(xpath = "//label[contains(text(),'Currently Viewing')]//following::div[1]")
WebElement currentFolder;
@FindBy(xpath = "//a[contains(text(),'Delete Folder')]")
WebElement deleteFolder;
@FindBy(xpath = "//span[contains(text(),'Folder created')]")
public static WebElement createFolderNotification;
@FindBy(xpath = "//span[contains(text(),'Folder has been deleted')]")
public static WebElement deleteFolderNotification;
@FindBy(xpath = "//span[contains(text(),'File uploaded')]")
public static WebElement addFileNotification;
@FindBy(xpath = "//span[contains(text(),'File has been deleted')]")
public static WebElement deleteFileNotification;

	public CustomReportTab(WebDriver driver) {
		PageFactory.initElements(driver, this)	;
		this.driver = driver;
		}
		public void createFolder(String foldername) throws InterruptedException {
			clickElement(addFolder);
			setInput(this.folderName,foldername);
			clickElement(createFolder);
		}
		public void changeDirectory(String foldername) throws InterruptedException {
			clickElement(currentFolder);
			clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//a[contains(text(),'"+foldername+"')]")));
		}
		
		public void addFile(String filename) throws FindFailed, InterruptedException {
			clickElement(addFile);
			clickElement(chooseFile);
			Screen screen = new Screen();
			String screenPath = dir_path + "\\src\\main\\resources\\WindowScreens\\InputBox.png"; 
			delay(1000);
			Pattern pattern = new Pattern(screenPath);
			screen.type(pattern, dir_path.replace("/", "\\")+"\\src\\main\\resources\\WindowScreens\\"+filename);
			screen.wait(pattern, 10);
			screenPath = dir_path + "\\src\\main\\resources\\WindowScreens\\openButton.png";
			pattern = new Pattern(screenPath);
			screen.click(screenPath);
			screen.wait(pattern,10);
			clickElement(createFile);
		}
		public void deleteFile(String filename) {
			delay(3000);
			clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//span[contains(text(),'"+filename+"')]//following::span[contains(text(),'Delete')]")));
			driver.switchTo().alert().accept();
		}
		public void deleteFolder(String foldername) {
			clickElementUsingJavaScript(driver,deleteFolder);
			driver.switchTo().alert().accept();
		}

}
