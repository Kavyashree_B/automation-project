package pages.mobilityexchange;

import static driverfactory.Driver.*;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage {
	WebDriver driver;
	
	@FindBy (xpath = "//h3[contains(text(),'Billing Address ')]")
	WebElement addressSection;
	@FindBy (id = "billingfirstname")
	WebElement firstName;
	@FindBy (id = "billinglastname")
	WebElement lastName;
	@FindBy (id = "billingaddress")
	WebElement address;
	@FindBy (id = "billingstate")
	WebElement state;
	@FindBy (id = "billingcity")
	WebElement city;
	@FindBy (id = "billingzip")
	WebElement zip;
	@FindBy (xpath = "//a[contains(text(),'Place Order')]")
	WebElement placeOrder;
	@FindBy(xpath = "//div[@id='dnn_ctr555_ModuleContent']//child::h2")
	public static WebElement checkoutPageHeader;
	
	public CheckoutPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public void checkBillingDetails() throws InterruptedException {
		clickElement(addressSection);
		if(firstName.getText().equalsIgnoreCase(""))
		{
		setInput(firstName,"John");
		setInput(lastName,"Mathews");
		setInput(address,"72, Park Avenue");
		setInput(city,"Kansas");
		selEleByVisbleText(state,"Delvine");
		setInput(zip,"5600");
		}
		
	}
	public void placeOrder() throws InterruptedException {
		clickElement(placeOrder);
	}
}
