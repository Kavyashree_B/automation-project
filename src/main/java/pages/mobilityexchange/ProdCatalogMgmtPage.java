package pages.mobilityexchange;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class ProdCatalogMgmtPage {
	WebDriver driver;
@FindBy(xpath ="//div[contains(@class,'product-importer')]//child::h1")
public static WebElement catalogPageHeader;

public ProdCatalogMgmtPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver = driver;
}
public void open(String button) throws InterruptedException {
clickElement(getEleByXpathContains("a",button,driver));
}
}