package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisappear;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyDataTab {
WebDriver driver;
public static boolean purchased;
public static boolean available;
@FindBy(xpath = "//img[contains(@src,'/portals/_default/skins/MercerSkin/img/progress.gif')]")
WebElement loadingImage;
@FindBy (xpath = "//label[contains(text(),'Currently Viewing')]//following::div[contains(@dropdown-onchange,'ProductList')]")
WebElement selectProduct;
@FindBy (css = "li.product:nth-child(2)")
WebElement purchasedProducts;
@FindBy (css = "li.item-to-buy:nth-child(1) > div:nth-child(1) > span:nth-child(2)")
WebElement availableForPurchase;
@FindBy(xpath = "//li[contains(@class,'item-to-buy')][1]//child::span[contains(@class,'checkbox')]")
WebElement selectProduct1;
@FindBy(xpath = "//li[contains(@class,'item-to-buy')][2]//child::span[contains(@class,'checkbox')]")
WebElement selectProduct2;
@FindBy (xpath = "//a[contains(text(),'add to cart')]")
WebElement addToCart;
	public MyDataTab(WebDriver driver) {
		PageFactory.initElements(driver, this)	;
		this.driver = driver;
		}
	
	public void productSelection(String product) throws InterruptedException {
		clickElement(selectProduct);
		clickElement(driver.findElement(By.partialLinkText(product)));
		waitForElementToDisappear(By.xpath("//img[contains(@src,'/portals/_default/skins/MercerSkin/img/progress.gif')]"));
		purchased= isElementExisting(driver,purchasedProducts,2);
		available = isElementExisting(driver,availableForPurchase,2);
		
	}
	public void addToCartFromMyDataPage(String product) throws InterruptedException {
		if(available== true) {
		clickElement(selectProduct1);
		clickElement(selectProduct2);
		clickElement(addToCart);
		}
	}
}
