package pages.mobilityexchange;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class ApplicationSettingsPage {
	WebDriver driver;
	@FindBy(xpath = "//span[contains(text(),'Current Data Year')]//following::input[1]")
	WebElement currentYear;
	@FindBy(xpath = "//input[contains(@value,'Update')]")
	WebElement update;
	@FindBy(xpath ="//div[contains(text(),'Settings has been updated successfully')]")
	public static WebElement updatedSettingsNotification;

public ApplicationSettingsPage(WebDriver driver) {
PageFactory.initElements(driver, this);
this.driver = driver;
}

public void edit(String year) throws InterruptedException {
	setInput(currentYear,year);
	clickElement(update);
}
}