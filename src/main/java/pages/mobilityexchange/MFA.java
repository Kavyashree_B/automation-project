package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.InitTests.props;
import static utilities.InitTests.input;

import java.io.IOException;
import java.util.ArrayList;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class MFA {
	WebDriver driver;
	String Code;
	@FindBy(id = "ctl00_MainSectionContent_ButtonSubmit")
	WebElement SendCode;
	@FindBy(id = "ctl00_MainSectionContent_ChallengeCode")
	WebElement SubmitCode;
	@FindBy(id = "username")
	WebElement OutlookEmailID;
	@FindBy(id = "password")
	WebElement OutlookPassword;
	@FindBy(xpath = "//span[text()='MFA']")
	WebElement MFAtab;
	@FindBy(xpath="//div[@autoid='_lv_i']//following::div[1]")
	WebElement Message;
	@FindBy(xpath = "//div[@id='Item.MessageUniqueBody']//div[@class='PlainText']")
	WebElement MFAmessage;
	@FindBy(xpath = "//span[text()='Mark as read']")
	WebElement MarkAsRead;
	@FindBy(xpath = "//button[contains(text(),'Confirm')]")
	WebElement Confirm;

	public MFA(WebDriver driver) throws IOException {
		PageFactory.initElements(driver, this);
		this.driver=driver;
		props.load(input);
	}
	
	String MFAChoice = props.getProperty("MobilityExchange_MFAChoice");
	String MFAEmailId = props.getProperty("MFAEmailId");
	String MFAEmailPassword = props.getProperty("MFAEmailPassword");
	
	public void authenticate() throws InterruptedException {
		
			
		clickElement(driver.findElement(By.id("ctl00_MainSectionContent_Credentials_"+MFAChoice)));
		clickElementUsingJavaScript(driver,SendCode);
		FetchMFACodeFromOutlookEmail();
		setInput(SubmitCode,Code);
		clickElement(Confirm);
//		delay(6000);
//		waitForElementToEnable(SubmitCode);
	}
	public void FetchMFACodeFromOutlookEmail() throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get("https://apac1mail.mmc.com/OWA/");
		waitForElementToDisplay(OutlookEmailID);
		setInput(OutlookEmailID,MFAEmailId);
		setInput(OutlookPassword,MFAEmailPassword+Keys.ENTER);
		clickElement(MFAtab);
		clickElement(Message);
		Code = MFAmessage.getText();
		Code = Code.substring(27, 33);
		clickElement(MarkAsRead);
		((JavascriptExecutor)driver).executeScript("window.close()");
		switchToWindow("Welcome - Mercer", driver);
		}
}

