package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RolesPage {
	WebDriver driver;
	@FindBy(xpath = "(//span[contains(@class,'title')]//child::h3)[2]")
	public static WebElement rolesPageHeader;
	@FindBy(xpath = "//button[contains(text(),'Create New Role')]")
	WebElement addRole;
	@FindBy(xpath = "//label[contains(text(),'Role Name')]//following::input[1]")
	WebElement roleName;
	@FindBy(xpath = "//label[contains(text(),'Description')]//following::textarea[1]")
	WebElement description;
	@FindBy(xpath = "//button[text()='Save']")
	WebElement save;
	@FindBy(xpath ="//button[text()='Delete']")
	WebElement delete;
	@FindBy(xpath = "//a[text()='Delete']")
	WebElement deleteConfirm;
	@FindBy(xpath = "//p[contains(text(),'Role created successfully')]")
	public static WebElement createdRoleNotification;
	@FindBy(xpath = "//p[contains(text(),'Role updated successfully')]")
	public static WebElement updatedRoleNotification;
	@FindBy(xpath = "//p[contains(text(),'Role deleted successfully')]")
	public static WebElement deletedRoleNotification;
	
	public RolesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void addNew(String rolename) throws InterruptedException {
		clickElement(addRole);
		setInput(roleName,rolename);
		clickElement(save);
	}
	public void update(String rolename,String desc) throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//div[text()='"+rolename+"']//following::a[@title='Edit Role'][1]")));
		setInput(description,desc);
		clickElement(save);
	}
	 public void delete(String rolename) throws InterruptedException {
		 clickElement(driver.findElement(By.xpath("//div[text()='"+rolename+"']//following::a[@title='Edit Role'][1]")));
		 clickElement(delete);
		 clickElement(deleteConfirm);
	 }
}
