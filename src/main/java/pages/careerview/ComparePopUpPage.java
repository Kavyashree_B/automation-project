package pages.careerview;

import static driverfactory.Driver.*;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ComparePopUpPage {

	WebDriver driver;
	@FindBy(xpath = "//a[@class = 'close-reveal-modal']")
	public  WebElement closeModal_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Start')]")
	public  WebElement startcompare_button;
	
	@FindBy(xpath = "//a[@data-reveal-id='modal-compare-roles']")
	public  WebElement compare_subheader_button;
	
	@FindBy(id= "modal-compare-roles")
	public  WebElement roleCompare_modal;
	

	
	public ComparePopUpPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void closeModal() throws InterruptedException {
		clickElement(closeModal_button);
	}
	
	public void performCompare() throws InterruptedException {
		waitForElementToDisplay(startcompare_button);
		clickElement(startcompare_button);
		LocateMePage locateMe = new LocateMePage(driver);
		clickElement(locateMe.itemToCompare);	
		clickElement(compare_subheader_button);
	}
	
	public void performCompareStg() throws InterruptedException {
		waitForElementToDisplay(startcompare_button);
		clickElement(startcompare_button);
		LocateMePage locateMe = new LocateMePage(driver);
		clickElement(locateMe.itemToComparesStg);	
		clickElement(compare_subheader_button);
	}
	public void performCompareProd() throws InterruptedException {
		waitForElementToDisplay(startcompare_button);
		clickElement(startcompare_button);
		LocateMePage locateMe = new LocateMePage(driver);
		clickElement(locateMe.itemToComparesProd);	
		clickElement(compare_subheader_button);
	}
	
	public void enterComparePage() throws InterruptedException {
		
		waitForElementToDisplay(startcompare_button);
		clickElement(startcompare_button);
	}
}
