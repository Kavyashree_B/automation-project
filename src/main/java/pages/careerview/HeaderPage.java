package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HeaderPage {

	WebDriver driver;
	//   "//a[contains(@class,'dropdown')]"
	@FindBy(xpath ="//a[@data-dropdown='profile-drop']")
	public  WebElement profile_dropdown;
	
	//        "//ul[@id='profile-drop']/li[1]/a"
	@FindBy(xpath ="//ul[@id='profile-drop']//a[@class='show-profile']")
	public  WebElement showProfile_option;

	@FindBy(id = "profile_image")
	public  WebElement profilePhoto;
	
	@FindBy(xpath = "//span[@class = 'icon-locate-me']")
	public  WebElement locateMe_button;
	
	@FindBy(id = "nav_navigate")
	public  WebElement navigate_button;
	
	@FindBy(xpath = "//ul[@id='profile-drop']//a[@class='sign-out']")
	public  WebElement logout_button;

	@FindBy(id = "nav_compare")
	public  WebElement compare_button;
	
	@FindBy(id = "nav_all_maps")
	public  WebElement home_button;
	
	@FindBy(id = "nav_help")
	public  WebElement help_button;
	
	@FindBy(xpath = "//a[@id='nav_zoom_out'][contains(@class,'has-tip disabled')]")
	public  WebElement zoomOutDisabled;
	
	@FindBy(id = "nav_zoom_in")
	public  WebElement zoomIn;
	
	@FindBy(xpath = "//a[@id='nav_zoom_out']/span[1][contains(@class,'icon-zoom-out')]")
	public  WebElement zoomOut;
	
	@FindBy(xpath = "//div[@id='toolbar-header']/nav/div[1]/a")
	public  WebElement signOutDropdown;
	
	@FindBy(xpath = "//ul[@id='profile-drop']/li[2]/a[contains(text(),'Sign Out')]")
	public  WebElement signOut;
	

	
	public HeaderPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectMyProfileOption() throws InterruptedException {
		clickElement(profile_dropdown);
		boolean isPresent = false;
		
		while(!isPresent) {
			 isPresent = isElementExisting(driver, showProfile_option);
			 
			 if(isPresent) {
				 clickElement(showProfile_option);
			 }
		}
	}

	public void logout() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(profile_dropdown);
		boolean isPresent = false;
		while(!isPresent) {
			 isPresent = isElementExisting(driver, showProfile_option);
			 
			 if(isPresent) {
				 clickElement(logout_button);
			 }
		}
	}
	
	public void navigateViaHeader(WebElement e) throws InterruptedException {
		waitForElementToEnable(e);
		clickElement(e);
	}
	

	public void zoomIn() throws InterruptedException {
		waitForElementToDisplay(zoomIn);
		clickElement(zoomIn);
				
	}
	
	public void clickSignout() throws InterruptedException {
		waitForElementToEnable(signOutDropdown);
		clickElement(signOutDropdown);
		clickElement(signOut);
	}
	public void zoomOut() throws InterruptedException {
		clickElement(zoomOut);
	}
	

	
/*	public void clickHomePageLogo() throws InterruptedException {

		waitForElementToDisplay(home_button);
		clickElement(home_button);
		
	}*/
	
	public void openProfile() throws InterruptedException {
		waitForElementToEnable(profile_dropdown);
		clickElement(profile_dropdown);
		waitForElementToDisplay(showProfile_option);
		clickElement(showProfile_option);
	}
	
	public void clickProfileDropdown() throws InterruptedException {
		waitForElementToEnable(profile_dropdown);
		clickElement(profile_dropdown);
	}
}
