package pages.careerview;


import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyProfileSideMenu {
	WebDriver driver;
	@FindBy(xpath = "//a[@data-panel-id='user_manager-assessment']")
	public  WebElement managerAssessment_link;
	
	@FindBy(xpath = "//a[@class='close-reveal-modal']")
	public  WebElement closePopup_button;
	
	public MyProfileSideMenu(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void closePopup() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(closePopup_button);
	}
	
	public void navigateToManagerAssessmentPage() throws InterruptedException {
		clickElement(managerAssessment_link);
	}
}
