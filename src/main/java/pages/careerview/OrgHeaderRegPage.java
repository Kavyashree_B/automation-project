package pages.careerview;


import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrgHeaderRegPage {

	/*@FindBy(xpath = "")
	public static WebElement ;*/
	
	@FindBy(xpath = "//div[@id='header']/h1[contains(text(),'Server Error')]")
	public  WebElement HeaderEle;
	
	@FindBy(xpath = "//div[@id='content']/div/fieldset/h2[contains(text(),'404 - File or directory not found.')]")
	public  WebElement ForbiddenAccess;
	
	@FindBy(xpath = "//div[@id='content']/div/fieldset/h3[contains(text(),'The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable.')]")
	public  WebElement PermissionMsg;
	WebDriver driver;
	
	public OrgHeaderRegPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void waitForHeaderEle() {
		waitForElementToDisplay(HeaderEle);
	}
}
