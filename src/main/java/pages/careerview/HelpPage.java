package pages.careerview;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HelpPage {

	WebDriver driver;
	@FindBy(xpath = "//h4[contains(text(),'FAQ')]")
	public  WebElement FAQ;
	
	@FindBy(xpath = "//h4[contains(text(),'Terms')]")
	public  WebElement Terms;
	
	@FindBy(xpath = "//h4[contains(text(),'Tutorial')]")
	public  WebElement Tutorial;
	
	@FindBy(xpath = "//a[@href='#panel_1'][contains(text(),'General')]")
	public  WebElement GeneralLink;
	
	@FindBy(xpath = "//a[@href='#panel_1'][contains(text(),'Glossary')]")
	public  WebElement Glossary;
	
	@FindBy(xpath = "//a[@href='#panel_1'][contains(text(),'How to Use the Site')]")
	public  WebElement HowToUseTheSite;
	
	@FindBy(xpath = "//section[@id='panel_1']/div/h1")
	public  WebElement GeneralHeader;
	
	@FindBy(xpath = "//section[@id='panel_1']/div/div[1]/ol")
	public  WebElement FAQList;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//ol[contains(@class,'glossary-index sticky')]/li[position()<10]"))    
	public List<WebElement> alphabetsGlossary;
	
	@FindBy(xpath = "//a[@href='#glossary_S']")
	public  WebElement glossaryAlphabetS;
	
	@FindBy(id = "glossary_S")
	public  WebElement glossarySContent1;
	
	@FindBy(xpath = "//section[@id='panel_1']//div[contains(@class,'modal-content-scroller')]//dd[7]//dd")
	public  WebElement glossarySContent2;
	
	@FindBy(xpath = "(//a[@class='image'])[1]")
	public  WebElement howToUseThisSiteVideo1;
	
	@FindBy(xpath = "(//a[@class='image'])[2]")
	public  WebElement howToUseThisSiteVideo2;
	
	@FindBy(xpath = "//img[@src='/Content/mercer/assets/img/tutorial/zoom.jpg']")
	public  WebElement zoomImg;
	
	@FindBy(xpath = "//img[@src='/Content/mercer/assets/img/tutorial/locate.jpg']")
	public  WebElement locateImg;
	
	@FindBy(xpath = "//img[@src='/Content/mercer/assets/img/tutorial/navigate.jpg']")
	public  WebElement navigateImg;
	
	@FindBy(xpath = "//img[@src='/Content/mercer/assets/img/tutorial/compare.jpg']")
	public  WebElement compareImg;
	
	@FindBy(xpath = "//img[@src='/Content/mercer/assets/img/tutorial/help.jpg']")
	public  WebElement helpImg;
	
	@FindBy(xpath = "//*[@id='modal-help']/a")
	public  WebElement closeModal_help;
	
	
	public HelpPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	/*public void clickGeneralTab() throws InterruptedException {
		waitForElementToEnable(GeneralHeader);
		clickElement(GeneralHeader);
	}*/
	
	public void navigateTo(WebElement element) throws InterruptedException {
		waitForElementToEnable(element);
		clickElement(element);
	}
	
	/*public void clickGlossary() throws InterruptedException {
		waitForElementToEnable(Glossary);
		clickElement(Glossary);
	}*/
	
	public void clickS() throws InterruptedException {
		waitForElementToEnable(glossaryAlphabetS);
		clickElement(glossaryAlphabetS);
	}
	
	/*public void clickHowToUseTheSite() throws InterruptedException {
		waitForElementToEnable(HowToUseTheSite);
		clickElement(HowToUseTheSite);
	}*/
	
	public void closeHelp() throws InterruptedException {
		waitForElementToEnable(closeModal_help);
		clickElement(closeModal_help);
	}
}
