package pages.careerview;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminHeaderPage {
	
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(), 'Analytics')]")
	public  WebElement analytics_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Import')]")
	public  WebElement import_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Admin Tasks')]")
	public  WebElement adminTask_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Details')]")
	public  WebElement details_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Users')]")
	public  WebElement users_button;
	
	public String users_xpath = "//a[contains(text(), 'Users')]";
	
	public AdminHeaderPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void navigateViaHeader(WebElement ele) throws InterruptedException {
		waitForElementToDisplay(ele);
		clickElement(ele);
	}
	

}
