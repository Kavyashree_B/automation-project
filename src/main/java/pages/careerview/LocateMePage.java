package pages.careerview;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LocateMePage {

	WebDriver driver;
	@FindBy(id = "nav_locate_me")
	public  WebElement LocateMe;
	
	@FindBy(xpath = "//li[contains(@class , 'myposition')]")
	public  WebElement ownRole_hightlighted;
	
	@FindBy(xpath = "//li[@id='nav-home_6_IT']")
	public  WebElement IT;
	
	@FindBy(xpath = "//li/a[@id = 'modal-5_AD0002']")
	public  WebElement itemToCompare;
	
	@FindBy(xpath = "//li/a[@id = 'modal-6_AD0002']")
	public  WebElement itemToComparesStg;
	
	@FindBy(xpath = "(//li/a[@id = 'modal-3_AD0007'])[1]")
	public  WebElement itemToComparesProd;
	
	@FindBy(xpath = "//a[@href='#panel_4']")
	public  WebElement dayInTheLife;
	
	@FindBy(id = "minimap")
	public  WebElement SideMinimap;
	
	@FindBy(id = "nav-subfuncM_6_IT_AD")
	public  WebElement miniAD;
	
	@FindBy(id = "nav-subfuncM_6_IT_DDA")
	public  WebElement miniDDA;
	
	@FindBy(id = "nav-subfuncM_6_IT_SI")
	public  WebElement miniSI;
	
	@FindBy(id = "nav-subfuncM_6_IT_QA")
	public  WebElement miniQA;
	
	@FindBy(id = "nav-subfuncM_6_IT_ECOM")
	public  WebElement miniECOM;
	
	@FindBy(id = "nav-subfuncM_6_IT_NetW")
	public static WebElement miniNetW;
	
	@FindBy(id = "nav-subfuncM_6_IT_Sec")
	public  WebElement miniSec;
	
	@FindBy(id = "nav-subfuncM_6_IT_TS")
	public  WebElement miniTS;
	
	@FindBy(id = "large-drop_6_Mark")
	public WebElement Arrowdropdown_Marketing;
	
	@FindBy(id = "large-drop_6_RandD")
	public WebElement ArrowDropdown_RnD;
	
	@FindBy(id = "large-drop_6_Log")
	public WebElement ArrowDropdown_Logistics;
	
	@FindBy(xpath = "//a[@id='large-drop_6_IT'][@class='button']")
	public WebElement ArrowDropdown_IT;
	
	@FindBy(xpath = "//a[@id='large-drop_6_IT']")
	public  WebElement sideMiniMapArrow;

	@FindBy(id = "nav-subfunc_6_IT_AD")
	public  WebElement macroAD;
	
	@FindBy(id = "nav-subfunc_6_IT_DDA")
	public  WebElement macroDDA;
	
	@FindBy(id = "nav-subfunc_6_IT_SI")
	public  WebElement macroSI;
	
	@FindBy(id = "nav-subfunc_6_IT_QA")
	public  WebElement macroQA;
	
	@FindBy(id = "nav-subfunc_6_IT_ECOM")
	public  WebElement macroECOM;
	
	@FindBy(id = "nav-subfunc_6_IT_NetW")
	public  WebElement macroNetW;
	
	@FindBy(id = "nav-subfunc_6_IT_Sec")
	public  WebElement macroSec;
	
	@FindBy(id = "nav-subfunc_6_IT_TS")
	public WebElement macroTS;
	
	
	public LocateMePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectItemToCompare() throws InterruptedException {
		clickElement(itemToCompare);
	}
	
	public void clickHighlightedRole() throws InterruptedException
	{
		clickElement(ownRole_hightlighted);
	}
	
	public void clickLogistics_UpArrow() throws InterruptedException {
		waitForElementToEnable(ArrowDropdown_Logistics);
		clickElement(ArrowDropdown_Logistics);
	}
	
	public void clickUpArrow() throws InterruptedException {
		waitForElementToEnable(sideMiniMapArrow);
		clickElement(sideMiniMapArrow);
	}
	
}
