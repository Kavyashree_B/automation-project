package pages.careerview;

import static driverfactory.Driver.*;
import static driverfactory.Driver.switchToDefaultContent;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;

import static utilities.SikuliActions.*;

public class CompareProcessPopUp {

	WebDriver driver;
	@FindBy(xpath= "//div[@class='modal-sidebar']")
	public WebElement sideModal;

	@FindBy(xpath= "//a[@href='#role_compare_6_AD0008']/h6")
	public WebElement roleSelected1;

	@FindBy(xpath= "//a[@href='#role_compare_6_AD0002']/h6")
	public WebElement roleSelected2;

	@FindBy(xpath= "//a[@href='#role_compare_6_AD0001']/h6")
	public WebElement roleSelected3;

	@FindBy(xpath= "//a[@href='#role_compare_6_MW0006']/h6")
	public WebElement roleSelected4;

	@FindBy(xpath= "//a[@href='#panel_5']")
	public WebElement coreCompetencies;

	@FindBy(xpath= "(//div[contains(@class,'modal-content-scroller')])[6]//h6[2]")
	public WebElement coachesAndDevCC;

	@FindBy(xpath= "(//div[contains(@class,'modal-content-scroller')]/h6[4])[1]")
	public WebElement customerFocusCC;

	@FindBy(xpath= "(//div[contains(@class,'modal-content content')])[7]//h6[2]")
	public WebElement coachesAndDevTC;

	@FindBy(xpath= "(//div[contains(@class,'modal-content content')])[7]//h6[4]")
	public WebElement customerFocusTC;

	@FindBy(xpath= "//a[@href='#panel_6']")
	public WebElement technicalCompetencies;

	@FindBy(xpath= "//a[@href='#panel_7']")
	public WebElement pathsOthersHaveTaken;

	@FindBy(xpath= "//input[@value='Save']")
	public WebElement compare_SaveBtn;

	@FindAll(@FindBy(how = How.XPATH, using = "//section[@id='panel_7']//li[2]//span"))    
	public List<WebElement> pathsToBeVerified;

	@FindBy(xpath= "(//button[contains(@class,'button orange-button saved-button')][contains(text(),'Saved')])[1]")
	public WebElement saveBtnAfterSaving;

	@FindBy(xpath= "//a[@href='#panel_9']")
	public WebElement othersInThisRole;

	@FindBy(xpath= "//a[@href='#panel_8']")
	public WebElement dayInTheLife;

	@FindBy(xpath= "//div[contains(@class,'ytp-cued-thumbnail-overlay-image')]")
	public WebElement progAnalystIVideo;

	@FindBy(xpath= "//button[contains(@class,'ytp-large-play-button ytp-button')]")
	public WebElement videoPlayButton;

	@FindBy(xpath="//div[contains(@class,'html5-video-player ytp-embed ad-created ytp-title-enable-channel-logo ytp-autohide playing-mode')]")
	public WebElement videoPlaying;

	@FindAll(@FindBy(how = How.XPATH, using = "//ul[contains(@class,'employees row')]/li[position()<4]"))    
	public List<WebElement> otherRolesToBeVerified;

	@FindBy(xpath= "//*[@id='panel_8']/div/div[1]/iframe")
	public WebElement iFrameLoc;

	@FindBy(xpath= "//section[@id='panel_9']/div/div[1]/h6")
	public WebElement progAnalystOthersInThisRoleMsg;

	@FindAll(@FindBy(how = How.XPATH, using = "//section[@id='panel_9']//li/div[1]"))    
	public List<WebElement> OtherInThisRoleProfilePhoto;

	@FindAll(@FindBy(how = How.XPATH, using = "//section[@id='panel_9']//p[@class='email']/a"))    
	public List<WebElement> OtherInThisRoleEmail;

	@FindBy(xpath= "//div[contains(@class,'modal-content-scroller')]//b")
	public WebElement noComparisonMessage;

	@FindBy(xpath= "(//div[contains(@class,'competency-ramp-container')]//tr/td[contains(@class,'  expected barColorBlack')])[2]")
	public List<WebElement> ExpectedBlack_graphCC;

	@FindBy(xpath= "//div[contains(@class,'competency-ramp-container')][3]//td[1][contains(@class,' manager barColorRed ')]")
	public List<WebElement> managerRed_graphCC;

	@FindBy(xpath= "//div[contains(@class,'competency-ramp-container')][3]//td[2][contains(@class,'  expected barColorBlack')]")
	public List<WebElement> ExpectedBlack_graphTC;

	@FindBy(xpath= "//div[contains(@class,'competency-ramp-container')][3]//td[2][contains(@class,'  expected barColorBlack')]//span[3]")
	public List<WebElement> ExpectedText;

	@FindBy(xpath= "(//div[contains(@class,'modal-content content')][1]//span[1])[1]")
	public List<WebElement> youYellowCC;

	@FindBy(xpath= "//div[contains(@class,'competency-ramp-container')]//td[contains(@class,'you barColorGreen  ')]//span[1]")
	public WebElement youGreenTC;

	@FindBy(xpath= "//div[contains(@class,'competency-ramp-container')]//td[contains(@class,'you barColorGreen  ')]")
	public WebElement youGreenGridTC;

	@FindBy(xpath= "//div[@id='modal-compare-roles']/a")
	public WebElement closeCompare_modal;


	public CompareProcessPopUp(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void navigateTo(WebElement element) throws InterruptedException {
		waitForElementToEnable(element);
		clickElement(element);
	}

	
	public void clickRole(WebElement element) throws InterruptedException {
		waitForElementToDisplay(element);
		clickElement(element);
		delay(2000);
	}



	public void SaveComparison(){
		clickElementUsingJavaScript(driver,compare_SaveBtn);

	}

	

	public String getAlertText() {
		Alert alert = null;
		String alertText = "";
		// Get a handle to the open alert, prompt or confirmation
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			alert = driver.switchTo().alert();
		}
		catch(Exception e){

		}
		finally {
			// Get the text of the alert or prompt
			delay(2000);
			alertText = alert.getText();
			System.out.println(alertText);

			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;

	}


	public String getAlertTextForDeleteComparison() {

		String alertText;
		Alert alert1 = null ;

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			alert1 = driver.switchTo().alert();

		} catch (Exception e) {
		}
		finally {

			alertText = alert1.getText();
			System.out.println(alertText);
			alert1.accept();

		}
		return alertText;

	}

	public String checkPresence() {

		waitForElementToDisplay(iFrameLoc);
		String src = iFrameLoc.getAttribute("src") ; 
		return src;

	}

	public boolean checkYoutubeVideoPlaying() throws FindFailed {

		List<WebElement> frameCount = driver.findElements(By.tagName("iframe"));
		System.out.println(frameCount.size());
		//switchToFrame(driver, frameCount.get(0));
		driver.switchTo().frame(0);
		System.out.println(isElementExisting(driver, progAnalystIVideo));
		click("youTubePlay.png");
		//clickElement(videoPlayButton);
		Boolean bool = isElementExisting(driver, videoPlaying);
		switchToDefaultContent(driver);
		return bool;

	}

	public void closeComparePopUp() throws InterruptedException {
		waitForElementToEnable(closeCompare_modal);
		clickElement(closeCompare_modal);
	}
}
