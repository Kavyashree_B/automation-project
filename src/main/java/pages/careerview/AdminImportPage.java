package pages.careerview;

import static driverfactory.Driver.*;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminImportPage {

	WebDriver driver;
	
	@FindBy(xpath = "//h4[contains(text(),'Upload the career view data for the company')]")
	public  WebElement importPage_header; 
	@FindBy(xpath = "//a[contains(@href,'/api/export/exportcareerframework/')]")
	public  WebElement download_import; 
	 
	@FindBy(xpath = "//button[@type='submit']")
	public  WebElement upload_import; 
	 
	@FindBy(xpath = "//tr[contains(@data-reactid,'.0.1:0:0')]/td")
	public List<WebElement> importHistory; 
	
	@FindBy(xpath = "//input[@value='1']")
	public  WebElement careerFramework_radiobutton; 
	
	@FindBy(xpath = "//input[@value='2']")
	public  WebElement users_radiobutton; 
	
	@FindBy(xpath = "//input[@name='uploadFile']")
	public  WebElement chooseFile_button; 
	
	@FindBy(xpath = "//label[@for='importType']")
	public  WebElement mandatoryMsg_radioUpload;
	
	@FindBy(xpath = "//input[@name='uploadFile']")
	public  WebElement mandatoryMsg_buttonUpload;
	
	@FindBy(xpath = "(//tr[contains(@data-reactid , '.2.1.0.0')])[1]/th")
	public List<WebElement> importHistory_columnNames; 
	
	public String[] columnNamesArray = new String[]{"Id","Submitted Date","Type","Completed Date","Status","Logs" };
	
	@FindBy(xpath = "//tr[contains(@data-reactid,'.5.2.1.2.1.0.')]/td[5]")
	public List<WebElement> importHistory_statusList; 
	
	
	public AdminImportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void download() throws InterruptedException {
		waitForElementToDisplay(download_import);
		clickElement(download_import);
		
	}
	
	 public boolean isFileDownloaded() {
		 
		 String downloadPath = System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator
                 + "downloadFiles";
		 
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
		      if (dirContents[i].getName().contains("Demo2016 Data")) {
		          dirContents[i].delete();
		          return true;
		      }
		          }
		      return false;
		  }
	 
	 public void upload() throws InterruptedException {
		 waitForElementToDisplay(upload_import);
		 clickElement(upload_import);
	 }
	 
	 public void clickCareerFramework() throws InterruptedException {
		 waitForElementToDisplay(careerFramework_radiobutton, driver, 20);
		 clickElement(careerFramework_radiobutton);
	 }
}
