package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminUsers {
	
	WebDriver driver;
	@FindBy(xpath = "//input[@placeholder='Filter Search by First Name or Last Name']")
	public  WebElement search_textbox;
	
	@FindBy(xpath = "//a[text()='Search']")
	public  WebElement search_button;
	
	/*@FindBy(xpath = "(//tr[contains(@class,'standard-row')])[6]/td[1]")
	public  WebElement sixthElementInTheTable;*/
	
	public String sixthEleInTable_xpath = "(//tr[contains(@class,'standard-row')])[6]/td[1]";
	
	@FindBy(xpath = "//td[text()='Pavan']")
	public  WebElement searchResult_FirstName;

	@FindBy(xpath = "//td[text()='QA11']")
	public  WebElement searchResult_LastName;
	
	@FindBy(xpath = "//button[@style='color:#222;border:none;background:none;margin:0 10px 0 0;']")
	public WebElement nextButton;
	
	@FindBy(xpath = "//button[@style='color:#222;border:none;background:none;margin:0 0 0 10px;']")
	public WebElement previousButton;
	
	public AdminUsers(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void searchUser()
	{
		setInput(search_textbox, "QA");
		try {
			clickElement(search_button);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickNext() throws InterruptedException {
		waitForElementToDisplay(nextButton);
		clickElement(nextButton);
	}
	
	public void clickPrevious() throws InterruptedException {
		waitForElementToDisplay(previousButton);
		clickElement(previousButton);
	}
}
