package pages.careerview;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ITPage {
	
	WebDriver driver;
	public HeaderPage header;
	public LocateMePage locateMe;
	
	@FindBy(xpath="//*[@id='nav-subfunc_6_IT_DDA']/div[8]")
	public  WebElement DbDevAdmin;
	
	@FindBy(xpath = "//li[contains(@class,'tbox c-roles-3 professional')]/a[@id='modal-6_AD0002']")
	public  WebElement progAnalyst;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0002']/span[2]")
	public  WebElement progAnalystNumber;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0002']/span[contains(text(),'Programmer Analyst')]")
	public  WebElement progAnalystText;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0001']/span[contains(text(),'Programmer Analyst I')]")
	public  WebElement progAnalystIText;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-2 professional']//a[@id='modal-6_AD0003']")
	public  WebElement SRProgAnalyst;

	@FindBy(xpath = "//li[@class='tbox c-roles-2 professional']//a[@id='modal-6_AD0003']/span")
	public  WebElement SRProgAnalystText;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-3 professional']/a[@id='modal-6_AD0007']")
	public  WebElement AppDev;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0007']/span[2]")
	public  WebElement AppDevNumber;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-3 professional']/a[@id='modal-6_AD0006']")
	public  WebElement JavaDev;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0006']/span[2]")
	public  WebElement JavaDevNumber;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 managerial']/a[@id='modal-6_AD0009']")
	public  WebElement DevManager;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0003']/span[2]")
	public  WebElement SrProgAnalystNumber;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-2 professional']//a[@id='modal-6_AD0008']")
	public  WebElement SwAppEnggIII;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0008']/span[contains(text(),'Software Application Engineer III')]")
	public  WebElement SwAppEnggIIIText;
	
	@FindBy(xpath = "//a[@id='modal-6_AD0008']/span[2]")
	public  WebElement SwAppEnggIIINumber;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 managerial']/a[@id='modal-6_DDA012']/span")
	public  WebElement TeamLeader;
	
	@FindBy(xpath = "//a[@class='blue-button modal-button-navigate-edit'][contains(text(),'Edit')]")
	public  WebElement editPathButton;
	
	@FindBy(xpath = "//input[@type='text']")
	public  WebElement careerPath;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 professional']/a[@id='modal-6_SI0008']")
	public  WebElement analyst;
	
	@FindBy(xpath = "//a[@class='has-tip save-button']")
	public  WebElement analystPlussign;
	
	@FindBy(xpath = "//a[@title='Save this career']")
	public  WebElement analystPlussign1;
	
	@FindBy(xpath = "//a[@title='Remove this career']")
	public  WebElement analystTickSign;
	
	@FindBy(xpath = "//*[@id='modal-role_info']/a")
	public  WebElement close_Modal;
	
	
	public ITPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);	
		header= new HeaderPage(driver);
		locateMe = new LocateMePage(driver);
	}

	/*public String selectRole() throws InterruptedException {
		waitForElementToDisplay(progAnalyst);
		clickElement(progAnalyst);
		String lastSelectedRoleByUser = progAnalystText.getText();
		return lastSelectedRoleByUser;

	}*/
	
	public void selectRole(WebElement element) throws InterruptedException {
		waitForElementToDisplay(element);
		clickElement(element);
	}
	
	public String getRoleText(WebElement element) {
		waitForElementToEnable(element);
		String lastSelectedRoleByUser = element.getText();
		return lastSelectedRoleByUser;
	}
	
	public void editPath() throws InterruptedException {
		clickElement(editPathButton);
		waitForElementToDisplay(SRProgAnalystText);
		
	}
	
	public void clickBasicRoles() throws Exception {
		waitForElementToEnable(AppDev);
		clickElement(AppDev);
		clickElement(progAnalyst);
		clickElement(JavaDev);		
	}
	
	public void clickOtherRoles() throws InterruptedException {
		header.zoomOut();
		clickElement(DevManager);
		clickElement(SRProgAnalyst);
		clickElement(SwAppEnggIII);
	}
	
	public void clickOtherFamilyRoles() throws InterruptedException {
		
		clickElement(AppDev);
		clickElement(progAnalyst);
		clickElement(JavaDev);
		
		locateMe.clickUpArrow();
		clickElement(locateMe.ArrowDropdown_Logistics);
		header.zoomOut();	
	}
	
	
	public void scrollToElement(WebDriver driver) {
		
		Actions action = new Actions(driver);
		action.moveToElement(TeamLeader).clickAndHold().moveByOffset(1000,0).release().build().perform();
	}
	
	public void addPositionToProfile() throws InterruptedException {
		clickElement(analystPlussign);
	}
	
	public void clickAnalyst() throws InterruptedException {
		waitForElementToEnable(analyst);
		clickElement(analyst);
	}
	
	public void removePostionFromProfile() throws InterruptedException {
		waitForElementToDisplay(analystTickSign);
		clickElement(analystTickSign);
	}
	
	public void close_modal() throws InterruptedException {
		clickElement(close_Modal);
	}
}
