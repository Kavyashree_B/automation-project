package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManagerPage {

	
	@FindBy(xpath = "//h3[text()='Manager Assessment']")
	public  WebElement managerHeader;
	
	@FindBy(xpath = "//a[contains(@class,'dropdown')]")
	public  WebElement manager_dropdown;
	
	@FindBy(xpath = "//a[@data-culture='en']")
	public  WebElement english_dropdown;
	
	@FindBy(xpath = "//a[@data-culture='es']")
	public  WebElement spanish_dropdown;
	
	@FindBy(xpath = "//p[contains(text(),'Un empleado solicitó una evaluación del gerente.')]")
	public  WebElement managerPage_content;
	
	@FindBy(xpath = "//h3[contains(text(),'Pavan QA11 ')]")
	public  WebElement requested_employeeName;
	
	@FindBy(xpath = "//span[contains(text(),'gunashekar.sv@mercer.com')]")
	public  WebElement requested_employeeEmail;
	
	@FindBy(xpath = "//span[contains(text(),'2019')]")
	public  WebElement requested_Date;
	
	@FindBy(xpath = "//button[contains(text(),'Provide Assessment')]")
	public  WebElement provideAssesment_button;
	
	@FindBy(xpath = "//a[@data-reactid='.3.0.0.1.2']")
	public  WebElement nextPage_button;
	
	@FindBy(xpath = "//button[@class='right button large response-awated']")
	public  WebElement nextButton_disabled;
	
	@FindBy(xpath = "//*[@id='answerForm']/div[1]/label/h5")
	public  WebElement Level1_radiobutton;
	
	@FindBy(xpath = "//button[@class='right button large ']")
	public  WebElement nextButton_enabled;
	
	@FindBy(xpath = "//span[@class='email subheader'][contains(text(),'Core Competency')]")
	public  WebElement coreCompetencyHeader_Manager;
	
	@FindBy(xpath = "//span[@class='email subheader'][contains(text(),'Technical Competency')]")
	public  WebElement technicalCompetencyHeader_Manager;
	
	@FindBy(id = "notes")
	public  WebElement notes_textbox;
	
	@FindBy(xpath = "//h5[contains(text(),'Notes for competency')]")
	public  WebElement notesForCompetency_text;
	
	@FindBy(xpath = "//a[@class='left']")
	public  WebElement exitAndContinueLater_Link;
	
	@FindBy(xpath = "//button[text()='Continue']")
	public  WebElement continue_button;
	
	@FindBy(xpath = "//span[text()='IT Service Operations']")
	public  WebElement page7Header;
	
	@FindBy(xpath = "//*[@id='content']/div/div[3]/nav/button")
	public  WebElement submit_button;
	
	@FindBy(xpath = "//span[text()='Completed']")
	public  WebElement completed_button;
	
	@FindBy(xpath = "//section[@id='panel_3']/div/div[1]/div/p[3]")
	public  WebElement newAssessmentPage;
	
	@FindBy(xpath = "(//h5[contains(text(),'Suggest Development Ideas')])[1]")
	public  WebElement developmentIdeas_TC;
	WebDriver driver;
	
	public ManagerPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickdropdown() throws InterruptedException {
		clickElement(manager_dropdown);
	}
	
	public void clickSpanish() throws InterruptedException {
		clickElement(spanish_dropdown);
	}
	
	public void selectEnglish() throws InterruptedException {
		clickElement(manager_dropdown);
		clickElement(english_dropdown);
	}
	
	public void provideAssessment() throws InterruptedException {
		clickElement(provideAssesment_button);
	}
	
	public void clickLevel1() throws InterruptedException {
		waitForElementToEnable(Level1_radiobutton);
		clickElement(Level1_radiobutton);
	}
	
	public String convertToUpperCase(String str) 
    { 
        
        char ch[] = str.toCharArray(); 
        for (int i = 0; i < str.length(); i++) { 
  
            if (i == 0 && ch[i] != ' ' ||  
                ch[i] != ' ' && ch[i - 1] == ' ') {                
                if (ch[i] >= 'a' && ch[i] <= 'z') {    
                    ch[i] = (char)(ch[i] - 'a' + 'A'); 
                } 
            } 
  
            else if (ch[i] >= 'A' && ch[i] <= 'Z')  

                ch[i] = (char)(ch[i] + 'a' - 'A');             
        } 

        String st = new String(ch); 
        return st; 
    } 
	
	public void clickNext() throws InterruptedException {
		clickElement(nextButton_enabled);
	}
	
	public void exitAndContinue() throws InterruptedException {
		clickElement(exitAndContinueLater_Link);
	}
	
	public void clickContinue() throws InterruptedException {
		waitForElementToEnable(continue_button);
		clickElement(continue_button);
	}
	
	public void clickSubmit() {
		clickElementUsingJavaScript(driver, submit_button);
	}
	
	public void enterNotes() {
		setInput(notes_textbox, "Test Notes");
	}
	
	public void clickDevelopmentIdeas() throws InterruptedException {
		clickElement(developmentIdeas_TC);
	}
}
