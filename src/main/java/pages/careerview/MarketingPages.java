package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MarketingPages {
	WebDriver driver;
	@FindBy(id = "nav-subfunc_6_Mark_BM")
	public  WebElement brandMgmt;

	@FindBy(id = "nav-subfunc_6_Mark_SM")
	public  WebElement strategicMarketing;

	@FindBy(id = "nav-subfunc_6_Mark_MC")
	public  WebElement marketComm;

	@FindBy(id = "nav-subfunc_6_Mark_MR")
	public  WebElement marketResearch;

	@FindBy(id = "nav_zoom_out")
	public  WebElement zoomOut;
	

	@FindBy(id = "nav_all_maps")
	public  WebElement HomePageLogo;

	public MarketingPages(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void zoomOut() throws InterruptedException {
waitForElementToEnable(zoomOut);
		clickElement(zoomOut);
	}

	
}
