package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {

	WebDriver driver;

	public RegisterPage register;
	
	@FindBy(xpath = "(//input[@id='EmployeeId'])[1]")
	public  WebElement username_input;

	@FindBy(id = "Password")
	public  WebElement password_input;

	@FindBy(xpath = "//input[@value = 'Log In']")
	public  WebElement submit_button;

	@FindBy(xpath = "//span[contains(@style,'url(/Demo2016/Logo)')]")
	public  WebElement LoginCompanyLogo;

	@FindBy(xpath = "//div[@id='forms']/h3")
	public WebElement Header1;

	@FindBy(xpath = "//div[@id='forms']/h6")
	public  WebElement Header2;

	@FindBy(xpath = "//div[@id='forms']/p/a[1]")
	public  WebElement LogIn_link;

	@FindBy(xpath = "//a[contains(@class,'link-register')]")
	public  WebElement Register_link;

	@FindBy(xpath = "//form[@id='form0']/p/a")
	public  WebElement ForgotYourPassword;

	@FindBy(xpath = "//div/div[1]/div/video/source[1]")
	public  WebElement videoVerify;

	@FindBy(xpath = "//a[@id='selectedLanguage']")
	public  WebElement languageSel;

	@FindBy(xpath = "//div[@id='selectLanguage']/a[2]")
	public  WebElement spanish;

	@FindBy(xpath = "//div[@id='selectLanguage']/a[1]")
	public  WebElement english;

	@FindBy(xpath = "//form[@id='form0']/div[4]/ul/li")
	public  WebElement invalidCredsMsg;

	@FindBy(xpath = "//form[@id='form0']/div[1]/div/span")
	public  WebElement noCredsMsgUsername;

	@FindBy(xpath = "//form[@id='form0']/div[2]/div/span")
	public  WebElement noCredsMsgPwd;

	@FindBy(xpath="//form[@id='form0']/p/a")
	public WebElement ForgotPwdLink;
	
	@FindBy(xpath="//input[contains(@value,'Send')]")
	public WebElement SendBtn;
	
	@FindBy(xpath="//span[contains(text(),'Email field is required')]")
	public WebElement noEmailMsg;
	
	@FindBy(id="Email")
	public WebElement Email;
	
	@FindBy(xpath="//span[contains(text(),'The Email field is not a valid e-mail address.')]")
	public WebElement invalidEmailMsg;
	
	@FindBy(xpath ="//p[contains(text(),'Invalid email address.Please confirm it or click the Register link.')]")
	public WebElement UnregisteredEmailMsg;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		register = new RegisterPage(driver);
	}

	public void clickLogin() throws InterruptedException {
		
		waitForElementToEnable(submit_button);
		clickElement(submit_button);
	}
	public void loginWithInvalidCredentials(String username,String password) throws InterruptedException {

		waitForElementToDisplay(username_input);
		setInput(username_input, username);
		waitForElementToDisplay(password_input);
		setInput(password_input, password);
		clickElement(submit_button);
		waitForElementToDisplay(invalidCredsMsg);

	}

	public void loginWithNoCredentials() throws InterruptedException {

		waitForElementToDisplay(username_input);
		username_input.clear();
		waitForElementToDisplay(password_input);
		setInput(password_input, "");
		clickElement(submit_button);
		delay(1000);

	}
	public void loginWithEmployee_id(String username) throws InterruptedException {

		waitForElementToDisplay(username_input);
		setInput(username_input, username);
		waitForElementToDisplay(password_input);
		setInput(password_input, "");
		clickElement(submit_button);
		delay(2000);

	}
	public void loginToCareerView(String username , String password) throws InterruptedException {
		waitForElementToDisplay(username_input);
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(submit_button);

		/*if(driver.getPageSource().contains("403 - Forbidden: Access is denied.")) {
			driver.navigate().back();
			waitForElementToDisplay(username_input);
			setInput(username_input, username);
			setInput(password_input, password);
			clickElement(submit_button);
		}

		else {
			System.out.println();
			System.out.println("Login successful.");
			System.out.println();
		}*/
		

	}
	
	public void loginToCareerViewProd(String adminusername , String password) throws InterruptedException {
		waitForElementToDisplay(username_input);
		setInput(username_input, adminusername);
		setInput(password_input, password);
		clickElement(submit_button);
	}

	public void clickSpanish() throws InterruptedException {
		waitForElementToDisplay(languageSel);
		clickElement(languageSel);
		waitForElementToEnable(spanish);
		clickElement(spanish);
	}

	public void clickEnglish() throws InterruptedException {
		waitForElementToDisplay(languageSel);
		clickElement(languageSel);
		waitForElementToEnable(english);
		clickElement(english);
	}
	public void clickLoginLink() throws InterruptedException {
		waitForElementToDisplay(LogIn_link);
		clickElement(LogIn_link);
	}

	public void clickRegisterLink() throws InterruptedException {

		waitForElementToDisplay(Register_link);
		clickElement(Register_link);
	}
	

	public void clickSend() throws InterruptedException {
		waitForElementToDisplay(SendBtn);
		clickElement(SendBtn);
	}
	
	public void enterUnregisteredEmail(String email) throws InterruptedException {
		waitForElementToDisplay(Email);
		Email.clear();
		setInput(Email, email);
		clickSend();
	
		
	}
	
	public void enterInvalidEmail(String email) throws InterruptedException {
		waitForElementToDisplay(Email);
		setInput(Email,email);
		clickSend();
	}
	public void clickForgotPwdLink() throws InterruptedException {
		waitForElementToDisplay(ForgotPwdLink);
		clickElement(ForgotPwdLink);
	}

	/*public boolean  verifyImageExists(String imageName){

		boolean isValid = false;
		try {
			Screen screen = new Screen();
			Pattern image = new Pattern(screenPath+imageName);
			//Wait 10ms for image 
			System.out.println(screenPath+imageName);
			System.out.println(image);
			try 
			{
				screen.wait(image, 20);
			} catch (FindFailed e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(screen.exists(image)!= null)
			{
				isValid = true;
			}
		}
		catch(Exception e){

		}
		return isValid;
	}*/
}
