package pages.careerview;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrgHeaderPage {

	/*@FindBy(xpath = "")
	public static WebElement ;*/
	
	@FindBy(xpath = "//div[@id='header']/h1[contains(text(),'Server Error')]")
	public  WebElement HeaderEle;
	
	@FindBy(xpath = "//div[@id='content']/div/fieldset/h2[contains(text(),'403 - Forbidden: Access is denied.')]")
	public  WebElement ForbiddenAccess;
	
	@FindBy(xpath = "//div[@id='content']/div/fieldset/h3[contains(text(),'You do not have permission to view this directory or page using the credentials that you supplied.')]")
	public  WebElement PermissionMsg;
	
	WebDriver driver;
	public OrgHeaderPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void waitForHeaderEle() {
		waitForElementToDisplay(HeaderEle);
	}
}
