package pages.careerview;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import verify.SoftAssertions;

import static driverfactory.Driver.*;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

import java.util.List;

public class ManagerAssessmentPage {
	WebDriver driver;
	@FindBy(xpath = "//a[@data-category-id= 'assessment']")
	public  WebElement managerAssessment_link;
	
	@FindBy(xpath = "//h1[text()='Manager Assessment']")
	public  WebElement managerAssessmentHeader;
	
	@FindBy(xpath = "//button[contains(text(),'Edit')]")
	public  WebElement edit_button;
	
	@FindBy(xpath = "//input[@name = 'managerName']")
	public  WebElement managerName_input;
	
	@FindBy(xpath = "//input[@name = 'managerEmail']")
	public  WebElement managerEmail_input;
	
	@FindBy(xpath = "//button[@class = 'button orange-button saveButton']")
	public  WebElement save_button;
	
	@FindBy(xpath = "//button[@class='button blue-button cancelButton']")
	public  WebElement cancel_button;
	
	@FindBy(xpath = "//button[@class='button blue-button right deleteButton']")
	public  WebElement returnToDefaultManager_button;
	
	public String defManager_xpath = "//button[@class='button blue-button right deleteButton']";
	
	@FindBy(xpath = "//button[text()= 'Request Assessment']")
	public  WebElement requestAssessment_button;
	
	@FindBy(xpath = "//button[text()= 'Request Sent']")
	public  WebElement requestSent_button;
	
	@FindBy(xpath = "//p[contains(text(),'Manager Name')]")
	public  WebElement managerName;
	
	@FindBy(xpath = "//p[contains(text(),'Manager Email')]")
	public  WebElement managerEmail;
	
	
	
	@FindBy(xpath = "//button[contains(text(),'Available')]")
	public  WebElement available_button;
	
	@FindBy(xpath = "//section[@id='panel_3']/div/div[1]/div/p[3]")
	public  WebElement newAssessmentPage;
	
	public ManagerAssessmentPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickSaveButton() throws InterruptedException {
		clickElement(save_button);
	}
	public void editManagerInfo(String name , String email) throws InterruptedException {
		
		clickElement(edit_button);
		managerName_input.clear();
		setInput(managerName_input, name);
		managerEmail_input.clear();
		setInput(managerEmail_input, email);
		clickElement(save_button);
		
	}
	
	public void editManagerInfoLast(String managerName , String managerEmail) throws InterruptedException {
		
		managerName_input.clear();
		setInput(managerName_input, managerName);
		managerEmail_input.clear();
		setInput(managerEmail_input, managerEmail);
		clickElement(save_button);
		
	}

	public String getManageName() {
		// TODO Auto-generated method stub
		return managerName_input.getAttribute("value");
	}
	
	public String getManageEmail() {
		// TODO Auto-generated method stub
		return managerEmail_input.getAttribute("value");
	}
	
	public void requestAssesment() throws InterruptedException {
		clickElement(requestAssessment_button);
		waitForElementToDisplay(requestSent_button);
	}
	
	public void clickEditButton() throws InterruptedException {
		waitForElementToEnable(edit_button);
		clickElement(edit_button);
	}
	
	public void clickCancelButton() throws InterruptedException {
		clickElement(cancel_button);
	}
	
	public String returnToDefaultManager() throws InterruptedException {
		
		clickElementUsingJavaScript(driver, returnToDefaultManager_button);
		Alert alert = null;
		String alertText;
		// Get a handle to the open alert, prompt or confirmation
		try {
			alert = driver.switchTo().alert();
		}
		catch(Exception e){
			
		}
		finally {
			// Get the text of the alert or prompt
			alertText = alert.getText();
			System.out.println(alertText);
			
			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;
		
	}
	
	public String getAlertText() {
		
		Alert alert = null;
		String alertText;
		// Get a handle to the open alert, prompt or confirmation
		try {
			alert = driver.switchTo().alert();
		}
		catch(Exception e){
			
		}
		finally {
			// Get the text of the alert or prompt
			alertText = alert.getText();
			System.out.println(alertText);
			
			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;
		

	}
	
	public void acceptAlert() {
		
		Alert alert = driver.switchTo().alert();
		// Get the text of the alert or prompt
		// And acknowledge the alert (equivalent to clicking "OK")
		alert.accept();

	}
	public boolean checkPresence(List<WebElement> element) {
		return(element.size() >0);
		
	}
}
