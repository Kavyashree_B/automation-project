package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CoreCompetencyPage {

	WebDriver driver;
	@FindBy(xpath = "//h1[contains(text(),'Core Competencies')]")
	public  WebElement coreCompetencyHeader;
	
	@FindBy(xpath = "//div[@id='user-competency_user_core-competencies_0']/label[1]/input")
	public  WebElement coreCompetencyRadioButton1;
	
	@FindBy(xpath = "//div[@id='user-competency_user_core-competencies_0']/label[2]/input")
	public  WebElement coreCompetencyRadioButton2;
	
	@FindBy(xpath = "//section[@id='panel_1']/div/button[1]")
	public  WebElement saveBtn;
	
	public CoreCompetencyPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}

	public void clickLevel1RadioButton() throws InterruptedException {
		waitForElementToEnable(coreCompetencyRadioButton1);
		clickElement(coreCompetencyRadioButton1);
	}
	
	public void clickLevel2RadioButton() throws InterruptedException {
		waitForElementToEnable(coreCompetencyRadioButton2);
		clickElement(coreCompetencyRadioButton2);
	}
	
	public void saveCompetency() throws InterruptedException {
		waitForElementToEnable(saveBtn);
		clickElement(saveBtn);
	}
}
