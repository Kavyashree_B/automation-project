package pages.careerview;

import static driverfactory.Driver.*;

import java.util.Random;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NavigateHomePage {

	WebDriver driver;
	HeaderPage header;
	ITPage itPage;

	@FindBy(xpath = "//a[@id='modal-6_AD0001']/span[contains(text(),'Programmer Analyst I')]")
	public  WebElement userCurrentRole;

	@FindBy(xpath = "//a[@id='modal-6_AD0001']/span[2][contains(text(),'1')]")
	public  WebElement defaultNumber;

	@FindBy(xpath = "//input[@type='text']")
	public  WebElement careerPath;

	@FindBy(xpath = "//a[contains(text(),'Save')]")
	public  WebElement saveButton;

	@FindBy(xpath = "//div[@id='navigate_isolation']/a[3]")
	public  WebElement prevButton;

	@FindBy(xpath = "//div[@id='navigate_isolation']/a[4]")
	public  WebElement nextButton;

	@FindBy(xpath = "//li[@id='nav-home_6_Mark']")
	public  WebElement mark;

	@FindBy(xpath = "//a[@class='blue-button modal-button-navigate-edit'][contains(text(),'Edit')]")
	public  WebElement editPathButton;


	@FindBy(xpath = "//p[contains(text(),'Saved to profile.')]")
	public  WebElement savedProfileMsg;

	@FindBy(xpath = "//a[contains(text(),'Make another')]")
	public  WebElement makeAnotherbutton;

	
	
	public NavigateHomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		header = new HeaderPage(driver);
		itPage = new ITPage(driver);
	}

	public boolean textboxWatermark() {

		String placeHolder = careerPath.getAttribute("placeholder");
		Boolean bool = placeHolder.equals("Name this career path");
		return bool;
	}


	public void clickSave() throws InterruptedException {
		waitForElementToDisplay(saveButton);
		clickElementUsingJavaScript(driver, saveButton);
		//clickElement(saveButton);
	}

	public String getAlertText() {
		
		Alert alert = null;
		String alertText;
		// Get a handle to the open alert, prompt or confirmation
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			alert = driver.switchTo().alert();
		}
		catch(Exception e){
			
		}
		finally {
			// Get the text of the alert or prompt
			alertText = alert.getText();
			System.out.println(alertText);
			
			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;
		
//		Boolean bool = alertText.equalsIgnoreCase("You must enter a name for this path.");
//		SoftAssertions.assertTrue(bool, "Alert box with a proper message is displayed.", test);
		

	}

	public void enterValidName() throws InterruptedException {
		waitForElementToEnable(careerPath);
		setInput(careerPath, nameGenerator());
		waitForElementToDisplay(saveButton);
		clickElementUsingJavaScript(driver, saveButton);
		delay(1000);
	}

	

	public static String nameGenerator() {
		Random Random = new Random();

		String generatedString = " " +
				(char) (Random.nextInt(26) + 'A') +
				(char) (Random.nextInt(26) + 'a') +
				(char) (Random.nextInt(26) + 'a') +
				(char) (Random.nextInt(26) + 'a') +
				(char) (Random.nextInt(26) + 'a');
		return generatedString;
	}
	
	public void clickMakeAnother() {
		clickElementUsingJavaScript(driver, makeAnotherbutton);
	}

	

	public String SelectNewPath() throws InterruptedException {
		
		Alert alert = null;
		String alertText;
		// Get a handle to the open alert, prompt or confirmation
		try {
			header.zoomOut();
			clickElement(itPage.SRProgAnalyst);
			setInput(careerPath, "Test123");
			clickElementUsingJavaScript(driver, saveButton);
			alert = driver.switchTo().alert();
		}
		catch(Exception e){
			
		}
		finally {
			// Get the text of the alert or prompt
			alertText = alert.getText();
			System.out.println(alertText);
			System.out.println(alertText.contains("Please confirm to save it as a new path or override existing path."));
			System.out.println(alertText.contains("Press OK to override the path."));
			System.out.println(alertText.contains("Press cancel to create a new path."));
			
			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;
		
	}
	

}
