package pages.flipkart;


import static driverfactory.Driver.*;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ProductsearchPage {

WebDriver driver;


public ProductsearchPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver,this);
		
	}
     @FindBy(xpath = "//input[@placeholder='Search for products, brands and more']")
     public  WebElement searchfield;
     
    @FindBy(xpath = "//button[@class='vh79eN']")
	public  WebElement search_button;
 
    
    @FindBy(xpath = "//a[@class='_1KHd47'][contains(text(),'Books')]") 
	public  WebElement booksverification;
    
    @FindBy(xpath = "//a[@class='_1KHd47'][contains(text(),'Pens & Stationery')]") 
   	public  WebElement pensverification;
    
    @FindBy(xpath = "//a[@class='_1KHd47'][contains(text(),'Footwear')]") 
   	public  WebElement shoesverification;

      



	public void clickSearch(String productname) throws InterruptedException {
		//System.out.println("searching");
		waitForElementToDisplay(searchfield);

		//waitForElementToClickable(searchfield);
		//clickElement(loginclose_button);
		//waitForElementToEnable(searchfield);
		clickElement(searchfield);
		

		//searchfield.sendKeys(productname);
		


         
		setInput(searchfield, productname);
		clickElement(search_button);
		waitForPageLoad(driver);
	       searchfield.clear();

		
		
	//driver.switchTo().window("0");

	}
}
