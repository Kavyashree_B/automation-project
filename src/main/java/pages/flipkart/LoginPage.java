package pages.flipkart;

import static driverfactory.Driver.*;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {

WebDriver driver;


     @FindBy(linkText = "Login")
     public  WebElement Loginlink;
   
     
    //@FindBy(xpath = "//span[contains(text(),'Enter Email')]")
 	@FindBy(xpath = "//input[@class='_2zrpKA _1dBPDZ']")
     public  WebElement username_input;
 
    
    //input[@class='_2zrpKA _3v41xv _1dBPDZ']
	//@FindBy(xpath ="//span[contains(text(),'Enter Password')]")
 	@FindBy(xpath = "//input[@class='_2zrpKA _3v41xv _1dBPDZ']")
 	public  WebElement password_input;

	@FindBy(css = "body.fk-modal-visible:div.mCRfo9:nth.row div.Km0IJL.col.col-3-5 div:nth-child(1) form:nth-child(1) div._1avdGP:nth-child(3) > button._2AkmmA._1LctnI._7UHT_c")
	public  WebElement submit_button;


	@FindBy(xpath = "//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[3]/div/div/div/div")
	public  WebElement SuccessfullLogin;

	@FindBy(xpath = "//div[contains(text(),'Logout')]")
	public  WebElement logout;

	@FindBy(xpath = "//span[contains(text(),'Please enter valid Email ID')]")
	public  WebElement invalidCredsMsgUserName;
	
	@FindBy(xpath = "//span[contains(text(),'Your username or password is incorrect')]")
	public  WebElement invalidCredsMsgPwd;
	
	

	
	//button[@class='_2AkmmA _29YdH8']

	
	@FindBy(xpath="//button[@class='_2AkmmA _29YdH8']")
	public WebElement closeLogindialogbox;
	

	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

	public void clickLogin(String username,String password) throws InterruptedException {
		//
		waitForElementToEnable(Loginlink);
		//clickElement(Loginlink);
		waitForElementToDisplay(username_input);
		
		setInput(username_input, username);
		waitForElementToDisplay(password_input);
		//setInput(password_input, password);
		password_input.sendKeys("Kavya@888");
			clickElement(submit_button);

		

		//clickElement(SuccessfullLogin);


   // clickElementUsingJavaScript(driver, SuccessfullLogin);
	
   // clickElementUsingJavaScript(driver, SuccessfullLogin);
		//clickElement(logout);
		
	//waitForElementToDisplay(SuccessfullLogin);
	    

		//waitForElementToDisplay(SuccessfullLogin);
		/*logoutdropdwn.click();
		Select sc=new Select(logoutdropdwn);
		sc.selectByIndex(9);
		
		*/
		
		
		//clickElement(closeloginbutton);
	}
	


	public void loginWithInvalidCredentials(String username,String password) throws InterruptedException {
		//waitForElementToDisplay(SuccessfullLogin);

		//Actions action = new Actions(driver);
		//action.moveToElement(SuccessfullLogin).perform();
		///clickElement(logout);
		waitForElementToEnable(Loginlink);

	
		waitForElementToDisplay(username_input);
		
		setInput(username_input, username);
		waitForElementToDisplay(password_input);
		//setInput(password_input, password);
		password_input.sendKeys("Kavya888");
		
		clickElement(submit_button);
	}
	


	public void loginWithNoCredentials() throws InterruptedException {
		
		waitForElementToDisplay(closeLogindialogbox);

		clickElement(closeLogindialogbox);
		clickElement(Loginlink);
		waitForElementToDisplay(username_input);
		username_input.clear();
		waitForElementToDisplay(password_input);
		setInput(password_input, "");
		clickElement(submit_button);
		delay(1000);

	}
	/*public void loginWithEmployee_id(String username) throws InterruptedException {

		waitForElementToDisplay(username_input);
		setInput(username_input, username);
		waitForElementToDisplay(password_input);
		setInput(password_input, "");
		clickElement(submit_button);
		delay(2000);

	}
	public void loginToCareerView(String username , String password) throws InterruptedException {
		waitForElementToDisplay(username_input);
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(submit_button);*/

		/*if(driver.getPageSource().contains("403 - Forbidden: Access is denied.")) {
			driver.navigate().back();
			waitForElementToDisplay(username_input);
			setInput(username_input, username);
			setInput(password_input, password);
			clickElement(submit_button);
		}

		else {
			System.out.println();
			System.out.println("Login successful.");
			System.out.println();
		}*/
		

	
		
	

	/*public boolean  verifyImageExists(String imageName){

		boolean isValid = false;
		try {
			Screen screen = new Screen();
			Pattern image = new Pattern(screenPath+imageName);
			//Wait 10ms for image 
			System.out.println(screenPath+imageName);
			System.out.println(image);
			try 
			{
				screen.wait(image, 20);
			} catch (FindFailed e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(screen.exists(image)!= null)
			{
				isValid = true;
			}
		}
		catch(Exception e){

		}
		return isValid;
	}*/
}
