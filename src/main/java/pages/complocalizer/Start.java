package pages.complocalizer;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.switchToWindowByTitle;
public class Start {

	@FindBy (xpath = "//*[@class='app-title']")
	public	WebElement ApplicationName;
	@FindBy(xpath ="//li[@class='active']//following::div")
	public WebElement ActiveTab;
	@FindBy (xpath = "//li[@id='emailId']//a")
	WebElement AccountDetailsDropdown;
	@FindBy(xpath = "//div[contains(text(),'ACCOUNT')]//following::div[1]")
	public WebElement LoggedInAccount;
	@FindBy(xpath = "//*[contains(text(),'Start Calculation')]")
	public WebElement StartCalculationHeader;
	
	@FindBy(xpath = "//select[@id='HomeLocation']")
	WebElement HomeLocationDropdown;
	
	@FindBy(xpath = "//select[@id='HostLocation']")
	WebElement HostLocationDropdown;//Switzerland, Zurich
	
	@FindBy(id = "checkboxId")
	WebElement AcknowledgementCheckBox;
	@FindBy (xpath = "//button[@id='submitbtnId']")
	WebElement ContinueButton;
	
	@FindBy(partialLinkText = "Compensation Localizer")
	WebElement compLnkProd;
	@FindBy(linkText = "Compensation Localizer (UAT)")
	WebElement compLnk;
	@FindBy(linkText = "Compensation Localizer (STG)")
	WebElement compLnkStaging;
	
	WebDriver driver;
	public Start(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this)	;
		}
		public void openCompensationLocalizer() throws InterruptedException {
			
			if((System.getProperty("env")).equalsIgnoreCase("Staging") )
				clickElement(compLnkStaging);
			else if ((System.getProperty("env")).equalsIgnoreCase("Production") || (System.getProperty("env")).equalsIgnoreCase("Prod") )
			{
				waitForElementToDisplay(compLnkProd);
				clickElement(compLnkProd);
			}
			else
			{
				clickElement(compLnk);
			}
			switchToWindowByTitle("COMPENSATOIN LOCALIZER",driver);
			//driver.manage().window().maximize();
			clickElement(AccountDetailsDropdown);
		}
		public void startCalculation(String home, String host) throws InterruptedException {
			waitForElementToDisplay(HomeLocationDropdown);
			selEleByVisbleText(HomeLocationDropdown,home);
			selEleByVisbleText(HostLocationDropdown,host);
			clickElement(AcknowledgementCheckBox);
			clickElement(ContinueButton);
			//delay(7000);
		}
}
