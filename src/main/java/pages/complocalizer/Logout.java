package pages.complocalizer;


import static driverfactory.Driver.clickElement;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Logout {

	@FindBy(xpath = "//a[text()='Return to Mobility Exchange']")
	WebElement ReturnToMobilityExchange;
	@FindBy(xpath = "//*[@class='hidden-xs hidden-sm top-icons']//i[@class='top-avatar-icon top-avatar']")
	WebElement MyAccountButton;
	@FindBy(linkText = "Logout")
	WebElement LogoutButton;
	@FindBy(linkText = "Are you an expatriate? Please take a short survey")
	public WebElement HomePageSurvey;
	
	WebDriver driver;
	public Logout(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void logout() throws InterruptedException {
		clickElement(ReturnToMobilityExchange);

		if(System.getProperty("env").equalsIgnoreCase("Staging"))
		{
			driver.navigate().to("https://qa-mobilityportal.mercer.com/");
		}
		
		clickElement(MyAccountButton);
		clickElement(LogoutButton);
		
	}
}
