package pages.pit;

public class InputPageValidationMessage {

	public static final String Mandatory_Field_MSG="*This field is required.";
	public static final String Numeric_Field_Length_MSG="*Only 12 numeric values allowed in this field.";
	public static final String Decimal_Field_Length_MSG="*Only 12 numeric values allowed in this field with up to 2 decimal places.";
	public static final String ExchangeRate_Field_MSG="*Only 12 numeric values allowed in this field with up to 6 decimal places.";
}
