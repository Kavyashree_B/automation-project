package pages.pit;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import driverfactory.Driver;

public class PIT_HomePage extends Driver

{
	WebDriver driver;
	
	public PIT_HomePage(WebDriver driver)
	{this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//top header section
	@FindBy(xpath="//*[@id='emailId']/a")
	public static WebElement bannerEmail;
	
	@FindBy(xpath="//*[@id='emailId']//div[text()='Client:']/following-sibling::div")
	public static WebElement clientName;
	
	@FindBy(xpath="//*[@id='emailId']//div[text()='Account:']/following-sibling::div")
	public static WebElement accountName;
	
	@FindBy(xpath="//a[contains(@href,'SignOut')]")
	public static WebElement returnToMobilityExchangeLink;
	
	@FindBy(xpath="//h3[@class='app-title']")
	public static WebElement appTitle;
	
	@FindBy(xpath="//h2[@class='talent-tagline mobilize']")
	public static WebElement logoText;
	
	@FindBy(xpath="//h2[contains(text(),'WELCOME')]")
	public static WebElement h2_WelcomeHeader;
	
	@FindBy(xpath="//div/p/i")
	public static WebElement h2_text;
	
	
	@FindBy(xpath="//div[@class='alert alert-info']")
	public static WebElement alertInfo;
	
	@FindBy(xpath="//div//h4[contains(text(),'Tax Alerts')]")
	public static WebElement taxAlerts;
	
	@FindBy(xpath="//h4[contains(text(),'Resources')]/parent::div//li/a")
	public static WebElement taxAlertsLink;
	
	@FindBy(xpath="//div//h4[contains(text(),'Resources')]")
	public static WebElement resources;
	
	@FindBy(xpath="//h4[contains(text(),'Resources')]/parent::div//li/a")
	public static WebElement resourcesLink;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//ul[contains(@class,'nav navbar-nav navbar-right main-nav')]/li"))  
	public static List<WebElement> topNavBarLinks;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//a[contains(text(),'ABOUT PERSONAL')]/following-sibling::ul/li"))  
	public static List<WebElement> aboutPITLinks;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//a[contains(text(),'OTHER RELATED SERVICES')]/following-sibling::ul/li"))  
	public static List<WebElement> otherRelatedServicesinks;
	
	
	/**
	 * @param serviceLink -Links under OtherRelatedServices
	 * */
	public static void clickOtherRelatedServices(String serviceLink) {
		for(WebElement e:otherRelatedServicesinks) {
			if(e.getText().contains(serviceLink))
				e.click();
			break;
		}
	}
	
	/**
	 * @param aboutPITLink -Links under aboutPITLinks
	 * */
	public static void clickAboutPITLinks(String aboutPITLink) {
		for(WebElement e:aboutPITLinks) {
			if(e.getText().contains(aboutPITLink))
				{e.click();
			delay(5000);
			break;}
		}
	}
	
	
	
	@FindBy(xpath="//a[contains(text(),'View Help')]")
	public static WebElement viewHelpLink;
	
	
  //Personal Tax Calculator section
	@FindBy(id="TaxLocation")
	public static WebElement countryDropdown;
	
	public static List<String> getCountryDropdownValues(){
		List<String> values = new ArrayList<>();
		Select s= new Select(countryDropdown);
		for(WebElement e:s.getOptions()) {
			values.add(e.getText().trim());
		}
		return values;
	}
	
	@FindBy(id="submitbtnId")
	public static WebElement beginCaclulationBtn;
	
//Personal Tax Report section 
	@FindBy(id="TaxReportLocation")
	public static WebElement taxReportLocationDropdown;
	
	@FindBy(id="Year")
	public static WebElement taxYearDropdown;
	
	@FindBy(id="submitbtnId")
	public static WebElement runYourReportBtn;
	
//Footer section
	@FindBy(xpath="//div[@class='footer container-fluid']//p")
	public static WebElement copyRightLabel;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//div[@class='social']//li/a"))  
	public static List<WebElement> socialLinks;
	
	@FindBy(xpath="//video//source[contains(@src,'VideoClip')]")
	public static WebElement videoClip;
	
	@FindBy(xpath="//a[contains(text(),'Contact Mercer')]")
	public static WebElement contactMercer;
	
	public  void clickOnContactMercer() {
		waitForElementToDisplay(contactMercer);
		contactMercer.click();
		waitForPageLoad(driver);
		delay(3000);
		switchToWindowWithURL(driver, "support");
	} 
	
	
	public  PIT_InputPage accessInputPage(String countryName,WebDriver driver) throws InterruptedException {
		delay(3000);
		waitForPageLoad(driver);
		waitForElementToDisplay(countryDropdown);
		selEleByVisbleText(countryDropdown, countryName);
		clickElement(beginCaclulationBtn);
		waitForPageLoad(driver);
		switchToWindowWithURL(driver,"Input");
		waitForPageLoad(driver);
		delay(5000);
		return new PIT_InputPage(driver);
	}
	
	public static void clickOnBannerEmail() {
		waitForElementToDisplay(bannerEmail);
		bannerEmail.click();
	}
	
	
	
	public static String getAccountCode(){
		delay(1000);
		bannerEmail.click();
		delay(1000);
		String account=accountName.getText();
		return account;
	
	}
	public static void runTaxReport(String country, String year) throws InterruptedException {
		selEleByVisbleText(taxReportLocationDropdown, country);
		waitForElementToDisplay(taxYearDropdown);
		selEleByVisbleText(taxYearDropdown, year);
		clickElement(runYourReportBtn);
		delay(3000);
	}
	public static void clickAboutPITLink() throws InterruptedException {
	clickElement(topNavBarLinks.get(0));
		}
	 
	public static void clickOnOtherServicesLink() {
		for(WebElement we: topNavBarLinks) {
			if(we.getText().contains("OTHER RELATED SERVICES")) 
				we.click();
		}
			}
	public static void clickViewHelpLink() throws InterruptedException {
		waitForElementToDisplay(viewHelpLink);
		clickElement(viewHelpLink);
			}
	public  void clickReturnToMobilityExchange() throws InterruptedException {
		clickElement(returnToMobilityExchangeLink);
		waitForPageLoad(driver);
		delay(3000);

	}
	
	public static void clickFooterSocialLinks(String link) throws InterruptedException {
		for(WebElement we:socialLinks) {
			System.out.println(we.getAttribute("href"));
			if(we.getAttribute("href").contains(link)){
				clickElement(we);
				delay(5000);
			}
		}
	}	
		public static List<String> getFooterSocialLinksURL() {
			List<String> socialLinkURLs=new ArrayList<>();
			for(WebElement we:socialLinks) {
				socialLinkURLs.add(we.getAttribute("href"));
				}	
		return socialLinkURLs;
	}
	
}
