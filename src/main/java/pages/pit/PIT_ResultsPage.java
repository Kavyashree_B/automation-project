package pages.pit;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.File;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class PIT_ResultsPage extends Driver
{
	WebDriver driver;
	public PIT_ResultsPage(WebDriver driver)
	{this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@class='col-xs-12 guts']//h2")
	public static WebElement headerLabel;
	
	@FindBy(xpath="//*[contains(text(),'Country / Market / City')]/parent::*//td[2]")
	public static WebElement countryMarketName;
	
	@FindBy(xpath="//*[contains(text(),'Annual Salary Details')]")
	public static WebElement annualSalaryDetailsLabel;
	
	@FindBy(xpath="//*[contains(text(),'Bonus')]/following-sibling::td")
	public static WebElement bonus;
	
	
	@FindBy(xpath="//*[contains(text(),'Net Income')]/parent::*//td[2]")
	public static WebElement netIncome;
	
	@FindBy(xpath= "//*[text()='Total Personal Income Tax']//parent::tr//td[2]")
	public static WebElement totalIncomeTax;
	
	@FindBy(xpath= "//*[contains(text(),'Family Allowance')]/parent::*//td[2]")
	public static WebElement familyAllowance;
	
	@FindBy(xpath= "//*[contains(text(),'Social Security Contributions')]/parent::*//td[2]")
	public static WebElement socialSecurity;
	
	
	@FindBy(xpath= "//*[contains(text(),'Estimated Employer Contributions')]/following-sibling::td")
	public static WebElement estimatedEmployerContributions;
	
	@FindBy(xpath= "//*[contains(text(),'Tax as Percentage of Gross Income')]/following-sibling::td")
	public static WebElement taxAsPercentageOfGrossIncome;
	
	@FindBy(xpath= "//*[contains(text(),'Tax & Social Security as Percentage of Gross Income')]/following-sibling::td")
	public static WebElement taxAndSocialSecurityAsPercentageofGrossIncome;
	
	@FindBy(xpath= "//*[contains(text(),'Marginal Rate Tax')]/following-sibling::td")
	public static WebElement marginalRateTax;
	
	@FindBy(xpath= "//*[contains(text(),'Marginal Rate Tax & Social Security')]/following-sibling::td")
	public static WebElement marginalRateTaxAndSocialSecurity;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//table[1]//tr//td")) 
	public static List<WebElement> resultsPageTable1; 
	
	@FindBy(xpath="(//table[1]//tr//td[contains(text(),'Exchange Rate')]/following::tr)[1]/td")
	public static WebElement exchangeRateType;
	
	
	@FindBy(id="ExportPDF")
	public static WebElement exportPDFRadioBtn;
	
	@FindBy(id="ExportExcel")
	public static WebElement exportEXCELRadioBtn;
	
	@FindBy(id="Export")
	public static WebElement exportBtn;
	
	// for net to gross
	@FindBy(xpath="//*[text()='Gross Income']//parent::tr//td[2]")
	public static WebElement grossIncome;
	

	public static String getValue(String label) {
		String actualValue=null;
		switch(label) {
		case "NetIncome":
			waitForElementToDisplay(netIncome);
			actualValue=netIncome.getText().toString().trim().replaceAll(",", "");
		    break;
		case "GrossIncome":
			waitForElementToDisplay(grossIncome);
			actualValue=grossIncome.getText().toString().trim().replaceAll(",", "");
		    break;   
		case "TotalPersonalIncomeTax":
			waitForElementToDisplay(totalIncomeTax);
			actualValue=totalIncomeTax.getText().toString().trim().replaceAll(",", "");
		    break; 
		case "SocialSecurity":
			waitForElementToDisplay(socialSecurity);
			actualValue=socialSecurity.getText().toString().trim().replaceAll(",", "");
		    break; 
		case "FamilyAllowance":
			waitForElementToDisplay(familyAllowance);
			actualValue=familyAllowance.getText().toString().trim().replaceAll(",", "");
		    break; 
		}
		
		
		return actualValue;
	}
	
	
	public static void downloadPDF() throws InterruptedException {
		waitForElementToDisplay(exportPDFRadioBtn);
		clickElement(exportPDFRadioBtn);
		clickElement(exportBtn);
		delay(8000);
	} 
	
	public static void downloadExcel() throws InterruptedException {
		waitForElementToDisplay(exportEXCELRadioBtn);
		clickElement(exportEXCELRadioBtn);
		clickElement(exportBtn);
		delay(8000);
	}
	
	
	
	public static WebElement get3rdColumnElement(String key) {
		WebElement e=null;
		
		for(int i=0;i<resultsPageTable1.size();i++) {
			if(resultsPageTable1.get(i).getText().trim().equals(key)) {
				e=resultsPageTable1.get(i+2);
				break;
			}
		}
		return e;
	}
	
	public static WebElement get2ndColumnElement(String key) {
		WebElement e=null;
		
		for(int i=0;i<resultsPageTable1.size();i++) {
			if(resultsPageTable1.get(i).getText().trim().equals(key)) {
				e=resultsPageTable1.get(i+1);
				break;
			}
		}
		return e;
	}
	
	public static WebElement get1stColumnElement(String key) {
		WebElement e=null;
		
		for(int i=0;i<resultsPageTable1.size();i++) {
			if(resultsPageTable1.get(i).getText().trim().equals(key)) {
				e=resultsPageTable1.get(i);
				break;
			}
		}
		return e;
	}
	
	
	
	public static boolean isFileDownloaded(String downloadPath, String fileName) {
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
			  
		      if (dirContents[i].getName().contains(fileName)) {
		          // File has been found, it can now be deleted:
		    	  System.out.println("File found "+dirContents[i].getName());
		    	System.out.println("Downloaded File name--- >> "+dirContents[i].getName());
		    	  dirContents[i].delete();
		          return true;
		      }
		          }
		      return false;
		  }	
}
