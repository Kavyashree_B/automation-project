package pages.pit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class ResultsPage extends Driver
{
	WebDriver driver;
	public ResultsExportPage export;
	public HeaderPage header;
	WebElement e = null;
	public ResultsPage(WebDriver driver){
		export= new ResultsExportPage(driver); 
		header= new HeaderPage(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}


	@FindAll(@FindBy(how = How.XPATH, using = "//table[1]//tr//td")) 
	public static List<WebElement> resultTable; 

	@FindBy(xpath="//table[1]//th")
	public WebElement tableHeader; //User-defined Assumptions

	public  WebElement get3rdColumnElement(String key) {
		WebElement e=null;

		for(int i=0;i<resultTable.size();i++) {
			if(resultTable.get(i).getText().trim().contains(key)) {
				e=resultTable.get(i+2);
				break;
			}
		}
		return e;
	}

	public  WebElement get2ndColumnElement(String key) throws InterruptedException
	{
		waitForElementToDisplay(tableHeader);
		driver.manage().window().maximize();
		for(int i=0;i<resultTable.size();i++) {
			if(resultTable.get(i).getText().trim().contains(key)) {
				e=resultTable.get(i+1);
				break;

			}
		}
		return e;
	}

	public  WebElement get1stColumnElement(String key) {
		WebElement e=null;

		for(int i=0;i<resultTable.size();i++) {
			if(resultTable.get(i).getText().trim().contains(key)) {
				e=resultTable.get(i);
				break;
			}
		}
		return e;
	}


}
