package pages.pit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class PIT_InputPage_ATC_TDCSection extends Driver {
	WebDriver driver;

	public PIT_InputPage_ATC_TDCSection(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "Id_LableTaxableAllowanceFirst")
	private static WebElement atc1Label;

	@FindBy(xpath = "//*[@id='Clean_DivSecond']/div[1]/div[2]/div[5]/select")
	public static WebElement TA1TypeDropdown;

	@FindBy(id = "inputId_TAFirst")
	public static WebElement TA1Value;

	@FindBy(xpath = "(//button[@title='Add another'])[1]")
	public static WebElement TAPlusButton;
	
	// TDC 
	@FindBy(xpath ="//button[@data-bind='click: SetVisibilityTDC']")
	public static WebElement TDCPlusButton;
	
	
	/*****
	 * @author pavankumar-hegde
	 * @param data format = ATC3:Fixed Amount:5000;ATC2:% of Gross:5000;
	 * @param type value could be TaxDeductible or TaxableAllowance 
	 * */
	
	
//type = TaxDeductible or TaxableAllowance
	public  void inputATC_TDCSection(String data,String type) throws Exception {
	String inputType="TA";
		if(type.equals("TaxDeductible")) {
			inputType="TDC";
		}
	
		String[] allATCorTDC = data.split(";");
		int numberOfTimesToClickPlus = allATCorTDC.length - 1;	
			switch (numberOfTimesToClickPlus) {
			case 0:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				break;
			case 1:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				performActionOnATC_TDCSection(allATCorTDC[1],type,inputType, "Second");
				break;
			case 2:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				performActionOnATC_TDCSection(allATCorTDC[1],type,inputType, "Second");
				performActionOnATC_TDCSection(allATCorTDC[2],type,inputType, "Third");
				break;
			case 3:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				performActionOnATC_TDCSection(allATCorTDC[1],type,inputType, "Second");
				performActionOnATC_TDCSection(allATCorTDC[2],type,inputType, "Third");
				performActionOnATC_TDCSection(allATCorTDC[3],type,inputType, "Fourth");
				break;
			case 4:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				performActionOnATC_TDCSection(allATCorTDC[1],type,inputType, "Second");
				performActionOnATC_TDCSection(allATCorTDC[2],type,inputType, "Third");
				performActionOnATC_TDCSection(allATCorTDC[3],type,inputType, "Fourth");
				performActionOnATC_TDCSection(allATCorTDC[4],type,inputType, "Fifth");
				break;
			case 5:
				performActionOnATC_TDCSection(allATCorTDC[0],type,inputType,"First");
				performActionOnATC_TDCSection(allATCorTDC[1],type,inputType, "Second");
				performActionOnATC_TDCSection(allATCorTDC[2],type,inputType, "Third");
				performActionOnATC_TDCSection(allATCorTDC[3],type,inputType, "Fourth");
				performActionOnATC_TDCSection(allATCorTDC[4],type,inputType, "Fifth");
				performActionOnATC_TDCSection(allATCorTDC[5],type,inputType, "Sixth");
				break;
			}
		

	}

	
	
	//data =label1 :Fixed amount:5000
	//labelType=TaxableAllowance or TaxDeductible
	//inputType=TA orTDC
	//rowNum= First,Second,Third ....etc
	public  void performActionOnATC_TDCSection( String data,String labelType, String inputType,String rowNum  ) throws Exception {
		if(inputType.equals("TA") && !rowNum.equals("First")) {
			waitForElementToDisplay(TAPlusButton);
			clickElement(TAPlusButton);
		}
		if(inputType.equals("TDC") && !rowNum.equals("First")) {
			waitForElementToDisplay(TDCPlusButton);
			clickElement(TDCPlusButton);
		}
		String[] eachRowItem=null;
		eachRowItem=data.split(":");
		WebElement label = getElement(driver,"//*[@id='Id_Lable"+labelType + rowNum + "']");
		label.clear();
		setInput(label, eachRowItem[0]);
		WebElement dropdown = getElement(driver,"(//*[@id='Id_Lable"+labelType + rowNum +"']/following::div/select)[1]");
		clickElement(dropdown);
		selEleByVisbleText(dropdown, eachRowItem[1]);
		WebElement value=getElement(driver,"//*[@id='inputId_"+inputType + rowNum + "']");
		value.clear();
		setInput(value, eachRowItem[2]);
		
		
	}
}
