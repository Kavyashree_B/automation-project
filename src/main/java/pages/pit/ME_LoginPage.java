package pages.pit;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;
import utilities.InitTests;

/**
 * @author Pavan
 *
 */
public class ME_LoginPage extends Driver
{WebDriver driver;
	public ME_LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	/////////////////////////////////////////////////
	public static ME_LoginPage initME_LoginPage(WebDriver driver){
		
		return new ME_LoginPage( driver);
	}
	//////////////////////////////////////////////////////////
	@FindBy(xpath="//*[@class='hidden-xs hidden-sm top-icons']//i[@class='top-avatar-icon top-avatar']")
	public static WebElement myAccountLink;
	
	@FindBy(xpath = "(//span[text()='Login'])[3]")
	public static WebElement loginBtn;
	
	@FindBy(id="ContentPlaceHolder1_PassiveSignInButton")
	public WebElement nxtButton;
	
	@FindBy(id="txtEmail")
	public WebElement UserName;
	
	@FindBy(xpath="//*[@id='txtPassword-clone']")
	public WebElement pwd;
	
	@FindBy(xpath="//*[@id='txtPassword']")
	public WebElement passwordEntry;
	
	@FindBy(xpath = "//*[contains(text(),'Enter')]")
	public WebElement enter;

	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement emailIdInput;

	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement passwordInput;

	@FindBy(xpath="//*[@id='ctl00_MainSectionContent_ButtonSignin']")
	public static WebElement signInBtn;
	
	/**
	 * @MethodName: doLogin 
	 * @purpose: To login in to Mobility exchange. 
	 * @author: pavan
	 **/
	public ME_ClientAccountSelectionPage doLogin(){
		try {
			/*waitForElementToDisplay(myAccountLink);
			myAccountLink.click();*/
			/*clickElement(myAccountLink);
			waitForElementToDisplay(loginBtn);
			loginBtn.click();
			waitForElementToDisplay(emailIdInput);
			emailIdInput.clear();
			setInput(emailIdInput, InitTests.USERNAME);
			passwordInput.clear();
			setInput(passwordInput, InitTests.PASSWORD);
			clickElement(signInBtn);*/
			clickElement(myAccountLink);
			waitForElementToDisplay(loginBtn);
			loginBtn.click();
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				clickElement(nxtButton);
				waitForElementToDisplay(UserName);
				UserName.clear();
				setInput(UserName, InitTests.USERNAME);
				waitForElementToDisplay(pwd);
				clickElement(pwd);
				setInput(passwordEntry,InitTests.PASSWORD);
				clickElement(enter);
				
			}
			else
			{
				waitForElementToDisplay(emailIdInput);
				emailIdInput.clear();
				setInput(emailIdInput, InitTests.USERNAME);
				passwordInput.clear();
				setInput(passwordInput, InitTests.PASSWORD);
				clickElement(signInBtn);
			}
			
		} catch (Exception e) {
			System.out.println("----- doLogin()"+e.getMessage());
		
		}		
		return new ME_ClientAccountSelectionPage(driver);
	}
	
}

