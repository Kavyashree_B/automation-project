package pages.pit;

import org.openqa.selenium.WebDriver;
import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class InputMainPage {
WebDriver driver;
public InputHelpIconsPage inputHelpIcons;

	public InputMainPage(WebDriver driver) {
		inputHelpIcons=new InputHelpIconsPage(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//label[contains(text(),'Optional Subtitle for Results Page')]")
	public static WebElement optionalTitleLabel;
	
	@FindBy(id="Subtitle")
	public static WebElement optionalTitleInput;
	
	@FindBy(id="ddlTaxTypeList")
	public static WebElement taxTypeDropdown;
	
	@FindBy(id="ddlTaxYearList")
	public static WebElement taxYearDropdown;
	
	@FindBy(id="AnnualSalary")
	public static WebElement salaryInput;
	
	@FindBy(xpath="//label[contains(text(),'Bonus Payment')]//following-sibling::div//select")
	public static WebElement bonusTypeDropdown;
	
	@FindBy(id="inputId_BonusPayment")
	public static WebElement bonusInput;
	
	@FindBy(id="ddlSalaryCurrencyList")
	public static WebElement currencyDropdown;
	
	@FindBy(id="ddlGivenSalaryIs")
	public static WebElement givenSalaryTypeDropdown;
	
	@FindBy(xpath="//label[contains(text(),'Marital')]/following::select[1]")
	public static WebElement marritalStatusDropdown;
	
	@FindBy(id="btnSubmit")
	public static WebElement viewResultsBtn;
	
	public void selectTaxYear(String taxYear) {
		selEleByVisbleText(taxYearDropdown, taxYear);
		
	}
	public void inputSalary(String salary) {
	setInput(salaryInput, salary);

	}
	public void selectMaritalStatus(String maritalStatus) {
		selEleByVisbleText(marritalStatusDropdown, maritalStatus);
		
	}
	public void setBonus(String bonusType, String bonusValue) {
		selEleByVisbleText(bonusTypeDropdown, bonusType);
		setInput(bonusInput, bonusValue);
	}
	public void selectGivenSalary(String givenSalary){
		selEleByVisbleText(givenSalaryTypeDropdown, givenSalary);
	}
	public void clickViewResults() throws InterruptedException {
	clickElement(viewResultsBtn);
	}

}
