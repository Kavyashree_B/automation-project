package pages.pit;

import static driverfactory.Driver.*;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
	WebDriver driver;
	public HeaderPage header;

	public HomePage(WebDriver driver) {
		header = new HeaderPage(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//button[contains(text(),'Begin Your Calculation')]")
	public WebElement CalculationBtn;

	@FindBy(xpath = "//*[@id='TaxLocation']")
	public static WebElement countryDropdown;

	public void selectCountry(String country) throws InterruptedException {
		selEleByVisbleText(countryDropdown, country);

	}

	public void BeginCalculation() throws InterruptedException 
	{
		driver.manage().window().maximize();
		waitForPageLoad(driver);
		clickElement(CalculationBtn);
	}

	public List<WebElement> getDropdownValues() {

		Select taxLocationsList = new Select(countryDropdown);
		return taxLocationsList.getOptions();

	}

}
