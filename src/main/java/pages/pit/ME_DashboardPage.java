package pages.pit;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;
import verify.SoftAssertions;

public class ME_DashboardPage extends Driver
{
	WebDriver driver;
	public  ME_DashboardPage(WebDriver driver)
	 {this.driver=driver;
		PageFactory.initElements(driver, this);
	}

// OR for ME landing page 
	
	@FindBy(xpath="//*[@id='dnn_accountContextSelectionControl_pnlAccountSelect']/div[1]/div/a")
	public static WebElement SignedINAsAccountCode;
	
	@FindBy(xpath="//a[@class='sapphire ng-binding'][contains(text(),'Personal Income Tax Solution (UAT)')]")
	//Personal Income Tax Solution (UAT)
	public static WebElement PITLink;
	
	@FindBy(xpath="//a[@class='sapphire ng-binding'][contains(text(),'Personal Income Tax Solution')]")
	public static WebElement PITLink_Prod;
	
	
	
	
	public PIT_HomePage accessPIT(WebDriver driver) throws InterruptedException{
		if((System.getProperty("env")).equalsIgnoreCase("QA") )
		{
			waitForPageLoad(driver);
			delay(2000);
			waitForElementToDisplay(PITLink);
			clickElement(PITLink);
		}
		else
		{
			waitForPageLoad(driver);
			delay(2000);
			waitForElementToDisplay(PITLink_Prod);
			clickElement(PITLink_Prod);
		}
		delay(3000);
		waitForPageLoad(driver);
		switchToWindowWithURL(driver,"PersonalIncomeTax");
		delay(6000);  
	
		return new PIT_HomePage(driver);
	}
	


}
