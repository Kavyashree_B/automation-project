package pages.pit;

import static driverfactory.Driver.waitForPageLoad;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class PIT_InputPage extends Driver
{static WebDriver driver;
	public PIT_InputPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//label[contains(text(),'Optional Subtitle for Results Page')]")
	public static WebElement optionalTitleLabel;
	
	@FindBy(id="Subtitle")
	public static WebElement optionalTitleInput;
	
	@FindBy(id="ddlTaxTypeList")
	public static WebElement taxTypeDropdown;
	
	@FindBy(id="ddlTaxYearList")
	public static WebElement taxYearDropdown;
	
	@FindBy(id="AnnualSalary")
	public static WebElement salaryInput;
	
	@FindBy(xpath="//label[contains(text(),'Bonus Payment')]//following-sibling::div//select")
	public static WebElement bonusTypeDropdown;
	
	
	@FindBy(id="inputId_BonusPayment")
	public static WebElement bonusInput;
	
	@FindBy(id="ddlSalaryCurrencyList")
	public static WebElement currencyDropdown;
	
	@FindBy(id="ddlGivenSalaryIs")
	public static WebElement givenSalaryTypeDropdown;
	
	@FindBy(xpath="//label[contains(text(),'Marital')]/following::select[1]")
	public static WebElement marritalStatusDropdown;
	
	
	//For GROSS TO NET 
	
	@FindBy(xpath="//*[@id='Clean_DivSecond']/div[1]/div[2]/div[5]/select")
	public static WebElement TaxableAllowanceTypeDropdown;
	
	@FindBy(id="inputId_TAFirst")
	public static WebElement TaxableAllowanceValueInput;
	
	@FindBy(xpath="//*[@id='Clean_DivSecond']/div[1]/div[8]/div[3]/select")
	public static WebElement TDCTypeDropdown;
	
	@FindBy(id="inputId_TDCFirst")
	public static WebElement TDCValueInput;
	
	@FindBy(id="ddTaxAllowance")
	public static WebElement IncludeinSocialSecurityCalculation_Dropdown;
	
	public void selectIncludeinSocialSecurityCalculation_Dropdown(String value) {
		selEleByVisbleText(IncludeinSocialSecurityCalculation_Dropdown, value);
	}
	
	//Additional Compensation to include in Gross Up section--NET TO GROSS
	@FindBy(id="IsIncludedRow1")
	public static WebElement additionalComp1CB;
	
	@FindBy(id="NameRow1")
	public static WebElement AdditionalCompLabel1INPUT;
	
	@FindBy(id="AmtValueRow1")
	public static WebElement AdditionalCompValue1INPUT;
	
	//Special Options section 
	@FindBy(id="IdSocialSecurityList")
	public static WebElement includeSocialSecurityCheckBox ;
	
	@FindBy(id="chkIncludeFamilyAllowance")
	public static WebElement includeFamilyAllowanceCheckBox ;
	
	@FindBy(id="chkChoosetoseeDetailedReport")
	public static WebElement chooseDetailedTaxCalcCheckBox ;
	
	@FindBy(id="btnSubmit")
	public static WebElement viewResultsBtn;
	
	public  void clickViewResultsBtn() throws InterruptedException {
		clickElement(viewResultsBtn);
	} 
	
	@FindBy(xpath="//*[@data-target='#ModalChoosetoseeDetailedReport']")
	public static WebElement chooseDetailedTaxCalcHelpBtn ;
	
	
	@FindBy(xpath="//*[@id='Clean_DivFirst']/div[6]/label")
	public static WebElement salaryLabel;
	
	@FindBy(xpath="//input[@name='chkStatistical']")
	public static WebElement checkboxSMA;
	
	public static void selectSMAcheckbox() throws InterruptedException {
		waitForElementToDisplay(checkboxSMA);
		clickElement(checkboxSMA);
	}
	
	
	/************** Validation Message XPATHS*****************************************************************/
	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='AnnualSalary']/following-sibling::span")) 
	public static List<WebElement> annualSalaryMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='inputId_BonusPayment']/parent::div/following-sibling::span")) 
	public static List<WebElement> bonusMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='inputId_CustomExchRate']//following-sibling::span")) 
	public static List<WebElement> customExRateMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='Clean_DivSecond']//span[contains(@class,'Error') and contains(@class,'TaxableAllowance')]")) 
	public static List<WebElement> taxableAllowanceLabelMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[contains(@id,'TA')]/parent::div/following-sibling::span")) 
	public static List<WebElement> taxableAllowanceValueMSG; 
	
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='Clean_DivSecond']//span[contains(@class,'Error') and contains(@class,'TaxDeductible')]")) 
	public static List<WebElement> taxDeductibleLabelMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[contains(@id,'TDC')]/parent::div/following-sibling::span")) 
	public static List<WebElement> taxDeductibleValueMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[contains(@id,'NameRow')]/following-sibling::span")) 
	public static List<WebElement> additionalCompensationLabelMSG; 
	
	@FindAll(@FindBy(how = How.XPATH, using = "//*[contains(@id,'ValueRow')]/following-sibling::span")) 
	public static List<WebElement> additionalCompensationValueMSG; 
	
	// salary validation message 
	public static WebElement getSalaryMsgElement(){
		WebElement msg= null;
		for(WebElement e:annualSalaryMSG) {
			//waitForElementToDisplay(e);
			delay(2000);
			if (e.getText().contains("*"))
				{msg = e;
			break;}
			}
		return msg;
	}
	
	// Bonus validation message 
	public static WebElement getBonusMsgElement(){
		WebElement msg= null;
		for(WebElement e:bonusMSG) {
			delay(2000);
		if (e.getText().contains("*"))
				{msg = e;
			break;}
			}
		return msg;
	}
	
	// Exchange rate validation message 
	public static WebElement getExchangeRateMsgElement(){
		WebElement msg= null;
		for(WebElement e:customExRateMSG) {
			delay(2000);
		if (e.getText().contains("*"))
				{msg = e;
			break;}
			}
		return msg;
	}
	
	// Taxable allowance Label validation message 
	public static WebElement getTaxableAllowanceLabelMsgElement(){
		WebElement msg= null;
		for(WebElement e:taxableAllowanceLabelMSG) {
			delay(2000);
		if (e.getText().contains("*"))
				{msg = e;
			break;}
			}
		return msg;
	}
	
	// Taxable allowance Label validation message 
	public static WebElement getTaxableAllowanceValueMsgElement(){
		WebElement msg= null;
		for(WebElement e:taxableAllowanceValueMSG) {
			delay(2000);
		if (e.getText().contains("*"))
				{msg = e;
			break;}
			}
		return msg;
	}
	// Tax Deductible Contributions Label validation message 
		public static WebElement getTaxDeductibleLabelMsgElement(){
			WebElement msg= null;
			for(WebElement e:taxDeductibleLabelMSG) {
				delay(2000);
			if (e.getText().contains("*"))
					{msg = e;
				break;}
				}
			return msg;
		}
		
		// Tax Deductible Contributions Value validation message 
		public static WebElement getTaxDeductibleValueMsgElement(){
			WebElement msg= null;
			for(WebElement e:taxDeductibleValueMSG) {
				delay(2000);
			if (e.getText().contains("*"))
					{msg = e;
				break;}
				}
			return msg;
		}
		
		
		// AdditionalCompensation LABEL validation message element
		public static WebElement getAdditionalCompensationLabelMsgElement(){
			WebElement msg= null;
			for(WebElement e:additionalCompensationLabelMSG) {
				delay(2000);
		      	if (e.getText().contains("*"))
				{msg = e;
				break;}
				}
			return msg;
		}
		// AdditionalCompensation VALUE validation message element
		public static WebElement getAdditionalCompensationLValueMsgElement(){
			WebElement msg= null;
			for(WebElement e:additionalCompensationValueMSG) {
				delay(2000);
		      	if (e.getText().contains("*"))
				{msg = e;
				break;}
				}
			return msg;
		}
		
		
		
	/****************************help icons ************************/
	public  String[] getHelpPopUpContentsOf(String nameOfHelp) throws Exception{
	String helpPopupContents[]=new String[2];
	
	clickElement(getElement(driver,"//button[contains(@data-target,'"+nameOfHelp+"')]"));
	clickElement(getElement(driver,"//*[contains(@id,'"+nameOfHelp+"')]/div//h4"));
		helpPopupContents[0]=getElement(driver,"//*[contains(@id,'"+nameOfHelp+"')]/div//h4").getText();
		helpPopupContents[1]=getElement(driver,"//*[contains(@id,'"+nameOfHelp+"')]/div//p").getText();
	clickElement(getElement(driver,"//*[contains(@id,'"+nameOfHelp+"')]/div//button"));
	return helpPopupContents;
	}
	
	/*public static WebElement[] getHelpPopUpContentsOf(String nameOfHelp) throws Exception{
		WebElement helpPopupContents[]=new WebElement[2];
		clickElement(getElement("//button[contains(@data-target,'"+nameOfHelp+"')]"));
		clickElement(getElement("//*[contains(@id,'"+nameOfHelp+"')]/div//h4"));
			helpPopupContents[0]=getElement("//*[contains(@id,'"+nameOfHelp+"')]/div//h4");
			helpPopupContents[1]=getElement("//*[contains(@id,'"+nameOfHelp+"')]/div//p");
		clickElement(getElement("//*[contains(@id,'"+nameOfHelp+"')]/div//button"));
		return helpPopupContents;
		}
	*/
	
/*	
	@FindBy(xpath="//button[@data-target='#ModalChoosetoseeDetailedReport']")
	public static WebElement choosetoseeDetailedReportIconBtn;
	*/
	public PIT_ResultsPage accessResultPage() {
		try {
		clickElement(viewResultsBtn);
		
		delay(3000);
		//PIT results page will be opened in next tab
		String parentWindow =driver.getWindowHandle();
		  Set<String> allWindows =driver.getWindowHandles();
		  for(String pitResultsWindow:allWindows){
			  
			  if(!parentWindow.equals(pitResultsWindow))
				  driver.switchTo().window(pitResultsWindow);
			  
			  }
		  delay(4000);
		  waitForPageLoad(driver);
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		return new PIT_ResultsPage(driver);
	}
	
	public static void selectTaxType(String taxType){
		waitForElementToDisplay(taxTypeDropdown);
		selEleByVisbleText(taxTypeDropdown, taxType);
	}
	
	public static void selectTaxYear(String taxYear){
		waitForElementToDisplay(taxYearDropdown);
		selEleByVisbleText(taxYearDropdown, taxYear);
	}
	
	
	public  void setSalary(String salary){
		waitForElementToDisplay(salaryInput);
		salaryInput.clear();
		setInput(salaryInput, salary);
	}
	public static void  setBonus(String bonusType, String bonusValue){
		waitForElementToDisplay(bonusTypeDropdown);
		selEleByVisbleText(bonusTypeDropdown,bonusType);
		waitForElementToDisplay(bonusInput);
		bonusInput.clear();
		setInput(bonusInput,bonusValue);
	}
	public static void selectCurrency(String currency){
		waitForElementToDisplay(currencyDropdown);
		selEleByVisbleText(currencyDropdown, currency);
	}
	public static void selectGivenSalary(String givenSalary){
		waitForElementToDisplay(givenSalaryTypeDropdown);
		selEleByVisbleText(givenSalaryTypeDropdown, givenSalary);
	}

	public static void selectMarritalStatusDropdown(String dropdownValue) {
		waitForElementToDisplay(marritalStatusDropdown);
		selEleByVisbleText(marritalStatusDropdown,dropdownValue);
	}
	
	public static void setTaxableAllowance(String TaxableAllowance1Type, String TaxableAllowance1Value ) {
		selEleByVisbleText(TaxableAllowanceTypeDropdown,TaxableAllowance1Type);
		waitForElementToDisplay(TaxableAllowanceValueInput);
		setInput(TaxableAllowanceValueInput, TaxableAllowance1Value);
	}
	public static void setTDC(String TDC1Type, String TDC1Value) {
		selEleByVisbleText(TDCTypeDropdown,TDC1Type);
		waitForElementToDisplay(TDCValueInput);
		setInput(TDCValueInput, TDC1Value);
	}
	
	
	public static void clickIncludeSocialSecurity() throws InterruptedException {
		waitForElementToDisplay(includeSocialSecurityCheckBox);
		clickElement(includeSocialSecurityCheckBox);
	}
	
	public static void clickIncludeFamilyAllowance() throws InterruptedException {
		waitForElementToDisplay(includeFamilyAllowanceCheckBox);
		clickElement(includeFamilyAllowanceCheckBox);
	}
	
	//help icons action
	
	public static void clickChoosetoseeDetailedReportIconBtn() throws InterruptedException {
		waitForElementToDisplay(chooseDetailedTaxCalcCheckBox);
		clickElement(chooseDetailedTaxCalcCheckBox);
	}
	
	/***********
	 * @author pavankumar-hegde
	 * @param data could be AC1:30000:No:No;
	 * ***********/
	public  void enterAdditionalCompensation(String data) throws Exception{ // data =AC1:30000:No:No;
		String[] rows=data.split(";");
	
		for(int row=1;row<=rows.length;row++) {
			clickElement(getElement(driver,"//*[@id='IsIncludedRow"+row+"']"));// to check number of rows 
			String []eachItem =rows[row-1].split(":");
			WebElement fieldName = driver.findElement(By.xpath("//input[@id='NameRow"+row+"']"));
			fieldName.sendKeys(eachItem[0]);
			WebElement fieldValue = driver.findElement(By.xpath("//input[@id='AmtValueRow"+row+"']"));
			fieldValue.sendKeys(eachItem[1]);
			/*setInput(getElement(driver,"//input[@id='NameRow"+row+"']"), eachItem[0]);  //label			
			setInput(getElement(driver,"//input[@id='AmtValueRow"+row+"']"), eachItem[1]); //value*/
			if( eachItem[2].equalsIgnoreCase("No")) {
				clickElement(getElement(driver,"//*[@id='chkIsTaxableRow"+row+"']")); //taxable checkbox
			}
			if( eachItem[3].equalsIgnoreCase("Yes")) {
				clickElement(getElement(driver,"//*[@id='chkIsBenifitInKindRow"+row+"']"));  // BIK checkBox
			}
		}
		
	}
	
	
	public  PIT_DetailedResultsPage goToDetailedTaxResultsPage(WebDriver driver) throws InterruptedException {
		waitForElementToDisplay(viewResultsBtn);
		clickElement(viewResultsBtn);
		delay(4000);
		return new PIT_DetailedResultsPage(driver);
	}
	
	//Exchange rate 
	@FindBy(id="ddlExchangeRateOptionsList")
	public static WebElement exchangeRateTypeDropdown;
	
	@FindBy(id="ddlWeeklyExchangeRateList")
	public static WebElement weeklyRateDropdown;
	
	@FindBy(id="ddlMonthlyExchangeRateList")
	public static WebElement monthlyRateDropdown;
	
	@FindBy(id="ddlAverageExchangeRateList")
	public static WebElement averageDropdown;
	
	@FindBy(id="inputId_CustomExchRate")
	public static WebElement customInput;
	
	/*****
	 * @author pavankumar-hegde
	 * @param data -format should be "type:value" Ex: Custom:4.5 
	 * @throws InterruptedException 
	 * 
	 * */
	public static void selectExchangeRate(String data) throws InterruptedException{
		String [] exRate=data.split(":");
		switch(exRate[0].toLowerCase()) {
		case "weekly" :
			waitForElementToDisplay(exchangeRateTypeDropdown);
			//clickElement(exchangeRateTypeDropdown);
			selEleByVisbleText(exchangeRateTypeDropdown, "Specific Week");
			waitForElementToDisplay(weeklyRateDropdown);
			selEleByVisbleText(weeklyRateDropdown, exRate[1]);
			break;
		case "monthly" :
			waitForElementToDisplay(exchangeRateTypeDropdown);
			//clickElement(exchangeRateTypeDropdown);
			selEleByVisbleText(exchangeRateTypeDropdown, "Specific Month");
			waitForPageLoad(driver);
			delay(3000);
			waitForElementToDisplay(monthlyRateDropdown);
			selEleByVisbleText(monthlyRateDropdown, exRate[1]);
			break;
		case "average" :
			waitForElementToDisplay(exchangeRateTypeDropdown);
			//clickElement(exchangeRateTypeDropdown);
			selEleByVisbleText(exchangeRateTypeDropdown, "Average of \"X\" Months");
			waitForPageLoad(driver);
			delay(3000);
			waitForElementToDisplay(averageDropdown);
			selEleByVisbleText(averageDropdown, exRate[1]);
			break;
		case "custom" :
			waitForElementToDisplay(exchangeRateTypeDropdown);
			//clickElement(exchangeRateTypeDropdown);
			selEleByVisbleText(exchangeRateTypeDropdown, "Custom Exchange Rate");
			waitForPageLoad(driver);
			delay(3000);
			waitForElementToDisplay(customInput);
			delay(2000);
			System.out.println(exRate[1]);
			//waitForElementToEnable(customInput);
			//customInput.clear();
			customInput.sendKeys(exRate[1]);
			//setInput(customInput, exRate[1]);
			break;
		}
		
		
	}
	
	
	public  PIT_ResultsPage clickViewResultsButton(WebDriver driver) throws InterruptedException {
		clickElement(viewResultsBtn);
		waitForPageLoad(driver);
		delay(4000);
		return new PIT_ResultsPage(driver);
	}
	
	public  PIT_InputPage_ATC_TDCSection initATCTDCSectionPage(WebDriver driver){
		
		return new PIT_InputPage_ATC_TDCSection(driver);
	}
	
}
