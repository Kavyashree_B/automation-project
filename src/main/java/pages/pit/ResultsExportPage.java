package pages.pit;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultsExportPage {
	WebDriver driver;
	public ResultsExportPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="ExportPDF")
	public  WebElement exportPDFRadioBtn;
	
	@FindBy(id="ExportExcel")
	public  WebElement exportEXCELRadioBtn;
	
	@FindBy(id="Export")
	public  WebElement exportBtn;
	
	public  boolean isFileDownloaded(String downloadPath, String fileName) {
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
			  
		      if (dirContents[i].getName().contains(fileName)) {
		          // File has been found, it can now be deleted:
		    	 System.out.println("Downloaded File name--- >> "+dirContents[i].getName());
		    	  dirContents[i].delete();
		          return true;
		      }
		          }
		      return false;
		  }	
}
