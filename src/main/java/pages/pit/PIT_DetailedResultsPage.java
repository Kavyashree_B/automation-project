package pages.pit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class PIT_DetailedResultsPage extends Driver {
	WebDriver driver;
	public PIT_DetailedResultsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindAll(@FindBy(how = How.XPATH, using = "//*[@id='DetailedTable']/table//tr//td/font"))  
	public static List<WebElement> detailedTaxResults;
	
	@FindAll(@FindBy(how = How.XPATH, using = "(//table)[2]//tr//td"))  
	public static List<WebElement> disclaimerTable;
	
	
	public static WebElement getValue(String key) {
		WebElement value=null;
		for(int i=0;i<detailedTaxResults.size();i++) {
	    	if(detailedTaxResults.get(i).getText().trim().equals(key)) {
	    		value= detailedTaxResults.get(i+1);
	    		break;
	    	}
	    }
		return value;
	}
	
	public static WebElement getTableElement(String key) {
		WebElement value=null;
		for(int i=0;i<detailedTaxResults.size();i++) {
			if(detailedTaxResults.get(i).getText().trim().contains(key)) {
	    		value= detailedTaxResults.get(i);
	    		break;
		}
		}
		return value;
		
	}
	
	
	}
	
	
	



