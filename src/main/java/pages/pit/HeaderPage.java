package pages.pit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static 	driverfactory.Driver.*;


public class HeaderPage {
	
	WebDriver driver;
	
public HeaderPage(WebDriver driver) {
	this.driver=driver;
	PageFactory.initElements(driver, this);
}	
	
@FindBy(xpath="//h3[@class='app-title']")
public WebElement appTitle;	

@FindBy(xpath="//a[contains(@href,'SignOut')]")
public  WebElement returnMELink;

//top header section
	@FindBy(xpath="//*[@id='emailId']/a")
	private WebElement bannerEmail;
	
	@FindBy(xpath="//*[@id='emailId']//div[text()='Client:']/following-sibling::div")
	public  WebElement clientName;
	
	@FindBy(xpath="//*[@id='emailId']//div[text()='Account:']/following-sibling::div")
	public  WebElement accountName;

	public void clickBannerEmail() throws InterruptedException {
	clickElement(bannerEmail);
	}
}
