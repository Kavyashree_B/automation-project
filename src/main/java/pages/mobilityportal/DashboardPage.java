package pages.mobilityportal;


import java.util.ArrayList;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.mobilityportal.HeaderPage;
public class DashboardPage {
	public HeaderPage header;
	WebDriver driver;
	@FindBy(xpath = "//label[contains(text(),'You are signed in as:')]/following::a[1]")
	public static WebElement loggedInAccountNo;

	@FindBy(xpath = "//a[contains(text(),'FEEDBACK')]")
	WebElement feedback;
	
	@FindBy(xpath = "//div//*[contains(text(),'Feedback Form')]")
	public static WebElement feedbackPageHeader; 
	
	@FindBy(xpath = "//a[contains(text(),'FAQ')]")
	WebElement fAQs;
	
	@FindBy(xpath = "//a[contains(@href,'Account-Client-selection')]")
	WebElement switchClient;
    
  
public DashboardPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	header=new HeaderPage(driver);
	this.driver=driver;
}
public void switchTab(String tabname) throws InterruptedException {
	clickElementUsingJavaScript(driver,getEleByXpathContains("uib-tab-heading",tabname,driver));
}
public void openLink(String link) throws InterruptedException {
	clickElement(driver.findElement(By.partialLinkText(link)));
	int i = driver.getWindowHandles().size();
	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window(tabs.get(i-1));
	delay(10000);		
}
public void feedbackLink() throws InterruptedException {
	clickElement(feedback);
	switchToWindow("Online Survey", driver);
}
public void faqsLink() throws InterruptedException {
	clickElement(fAQs);	
}
public void switchClientLink() throws InterruptedException {
	clickElement(switchClient);
}
public void socialShareIcons(String socialIcon) throws InterruptedException {
	clickElement(driver.findElement(By.xpath("//a[contains(@href,'"+socialIcon+"')]")));
	switchToWindowWithURL(socialIcon , driver);
}

}
