package pages.mobilityportal;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.getEleByLinkText;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;

public class HeaderPage {
	WebDriver driver;

	@FindBy(xpath = "//*[@class='hidden-xs hidden-sm top-icons']//i[@class = 'top-avatar-icon top-avatar']")
	public WebElement myAccounts;

	@FindBy(xpath = "//*[@class='hidden-xs hidden-sm top-icons']//span[text()='Logout']")
	public WebElement logoutLnk;
	@FindBy(xpath = "//div[contains(@id,'dnn_ContentPane')]//child::div[contains(@class,'DnnModule')]")
	public static WebElement pageContent; 
	
	@FindBy(css= "a[class*='my-account-lnk']")
	public WebElement myAccount;
	@FindBy(xpath = "//div[contains(@class,'visible-md')]//span[contains(text(),'CONTACT US')]")
	WebElement contactUs;
	@FindBy(xpath = "//div[contains(@class,'visible-md')]//a[contains(text(),'My Dashboard')]")
	WebElement dashboard;
	
	@FindBy(linkText = "Are you an expatriate? Please take a short survey")
	public static WebElement homePageSurvey;
	

	
	public HeaderPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	/*
	 * menuLinkTxt can be MyDashBoard/BalanceSheetCost/PlatformAdministrationTool/CompensationLocalizer
	 */
	public void navigateToMyAccMenu(String menuLinkTxt) throws InterruptedException {
		if(menuLinkTxt == "Platform Administration")
		{
			clickElement(myAccounts);
			clickElement(getEleByLinkText(menuLinkTxt,driver));
		}
		else
		{
			clickElement(getEleByLinkText(menuLinkTxt,driver));
		}


	}


	public void navigateLink(String tabname,String option) throws InterruptedException {
		hoverOverElement(driver,driver.findElement(By.xpath("//a[contains(@data-toggle,'dropdown') and contains(@href,'mercer') and contains(text(),'"+tabname+"')]")));
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//li[@class]//a[contains(text(),'"+option+"')]")));
		delay(1000);
	}
	public void logout() throws InterruptedException
	{
		clickElement(logoutLnk);	
	}
	
	public void contactUs() throws InterruptedException {
		clickElement(contactUs);
	}
	public void dashboard() throws InterruptedException {
		clickElement(myAccount);
		clickElement(dashboard);
	}
	


}
