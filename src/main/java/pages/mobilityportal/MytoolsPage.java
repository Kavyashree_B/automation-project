package pages.mobilityportal;


import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.mobilityportal.HeaderPage;
public class MytoolsPage {
	public HeaderPage header;
	@FindBy(xpath = "(//a[text()='One-Time Tables'])[4]")
	public static WebElement onetimetable;
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'My Tools')]")
    public static WebElement mytool_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'My Data')]")
    public static WebElement mydata_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'Custom Report')]")
    public static WebElement customreport_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'Notification')]")
    public static WebElement notification_link;
    
    @FindBy(css = "h1.title-my-tools")
    public  WebElement myToolsHeaderTxt;
    @FindBy(css = "div[id*='accountContextSelectionControl']>div>div>label")
    public  WebElement youAresignedInTxt;

    @FindBy(css = " div[id*='AccountSelect'] div:nth-child(2)>label")
    public  WebElement accNameTxt;

    @FindBy(css = "div[id*='AccountSelect']>div>label")
    public  WebElement welcomeUserTxt;
    
    
   
    @FindBy(linkText = "One-Time Tables")
    public static WebElement oneTimeTablesLnk;
    
    
    @FindBy(linkText = "Housing and Education Reports")
    public static WebElement housingEduReportLnk;
    
    
    @FindBy(linkText = "Quick Calc")
    public static WebElement quickCalcLnk;
    
  
public MytoolsPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	header=new HeaderPage(driver);
}

public void clickOnOneTimeTable() throws InterruptedException {
	clickElement(oneTimeTablesLnk);

}
public void clickOnQuickCalc() throws InterruptedException {
	clickElement(quickCalcLnk);

}

public void clickOnHousingEdcation() throws InterruptedException {
	// TODO Auto-generated method stub
	clickElement(housingEduReportLnk);
}
}
