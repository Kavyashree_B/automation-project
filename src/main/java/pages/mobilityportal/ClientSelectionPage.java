package pages.mobilityportal;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.waitForElementToClickable;
//import static driverfactory.Driver.getEleByXpathTxt;
import static driverfactory.Driver.getEleByXpathContains;

import static driverfactory.Driver.refreshpage;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ClientSelectionPage {
WebDriver driver;
	

public static  WebElement customer;

public static  WebElement account;

@FindBy(xpath = "//h3[contains(text(),'Select a customer:')]")
public static WebElement selectcustomer;


@FindBy(xpath="//button[contains(text(),'Continue')]")
public static  WebElement continueBtn;
 
@FindBy(css="a[ng-repeat*='clients']")
public static  List<WebElement> customerList;
@FindBy(css="a[ng-repeat*='clients']")
public static WebElement firstcustomer;


@FindBy(css="a[ng-repeat*='accounts']")
public static  WebElement firstAcc;

@FindBy(css="a[ng-repeat*='accounts']")
public static  List<WebElement> accountList;


//******************************************************************

@FindBy(id="ContentPlaceHolder1_PassiveSignInButton")
public WebElement nxtButton;

@FindBy(id="txtEmail")
public WebElement UserName;

@FindBy(xpath="//*[@id='txtPassword-clone']")
public WebElement pwd;

@FindBy(xpath="//*[@id='txtPassword']")
public WebElement passwordEntry;

@FindBy(xpath = "//*[contains(text(),'Enter')]")
public WebElement enter;

@FindBy(id = "ctl00_MainSectionContent_Email")
public WebElement email;

@FindBy(id = "ctl00_MainSectionContent_Password")
public WebElement password;

@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
public WebElement signIn;

@FindBy(xpath = "//a[contains(text(),'Mercer (New York)                                                               ')]")
public List <WebElement> mercerNYStaging;

//***********************************************************************


	public ClientSelectionPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectAccounts(String accountName) throws InterruptedException {
		
		waitForElementToDisplay(firstcustomer);
		System.out.println("List of customers: " + customerList.size());
		outerloop:
		for(WebElement customer: customerList) 
		{
			clickElement(customer);
			waitForElementToDisplay(firstAcc);
			System.out.println("List of accounts: " +accountList.size());
			for(WebElement account : accountList) 
			{
				System.out.println("Account Name is : "+account.getText());
				if(account.getText().trim().contains(accountName)) 
				{
					System.out.println("Inside if loop");
					clickElement(account);
					clickElement(continueBtn);
					break outerloop;
					
				}
				else
				{
					System.out.println("Move to next account");
				}
				
			}
			
		}
		System.out.println("SUCCESS");
	}
	
	public void selectCustomer(String customerTxt) throws InterruptedException {
		//refreshpage();
		waitForElementToDisplay(getEleByXpathContains("a",customerTxt,driver));
		customer=getEleByXpathContains("a",customerTxt,driver);
		clickElement(customer);
		
	}
	
	//**********************************************************************
	
	public void selectAccount(String accountTXt) throws InterruptedException {
		account=getEleByXpathContains("a",accountTXt,driver);
		clickElement(account);
		
	}
	
	public void clickOnContinue() throws InterruptedException
	{
		clickElement(continueBtn);
		
	}
	
	 public void stagingLogin(String username, String passwordTxt) throws Exception {
		 
		 	clickElement(nxtButton);
			waitForElementToDisplay(UserName);
			setInput(UserName,username);
			clickElement(pwd);
			setInput(passwordEntry,passwordTxt);
			clickElement(enter);
	    	
	    	/*waitForElementToDisplay(email);
			email.sendKeys(username);
			waitForElementToDisplay(password);
			setInput(password,passwordTxt);
			//password.sendKeys(passwordTxt);
			clickElement(signIn);*/
	    	
	    }
	 
	 /*public void stagingLogin_2ndIteration(String username, String passwordTxt) throws Exception {
		 
		 	clickElement(nxtButton);
			waitForElementToDisplay(UserName);
			setInput(UserName,username);
			clickElement(pwd);
			setInput(passwordEntry,passwordTxt);
			clickElement(enter);
			
	 }*/
	
	//******************************************************************************
	
	
	
}

