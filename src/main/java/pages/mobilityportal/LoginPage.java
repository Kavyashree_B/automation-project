package pages.mobilityportal;

import static driverfactory.Driver.waitForElementToDisplay;

import java.util.List;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LoginPage {


	@FindBy(id = "ctl00_MainSectionContent_Email")
	public WebElement email;
	@FindBy(id="ContentPlaceHolder1_PassiveSignInButton")
	public WebElement nxtButton;
	
	@FindBy(id="txtEmail")
	public WebElement UserName;
	
	@FindBy(xpath="//*[@id='txtPassword-clone']")
	public WebElement pwd;
	
	@FindBy(xpath="//*[@id='txtPassword']")
	public WebElement passwordEntry;
	
	@FindBy(xpath = "//*[contains(text(),'Enter')]")
	public WebElement enter;

	@FindBy(xpath = "//span[contains(text(),'Logout')]")
	public WebElement logoutLnk;

	@FindBy(id = "ctl00_MainSectionContent_Password")
	public WebElement password;

	@FindBy(xpath="//*[@class='hidden-xs hidden-sm top-icons']//i[@class='top-avatar-icon top-avatar']")
	public WebElement myAccounts;

	@FindBy(xpath = "(//span[text()='Login'])[3]")
	public WebElement login;

	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public WebElement signIn;

	@FindBy(css = "h1.title-my-tools")
	public WebElement myToolsTxt;
	WebDriver driver;


	//********************************************************
	
	@FindBy(xpath = "//i[@class=\"top-avatar-icon top-avatar\"]")
	public List <WebElement> myAccountsList;
	
	@FindBy(xpath = "(//span[text()='My Account '])[1]")
	public  WebElement myAccount;

	//********************************************************

	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void loginViaPIT(String username, String passwordTxt) throws InterruptedException {
		waitForElementToDisplay(email);
		setInput(email,username);
		waitForElementToDisplay(password);
		setInput(password,passwordTxt);
		clickElement(signIn);
	}

	public  void login(String username, String passwordTxt) throws Exception {
		clickElement(myAccounts);
		Thread.sleep(8000);
		waitForElementToDisplay(login);
		login.click();
		Thread.sleep(8000);
		if((System.getProperty("env")).equalsIgnoreCase("prod") || (System.getProperty("env")).equalsIgnoreCase("production")  )
		{
			waitForElementToDisplay(email);
			setInput(email,username);
			waitForElementToDisplay(password);
			setInput(password,passwordTxt);
			clickElement(signIn);
		}
		else
		{
			clickElement(nxtButton);
			waitForElementToDisplay(UserName);
			setInput(UserName,username);
			clickElement(pwd);
			setInput(passwordEntry,passwordTxt);
			clickElement(enter);
		}
	}
}

