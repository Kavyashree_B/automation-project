package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserTypeMgmtPage {

	//UsertypeMgmt
	WebDriver driver;
	

	@FindBy(id="divUserTypeManagement")
	public WebElement UserTypeMgmt;

	@FindBy(xpath="//h4[contains(text(),'User Type Management')]")
	public WebElement userTypeHeader;
	
	public UserTypeMgmtPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	
}
