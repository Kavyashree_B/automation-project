package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getEleByCssContains;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class PlatformAdminPage {

	// UserManagement xpath
	@FindBy(id = "divUserManagement")
	public static WebElement UserMgmt;
	
	@FindBy(id = "divRoleManagement")
	public static WebElement roleMgt;
	
	@FindBy(css = "h3.app-title")
	public WebElement appheaderTxt;

	@FindBy(xpath = "//a[@href='/PlatformAdministration/v1/Home/SignOut']")
	public WebElement signOutLink;

	@FindBy(xpath = "//h1[contains(text(),'My Tools & Data')]")
	public WebElement dashboardHeader;

	
	WebDriver driver;

	public PlatformAdminPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clicksignOut() throws Exception {
		clickElement(signOutLink);
		waitForElementToDisplay(dashboardHeader);
	}

	public void navigateToMenuLnk(String lnkTxt) throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(getEleByCssContains("a","href",lnkTxt,driver));
	}

}
