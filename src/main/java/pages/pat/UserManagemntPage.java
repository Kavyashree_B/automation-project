package pages.pat;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UserManagemntPage {

	WebDriver driver;
	
	@FindBy(id="divUserManagement")
	public static WebElement UserMgmt;
	
	@FindBy(xpath="//a[contains(text(),' Compensation Localizer ')]")
	public WebElement CompLocalizer;

	@FindBy(xpath="//a[contains(text(),'All Other Applications')]")
	public WebElement allOtherApps;

	@FindBy(css = "a[href$='UserManagement_CL']")
	public WebElement CompLocalizerLnk;
	
	public UserManagemntPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}


}
