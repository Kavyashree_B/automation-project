package pages.pat;

import static driverfactory.Driver.*;
import static utilities.InitTests.props;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CalendarPage {
	
	WebDriver driver;
	String appName = "PAT";
	
	@FindBy(id="dpStartDate")
	public WebElement dateField;

	@FindBy(xpath="(//img[@class='ui-datepicker-trigger'])[1]")
	public WebElement calIcon;

	@FindBy(xpath="//select[@class='ui-datepicker-month']")
	public WebElement monthDropdown;

	@FindBy(xpath="//select[@class='ui-datepicker-year']")
	public WebElement yearDropdown;

	@FindBy(tagName = "td") 
	public List<WebElement> cols;

	@FindBy(tagName = "tr") 
	public List<WebElement> rows;

	@FindBy(id= "btnExport")
	public WebElement exportBtn;
	
	public CalendarPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void selectDateByJS() throws InterruptedException {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MMM/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now)); 
		LocalDate value = now.minusMonths(1).toLocalDate();
		System.out.println(dtf.format(value));
		String dateValue = dtf.format(value);
		String[] date = dateValue.split("/", 5);
		Calendar cal = Calendar.getInstance();
		int d = cal.get(Calendar.DATE);
//		System.out.println(" Date is : " +d);
		selEleByVisbleText(monthDropdown, date[1]);
		selEleByVisbleText(yearDropdown, date[2]);
		String dates = Integer.toString(d);

		for (WebElement cell: cols){
			
			if (cell.getText().equals(dates)){
				clickElementUsingJavaScript(driver, cell.findElement(By.linkText(dates)));
				break;
			}

		}
	}
	
	public void clickExport() throws InterruptedException {
		clickElement(exportBtn);
		Thread.sleep(5000);
	}
	
	
	 public boolean CheckFile() throws Exception // the name of the zip file which is obtained, is passed in this method
    {
		 String downloadPath = System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator
                 + "downloadFiles";
			
		 boolean flag = false;
		    File dir = new File(downloadPath);
		    File[] dir_contents = dir.listFiles();
		  	    
		    for (int i = 0; i < dir_contents.length; i++) {
		        if (dir_contents[i].getName().equals("CompLocReportResults.xlsx"))
		        	dir_contents[i].delete();
		            return true;
		            }

		    return flag;
    }
	 
	 
	 public void clickCalIcon() throws InterruptedException {
		 waitForElementToDisplay(calIcon);
		 clickElementUsingJavaScript(driver, calIcon);
		 
	 }
}
