package pages.pat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static driverfactory.Driver.clickElement;



public class HeaderPage {
  
 
   
    @FindBy(xpath = "//a[@href='/PlatformAdministration/v1/Home/SignOut']")
	public WebElement logout;




	public HeaderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);	
	}

	public void signOut() throws Exception{
		clickElement(logout);
		
	}

	

}
