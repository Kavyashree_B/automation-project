package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReleaseManagerPage {

	WebDriver driver;
	
	//Release Manager
	@FindBy(id="divReleaseManager")
	public WebElement RelManager;

	@FindBy(xpath="//h4[(normalize-space(' Release  Manager '))]")
	public WebElement RelManagerHeader;

	@FindBy(xpath="//a[contains(text(),'Tax Data Management')]")
	public WebElement TaxDataMgmt;

	@FindBy(xpath="//a[contains(text(),'Tax PDF Management')]")
	public WebElement TaxPDFMgmt;

	@FindBy(xpath="//a[contains(text(),'Tax Data - Bulk Management')]")
	public WebElement TaxDataBulkMgmt;
	
	public ReleaseManagerPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void clickRelManager() throws InterruptedException {
		clickElement(RelManager);
		waitForPageLoad(driver);
		waitForElementToDisplay(RelManagerHeader);
	}
}
