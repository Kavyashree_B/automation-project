package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContentManagementPage {

	WebDriver driver;
	//Content Managament
	@FindBy(id="divContentManager")
	public WebElement ContMgmt;

	@FindBy(xpath="//h4[contains(text(),'Content Management')]")
	public WebElement ContMgmtHeader;
	
	public ContentManagementPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickContMgmt() throws InterruptedException {
		clickElement(ContMgmt);
		waitForPageLoad(driver);
	}
}
