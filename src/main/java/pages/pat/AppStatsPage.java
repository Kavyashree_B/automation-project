package pages.pat;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AppStatsPage {

	WebDriver driver; 
	
	//Application Statistics
	@FindBy(id="divApplicationStatistics")
	public WebElement AppStats;

	@FindBy(xpath="//h4[contains(text(),'Application Statistics')]")
	public WebElement AppStatsHeader;

	@FindBy(xpath="//a[contains(text(),'Compensation Localizer Calculation Report')]")
	public WebElement CLCReport;
	
	@FindBy(xpath="//label[contains(text(),'View Calculation Report By:')]")
	public WebElement CLCReportLabel;
	public HeaderPage header;
	public AppStatsPage(WebDriver driver) {
		header=new HeaderPage(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void clickAppStats() throws InterruptedException {
	
			clickElement(AppStats);
			waitForPageLoad(driver);
		
		
	}
	
	public void clickCLCReport() throws InterruptedException {
		waitForElementToDisplay(CLCReport);
		clickElement(CLCReport);
	}
	
}
