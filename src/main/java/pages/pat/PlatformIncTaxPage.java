package pages.pat;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PlatformIncTaxPage {

	WebDriver driver;
	
	@FindBy(xpath = "//a[contains(text(),'Platform Administration')]")
	public WebElement patLink;
	
	@FindBy(xpath = "//a[@href='/PersonalIncomeTax/v1/Home/SignOut']")
	public WebElement signout;
	
	
	
	public PlatformIncTaxPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToPat() throws InterruptedException {
		
		clickElement(patLink);
	}
	
	public void signoutFromPIT() throws InterruptedException {
		clickElement(signout);
	}
}
