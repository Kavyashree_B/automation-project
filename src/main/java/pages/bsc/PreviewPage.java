package pages.bsc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class PreviewPage {
	@FindBy(id = "btnContinue")
	public static WebElement continueButton;

	@FindBy(css = "textarea[id='txtSubtitle']")
	public static WebElement textArea;
	@FindBy(xpath = "//h2[contains(text(),'Preview Calculation')]")
	public static WebElement previewText;
	
	
	@FindBy(css = "input[id='rdoFullReport']")
	public static WebElement fullReportRadioButton;
	
	
	WebDriver driver;
   
	public PreviewPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void previewContinueGHRM() throws InterruptedException {

		 
		waitForElementToDisplay(textArea);
		clickElement(textArea);
		Thread.sleep(3000);
		waitForElementToDisplay(continueButton);
		scrollToElement(driver, continueButton);
		clickElement(continueButton);
		
	}
	public void previewContinueICS() throws InterruptedException {

		 
		waitForElementToDisplay(textArea);
		clickElement(textArea);
		Thread.sleep(3000);
		waitForElementToDisplay(fullReportRadioButton);
		scrollToElement(driver, fullReportRadioButton);
		clickElement(fullReportRadioButton);
		waitForElementToDisplay(continueButton);
		scrollToElement(driver, continueButton);
		clickElement(continueButton);
		
	}
	
}
