package pages.bsc;

import java.util.List;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalculationStartPage {

	@FindBy(xpath = "//h1[contains(text(),'Balance Sheet Calculator')]")
	public static WebElement bscTitleText;

	@FindBy(css = "a[href='/BalanceSheet/v1/Home/SignOut']")
	public static WebElement returnToMobilityExchangeLink;

	@FindBy(xpath = "//span[contains(text(),' Balance Sheet and Cost Projection Calculators')]")
	public static WebElement welcomeText;


	@FindBy(css = "select[id='HomeLocation']")
	List<WebElement> homeLocationOption;

	@FindBy(css = "select[id='HomeLocation']")
	public static WebElement homeDropDownLink;

	@FindBy(xpath = "//select[contains(@data-bind,'HomeLocationList1')]")
	public static WebElement homeDropDownList1;

	@FindBy(css = "select[id='HostLocation']")
	List<WebElement> hostLocationOption;

	@FindBy(css = "select[id='HostLocation']")
	public static WebElement hostDropDownLink;

	@FindBy(xpath = "//select[contains(@data-bind,'HostLocationList1')]")
	public static WebElement hostDropDownList1;

	@FindBy(css = "button[id='submitbtnId']")
	public static WebElement runANewCalculationButton;

	@FindBy(css = "select[id='IndexType']")
	public static WebElement icsIndexType;

	@FindBy(xpath = "//h3[contains(text(),'Start a Calculation')]")
	public static WebElement startCalculationText;

	@FindBy(css = "input[id='rdoHomeLoc']")
	public static WebElement homeLocationRadioButton;

	@FindBy(css = "input[id='rdoHostLoc']")
	public static WebElement hostLocationRadioButton;


	@FindBy(css = "input[type='text']")
	public static WebElement selectCustomerTextbox;

	@FindBy(css = "a[class='btn btn-primary']")
	public static WebElement searchIcon;

	@FindBy(xpath = "//h3[contains(text(),'Start a Calculation')]")
	public static List <WebElement> startCalProd;

	@FindBy(xpath = "//option[contains(text(),'Chile, Santiago')]")
	public static List <WebElement> OptionOneProd;

	WebDriver driver;


	public CalculationStartPage(WebDriver driver) {
		this.driver = driver;

		PageFactory.initElements(driver, this);
	}

	public void clientSelectionSearch(String Client) throws InterruptedException {
		waitForElementToDisplay(selectCustomerTextbox);
		clickElement(selectCustomerTextbox);
		setInput(selectCustomerTextbox, Client);
		clickElement(searchIcon);
	}

	public void startCalculationGHRM(String country1, String country2) throws InterruptedException {
		waitForElementToDisplay(homeDropDownLink);
		clickElement(homeDropDownLink);
		waitForElementToDisplay(homeDropDownLink);
		selEleByVisbleText(homeDropDownLink, country1);
		selEleByVisbleText(hostDropDownLink, country2);
		clickElement(runANewCalculationButton);


	}

	public void startCalculationICS() throws InterruptedException {
		waitForElementToDisplay(icsIndexType);
		selEleByVisbleText(icsIndexType, "Standard");
		clickElement(homeLocationRadioButton);
		/*//clickElement(homeDropDownLink);
		selEleByVisbleText(homeDropDownLink, country2);
		//clickElement(homeLocationRadioButton);
		//clickElement(icsIndexType);
		scrollToElement(driver, hostDropDownLink);
		waitForElementToDisplay(hostDropDownLink);
		selEleByVisbleText(hostDropDownLink, country3);*/
		clickElement(runANewCalculationButton);

	}

	public void selectHomeLocation(String country) {
		// TODO Auto-generated method stub
		for (WebElement e : homeLocationOption) {
			if (e.getText().contains(country)) {
				e.click();
				break;
			}
		}
	}

	public void selectHostLocation(String country1) {
		// TODO Auto-generated method stub
		for (WebElement e : hostLocationOption) {
			if (e.getText().contains(country1)) {
				e.click();
				break;
			}
		}

	}

}
