package pages.bsc;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashBoardPage {

	@FindBy(xpath = "//a[@class='sapphire ng-binding' and contains(text(),'Balance Sheet Calculator (QA)')]")
	public static WebElement balanceSheetCalculatorQA;

	@FindBy(xpath = "//h4[contains(text(),'International Compensation Calculators')]")
	public static WebElement internationalCompensationCalculatorText;

	@FindBy(xpath = "//a[contains(text(),'11325 Multi-National Pay with Americans Abroad - Catherine Bruning ')]")
	public static WebElement link11325;

	@FindBy(xpath = "//a[@class='btn btn-link switch-client']")
	public static WebElement switchClient;
	
	 @FindBy(xpath = "(//a[contains(text(),'Balance Sheet Calculator (QA)')])[4]")
	    public static WebElement bsc_link;
	 
	 @FindBy(xpath = "//a[contains(text(),'Balance Sheet Calculator (QA)')]")
	    public static List <WebElement> bsclinkList;

	 @FindBy(xpath = "//*[@id=\"dnn_ctr490_view_pnlMain\"]/div/div/ui-view/div/div/div[10]/ul/li[2]/a")
	    public static WebElement bsclinkstage;
	WebDriver driver;

	public DashBoardPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void balanceSheetSelection() throws InterruptedException {

		scrollToElement(driver, internationalCompensationCalculatorText);
		waitForElementToDisplay(internationalCompensationCalculatorText);
		clickElementUsingJavaScript(driver, balanceSheetCalculatorQA);

	}
	
	public void balanceSheetSelectionStaging() throws InterruptedException {

		scrollToElement(driver, bsc_link);
		waitForElementToDisplay(bsc_link);
		clickElementUsingJavaScript(driver, bsc_link);

	}

	public void switchClient() throws InterruptedException {

		Thread.sleep(2000);
		waitForElementToDisplay(switchClient);
		clickElement(switchClient);
	}

}
