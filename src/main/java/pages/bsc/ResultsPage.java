package pages.bsc;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import static driverfactory.Driver.*;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.thoughtworks.selenium.webdriven.commands.GetText;

public class ResultsPage {
	
	

	@FindBy(xpath = "//h2[contains(text(),'Balance Sheet Calculation Summary')]")
	public static WebElement calculationSummaryText;

	@FindBy(xpath = "//a[contains(text(),'Return to Mobility Exchange')]")
	public static WebElement returnToMobility;

	@FindBy(xpath = "//label[contains(@data-bind,'TotalHomeCountryCompensationHome')]")
	public static WebElement countryCompense;

	@FindBy(xpath="(//label[contains(@class,'control-label light ThousandSeparator')])[9]")
	public static WebElement personalIncomeValue;

	//@FindBy(xpath = "//label[contains(@data-bind,'EstimatedHomeSocialSecurityHome')]")
	@FindBy(xpath="(//label[contains(@class,'control-label light ThousandSeparator')])[11]")
	public static WebElement socialSecurityValue;

	@FindBy(xpath = "(//label[contains(@data-bind,'TotalHomeCountryCompensationHome')])[1]")
	public static WebElement icsCountryTax;

	@FindBy(xpath = "(//label[contains(@data-bind,'HostEstimatedPersonalIncomeTaxHome')])[1]")
	public static WebElement hostIncomeIcs;

	@FindBy(xpath = "(//label[contains(@data-bind,'EstimatedHomeSocialSecurityHome')])[1]")
	public static WebElement socialSecurityIcs;

	@FindBy(xpath = "(//label[contains(@data-bind,'TotalCompensationYearlyAmountHome')])[1]")
	public static WebElement netCompensIcs;

	@FindBy(xpath = "//button[contains(text(),'Export')]")
	public static WebElement exportButton;

	@FindBy(xpath = "//a[contains(text(),'EXCEL')]")
	public static WebElement excelDownload;

	@FindBy(xpath = "//a[contains(text(),'PDF')]")
	public static WebElement pdfDownload;

	WebDriver driver;
	public ResultsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void returnToMobility() throws InterruptedException {

		waitForElementToDisplay(returnToMobility);
		clickElement(returnToMobility);

	}

	public void compareValues() {

		try {
			System.out.println(countryCompense.getText());

			Integer i = Integer.parseInt(countryCompense.getText());
			System.out.println(i);
		} catch (NumberFormatException ex) { // handle your exception
			System.out.println("not a number");
		}

	}

	public Boolean checkValues(String s) {

		String rep = s.replaceAll("[(),]", "");
		System.out.println(rep);
		// the String to int conversion happens here
		int i = Integer.parseInt(rep.trim());

		// print out the value after the conversion

		if (i > 0) {
			System.out.println("Value is greater than 0");
			return true;
		}

		return false;

	}

	public void excelDownload() throws InterruptedException {

		clickElement(exportButton);
		clickElement(excelDownload);
	}

	public void pdfDownload() throws InterruptedException {

		clickElement(exportButton);
		clickElement(pdfDownload);
	}

	public void getExcelNames() throws EncryptedDocumentException, InvalidFormatException, IOException {
		File myFile = getLatestFilefromDir();
		Workbook wb = WorkbookFactory.create(myFile);

		List<String> sheetNames = new ArrayList<String>();

		for (int i = 0; i < wb.getNumberOfSheets(); i++) {
			System.out.println(wb.getSheetName(i));
			sheetNames.add(wb.getSheetName(i));
		}

		// System.out.println(sheetNames);

	}

	public File getLatestFilefromDir() {
		// String dirPath
		File dir = new File("C:/Users/logesh-Chandrasekar/Downloads");
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}

		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		System.out.println(lastModifiedFile);
		return lastModifiedFile;
		

	}

	/*
	 * public void VerifyExcelDownloaded() {
	 * 
	 * String downloadPath="C:/Users/logesh-Chandrasekar/Downloads"; File
	 * getLatestFile = getLatestFilefromDir(downloadPath); String fileName =
	 * getLatestFile.getName(); System.out.println(fileName); //
	 * Assert.assertTrue(fileName.equals("mailmerge.xls"),
	 * "Downloaded file name is not matching with expected file name"); }
	 */

	public void waitUntilFileToDownload(String folderLocation) throws InterruptedException {
		File directory = new File(folderLocation);
		boolean downloadinFilePresence = false;
		File[] filesList = null;
		LOOP: while (true) {
			filesList = directory.listFiles();
			for (File file : filesList) {
				downloadinFilePresence = file.getName().contains("Balance Sheet Calculation_");
			}
			if (downloadinFilePresence) {
				for (; downloadinFilePresence;) {
					
					continue LOOP;
				}
			} else {
				break;
			}
		}
	}

}
