package pages.bsc;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class InputsPage {

	@FindBy(css = "input[id='salary']")
	public static WebElement annualBasepayAmountField;

	@FindBy(css = "select[id='MaritalHome']")
	public static WebElement maritalStatusAtHome;

	@FindBy(css = "select[id='MaritalHost']")
	public static WebElement maritalStatusAtHost;

	@FindBy(css = "a[id='iconExpandCollapseCOL']")
	public static WebElement costOfLivingIcon;

	@FindBy(css = "select[id='COLSurveyDate']")
	public static WebElement surveyDate;

	@FindBy(css = "a[id='iconExpandCollapseHousing']")
	public static WebElement hostLocationHousingIcon;

	@FindBy(css = "select[id='HousingData']")
	public static WebElement housingDataValue;

	@FindBy(css = "select[id='HousingSurveyDates']")
	public static WebElement housingSurveyDates;

	@FindBy(css = "a[href='#TaxHostGrossUpContent']")
	public static WebElement taxHostGrossUpIcon;

	@FindBy(xpath = "(//div[@class='col-sm-5 custom-style-inputs']/input[@type='checkbox'])[3]")
	public static WebElement taxHostGrossUpCheckBox;

	@FindBy(id = "btnContinue")
	public static WebElement continueButton;

	@FindBy(xpath = "//h2[contains(text(),'Calculation Inputs ')]")
	public static WebElement calculationInputsText;

	
	
	WebDriver driver;

	public InputsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void inputSelectionGHRM() throws InterruptedException {

		waitForElementToDisplay(calculationInputsText);
		scrollToElement(driver, annualBasepayAmountField);
		setInput(annualBasepayAmountField, "150000");
		scrollToElement(driver, maritalStatusAtHome);
		selEleByVisbleText(maritalStatusAtHome, "Married with 2 children");
		selEleByVisbleText(maritalStatusAtHost, "Married with 2 children");
		clickElement(costOfLivingIcon);
		selEleByVisbleText(surveyDate, "March 2018");
		waitForPageLoad(driver);
		delay(8000);
		waitForElementToDisplay(hostLocationHousingIcon);
		scrollToElement(driver, hostLocationHousingIcon);
		clickElement(hostLocationHousingIcon);
		selEleByVisbleText(housingDataValue, "Host location housing costs");
		selEleByVisbleText(housingSurveyDates, "March 2018");
		scrollToElement(driver, taxHostGrossUpIcon);
		clickElement(taxHostGrossUpIcon);
		clickElement(taxHostGrossUpCheckBox);
		scrollToElement(driver, continueButton);
		clickElement(continueButton);

	}

	public void inputSelectionICS() throws InterruptedException {

		waitForElementToDisplay(annualBasepayAmountField);
		setInput(annualBasepayAmountField, "1500000");
		scrollToElement(driver, maritalStatusAtHome);
		selEleByVisbleText(maritalStatusAtHome, "Married with 1 child");
		selEleByVisbleText(maritalStatusAtHost, "Married with 1 child");
		scrollToElement(driver, hostLocationHousingIcon);
		clickElement(hostLocationHousingIcon);
		selEleByVisbleText(housingDataValue, "Expatriate accommodation costs by income level and family size");
		selEleByVisbleText(housingSurveyDates, "March 2018");
		scrollToElement(driver, taxHostGrossUpIcon);
		clickElement(taxHostGrossUpIcon);
		clickElement(taxHostGrossUpCheckBox);
		scrollToElement(driver, continueButton);
		clickElement(continueButton);

	}

}
