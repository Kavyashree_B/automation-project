package pages.imercer;

import static driverfactory.Driver.waitForElementToDisplay;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(id="ContentPlaceHolder1_PassiveSignInButton")
	public WebElement nxtButton;

	@FindBy(id="txtEmail")
	public WebElement UserName;

	@FindBy(xpath="//*[@id='txtPassword-clone']")
	public WebElement pwd;

	@FindBy(xpath="//*[@id='txtPassword']")
	public WebElement passwordEntry;

	@FindBy(xpath = "//*[contains(text(),'Enter')]")
	public WebElement enter;
	
	@FindBy(id = "ctl00_MainSectionContent_Email")
	public WebElement email;

	@FindBy(xpath = "//span[contains(text(),'Logout')]")
	public WebElement logoutLnk;

	@FindBy(id = "ctl00_MainSectionContent_Password")
	public WebElement password;

	// a[@class = 'btn action my-account-lnk dropdown-toggle']
	@FindBy(xpath = "//span[text()='My Account ']")
	public WebElement myAccounts;

	@FindBy(xpath = "//span[text()='Login']")
	public WebElement login;

	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public WebElement signIn;
		WebDriver driver;
		public LoginPage(WebDriver driver) {
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}

		public void login(String username, String passwordTxt) throws InterruptedException {
			
			clickElement(nxtButton);
			waitForElementToDisplay(UserName);
			setInput(UserName,username);
			clickElement(pwd);
			setInput(passwordEntry,passwordTxt);
			clickElement(enter);

			/*waitForElementToDisplay(email);
			setInput(email,username);
			waitForElementToDisplay(password);
			setInput(password,passwordTxt);
			clickElement(signIn);*/

		}
		
	
	}

