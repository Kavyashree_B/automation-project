package pages.ics;

import static driverfactory.Driver.switchToWindow;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class HomePage {

	@FindBy(xpath = "//a[@href=\"/icsweb/MyAccountShow.do\"]")
	public  WebElement welcomeimatest;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]")
	public  WebElement knowledgeCenter;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]/../..//div[@class='body-pad']")
	public  WebElement knowledgeCenterpdf_List;
	
	@FindBy(xpath="//a[contains(text(),'Balance Sheet Booklet')]")
	public  List<WebElement> productpdf_List;
	public HeaderPage headerpage;

	
	public HomePage(WebDriver driver) {
		headerpage=new HeaderPage(driver);

		PageFactory.initElements(driver, this);
	}
	
	public void openICS(WebDriver driver) {
		switchToWindow("Compensation Tables",driver);
		
	}
		
}
