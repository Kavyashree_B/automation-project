package pages.ics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class Products {
    @FindBy(css = "div#productsTabView li:nth-child(3) em")
    public WebElement housingEduTab;
    
    @FindBy(xpath = "//b[contains(text(),'Purchased Products')]")
    public WebElement purchasedProdTxt;
    
    
    
    @FindBy(xpath = "//b[contains(text(),'Available for Purchase')]")
    public WebElement availForPurchaseTxt;
    		
    @FindBy(css = "div#productsTabView a[href*='tab0']")
    public WebElement selectedTab;
    
    @FindBy(css = "form[id='utilityReportsForm'] div[class*='yui-layout-unit']:nth-child(1) b")
    public WebElement utilityRepPurchaseTxt;
    
    @FindBy(css = "form[id='utilityReportsForm'] div[class*='yui-layout-unit']:nth-child(2) b")
    public WebElement utilityRepAvailTxt;
    
    
    @FindBy(css = "form[id='housingPagesForm'] div[class*='yui-layout-unit']:nth-child(1) b")
    public WebElement houseEduRepPurchaseTxt;
    
    @FindBy(css = "form[id='housingPagesForm'] div[class*='yui-layout-unit']:nth-child(2) b")
    public WebElement houseEduRepAvailTxt;
    
    @FindBy(css = "form#hardshipReportsForm input[id='excelButton']")
    public WebElement mercerRecommLocPremBtn;
    
    @FindBy(css = "form[id='hardshipReportsForm'] div.hd-l>b")
    public WebElement locHardAvailTxt;
    
    
    @FindBy(css = "div#media>h2")
    public WebElement mercerPassportInfoTxt;
    @FindBy(xpath = "//a[(text()='Download Brochure')]")
    public WebElement downloadBrochureLnk;
    
    
    @FindBy(xpath = "//em[contains(text(),'Mercer Global Insights')]")
    public WebElement mercerGlobalInsightTab;
    
public Products(WebDriver driver) {
	PageFactory.initElements(driver, this);
}

}
