package pages.ics;



import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class FooterPage {
	
	
			
			@FindBy(css = "a[id*='Terms']")
			public  WebElement termsLnk;
			
			@FindBy(css = "a[id*='Privacy']")
			public  WebElement privacyLnk;
			
			@FindBy(css = "p.footerText")
			public  WebElement mercerLLTxt;
			

	WebDriver driver;
	public FooterPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickOnTermsLnk() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(termsLnk);
	}
	
	public void clickOnPrivacyLnk() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(privacyLnk);
	}
	
}
