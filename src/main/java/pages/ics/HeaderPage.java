package pages.ics;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HeaderPage {
	
	@FindBy(xpath = "//a[@title='Compensation Tables']")
	public static WebElement compensationTableLink;
	
	@FindBy(xpath = "//a[text()='Add Table']")
	public static WebElement addtableLink;
	
	@FindBy(xpath = "(//a[text()='Home'])[1]")
	public static WebElement homelink;
	
			
			@FindBy(id = "welcomeText")
			public  WebElement welcomeUserTxt;
			
			@FindBy(id = "u_nav_admin")
			public  WebElement adminLnk;
			@FindBy(id = "u_nav_my_acct")
			public  WebElement accLnk;
			@FindBy(id = "u_nav_contact")
			public  WebElement contactLnk;
			@FindBy(id = "u_nav_logout")
			public  WebElement logoutLnk;
			@FindBy(css = "div#curve>ul")
			public  WebElement subscriptionTxt;
			
			@FindBy(id = "p_nav_home")
			public  WebElement homeTabLnk;
			@FindBy(id = "p_nav_my_ics")
			public  WebElement compTab;
			@FindBy(id = "p_nav_calculator")
			public  WebElement calcTabLnk;
			@FindBy(id = "p_nav_products")
			public  WebElement prodTab;
			
			@FindBy(id = "p_nav_loc")
			public  WebElement locTab;
			
			
			@FindBy(linkText = "Current Tables")
			public  WebElement currTableLnk;
			@FindBy(linkText = "One-Time Tables")
			public  WebElement oneTimeTableLnk;
			@FindBy(linkText = "Inactive Tables")
			public  WebElement inactiveTableLnk;
			@FindBy(linkText = "Add Table")
			public  WebElement addTableLnk;
			
			
	@FindBy(xpath = "//a[@href=\"/icsweb/MyAccountShow.do\"]")
	public  WebElement welcomeimatest;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]")
	public  WebElement knowledgeCenter;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]/../..//div[@class='body-pad']")
	public  WebElement knowledgeCenterpdf_List;
	
	@FindBy(xpath="//a[contains(text(),'Balance Sheet Booklet')]")
	public  List<WebElement> productpdf_List;
	
	

			@FindBy(css = "div#p_nav_products-menu a[href*='HomeProfilesShow']")
	public  WebElement homeCountrySubMenuLnk;
			
			@FindBy(css = "div#p_nav_products-menu a[href*='UtilityReports']")
			public  WebElement utilityReportsSubMenuLnk;
			
			
			@FindBy(css = "div#p_nav_products-menu a[href*='HousingPagesShow']")
			public  WebElement housingAndEducationSubMenuLnk;
			
			@FindBy(css = "div#p_nav_products-menu a[href*='HardshipReportsShow']")
			public  WebElement locationEvalSubMenuLnk;
			
			
			@FindBy(css = "div#p_nav_products-menu a[href*='CustomReportsShow']")
			public  WebElement customReportsShowlSubMenuLnk;
			
			
			@FindBy(css = "div#p_nav_products-menu a[href*='MercerPassport']")
			public  WebElement mercerPassportSubMenuLnk;
			
			@FindBy(css = "	div#p_nav_products-menu a[href*='MercerGlobalInsights']")
			public  WebElement mercerGlobalInsightsSubMenuLnk;
			
			@FindBy(css = "a[title*='Data by Location']")
			public  WebElement dataByLocTab;
			
			@FindBy(css = "a[href*='HostLocation']")
			public  WebElement hostLocSubMenulnk;
			@FindBy(css = "a[href*='HomeLocation']")
			public  WebElement homeLocationSubMenulnk;
			
			@FindBy(css = "a[href*='GlobalMap']")
			public  WebElement globalMapSubMenulnk;
			@FindBy(linkText = "Company Accounts")
			public  WebElement companyAccounts;
			@FindBy(linkText = "Manage Subscriptions")
			public  WebElement manageSubscriptions;
			
			@FindBy(linkText = "Order History")
			public  WebElement orderHistory;
			
			@FindBy(linkText = "Contacts And Roles")
			public  WebElement contactsAndRoles;
			
			@FindBy(linkText = "Products Maintenance")
			public  WebElement productsMaintenance;
			
			@FindBy(linkText = "Manage Maps")
			public  WebElement manageMaps;
			
			@FindBy(linkText = "Manage Logging")
			public  WebElement manageLogging;
			
			

	WebDriver driver;
	public HeaderPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	 public void clickHomeLink(WebElement headerLink) throws InterruptedException {
		 clickElement(headerLink);
	 }

	
	public void navigateToCompLnk(String lnkTxt) throws InterruptedException {
		waitForElementToDisplay(compensationTableLink);
		hoverOverElement(compensationTableLink, driver);
		clickElement(getEleByXpathContains("a",lnkTxt,driver));
	 }
	
	public void navigateToHome() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(homeTabLnk);
	}
	public void navigateToCalMenu(String lnkTxt) throws InterruptedException {
		waitForElementToDisplay(calcTabLnk);
		hoverOverElement(calcTabLnk, driver);
		clickElement(getEleByXpathContains("a",lnkTxt,driver));
	 }
	public void navigateToProdMenu(String lnkTxt) throws InterruptedException {
		waitForElementToDisplay(prodTab);
		hoverOverElement(prodTab, driver);
		clickElement(getEleByXpathContains("a",lnkTxt,driver));
	 }

	public void navigateToDataMenu(String lnkTxt) throws InterruptedException {
		waitForElementToDisplay(dataByLocTab);
		hoverOverElement(dataByLocTab, driver);
		clickElement(getEleByXpathContains("a",lnkTxt,driver));
	 }
	public void navigateToMenu(String lnkText) throws InterruptedException
	{
		clickElement(getEleByLinkText(lnkText, driver));
	
	}

	public void navigateToAdminMenu(String subMenuLnk) throws InterruptedException {
		waitForElementToDisplay(adminLnk);
		hoverOverElement(adminLnk, driver);
		clickElement(getEleByXpathContains("a",subMenuLnk,driver));		
	}
	
}
