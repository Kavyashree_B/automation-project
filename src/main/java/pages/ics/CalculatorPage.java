package pages.ics;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalculatorPage 
{
	@FindBy(css = "div[id*='myCalcTabView']>ul a")
	public WebElement quickCalcTab;
	WebDriver driver;
	//******************************TestCase: 14************************************//
	
	public CalculatorPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
}