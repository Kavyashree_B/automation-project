package pages.ics;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ICSHomePage {
	
	@FindBy(xpath = "(//div[@class='title'])[1]")
	public  WebElement upcomingEventsTxt;
	
	@FindBy(id = "welcomeText")
	public  WebElement welcomeTxt;
	@FindBy(xpath = "(//div[@class='title'])[3]")
	public  WebElement productsTxt;
	@FindBy(xpath = "(//div[@class='title'])[4]")
	public  WebElement knowledgCenter;
	
	@FindBy(linkText = "Current events")
	public  WebElement currEventsLnk;
	
	@FindBy(linkText = "Webcasts")
	public  WebElement webcastsLnk;
	@FindBy(linkText = "Balance Sheet")
	public  WebElement balSheetLnk;
	@FindBy(linkText = "New Calculator Webcast")
	public  WebElement newCalculatoLnk;
	

	
	
	public HeaderPage headerpage;
	public FooterPage footerPage;
	public ICSHomePage(WebDriver driver) {
		headerpage=new HeaderPage(driver);
		footerPage=new FooterPage(driver);
		PageFactory.initElements(driver, this);
	}
	
	
}
