package pages.ics;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.selEleByIndex;
public class BalanceSheet {
	
	
	
	@FindBy(css = "input[value*='Export']")  
	public  WebElement exportBtn;
	@FindBy(css = "input[value*='Print']")
	public  WebElement printBtn;
	
	@FindBy(css = "input[value*='Show Tables']")
	public  WebElement showTables;
	
	@FindBy(id = "oneTimeTablesfrom")
	public  WebElement fromDate;
	@FindBy(id = "oneTimeTablesto")
	public  WebElement toDate;
	
	//div Housing Information
	//td  General Information
	  //div Tax Information
	//div Standard Premiums / Allowances / Incentives
	//calculateButton
	//resetButton
	
	
	@FindBy(css = "div[id*='balanceSheetCalcTables'] tr>th span")
	public  List<WebElement> tableHeaders;
			@FindBy(xpath = "(//a[contains(@href,'balanceSheetCalcTables')]//div)[3]")
			public  WebElement tabInfoIconLnk;
	@FindBy(xpath = "(//div[@class='sectionHeader'])[1]")
	public  WebElement generalInfo;
	
	@FindBy(xpath = "(//div[@class='sectionHeader'])[2]")
	public  WebElement taxInfo;
	
	@FindBy(xpath = "(//div[@class='sectionHeader'])[3]")
	public  WebElement housingInfo;
	
	@FindBy(xpath = "(//div[@class='sectionHeader'])[4]")
	public  WebElement standardPremiums;
	
	@FindBy(css = "input[value='Reset']")
	public  WebElement resetBtn;
	
	
	@FindBy(id = "doneButton")
	public  WebElement doneBtn;
	
	@FindBy(id = "modifyButton")
	public  WebElement modifyButton;
	
	@FindBy(id = "compensation")
	public  WebElement compensationInput;
	@FindBy(id = "exchangeRate")
	public  WebElement selExchangeRate;

	@FindBy(id = "customFX")
	public  WebElement exchangeRateInput;
	@FindBy(id = "calculateButton")
	public  WebElement calculateButton;
		
	@FindBy(id = "basicSalary")
	public  WebElement basicSalInput;
	
	@FindBy(id = "hypoTaxTypeBSC")
	public  WebElement hypoTax;
	
	@FindBy(css = "form#balanceSheetScheduleCalculatorForm h1")
	public  WebElement balSheetSchedulTxt;
	
	
	
	public BalanceSheet(WebDriver driver) {  
		PageFactory.initElements(driver, this);
	}


	public void showTables() throws InterruptedException
	{
	clickElement(showTables);
	}
	public void clickTableInfo() throws InterruptedException
	{
	clickElement(tabInfoIconLnk);
	}
	
	public void doCalculate() throws InterruptedException
	{
		setInput(basicSalInput,"50000");
		selEleByIndex(hypoTax,1);
		Thread.sleep(4000);
		clickElement(calculateButton);
	}
	
	public void clickDone() throws InterruptedException
	{
	clickElement(doneBtn);
	}


	public void clickOnExport() throws InterruptedException {
		clickElement(exportBtn);
		
	}


	
	
}
	
	


