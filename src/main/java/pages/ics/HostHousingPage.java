package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.selEleByIndex;

public class HostHousingPage {
	
	

	
	@FindBy(linkText = "Purchase New Location")
	public  WebElement purchaseNewLocationsLnk;
	
	
	@FindBy(id = "calculateButton")
	public  WebElement calculateButton;
	
	@FindBy(id = "countryId")
	public  WebElement selCountry;
	
	@FindBy(id = "locationId")
	public  WebElement selLocationId;
	
	@FindBy(id = "effectiveDate")
	public  WebElement selEffectiveDate;
	@FindBy(id = "costCategoryId")
	public  WebElement selCostCategoryId;
	@FindBy(id = "propertyTypeId")
	public  WebElement selPropertyTypeId;
	@FindBy(id = "furnishingsId")
	public  WebElement selFurnishingsId;
	@FindBy(id = "noOfBedRooms")
	public  WebElement selNoOfBedRooms;
	
	@FindBy(css = "div[class*='yui-u first'] div[class*='sectionHeader']:nth-child(1)")
	public  WebElement propertyDescript;
	
	@FindBy(xpath = "//b[contains(text(),'Housing Survey Date')]")
	public  WebElement housingSurveyDate;
	
	
	@FindBy(id = "printButton")
	public  WebElement printBtn;
	
	@FindBy(id = "doneButton")
	public  WebElement doneBtn;
	WebDriver driver;

	public HostHousingPage(WebDriver driver) {  
		this.driver=driver;

		PageFactory.initElements(driver, this);
	}



	

	public void clickOnDone() throws InterruptedException
	{
	clickElement(doneBtn);
	}

	public void calculate(String country,String city,String date,String category,String type,String furnish,String noOfRooms) throws InterruptedException
	{
		selEleByVisbleText(selCountry,country);
		selEleByVisbleText(selLocationId,city);
		selEleByIndex(selEffectiveDate,1);
		selEleByVisbleText(selCostCategoryId,category);
		selEleByIndex(selPropertyTypeId,1);
		selEleByVisbleText(selFurnishingsId,furnish);
		selEleByIndex(selNoOfBedRooms,1);	
		clickElement(calculateButton);

	}





	public void clickOnPurchaseLoc() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(purchaseNewLocationsLnk);

	}
	
	
}
	
	


