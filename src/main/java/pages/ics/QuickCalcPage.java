package pages.ics;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.selEleByIndex;
public class QuickCalcPage {
	
	
	
	@FindBy(css = "input[value*='Export']")  
	public  WebElement exportBtn;
	@FindBy(css = "input[value*='Print']")
	public  WebElement printBtn;
	
	@FindBy(css = "input[value*='Show Tables']")
	public  WebElement showTables;
	
	@FindBy(id = "oneTimeTablesfrom")
	public  WebElement fromDate;
	@FindBy(id = "oneTimeTablesto")
	public  WebElement toDate;
	
	
	
	@FindBy(css = "div[id*='dataTablequickCalcTables'] tr>th span")
	public  List<WebElement> tableHeaders;
	
	
	
			@FindBy(xpath = "(//a[contains(@href,'quickCalcTables')]//div)[1]")
			public  WebElement tabInfoIconLnk;
	@FindBy(xpath = "//h2[contains(text(),'Calculated Results')]")
	public  WebElement calcResultTxt;
	
	
	@FindBy(id = "doneButton")
	public  WebElement doneBtn;
	
	@FindBy(id = "compensation")
	public  WebElement compensationInput;
	@FindBy(id = "exchangeRate")
	public  WebElement selExchangeRate;

	@FindBy(id = "customFX")
	public  WebElement exchangeRateInput;
	@FindBy(id = "calculateButton")
	public  WebElement calculateButton;
		
	public QuickCalcPage(WebDriver driver) {  
		PageFactory.initElements(driver, this);
	}


	public void showTables() throws InterruptedException
	{
	clickElement(showTables);
	}
	public void clickTableInfo() throws InterruptedException
	{
	clickElement(tabInfoIconLnk);
	}
	
	public void doCalculate() throws InterruptedException
	{
		setInput(compensationInput,"50000");
		selEleByIndex(selExchangeRate,1);
		setInput(compensationInput,"50");
		clickElement(calculateButton);
	}
	
	public void clickDone() throws InterruptedException
	{
	clickElement(doneBtn);
	}


	
	
}
	
	


