package pages.ics;


import java.util.List;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LoginPage {


	@FindBy(id = "j_username")
	public WebElement email;

	@FindBy(xpath = "//span[contains(text(),'Logout')]")
	public WebElement logoutLnk;

	@FindBy(id = "j_password")
	public WebElement password;

	@FindBy(xpath = "//span[text()='My Account ']")
	public WebElement myAccounts;

	

	@FindBy(id = "buttonAction")
	public WebElement signIn;

	@FindBy(css = "h1.title-my-tools")
	public WebElement myToolsTxt;
	WebDriver driver;


	//********************************************************
	@FindBy(xpath = "//span[text()='My Account ']")
	public List <WebElement> myAccountsList;
	
	@FindBy(xpath = "(//span[text()='My Account '])[1]")
	public  WebElement myAccount;

	//********************************************************

	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	
	public void login(String username, String passwordTxt) throws Exception {
		setInput(email,username);
		setInput(password,passwordTxt);
		clickElement(signIn);
	}
}

