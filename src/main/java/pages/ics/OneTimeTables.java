package pages.ics;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
public class OneTimeTables {
	
	
	
	@FindBy(css = "input[value*='Export']")  
	public  WebElement exportBtn;
	@FindBy(css = "input[value*='Print']")
	public  WebElement printBtn;
	
	@FindBy(css = "input[value*='Show Tables']")
	public  WebElement showTables;
	
	@FindBy(id = "oneTimeTablesfrom")
	public  WebElement fromDate;
	@FindBy(id = "oneTimeTablesto")
	public  WebElement toDate;
	
	@FindBy(xpath = "(//a[contains(@href,'oneTimeTables')]//div)[1]")
	public  WebElement tabInfoIconLnk;
	
	
			
	@FindBy(css = "div[id*='dataTableoneTimeTables'] tr>th span")
	public  List<WebElement> tableHeaders;
	
	@FindBy(css = "div[id*='history'] thead tr>th span")
	public  List<WebElement> historyHeaders;
	
	@FindBy(xpath = "//span[contains(text(),'Select')]")
	public  WebElement selectAllTxt;
	
	@FindBy(css = "div[id*='compensationTableDetailTabView'] a[href*='tab0']")
	public  WebElement historyLnk;
	
	@FindBy(css = "div[id*='history'] thead tr>th span")
	public  WebElement historyHeader;
	
	public OneTimeTables(WebDriver driver) {  
		PageFactory.initElements(driver, this);
	}
	public void clickTableInfo() throws InterruptedException
	{
	clickElement(tabInfoIconLnk);
	}

	public void showTables() throws InterruptedException
	{
	clickElement(showTables);
	}
}
	
	


