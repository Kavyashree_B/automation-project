package pages.ics;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getEleByXpathContains;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.selEleByIndex;
public class PerDiemPage {
	
	

	
	@FindBy(xpath = "(//div[@id='stpdCalcTabView']//em)[1]")
	public  WebElement currPerDiemLocTab;
	
	
	@FindBy(xpath = "(//div[@id='stpdCalcTabView']//em)[2]")
	public  WebElement addperDiemLocTab;
	
	@FindBy(xpath = "(//div[@id='stpdCalcTabView']//em)[3]")
	public  WebElement createNewPerDiemTab;
	
	@FindBy(id = "currentPerDiemTableUpdateButton")
	public  WebElement runPerDiem;
	
	@FindBy(css = "div[id*='yui-dt0-th-selected-liner'] span")
	public  WebElement selectAllTxt;
	
	@FindBy(css = "a[href*='hostCountry']")
	public  WebElement hostCountryCol;
	
	@FindBy(css = "a[href*='hostLocation']")
	public  WebElement hostLocCol;
	
	@FindBy(css = "a[href*='OrderDate']")
	public  WebElement orderDateCol;
	
	
	@FindBy(css = "	a[href*='PricingDate']")
	public  WebElement priceDateCol;
	
	@FindBy(css = "a[href*='purchaseUpdate']")
	public  WebElement purchaseUpdateCol;

	@FindBy(id = "addPerDiemTable")
	public  WebElement addPerDiemTable;
	@FindBy(id = "selectedaddPerDiemTable")
	public  WebElement checkBoxAddPerDiemTab;
	@FindBy(css = "a[href*='perDiemTemplateShortName']")
	public  WebElement shortName;
	
	
	
	WebDriver driver;

	public PerDiemPage(WebDriver driver) {  
		this.driver=driver;

		PageFactory.initElements(driver, this);
	}



	public void runPerDiem() throws InterruptedException
	{
	clickElement(runPerDiem);
	}
	
	

	public void navigateToTab(String tabLink) throws InterruptedException
	{
		clickElement(getEleByXpathContains("em",tabLink,driver));

	}
	
	
}
	
	


