package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class ManageSubscripPage {
  
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    

    @FindBy(css = "a[href*='tab0']>em")
    public WebElement subscriptionsTab;
   
    
    @FindBy(css = "a[href*='View']")
    public WebElement viewLnk;
    
    
    @FindBy(css = "form#subscriptionForm table>tbody tr:nth-child(1)>td>b")
    public WebElement subscriptNameTxt;
    
    
    
    @FindBy(css = "form#subscriptionForm table>tbody tr:nth-child(2)>td>b")
    public WebElement subscriptDetailTxt;
   
  

	public HeaderPage headerpage;

public ManageSubscripPage(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}

}
