package pages.ics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class DataByLocationPage {
    @FindBy(css = "div.yui-u.first>h1")
    public WebElement locationResourcetxt;
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    
    
    @FindBy(css = "p.tabIntroText")
    public WebElement hardShipTrollTxt;
    
    
    @FindBy(css = "a[href*='LocationResourcesDetailShow']")
    public WebElement locationDetailLnk;
    
    @FindBy(css = "a[href*='tab1']>em")
    public WebElement tablestab;
    
    @FindBy(css = "a[href*='tab2']>em")
    public WebElement localmapTab;
    
    @FindBy(css = "div.yui-layout-doc>div[id*='yui-gen']:nth-child(1) b")
    public WebElement availProdResourceTab;
    
    
    @FindBy(css = "div.yui-layout-doc>div[id*='yui-gen']:nth-child(2) b")
    public WebElement otherInfoResourceTab;
    
    @FindBy(id = "excelButton")
    public WebElement excelButton;
    
    
    @FindBy(css = "div#dataTablelocationTables tbody>tr")
    public WebElement tablesData;
    

    public By ajaxLoader=By.cssSelector("div#gmap>img[src*='ajax']");
    
    
    @FindBy(css = "form[name*='GlobalMapSearchForm'] label")
    public WebElement searchByCity;
    
    
    
    
public DataByLocationPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
}

}
