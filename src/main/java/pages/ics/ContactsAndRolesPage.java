package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.selEleByOptionValue;;


public class ContactsAndRolesPage {
  
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    

    @FindBy(css = "a[href*='tab1']>em")
    public WebElement deleteClinetTab;
   
    @FindBy(css = "a[href*='tab2']>em")
    public WebElement invalidClientTab;
    @FindBy(css = "a[href*='tab3']>em")
    public WebElement roleAdminTab;

    @FindBy(css = "a[href*='RoleDesc']")
    public WebElement roleDesc;
    
   
	public HeaderPage headerpage;

public ContactsAndRolesPage(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}

}
