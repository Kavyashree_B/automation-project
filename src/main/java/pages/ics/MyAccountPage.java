package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class MyAccountPage {
  
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    

    @FindBy(css = "a[href*='tab0']>em")
    public WebElement contactInfoTab;
   
    @FindBy(css = "a[href*='tab1']>em")
    public WebElement myPrefTab;
    
    @FindBy(css = "div#myPreferencesTabView em")
    public WebElement eventNotifyTab;
    
    @FindBy(xpath = "//b[contains(text(),'Mercer Compensation Tables Updated')]/../input")
    public WebElement mercerCompTableCheck;

    
    @FindBy(css = "input[value*='Submit']")
    public WebElement submitBtn;
    
    @FindBy(css = "input[value*='OK']")
    public WebElement okBtn;
    
    
    @FindBy(xpath = "//div[contains(text(),'User Information')]")
    public WebElement userInfoTxt;
    
    
    @FindBy(xpath = "//div[contains(text(),'Address')]")
    public WebElement addressTxt;
    
    
    @FindBy(xpath = "//div[contains(text(),'Contact Information')]")
    public WebElement contactInfoTxt;
    
    
    @FindBy(xpath = "//li[contains(text(),'@mercer.com')]")
    public WebElement emailTxt;
 
	public HeaderPage headerpage;

public MyAccountPage(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}

}
