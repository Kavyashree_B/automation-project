package pages.ics;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
public class CurrentTables {
	
	@FindBy(css = "input[id*='currentTablesUpdateButton']")
	public  WebElement updateSelTabBtn;
	
	@FindBy(css = "input[id*='groupManagerButton']")
	public  WebElement groupManagBtn;
	
	@FindBy(id = "percentChangeButton")
	public  WebElement percentageBtn;
	
	@FindBy(css = "input[value*='Export']")  
	public  WebElement exportBtn;
	@FindBy(css = "input[value*='Print']")
	public  WebElement printBtn;
	
	@FindBy(css = "input[value*='Show Tables']")
	public  WebElement showTables;
	
	
	@FindBy(css = "div[id*='dataTablecurrentTables'] tr>th span")
	public  List<WebElement> tableHeaders;
	
	@FindBy(xpath = "//span[contains(text(),'Select')]")
	public  WebElement selectAllTxt;
	
	public CurrentTables(WebDriver driver) {  
		PageFactory.initElements(driver, this);
	}


	public void showTables() throws InterruptedException
	{
	clickElement(showTables);
	}
}
	
	


