package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.selEleByOptionValue;;


public class OrderHistory {
  
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    

    @FindBy(css = "a[href*='tab1']>em")
    public WebElement productOrderTab;
   
    
    @FindBy(id = "tableOrdersTableFilterButton")
    public WebElement tableOrdersShowBtn;
    
    
    @FindBy(id = "productOrdersTableFilterButton")
    public WebElement productOrdersShowBtn;
    
    
    @FindBy(id = "typeOfTable")
    public WebElement selTypeOfTable;
    
    
    
    @FindBy(id = "clientDDProduct")
    public WebElement selClientName;
    
    
    
    @FindBy(css = "div#dataTabletableOrdersTable td[class*='clientName'] div")
    public WebElement dataTabClientName;
    
    @FindBy(css = "div#dataTableproductOrdersTable td[class*='clientName'] div")
    public WebElement prodClientName;
   
    @FindBy(id = "clientDD")
    public WebElement selTablesClientName;

	public HeaderPage headerpage;

public OrderHistory(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}
public void selectTableType(String tableType)
{
	selEleByVisbleText(selTypeOfTable,tableType);

	
}
public void selectClient(String client)
{
	selEleByOptionValue(selClientName,client);
}

public void selectTablesClient(String client)
{
	selEleByOptionValue(selTablesClientName,client);
}
}
