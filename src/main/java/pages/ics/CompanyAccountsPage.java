package pages.ics;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class CompanyAccountsPage {
  
    
    

    @FindBy(css = "a[href*='filter=M']")
    public WebElement namesWithM;
    

    @FindBy(css = "div.yui-gb ul>li a")
    public List<WebElement> resultsWithM;
    

    @FindBy(css = "div.yui-gb ul>li a")
    public WebElement resultsWithMEle;

    
 
	public HeaderPage headerpage;

public CompanyAccountsPage(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}

}
