package pages.ics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class ContactPage {
  
    
   
    @FindBy(css = "li.selected>a>em")
    public WebElement selectedTab;
    

    
    @FindBy(css = "div.yui-content p")
    public WebElement consultantTxt;
    
    @FindBy(css = " div#bd h1")
    public WebElement contactTxt;
    
    
   
 
	public HeaderPage headerpage;

public ContactPage(WebDriver driver) {
	headerpage=new HeaderPage(driver);

	PageFactory.initElements(driver, this);
}

}
