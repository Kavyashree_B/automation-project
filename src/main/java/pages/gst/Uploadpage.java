package pages.gst;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import static driverfactory.Driver.setInput;

import static utilities.InitTests.dir_path;

public class Uploadpage {
	@FindBy(id = "_titleLabel")
	public static WebElement surveyTitle;
	
	@FindBy(id = "_regionLabel")
	public static WebElement RegionLabel;
	
	@FindBy(id = "_languageLabel")
	public static WebElement languageLabel;
	
	@FindBy(id = "_regionDropDownList")
	public static WebElement regionDropDown;
	
	@FindBy(id = "_languageDropDownList")
	public static WebElement languageDropDown;

	
	@FindBy(id = "_emailInstructionsTextBox")
	public static WebElement Instructiontextbox;
	
	@FindBy(id = "_emailTextBox")
	public static WebElement emailtextbox;
	
	@FindBy(id = "_fileUpload")
	public static WebElement Choosefile;
	
	@FindBy(id = "_uploadButton")
	public static WebElement Uploadbutton;
	
	@FindBy(xpath = "//h4[contains(text(),'Your file file.xlsm was uploaded successfully.')]")
	public static WebElement successMgs;
	
	
		
	public Uploadpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	public void uploadFile(String uploadPath) throws InterruptedException
	{
		waitForElementToDisplay(Instructiontextbox);
		setInput(Instructiontextbox,"text");
		setInput(emailtextbox,"shamitha.s@mercer.com");
		waitForElementToDisplay(Choosefile);
		Choosefile.sendKeys(dir_path+uploadPath);
		clickElement(Uploadbutton);
		
	}
	}

