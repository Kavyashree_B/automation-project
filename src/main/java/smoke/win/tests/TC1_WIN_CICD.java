package smoke.win.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.win.*;
import utilities.InitTests;

public class TC1_WIN_CICD extends InitTests {
	public static WebDriver driver;
	public static Driver driverObj;
	public static ExtentTest test = null;
	public static WebDriver webdriver;

	public TC1_WIN_CICD(String appName) {
		super(appName);
	}
	@Test
	public void navigationToWinPages() throws Exception {

		try {
			TC1_WIN_CICD wincicd=new TC1_WIN_CICD("WIN");
			driverObj = new Driver();
			test = reports.createTest("navigationToWinPages");
			test.assignCategory("smoke");
			webdriver = driverObj.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverObj.getEventDriver(webdriver, test); 
			
			LoginPage log=new LoginPage(driver);
			log.login(USERNAME, PASSWORD);
			
			SelectOrgPage org=new SelectOrgPage(driver);
			waitForElementToDisplay(org.selectYourorganization);
			verifyElementText(org.selectYourorganization,"Select Your Organization",test);
			SelectOrgPage.selectOrg(driver,"QA Verification [QA Verification (MMC)]");
			
			HomePage homepage=new HomePage(driver);
			waitForElementToDisplay(homepage.homepagetext);
			verifyElementText(homepage.homepagetext,"My Homepage",test);
			
			homepage.accessModule(homepage.diagnosticreport);
			assertTrue(homepage.diagnosticreportpage.isDisplayed(),"Navigated to diagnostic Report page",test);
			waitForElementToDisplay(homepage.diagnosticreportpage);
			verifyElementText(homepage.diagnosticreportpage,"Diagnostic Report",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.homebreadcrumb);
			homepage.accessModule(homepage.myjobs);
			waitForElementToDisplay(homepage.myjobspage);
			verifyElementText(homepage.myjobspage,"My Jobs",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.homebreadcrumb);
			homepage.accessModule(homepage.myemployees);
			waitForElementToDisplay(homepage.myemployeespage);
			verifyElementText(homepage.myemployeespage,"My Employees",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.homebreadcrumb);
			homepage.accessLibrary(homepage.mymarketlibrary);
			waitForElementToDisplay(homepage.mymarketlibrarypage);
			verifyElementText(homepage.mymarketlibrarypage,"My Market Library: All",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.homebreadcrumb);
			homepage.accessLibrary(homepage.myreferencejoblibrary);
			waitForElementToDisplay(homepage.myreferencejoblibraryPage);
			verifyElementText(homepage.myreferencejoblibraryPage,"Reference Jobs",test);
			
			homepage.headerpage.navigateToAdministrationPage();
			AdministrationPage adminpage= new AdministrationPage(driver);
			waitForElementToDisplay(adminpage.merceradministartion);
			verifyElementText(adminpage.merceradministartion,"Mercer WIN - System Administration",test);
			adminpage.accessAdminModule(adminpage.managejobachitecture);
			waitForElementToDisplay(adminpage.jobarchitecturepage);
			verifyElementText(adminpage.jobarchitecturepage,"Job Architecture:",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.administrationbreadcrumb);
			adminpage.accessAdminModule(adminpage.importdata);
			waitForElementToDisplay(adminpage.importcenterpage);
			verifyElementText(adminpage.importcenterpage,"Import Center",test);
			
			homepage.headerpage.navigateViaBreadCrumb(homepage.headerpage.administrationbreadcrumb);
			adminpage.accessAdminModule(adminpage.mupcscatalog);
			waitForElementToDisplay(adminpage.mupcscatalogpage);
			verifyElementText(adminpage.mupcscatalogpage,"MUPCS Catalog",test);
			
			SignOut signout= new SignOut(driver);
			signout.signOutUser();
			waitForElementToDisplay(signout.signback);
			verifyElementText(signout.signback,"Sign back into your account",test);
			
		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

		finally {

			reports.flush();
			driver.close();

		}

	}
}
