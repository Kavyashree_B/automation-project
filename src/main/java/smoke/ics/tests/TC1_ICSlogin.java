package smoke.ics.tests;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.switchToWindowWithURL;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.fail;
import static verify.SoftAssertions.verifyElementHyperLink;
import static verify.SoftAssertions.verifyElementTextContains;
import java.io.File;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.ics.*;
import pages.mobilityportal.MytoolsPage;
import pages.ics.LoginPage;
import pages.mobilityportal.ClientSelectionPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_ICSlogin extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;
	ICSHomePage home;
	String browser = InitTests.BROWSER_TYPE;
	public static String fileName1 = "";
	public static String fileName2 = "";
	public TC1_ICSlogin(String appname) {
		super(appname);
	}
	Driver driverObj;
	@BeforeClass
	public void beforeClass() throws Exception {
		driverObj = new Driver();
		props.load(input);
		fileName1 = props.getProperty("FileName1");
		fileName2 = props.getProperty("FileName2");
		TC1_ICSlogin obj = new TC1_ICSlogin("ICS");
		webdriver = driverObj.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
		
	}

	@Test(enabled = true, priority = 1)
	public void verifyLogin() throws Exception {

		try {
			test = reports.createTest("verifyLogin");
			test.assignCategory("smoke");
			
			driver = driverObj.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			home=new ICSHomePage(driver);
			waitForElementToDisplay(home.welcomeTxt);
			verifyElementTextContains(home.welcomeTxt, "Welcome", test);
			verifyElementTextContains(home.headerpage.compTab, "Compensation Tables", test);
			verifyElementTextContains(home.headerpage.calcTabLnk, "Calculators", test);
			verifyElementTextContains(home.headerpage.prodTab, "Products", test);

			home.headerpage.navigateToCompLnk("Add Table");

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}

	}

	@Test(enabled = true, priority = 2)
	public void verifyAddtable() throws Exception {


		try {
			test = reports.createTest("verifyAddtable");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			CompensationTablePage addtable = new CompensationTablePage(driver);
			addtable.selectRadioBtn();

			waitForElementToDisplay(CompensationTablePage.hostLocationLabel);
			waitForElementToDisplay(CompensationTablePage.hostLocation);
			selEleByVisbleText(CompensationTablePage.hostLocation, "Athens, Greece");
			
			
			addtable.reportType();
			waitForElementToDisplay(CompensationTablePage.reportTypeLabel);
			waitForElementToDisplay(CompensationTablePage.reportType);
			selEleByVisbleText(CompensationTablePage.reportType, "Standard Table");
			
			waitForElementToDisplay(CompensationTablePage.homeLocationLabel);
			waitForElementToDisplay(CompensationTablePage.homeLocation);
			selEleByVisbleText(CompensationTablePage.homeLocation, "Belgium, Belgium                 ");               

			Thread.sleep(5000);
			addtable.selectConfirm();
			
			addtable.purchase();
			Thread.sleep(5000);
			home.headerpage.navigateToCompLnk("Compensation Tables");
			home.headerpage.navigateToCompLnk("Current Tables");
			/*Driver.switchToWindowWithURL("https://www.mercer-icsnp.com/", driver);
			addtable.selectGotoCurrentBtn();*/

			if (browser.contains("CHROME")) {

				CurrentTablesShowPage currenttable = new CurrentTablesShowPage(driver);

				String downloadPath = System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator
						+ "downloadFiles";

				currenttable.getpdf(driver);

				
				SoftAssertions.assertTrue(CurrentTablesShowPage.isFileDownloaded(downloadPath, fileName1),
						"Returns true if file is downloaded successfully", test);

				CurrentTablesShowPage.exportfile();
				

				SoftAssertions.assertTrue(CurrentTablesShowPage.isFileDownloaded(downloadPath, fileName2),
						"Returns true if file is downloaded successfully", test);
				
				CurrentTablesShowPage.deleteFilesInPath(downloadPath);
			} else {
				System.out.println("Browser type is :" + browser);

			}
			addtable.logout();

			/*waitForElementToDisplay(CompensationTablePage.loginText);
			verifyElementTextContains(CompensationTablePage.loginText, "Login", test);*/

			driver.close();

			/*switchToWindowByTitle("Home",driver);
			MytoolsPage tools=new MytoolsPage(driver);	
			tools.header.logout();*/

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@AfterSuite
	public void killDriver() {

		driver.quit();
		killBrowserExe(BROWSER_TYPE);

	}

}
