package smoke.bsc.tests;

import static utilities.MyExtentReports.reports;
import static driverfactory.Driver.*;
import static utilities.DataRepository.*;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import verify.SoftAssertions;

import pages.bsc.CalculationStartPage;
import pages.bsc.DashBoardPage;
import pages.bsc.InputsPage;

import pages.bsc.PreviewPage;
import pages.bsc.ResultsPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.HeaderPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import utilities.InitTests;

import static verify.SoftAssertions.*;

public class BSC_SmokeTest extends InitTests {

	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;
	public static String env;

	public BSC_SmokeTest(String appname) {
		super(appname);
	}


	@BeforeClass
	public void beforeClass() throws Exception {
		env=System.getProperty("env");
		BSC_SmokeTest Tc1 = new BSC_SmokeTest("BSC");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
	}

	@Test(priority = 1, enabled = true)
	public void verifyLogin() throws Exception {

		try {
			System.out.println("test started");
			test = reports.createTest("Test BSC Login");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			driver.manage().window().maximize();
			LoginPage mobilityLogin = new LoginPage(driver);
			//int n= mobilityLogin.myAccountsList.size();
			System.out.println("Not yet");
			//mobilityLogin.login(USERNAME, PASSWORD);
			
			if (mobilityLogin.myAccountsList.size() == 3) {
				System.out.println("Entered");
				mobilityLogin.login(USERNAME, PASSWORD);
				ClientSelectionPage clientSelectionPage = new ClientSelectionPage(driver);
				waitForElementToDisplay(ClientSelectionPage.selectcustomer);
				verifyElementTextContains(ClientSelectionPage.selectcustomer, "Select a customer:", test);
				String urlProd = driver.getCurrentUrl();
				System.out.println(urlProd);
				clientSelectionPage.selectCustomer("Mercer                                  ");
				clientSelectionPage.selectAccount("97216906 GHRM Clients");
				clientSelectionPage.clickOnContinue();

				driver.manage().window().maximize();
				MytoolsPage mytool = new MytoolsPage(driver);
				waitForElementToDisplay(MytoolsPage.mytool_link);
				verifyElementHyperLink(MytoolsPage.mytool_link, test);
				waitForElementToDisplay(MytoolsPage.mydata_link);
				verifyElementHyperLink(MytoolsPage.mydata_link, test);
				waitForElementToDisplay(MytoolsPage.customreport_link);
				verifyElementHyperLink(MytoolsPage.customreport_link, test);
				waitForElementToDisplay(MytoolsPage.notification_link);
				verifyElementHyperLink(MytoolsPage.notification_link, test);
				String url = driver.getCurrentUrl();
				System.out.println(url);

				if (url.matches("https://qa-mobilityportal.mercer.com/dashboard/MyTools"))
				{
					mytool.header.navigateToMyAccMenu("Balance Sheet and Cost Projection Calculators (UAT)");
				} 
				else 
				{
					mytool.header.navigateToMyAccMenu("Balance Sheet and Cost Projection Calculators");

				}

				switchToWindowByTitle("-BALANCE SHEET", driver);
				CalculationStartPage calcStart = new CalculationStartPage(driver);
				waitForElementToDisplay(CalculationStartPage.welcomeText);
				verifyElementTextContains(CalculationStartPage.welcomeText,
						" Balance Sheet and Cost Projection Calculators", test);

			} else {

				ClientSelectionPage clientSelectionPage = new ClientSelectionPage(driver);
				clientSelectionPage.stagingLogin(USERNAME, PASSWORD);
				CalculationStartPage calcStart = new CalculationStartPage(driver);
				waitForElementToDisplay(CalculationStartPage.welcomeText);
				verifyElementTextContains(CalculationStartPage.welcomeText,
						" Balance Sheet and Cost Projection Calculators", test);
			}
		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("BSC_loginprocess is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("BSC_loginprocess is failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}

		finally {
			reports.flush();
		}
	}

	@Test(priority = 2, enabled = true)
	public void verifyGHRMCalculation() throws Exception {

		try {
			test = reports.createTest("Test GHRM Calculation");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			CalculationStartPage calcStart = new CalculationStartPage(driver);
			Thread.sleep(5000);
			calcStart.startCalculationGHRM("United States, New York City", "Germany, Berlin");
			InputsPage input = new InputsPage(driver);
			waitForElementToDisplay(InputsPage.calculationInputsText);
			verifyElementTextContains(InputsPage.calculationInputsText, "Calculation Inputs", test);
			input.inputSelectionGHRM();
			PreviewPage preview = new PreviewPage(driver);
			waitForElementToDisplay(PreviewPage.previewText);
			verifyElementTextContains(PreviewPage.previewText, "Preview Calculation", test);
			preview.previewContinueGHRM();
			ResultsPage results = new ResultsPage(driver);
			waitForElementToDisplay(ResultsPage.calculationSummaryText);
			verifyElementTextContains(ResultsPage.calculationSummaryText, "Balance Sheet Calculation Summary", test);

			String s = ResultsPage.countryCompense.getText();
			System.out.println("Total HomeCountry Compensation :");
			assertTrue(results.checkValues(s), "Total Homecountry Compensation is greater than 0 validation", test);
			String s1 = ResultsPage.personalIncomeValue.getText();
			System.out.println("Host Estimated Personal IncomeTax :");
			assertTrue(results.checkValues(s1), "Host Estimated Personal IncomeTax greater than 0 validation", test);
			String s2 = ResultsPage.socialSecurityValue.getText();
			System.out.println("Estimated Home Social Security :");
			assertTrue(results.checkValues(s2), "Estimated Home Social Security greater than 0 validation", test);
			results.returnToMobility();

			ClientSelectionPage clientSelectionPage = new ClientSelectionPage(driver);
			//if (clientSelectionPage.mercerNYStaging.size() == 1)
			if(env.equalsIgnoreCase("production")|| env.equalsIgnoreCase("prod") || env.equalsIgnoreCase("QA"))
			{
				System.out.println("DEV");
				DashBoardPage sheetSel = new DashBoardPage(driver);
				driver.manage().window().maximize();
				sheetSel.switchClient();
				//waitForElementToDisplay(ClientSelectionPage.selectcustomer);
				/*
				 * verifyElementTextContains(ClientSelectionPage.selectcustomer,
				 * "Select a customer:", test);
				 * calcStart.clientSelectionSearch("Mercer (New York)");
				 * clientSelectionPage.selectCustomer("Mercer (New York)"); clientSelectionPage.
				 * selectAccount("11325 Multi-National Pay with Americans Abroad - Catherine Bruning"
				 * ); clientSelectionPage.clickOnContinue();
				 */

			} 
			else
			{
				System.out.println("Stage");
				/*DashBoardPage sheetSel = new DashBoardPage(driver);
				driver.manage().window().maximize();
				sheetSel.switchClient();*/
			}

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("GHRM_Calculation is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("GHRM_Calculation is failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		}

		finally {
			reports.flush();
		}
	}

	@Test(priority = 3, enabled = true)
	public void verifyICSCalculation() throws Exception {

		try {
			test = reports.createTest("Test ICS Calculation");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			ClientSelectionPage clientSelectionPage = new ClientSelectionPage(driver);

			if (clientSelectionPage.mercerNYStaging.size() == 1) {

				waitForElementToDisplay(ClientSelectionPage.selectcustomer);
				verifyElementTextContains(ClientSelectionPage.selectcustomer, "Select a customer:", test);
			}

			else {

				CalculationStartPage calcStart = new CalculationStartPage(driver);
				waitForElementToDisplay(ClientSelectionPage.selectcustomer);
				verifyElementTextContains(ClientSelectionPage.selectcustomer, "Select a customer:", test);
				calcStart.clientSelectionSearch("Mercer (New York)");
				clientSelectionPage.selectCustomer("Mercer (New York)");
				clientSelectionPage.selectAccount("11325 Multi-National Pay with Americans Abroad - Catherine Bruning");
				clientSelectionPage.clickOnContinue();

				MytoolsPage mytool = new MytoolsPage(driver);
				HeaderPage header = new HeaderPage(driver);
				waitForElementToDisplay(MytoolsPage.mytool_link);
				verifyElementHyperLink(MytoolsPage.mytool_link, test);
				waitForElementToDisplay(MytoolsPage.mydata_link);
				verifyElementHyperLink(MytoolsPage.mydata_link, test);
				waitForElementToDisplay(MytoolsPage.customreport_link);
				verifyElementHyperLink(MytoolsPage.customreport_link, test);
				waitForElementToDisplay(MytoolsPage.notification_link);
				verifyElementHyperLink(MytoolsPage.notification_link, test);
				DashBoardPage sheetSel = new DashBoardPage(driver);
				driver.manage().window().maximize();
				String url = driver.getCurrentUrl();
				System.out.println(url);
				if (url.matches("https://qa-mobilityportal.mercer.com/dashboard/MyTools")) {
					mytool.header.navigateToMyAccMenu("Balance Sheet and Cost Projection Calculators (UAT)");
				}

				else {
					mytool.header.navigateToMyAccMenu("Balance Sheet and Cost Projection Calculators");
				}
				switchToWindowByTitle("-BALANCE SHEET", driver);
				calcStart.startCalculationICS();
				/*if (CalculationStartPage.OptionOneProd.size() == 2) {
					calcStart.startCalculationICS("Argentina, Argentina", "Chile, Santiago");
				} 
				else {
					calcStart.startCalculationICS("Argentina, Argentina", "Brazil, Sao Paulo");
				}*/
				InputsPage input = new InputsPage(driver);
				waitForElementToDisplay(InputsPage.calculationInputsText);
				verifyElementTextContains(InputsPage.calculationInputsText, "Calculation Inputs", test);
				input.inputSelectionICS();
				PreviewPage preview = new PreviewPage(driver);
				waitForElementToDisplay(PreviewPage.previewText);
				verifyElementTextContains(PreviewPage.previewText, "Preview Calculation", test);
				preview.previewContinueICS();

				ResultsPage results = new ResultsPage(driver);

				waitForElementToDisplay(ResultsPage.icsCountryTax);
				String s1 = ResultsPage.icsCountryTax.getText();
				System.out.println("Country Tax :");
				assertTrue(results.checkValues(s1), "Country Tax is greater than 0 validation", test);

				waitForElementToDisplay(ResultsPage.hostIncomeIcs);
				String s2 = ResultsPage.hostIncomeIcs.getText();
				System.out.println("Host Estimated Personal Income Value");
				assertTrue(results.checkValues(s2), "Host Estimated Personal Income Value is greater than 0 validation",
						test);

				waitForElementToDisplay(ResultsPage.socialSecurityIcs);
				String s3 = ResultsPage.socialSecurityIcs.getText();
				System.out.println("Social Security Value");
				assertTrue(results.checkValues(s3), "Social Security Value is greater than 0 validation", test);

				String s4 = ResultsPage.netCompensIcs.getText();
				System.out.println("Total Net Compensation value");
				assertTrue(results.checkValues(s4), "Total Net Compensation value is greater than 0 validation", test);

				results.returnToMobility();
				driver.manage().window().maximize();
				waitForElementToDisplay(header.myAccounts);
				clickElement(header.myAccounts);
				waitForElementToDisplay(header.logoutLnk);
				mytool.header.logout();
			}

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("ICS_Calculation is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("ICS_Calculation is failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} finally {
			reports.flush();
		}
	}

	@AfterSuite
	public void closeBrowser() {
		driver.quit();
	}

}
