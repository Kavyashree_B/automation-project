package smoke.datasubmission.tests;


import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.datasubmission.*;
import utilities.InitTests;
import verify.SoftAssertions;


	public class TC01_Uploadfile extends InitTests {
		  WebDriver driver=null;
		  Driver driverObj;
		  ExtentTest test=null;
		 String browser="chrome";
		public TC01_Uploadfile(String appName) {
			super(appName);
			// TODO Auto-generated constructor stub
		}

		@Test(enabled = true)
		public void uploadfile() throws Exception {
			try {
				//browser=br;
				 driverObj=new Driver();
				TC01_Uploadfile file = new TC01_Uploadfile("DataSubmission");
				test = reports.createTest("DataSubmission--uploadfile--"+browser);
				test.assignCategory("DataSubmission");
				 WebDriver webdriver=driverObj.initWebDriver( BASEURL,BROWSER_TYPE,EXECUTION_ENV, "");
				 driver=driverObj.getEventDriver(webdriver,test);
				System.out.println("Logging in to application");
				System.out.println("BaseURL is: " + BASEURL);
				Uploadpage uploadfile=new Uploadpage(driver);
				waitForElementToDisplay(Uploadpage.surveyTitle);
				SoftAssertions.verifyElementTextContains(Uploadpage.surveyTitle, "Survey Submission Upload",test);
				waitForElementToDisplay(Uploadpage.RegionLabel);
				Driver.selEleByVisbleText(Uploadpage.regionDropDown,"United States");
				waitForElementToDisplay(Uploadpage.languageLabel);
				Driver.selEleByVisbleText(Uploadpage.languageDropDown,"English (United States)");
				uploadfile.uploadFile("\\src\\main\\resources\\testdata\\file.xls");
				waitForElementToDisplay(Uploadpage.successMgs);
				SoftAssertions.verifyElementTextContains(Uploadpage.successMgs, "Your file file.xls was uploaded successfully.",test);
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("uploadfile()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();
				reports.flush();
				driver.close();

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("uploadfile()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();
				reports.flush();
				driver.close();

			}
			finally {
				reports.flush();
				driver.quit();
			}

		}

		
	}