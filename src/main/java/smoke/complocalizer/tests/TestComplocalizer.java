
package smoke.complocalizer.tests;


import static driverfactory.Driver.killBrowserExe;
import static pages.complocalizer.Input.AdjustedNetIncomeInHostValue;
import static pages.complocalizer.Input.AdjustedNetIncomeValue;
import static pages.complocalizer.Input.Allowance1;
import static pages.complocalizer.Input.Allowance2;
import static pages.complocalizer.Input.COLAValue;
import static pages.complocalizer.Input.Deduction1;
import static pages.complocalizer.Input.Deduction2;
import static pages.complocalizer.Input.HardshipPremiumValue;
import static pages.complocalizer.Input.HomeAdjustedNetValue;
import static pages.complocalizer.Input.HomeNetIncomeValue;
import static pages.complocalizer.Input.HomeTotalCompensationValue;
import static pages.complocalizer.Input.HostAdjustedNetValue;
import static pages.complocalizer.Input.HostNetIncomeValue;
import static pages.complocalizer.Input.HostTotalCompensationValue;
import static pages.complocalizer.Input.NetIncomeValue;
import static pages.complocalizer.Input.ProposedHostAdjustedNetValue;
import static pages.complocalizer.Input.ProposedHostNetIncomeValue;
import static pages.complocalizer.Input.ProposedHostTotalCompensationValue;
import static utilities.MyExtentReports.reports;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import pages.complocalizer.Input;
import pages.complocalizer.Login;
import pages.complocalizer.Logout;
import pages.complocalizer.Preview;
import pages.complocalizer.Results;
import pages.complocalizer.Start;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TestComplocalizer extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailId = "";
	public static String MFAEmailPassword = "";
	public static String env;
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;
	Login loginobj;
	Start startobj;
	Input inputobj;
	Preview previewobj;
	Results resultsobj;
	Logout logoutobj;

public TestComplocalizer(String appname) {
	super(appname);
	env=System.getProperty("env");
}
@BeforeMethod
public void setUp() throws Exception {
	props.load(input);
	

	
	
	
}
@Test(priority = 1, enabled = true)
public void VerifyGHRM() throws Exception {
	try {
		TestComplocalizer obj = new TestComplocalizer("CompensationLocalizer");
		test = reports.createTest("VerifyGHRM");
		test.assignCategory("smoke");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
		driver = driverFact.getEventDriver(webdriver, test);
		if(!(env.equalsIgnoreCase("Staging")))
		{
		LoginPage mobilityLogin = new LoginPage(driver);
		mobilityLogin.login(USERNAME, PASSWORD);
		
		ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
	
		clientSelectionPage.selectAccounts("97216906");
		MytoolsPage myTools=new MytoolsPage(driver);
		waitForElementToDisplay(myTools.myToolsHeaderTxt);
		startobj = new Start(driver);
		startobj.openCompensationLocalizer();
		waitForElementToDisplay(startobj.ApplicationName);
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		verifyElementTextContains(startobj.LoggedInAccount,"97216906",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		}
		else
		{
			
			pages.imercer.LoginPage login = new pages.imercer.LoginPage(driver);
			login.login(USERNAME, PASSWORD);
		}
		startobj = new Start(driver);
		startobj.startCalculation("Germany, Berlin", "Switzerland, Zurich");
		inputobj = new Input(driver);
		waitForElementToDisplay(inputobj.calHeaderTxt);
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("Yes","40000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		previewobj = new Preview(driver);
		waitForElementToDisplay(previewobj.summOfCalcTxt);
		
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);

		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"0",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		resultsobj = new Results(driver);
		waitForElementToDisplay(resultsobj.viewResultTxt);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("LessThan",10),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
		logoutobj = new Logout(driver);


		logoutobj.logout();
		
		
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} finally {
		reports.flush();
		driver.manage().deleteAllCookies();
		driver.quit();
	}

	}
@Test(priority = 2, enabled = false)
public void VerifyICS() throws Exception {
	try {
		TestComplocalizer obj = new TestComplocalizer("CompensationLocalizer");
		test = reports.createTest("VerifyICS");
		test.assignCategory("smoke");
	
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
		driver = driverFact.getEventDriver(webdriver, test);
		if(!(env.equalsIgnoreCase("Staging")))
		{
		LoginPage mobilityLogin = new LoginPage(driver);
		mobilityLogin.login(USERNAME, PASSWORD);
		ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
	
		//clientSelectionPage.selectCustomer("Mercer (New York)");
		clientSelectionPage.selectAccounts("11325");
		MytoolsPage myTools=new MytoolsPage(driver);
		waitForElementToDisplay(myTools.myToolsHeaderTxt);
		startobj = new Start(driver);
		startobj.openCompensationLocalizer();
		waitForElementToDisplay(startobj.ApplicationName);
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		//verifyElementTextContains(startobj.LoggedInAccount,"11325",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		}
		startobj = new Start(driver);
		if(env.equalsIgnoreCase("Staging"))
		{
			startobj.startCalculation("United Kingdom, Oxford", "Germany, Berlin");
	
		}
		else
		{
		startobj.startCalculation("United Kingdom, Urban United Kingdom", "Germany, Berlin");
		}
		inputobj = new Input(driver);

		waitForElementToDisplay(inputobj.calHeaderTxt);
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Week");
		inputobj.setTargetSalary("Yes","100000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		previewobj = new Preview(driver);
		waitForElementToDisplay(previewobj.summOfCalcTxt);
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("Equals",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		resultsobj = new Results(driver);
		waitForElementToDisplay(resultsobj.viewResultTxt);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		//assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("Equals",0),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("GreaterThan",0),"Compensation difference is within the range",test);
		logoutobj = new Logout(driver);

		logoutobj.logout();
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e,driverFact. getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} finally {
		reports.flush();
		driver.manage().deleteAllCookies();
		driver.quit();
	}

	}

@AfterSuite
public void tearDown() {
	killBrowserExe(BROWSER_TYPE);
}

}