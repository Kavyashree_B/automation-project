package smoke.pat.tests;

import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForNoOfwindowsTobe;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.fail;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.pat.AppStatsPage;
import pages.pat.CalendarPage;
import pages.pat.PlatformAdminPage;
import pages.pat.PlatformIncTaxPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import utilities.InitTests;

public class TC02_verifyReport  extends InitTests{

	
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	public static String env;
	public TC02_verifyReport(String appname) {
		// TODO Auto-generated constructor stub
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		env=System.getProperty("env");
		TC02_verifyReport TC02 = new TC02_verifyReport("PAT");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
		
	}

	@Test(enabled = true ,priority = 2)
	public void verifyCLCReport() throws Exception {
		try {
			test = reports.createTest("PAT--verify CLC report--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			
			driver=driverFact.getEventDriver(webdriver,test);
			
			if (env.equalsIgnoreCase("prod")||env.equalsIgnoreCase("production") ) {

				LoginPage mobilityLogin = new LoginPage(driver);
				mobilityLogin.login(USERNAME, PASSWORD);
				ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
//				clientSelectionPage.selectCustomer("Mercer Demo");
				clientSelectionPage.selectAccounts("97216906");
				
				MytoolsPage myTools=new MytoolsPage(driver);
				myTools.header.navigateToMyAccMenu("Platform Administration");
				String parentWindow=driver.getWindowHandle();
				waitForNoOfwindowsTobe(2);
				switchToWindowByTitle("Administration",driver);
			} 

			else if(env.equalsIgnoreCase("Staging")|| env.equalsIgnoreCase("uat"))
			{
				pages.imercer.LoginPage patLogin = new pages.imercer.LoginPage(driver);
				patLogin.login(USERNAME, PASSWORD);
			}
			
			else {
				
				LoginPage mobilityLogin = new LoginPage(driver);
				mobilityLogin.login(USERNAME, PASSWORD);
				ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
				clientSelectionPage.selectCustomer("Mercer                                  ");
				Thread.sleep(6000);
				clientSelectionPage.selectAccounts("97216906");
				//clientSelectionPage.clickOnContinue();
				MytoolsPage myTools=new MytoolsPage(driver);
				myTools.header.navigateToMyAccMenu("Platform Administration");
				String parentWindow=driver.getWindowHandle();
				waitForNoOfwindowsTobe(2);
				switchToWindowByTitle("Administration",driver);

			}
			
		
			PlatformAdminPage patAdminPage=new PlatformAdminPage(driver);
			waitForElementToDisplay(patAdminPage.appheaderTxt);
			patAdminPage.navigateToMenuLnk("ApplicationStatistics");
			AppStatsPage appStats = new AppStatsPage(driver);
			CalendarPage calendar = new CalendarPage(driver);
			verifyElementTextContains(appStats.AppStatsHeader, " Application Statistics ",test);
			verifyElementTextContains(appStats.CLCReport, " Compensation Localizer Calculation Report ",test);
			appStats.clickCLCReport();
			verifyElementTextContains(appStats.CLCReportLabel, "View Calculation Report By:",test);
			calendar.clickCalIcon();
			calendar.selectDateByJS();
			calendar.clickExport();
			calendar.CheckFile();
			//assertTrue(calendar.CheckFile(), "File is verified.",test);
			appStats.header.signOut();
		
			if(!(env.equalsIgnoreCase("staging") || env.equalsIgnoreCase("uat"))) {
				
				switchToWindowByTitle("Mercer Mobility > Home > Mobility > Dashboard", driver);
				MytoolsPage myTools=new MytoolsPage(driver);
				myTools.header.logout();
			}
			
		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally
		{
			reports.flush();
			driver.quit();
						
		}
		
}
}
