package smoke.pit.tests;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForNoOfwindowsTobe;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import pages.pit.HomePage;
import pages.pit.InputMainPage;
import pages.pit.ResultsPage;
import utilities.InitTests;
import verify.SoftAssertions;
import static  verify.SoftAssertions.*;
public class TC02_VerifyNetToGross extends InitTests {
	
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	public TC02_VerifyNetToGross(String appName) {
		super(appName);
	} 
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFORE METHOD****************");
		driverFact=new Driver();
		new TC02_VerifyNetToGross("PIT");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}
	@Test(priority = 1, enabled = true)
	public void verifyOptionalSubtitle() throws Exception {
		ExtentTest test=null;
		try {
			String client="Mercer (New York)";
			String account="11325";
			test = reports.createTest("TC02_Verify access using ICS account & run Net To Gross calculation");
			test.assignCategory("Smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			LoginPage mobilityLogin = new LoginPage(driver);
			mobilityLogin.login(USERNAME, PASSWORD);
			ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
			clientSelectionPage.selectCustomer(client);
			clientSelectionPage.selectAccounts(account);
			//clientSelectionPage.clickOnContinue();
			MytoolsPage myTools=new MytoolsPage(driver);
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
				myTools.header.navigateToMyAccMenu("Personal Income Tax Solution (UAT)");
			else if ((System.getProperty("env")).equalsIgnoreCase("Production") || (System.getProperty("env")).equalsIgnoreCase("Prod") )
			{
				myTools.header.navigateToMyAccMenu("Personal Income Tax Solution");
			}
			waitForNoOfwindowsTobe(2);
			waitForPageLoad(driver);
			switchToWindowByTitle("PersonalIncomeTax",driver);
			waitForPageLoad(driver);
			HomePage homePage = new HomePage(driver);
			waitForElementToDisplay(homePage.CalculationBtn);
			verifyElementTextContains(homePage.header.appTitle, "Personal Income Tax",test);
			homePage.header.clickBannerEmail();
			verifyElementTextContains(homePage.header.accountName, account,test);
			homePage.selectCountry("Switzerland - Geneva");
			homePage.BeginCalculation();
			waitForNoOfwindowsTobe(3);
			waitForPageLoad(driver);
			switchToWindowByTitle("Input",driver);
			InputMainPage  inputMainPage= new InputMainPage(driver);
			inputMainPage.selectTaxYear("2018");
			inputMainPage.inputSalary("80000");
			inputMainPage.setBonus("% of Gross", "15");
			inputMainPage.selectGivenSalary("Net");
			inputMainPage.selectMaritalStatus("Married with 2 children");
			inputMainPage.clickViewResults();
			waitForNoOfwindowsTobe(4);
			waitForPageLoad(driver);
			switchToWindowByTitle("SimpleResult",driver);
			ResultsPage resultsPage = new ResultsPage(driver);
			verifyElementText(resultsPage.get2ndColumnElement("Market"), "Switzerland - Geneva", test);
			verifyElementTextContains(resultsPage.tableHeader, "Net to Gross",test);
			verifyElementTextContains(resultsPage.get1stColumnElement("Annual Salary Details"), "(Net)",test);
			verifyElementText(resultsPage.get2ndColumnElement("Bonus"), "11,359 CHF", test);
			verifyElementText(resultsPage.get2ndColumnElement("Gross Income"), "87,084", test);
			verifyElementText(resultsPage.get2ndColumnElement("Income Tax"), "5,082", test);	
			verifyElementText(resultsPage.get2ndColumnElement("Social Security Contributions"), "9,202", test);	
			verifyElementText(resultsPage.get2ndColumnElement("Family Allowance"), "7,200", test);	
			resultsPage.header.returnMELink.click();
			verifyContains(driver.getCurrentUrl(), BASEURL.trim()+"dashboard/MyTools", "Return to Mobility Exchange Dashborad page", test);
			myTools.header.myAccounts.click();
			myTools.header.logout();
			
			} 
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			//driver.quit();
			//killBrowserExe(BROWSER_TYPE);			

		}
	}

}
