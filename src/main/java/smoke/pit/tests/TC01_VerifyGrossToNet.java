package smoke.pit.tests;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.getEleByLinkText;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForNoOfwindowsTobe;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import pages.pit.HomePage;
import pages.pit.InputMainPage;
import pages.pit.ResultsPage;
import utilities.InitTests;
import verify.SoftAssertions;
import static  verify.SoftAssertions.*;

import java.awt.Dimension;
import java.util.Map;
public class TC01_VerifyGrossToNet extends InitTests {
	
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	
	public TC01_VerifyGrossToNet(String appName) {
		super(appName);
	} 
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFORE METHOD****************");
		driverFact=new Driver();
		new TC01_VerifyGrossToNet("PIT");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}
	@Test(priority = 1, enabled = true)
	public void verifyOptionalSubtitle() throws Exception {
		
		try {
			String account="97216906";
			test = reports.createTest("TC01_Verify access using GHRM account & run Gross To Net calculation");
			test.assignCategory("Smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			LoginPage mobilityLogin = new LoginPage(driver);
			mobilityLogin.login(USERNAME, PASSWORD);
			ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
			if(EXECUTION_ENV.contains("PROD"))
			{
			String client="Mercer";
			clientSelectionPage.selectCustomer(client);
			}
			else if (EXECUTION_ENV.contains("QA")){
				String client="MERCER";
				clientSelectionPage.selectCustomer(client);
			}
			
			
			clientSelectionPage.selectAccounts(account);
			//clientSelectionPage.clickOnContinue();
			MytoolsPage myTools=new MytoolsPage(driver);
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
				myTools.header.navigateToMyAccMenu("Personal Income Tax Solution (UAT)");
			else if ((System.getProperty("env")).equalsIgnoreCase("Production") || (System.getProperty("env")).equalsIgnoreCase("Prod") )
			{
				myTools.header.navigateToMyAccMenu("Personal Income Tax Solution");
			}
			waitForNoOfwindowsTobe(2);
			waitForPageLoad(driver);
			switchToWindowByTitle("PersonalIncomeTax",driver);
			driver.manage().window().maximize();
			waitForPageLoad(driver);
			HomePage homePage = new HomePage(driver);
			waitForPageLoad(driver);
			waitForElementToDisplay(homePage.CalculationBtn);
			verifyElementTextContains(homePage.header.appTitle, "Personal Income Tax",test);
			homePage.header.clickBannerEmail();
			verifyElementTextContains(homePage.header.accountName, account,test);
			homePage.selectCountry("India");
			homePage.BeginCalculation();
			waitForNoOfwindowsTobe(3);
			waitForPageLoad(driver);
			switchToWindowByTitle("Input",driver);
			InputMainPage  inputMainPage= new InputMainPage(driver);
			inputMainPage.selectTaxYear("2017");
			inputMainPage.inputSalary("900000");
			inputMainPage.selectMaritalStatus("Married with 2 children");
			inputMainPage.clickViewResults();
			waitForNoOfwindowsTobe(4);
			waitForPageLoad(driver);
			switchToWindowByTitle("SimpleResult",driver);
			ResultsPage resultsPage = new ResultsPage(driver);
			verifyElementText(resultsPage.get2ndColumnElement("Market"), "India", test);
			verifyElementTextContains(resultsPage.tableHeader, "Gross to Net",test);
			verifyElementTextContains(resultsPage.get1stColumnElement("Annual Salary Details"), "(Gross)",test);
			verifyElementText(resultsPage.get2ndColumnElement("Net Income"), "804,725", test);
			verifyElementText(resultsPage.get2ndColumnElement("Income Tax"), "95,275", test);	
			verifyElementText(resultsPage.get2ndColumnElement("Social Security Contributions"), "0", test);	
			verifyElementText(resultsPage.get2ndColumnElement("Family Allowance"), "N/A", test);
			/********************* FILE DOWNLOAD TEST**********************************************/
		/*	String filePath= System.getProperty("user.home")+"\\Downloads";
			String fileName= "Mercer Simple Personal Income Tax Results.pdf";
			resultsPage.export.exportPDFRadioBtn.click();
			resultsPage.export.exportBtn.click();
			delay(9000);
			assertTrue(resultsPage.export.isFileDownloaded(filePath, fileName), "Verify PDF export", test);*/
			
			resultsPage.header.returnMELink.click();
			verifyContains(driver.getCurrentUrl(), BASEURL.trim()+"dashboard/MyTools", "Return to Mobility Exchange Dashborad page", test);
			myTools.header.myAccounts.click();
			myTools.header.logout();
			
			} 
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			//driver.quit();
			//killBrowserExe(BROWSER_TYPE);
			
			
			
		

		}
	}

}
