package smoke.flipkart.tests;

import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.flipkart.*;
import utilities.InitTests;
import utilities.ProductdataReader;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;

import static verify.SoftAssertions.*;

import java.util.Map;

public class ProductsearchFlipkart extends InitTests{


	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public ProductsearchFlipkart(String appName) {
		super(appName);
	}
	
	@Test(enabled =true, priority = 1,retryAnalyzer=RetryAnalyzer.class)
	public void verifyLoginPagwithinvalidCreditials() throws Exception {

		try {
			test = reports.createTest("Verifying Login page with Invalid Credentials");
			test.assignCategory("smoke");
			ProductsearchFlipkart Flipkart = new ProductsearchFlipkart("FLIPKART");
			webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
			driver=driverFact.getEventDriver(webdriver,test);

			
			
			LoginPage login = new LoginPage(driver);
			login.loginWithInvalidCredentials(USERNAME,PASSWORD);

			verifyElementTextContains(login.Loginlink, "Login", test);
			//verifyElementTextContains(login.Register_link, "Register", test);
			//verifyElementTextContains(login.ForgotYourPassword, "Forgot your password?", test);
			//verifyContains(login.username_input.getAttribute("placeholder"), "Enter Email/Mobile number", "Placeholder is verified.",test);
		//verifyContains(login.password_input.getAttribute("placeholder"), "Enter Password", "Placeholder is verified.",test);
		  assertTrue(isElementExisting(driver, login.submit_button),"LogIn button is present", test);
		  assertTrue(isElementExisting(driver, login.invalidCredsMsgPwd)," 'Your username or password is incorrect'Error Message Displayed", test);
	
		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		//driver.close();
	 		
		}
	}
	

	@BeforeClass
	public void beforeClass() throws Exception
	{
		//ProductsearchFlipkart Flipkart = new ProductsearchFlipkart("FLIPKART");
		//webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}


	

	

@Test(enabled = false, priority = 1,retryAnalyzer=RetryAnalyzer.class)
	public void verifyLoginPage() throws Exception {

		try {
			test = reports.createTest("Verifying Login page with valid Credentials");
			test.assignCategory("smoke");

			driver=driverFact.getEventDriver(webdriver,test);

			
			LoginPage login = new LoginPage(driver);
			login.clickLogin(USERNAME,PASSWORD);
			//ProductsearchPage product= new ProductsearchPage(driver);
           // product.clickSearch("fans");
			//login.loginWithNoCredentials();
			//waitForElementToDisplay(login.LoginCompanyLogo);

			//System.out.println(isElementExisting(driver, login.LoginCompanyLogo));
			/*assertTrue(isElementExisting(driver, login.LoginCompanyLogo),"CompanyLogo is present", test);
			verifyElementTextContains(login.Header1, "Welcome to Career View", test);
			verifyElementTextContains(login.Header2, "A tool to map your future.", test);*/
			//verifyElementTextContains(login.Loginlink, "Login", test);
			//verifyElementTextContains(login.Register_link, "Register", test);
			//verifyElementTextContains(login.ForgotYourPassword, "Forgot your password?", test);
			//verifyContains(login.username_input.getAttribute("placeholder"), "Enter Email/Mobile number", "Placeholder is verified.",test);
		//verifyContains(login.password_input.getAttribute("placeholder"), "Enter Password", "Placeholder is verified.",test);
		//assertTrue(isElementExisting(driver, login.submit_button),"LogIn button is present", test);
	  assertTrue(isElementExisting(driver, login.SuccessfullLogin),"User has been successfully logged in.", test);

			//verifyContains(login.videoVerify.getAttribute("type"), "video/mp4","Video is present here", test);


		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		//driver.close();
			
		}
	}

@Test(priority = 2,enabled=false,dataProvider="getDataFromExcel1",dataProviderClass=ProductdataReader.class,retryAnalyzer=RetryAnalyzer.class)
public void searchProduct(Map<String,String> data) throws Exception {
	try {
		System.out.println(data.toString());
		test = reports.createTest(data.get("TestCaseName"));
		//test.assignCategory("Smoke");
		ProductsearchPage product= new ProductsearchPage(driver);
        
if(data.get("ProductName").equalsIgnoreCase("books")) 
		{
		 
               System.out.println(data.get("ProductName"));

			driver=driverFact.getEventDriver(webdriver,test);
			product.clickSearch(data.get("ProductName"));

			System.out.println(product.searchfield.getAttribute("placeholder"));
	assertTrue(Driver.isElementExisting(driver,product.searchfield),"search text field is present ", test);
	verifyContains(product.searchfield.getAttribute("placeholder"), "Search for products, brands and more", "Placeholder is verified.",test);
	
		verifyElementTextContains(product.searchfield,data.get("ProductName").trim() ,test);
   
		assertTrue(isElementExisting(driver,product.booksverification),"User searched for books.", test);
       

	
		
		}
else if(data.get("ProductName").equalsIgnoreCase("pens")) 
		{
		 
               System.out.println(data.get("ProductName"));

			driver=driverFact.getEventDriver(webdriver,test);
			product.clickSearch(data.get("ProductName"));
			

			assertTrue(Driver.isElementExisting(driver,product.searchfield),"search text field is present ", test);
			verifyContains(product.searchfield.getAttribute("placeholder"), "Search for products, brands and more", "Placeholder is verified.",test);
			
				verifyElementTextContains(product.searchfield,data.get("ProductName").trim() ,test);
		assertTrue(isElementExisting(driver,product.pensverification),"User searched for pens.", test);
	
		}
		
			
  else	if(data.get("ProductName").equalsIgnoreCase("shoes")) 
			{
			 
	               System.out.println(data.get("ProductName"));

				driver=driverFact.getEventDriver(webdriver,test);
				product.clickSearch(data.get("ProductName"));
				

				assertTrue(Driver.isElementExisting(driver,product.searchfield),"search text field is present ", test);
				verifyContains(product.searchfield.getAttribute("placeholder"), "Search for products, brands and more", "Placeholder is verified.",test);
				
					verifyElementTextContains(product.searchfield,data.get("ProductName").trim() ,test);
			assertTrue(isElementExisting(driver,product.shoesverification),"User searched for shoes.", test);

			//driver.switchTo().window("0");
			//wait(1000);
			//product.searchfield.clear();
					

			}
			
			
		else {
			test.log(Status.SKIP, "Skipping the testcase because ProductName is Empty");
		 }
		
	}
	
	 catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	

	} catch (Exception e) {
		
		
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	
	} 
	finally
	{
		reports.flush();
		//driver.quit();
		
	}
	}





	  //assertTrue(isElementExisting(driver, login.SuccessfullLogin),"User has been successfully logged in.", test);

	  //verifyContains(login.videoVerify.getAttribute("type"), "video/mp4","Video is present here", test);



	@Test(enabled = false, priority = 4,retryAnalyzer=RetryAnalyzer.class)
	public void langChange() throws Exception {

		try {
			test = reports.createTest("Verifying Login page without Credentials .");
			test.assignCategory("smoke");

			driver=driverFact.getEventDriver(webdriver,test);

		
		LoginPage login = new LoginPage(driver);
		login.loginWithNoCredentials();
			

			
			verifyElementTextContains(login.Loginlink, "Login", test);
			 assertTrue(isElementExisting(driver, login.submit_button),"LogIn button is present", test);
			  assertTrue(isElementExisting(driver, login.invalidCredsMsgUserName)," 'Please enter valid Email ID/Mobile number' Error Message Displayed.", test);
			  assertTrue(isElementExisting(driver, login.SuccessfullLogin),"User has been successfully logged in.", test);
			
		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally {
			reports.flush();
			//driver.close();
		}
	}
}

	