/**
 * 
 */
package regression.pit.tests;


import static driverfactory.Driver.checkFileDownloaded;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import driverfactory.Driver;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_DetailedResultsPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import pages.pit.PIT_ResultsPage;
import utilities.DataReaderMap;
import utilities.InitTests;
import verify.SoftAssertions;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForPageLoad;

/**
 * @author pavankumar-hegde
 *
 */
public class SC_006_DetailedTaxResults extends InitTests {
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	private PIT_InputPage pIT_InputPage_Obj=null;
	private PIT_ResultsPage pIT_ResultsPage=null;
	public SC_006_DetailedTaxResults(String appName) {

		super(appName);
	}
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFOREMETHOD****************");
		System.out.println("Executing TC6");
		driverFact=new Driver();
		new SC_006_DetailedTaxResults("PIT");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	@Test(priority = 1, enabled = true,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
	public void detailedTaxResults(Map<String,String> data) throws Exception {
		ExtentTest test=null;
		try {
		
			System.out.println(data.toString());
			test = reports.createTest(data.get("TestCaseName"));
			test.assignCategory("regression");
			
			if(data.get("RunMode").equalsIgnoreCase("Yes")) 
			{
				beforeMethod();
				driver=driverFact.getEventDriver(webdriver,test);
				mE_LoginPage = new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
			assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 15),"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),data.get("AccountCode").trim() );
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			else
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink_Prod, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			pIT_HomePage=mE_DashboardPage.accessPIT(driver);
			delay(6000);
			verifyElementTextContains(PIT_HomePage.beginCaclulationBtn, "Begin Your Calculation", test);
			pIT_InputPage_Obj=pIT_HomePage.accessInputPage(data.get("Country"),driver);
			delay(3000);
			//verifyElementTextContains(PIT_InputPage.viewResultsBtn, "View Results", test);
			assertTrue(driverFact.isElementExisting(driver, PIT_InputPage.salaryInput, 10),"User is on Input page ", test);
			PIT_InputPage.selectTaxType(data.get("TaxType"));
			PIT_InputPage.selectTaxYear(data.get("TaxYear"));
			PIT_InputPage.selectGivenSalary(data.get("GivenSalary"));
			pIT_InputPage_Obj.setSalary(data.get("AnnualSalary"));
			PIT_InputPage.selectCurrency(data.get("Currency"));
			delay(2000);
			 if(!data.get("ExchangeRate").equals("NA")) {
				 PIT_InputPage.selectExchangeRate(data.get("ExchangeRate"));
			 }
			
			PIT_InputPage.selectMarritalStatusDropdown(data.get("MarritalStatus"));
			PIT_InputPage.clickChoosetoseeDetailedReportIconBtn();
			/*********************Special option section********************************************************/
			if(data.get("IncludeSS").equalsIgnoreCase("No")) {
				PIT_InputPage.clickIncludeSocialSecurity();
			}
			if(data.get("IncludeFA").equalsIgnoreCase("No")) {
				PIT_InputPage.clickIncludeFamilyAllowance();
			}
			pIT_InputPage_Obj.goToDetailedTaxResultsPage(driver);
			
			Driver.switchToWindowWithURL(driver,"DetailedResult");
			verifyElementContains(PIT_DetailedResultsPage.getTableElement("REGIONAL"), data.get("ExpRegional"), "REGIONAL", test);
			verifyElementContains(PIT_DetailedResultsPage.getTableElement("RESIDENCE STATUS"), data.get("ExpResidenceStatus"), "REGIONAL", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("Gross Income"), data.get("ExpGrossIncome").trim(),"Total Tax and Social Security", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("Total Tax and Social Security"), data.get("ExpTotalTaxAndSS").trim(),"Total Tax and Social Security", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("Total Employee Social Security"), data.get("ExpSS").trim(),"Total Employee Social Security", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("Family Allowance"), data.get("ExpFA").trim(),"Total Tax and Social Security", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("NET INCOME (Including Family Allowances)"), data.get("NI_IncludingFA"),"NET INCOME (Including Family Allowances)", test);
			verifyElementText(PIT_DetailedResultsPage.getValue("Total Employers' Cost"), data.get("TotalEmployerCost").trim(),"Total Employers' Cost", test);
			
			
			// File download Test
			if(data.get("Export").trim().equals("Yes")) {
				pIT_ResultsPage= new PIT_ResultsPage(driver);
				assertFalse(PIT_ResultsPage.exportBtn.isEnabled(), "Check By default Export button disabled ", test);
				PIT_ResultsPage.downloadPDF();
				String filePath= System.getProperty("user.home")+"\\Downloads";
				String fileName= "Mercer Detailed Personal Income Tax Results.pdf";
			SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName, test), "Returns true if file is downloaded successfully", test);
			PIT_ResultsPage.downloadExcel();
			 fileName= "Mercer Detailed Personal Income Tax Results.xlsx";
			 SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName,test), "Returns true if file is downloaded successfully", test);
			}
			
			
			}
			else {test.log(Status.SKIP, "Skipping the testcase because Run mode is NO");
			// throw new SkipException("Skipping the testcase because Run mode is NO");
			 }
			
		}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		} 
	finally
	{
		reports.flush();
		driver.quit();
	}
}

@AfterTest
public void tearDown() {
	killBrowserExe(BROWSER_TYPE);
}
}
