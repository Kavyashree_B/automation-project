/**
 * 
 */
package regression.pit.tests;


import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;
import static driverfactory.Driver.*;

import java.io.IOException;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import listeners.EventListner;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import pages.pit.PIT_InputPage_ATC_TDCSection;
import utilities.DataReaderMap;
import utilities.InitTests;
import verify.SoftAssertions;
import static pages.pit.InputPageValidationMessage.*;
/**
 * @author pavankumar-hegde
 *
 */
public class SC_008_InputPageFieldValidation extends InitTests {
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	private ME_LoginPage mE_LoginPage = null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage = null;
	private ME_DashboardPage mE_DashboardPage = null;
	private PIT_HomePage pIT_HomePage = null;
	private PIT_InputPage pIT_InputPage_Obj=null;
	private PIT_InputPage_ATC_TDCSection pIT_InputPage_ATC_TDCSection_Obj= null;
	
	public SC_008_InputPageFieldValidation(String appName) {
		super(appName);
	}
		
	@BeforeClass
	public void beforeClass() throws Exception
	{
		driverFact=new Driver();
		new SC_008_InputPageFieldValidation("PIT");
		System.out.println("Executing TC8");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
	}
	@Test(enabled = true,priority=1 )
	public void annualSalaryDetails() throws IOException {
		ExtentTest test=null;
		
		try {
			test = reports.createTest("SC_008_TC01_InputPage > Field Validation >Annual Salary Details");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			mE_LoginPage = new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage = mE_LoginPage.doLogin();
			assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 15),
					"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage = mE_ClientAccountSelectionPage.selectClientAccount("Mercer                                  ","97216906");
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			else
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink_Prod, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			pIT_HomePage = mE_DashboardPage.accessPIT(driver);
			verifyElementTextContains(PIT_HomePage.beginCaclulationBtn, "Begin Your Calculation", test);
			pIT_InputPage_Obj=pIT_HomePage.accessInputPage("India",driver);
			verifyElementTextContains(PIT_InputPage.viewResultsBtn, "View Results", test);
			assertTrue(driverFact.isElementExisting(driver, PIT_InputPage.salaryInput, 10), "User is on Input page ", test);
   
			    pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getSalaryMsgElement(), Mandatory_Field_MSG, test);
				pIT_InputPage_Obj.setSalary("1234567989123");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getSalaryMsgElement(), Numeric_Field_Length_MSG, test);
				pIT_InputPage_Obj.setSalary("12345679.89123");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getSalaryMsgElement(), Numeric_Field_Length_MSG, test);
				pIT_InputPage_Obj.setSalary("Abc#");
				pIT_InputPage_Obj.clickViewResultsBtn();
				System.out.println("End of Test Run : 1");
			
				
				
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
		}
	
	}

	@Test(priority=2 ,enabled = true)
	public void bonus() throws IOException {
		ExtentTest test=null;
		try {
			
			test = reports.createTest("SC_008_TC02_InputPage > Field Validation > Bonus");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage_Obj=new PIT_InputPage(driver);
			pIT_InputPage_Obj.setSalary("50,000");
				PIT_InputPage.setBonus("Fixed Amount", "546123456123456");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Numeric_Field_Length_MSG, test);
				PIT_InputPage.setBonus("Fixed Amount", "gddjh^%");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Numeric_Field_Length_MSG, test);
				PIT_InputPage.setBonus("Fixed Amount", "256.458632");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Numeric_Field_Length_MSG, test);
				
				PIT_InputPage.setBonus("% of Gross", "65464564545644566");
				pIT_InputPage_Obj.clickViewResultsBtn();
				//PIT_InputPage.optionalTitleLabel.click();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Decimal_Field_Length_MSG, test);
				PIT_InputPage.setBonus("% of Gross", "45565656.789");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Decimal_Field_Length_MSG, test);
				PIT_InputPage.setBonus("% of Gross", "45hhkdlds");
				 pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getBonusMsgElement(), Decimal_Field_Length_MSG, test);
				System.out.println("End of Test Run : 2");
				
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
		}
	
	}
	
	@Test(priority=3 ,enabled = true)
	public void exchangeRate() throws IOException {
		ExtentTest test=null;
		try {
		
			test = reports.createTest("SC_008_TC03_InputPage > Field Validation > Exchange Rate");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage_Obj=new PIT_InputPage(driver);
			switchToWindowWithURL(driver,"Input");
			refreshpage();
				pIT_InputPage_Obj.setSalary("50,000");
				PIT_InputPage.setBonus("Fixed Amount", "5000");
				PIT_InputPage.selectCurrency("USD");
				delay(2000);
				PIT_InputPage.selectExchangeRate("custom: ");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getExchangeRateMsgElement(), Mandatory_Field_MSG, test);
				PIT_InputPage.selectExchangeRate("custom:454565.45678952");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getExchangeRateMsgElement(), ExchangeRate_Field_MSG, test);
				
				PIT_InputPage.selectExchangeRate("custom:45456545665645465.456789");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getExchangeRateMsgElement(), ExchangeRate_Field_MSG, test);
				
				PIT_InputPage.selectExchangeRate("custom:4hh#$456");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getExchangeRateMsgElement(), Mandatory_Field_MSG, test);
				System.out.println("End of Test Run : 3");
				
				
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
		}
	}
	
	
	
	
	@Test(priority=4 ,enabled = true)
	public void taxableAllowance() throws IOException {
		ExtentTest test=null;
		try {
			
			test = reports.createTest("SC_008_TC04_InputPage > Field Validation > Taxable Allowance");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage_Obj=new PIT_InputPage(driver);
			switchToWindowWithURL(driver,"Input");
			refreshpage();
			driverFact.waitForPageLoad(driver);
			//driver.switchTo().alert().accept();
			
			
				pIT_InputPage_Obj.setSalary("50,000");
				pIT_InputPage_ATC_TDCSection_Obj=pIT_InputPage_Obj.initATCTDCSectionPage(driver);
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection(" :Fixed Amount:5000200000066", "TaxableAllowance");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxableAllowanceLabelMsgElement(), Mandatory_Field_MSG,"TaxableAllowance Label", test);
				verifyElementText(PIT_InputPage.getTaxableAllowanceValueMsgElement(),Numeric_Field_Length_MSG,"TaxableAllowance Value", test);
				
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection("xxxx:% of Gross:5000200000066.9885", "TaxableAllowance");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxableAllowanceValueMsgElement(),Decimal_Field_Length_MSG,"TaxableAllowance Value", test);
				
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection(" :% of Gross:50.988845654546", "TaxableAllowance");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxableAllowanceValueMsgElement(),Decimal_Field_Length_MSG,"TaxableAllowance Value", test);
				System.out.println("End of Test Run : 4");
				
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		
	
	}
	@Test(priority=5 ,enabled = true)
	public void taxDeductibleContributions() throws IOException {
		ExtentTest test=null;
		try {
			
			test = reports.createTest("SC_008_TC05_InputPage > Field Validation > Tax Deductible Contributions");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage_Obj=new PIT_InputPage(driver);
			switchToWindowWithURL(driver,"Input");
			refreshpage();
			driverFact.waitForPageLoad(driver);
			//driver.switchTo().alert().accept();
			
			
				pIT_InputPage_Obj.setSalary("50,000");
				pIT_InputPage_ATC_TDCSection_Obj=pIT_InputPage_Obj.initATCTDCSectionPage(driver);
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection(" :Fixed Amount:5000200000066", "TaxDeductible");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxDeductibleLabelMsgElement(), Mandatory_Field_MSG,"TaxDeductible Label", test);
				verifyElementText(PIT_InputPage.getTaxDeductibleValueMsgElement(),Numeric_Field_Length_MSG,"TaxDeductible Value", test);
				
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection("xxxx:% of Gross:5000200000066.9885", "TaxDeductible");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxDeductibleValueMsgElement(),Decimal_Field_Length_MSG,"TaxDeductible Value", test);
				
				pIT_InputPage_ATC_TDCSection_Obj.inputATC_TDCSection(" :% of Gross:50.988845654546", "TaxDeductible");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getTaxDeductibleValueMsgElement(),Decimal_Field_Length_MSG,"TaxDeductible Value", test);
				System.out.println("End of Test Run : 5");
				
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		
		finally
		{
			reports.flush();
		}
	}
	
	@Test(priority=6 ,enabled = true)
	public void additionalCompensation() throws IOException {
		ExtentTest test=null;
		try {
			
			test = reports.createTest("SC_008_TC06_InputPage > Field Validation > Additional Compensation to include in Gross Up");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage_Obj=new PIT_InputPage(driver);
			switchToWindowWithURL(driver,"Input");
			refreshpage();
			driverFact.waitForPageLoad(driver);
			//driver.switchTo().alert().accept();
			
			
				pIT_InputPage_Obj.setSalary("50,000");
				PIT_InputPage.selectGivenSalary("Net");
				pIT_InputPage_Obj.enterAdditionalCompensation(":1234567891234:No:No;");
				PIT_InputPage.viewResultsBtn.click();
				verifyElementText(PIT_InputPage.getAdditionalCompensationLabelMsgElement(), Mandatory_Field_MSG,"AdditionalCompensation Label = Blank ", test);
				verifyElementText(PIT_InputPage.getAdditionalCompensationLValueMsgElement(),Numeric_Field_Length_MSG,"AdditionalCompensation Value = 13 digit value", test);
				refreshpage();
				driverFact.waitForPageLoad(driver);
				pIT_InputPage_Obj.setSalary("50,000");
				PIT_InputPage.selectGivenSalary("Net");
				pIT_InputPage_Obj.enterAdditionalCompensation("aaaa:5000:No:No;aaaa:5000.456:Yes:Yes;");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getAdditionalCompensationLValueMsgElement(),Numeric_Field_Length_MSG,"AdditionalCompensation Value = Decimal", test);
				refreshpage();
				driverFact.waitForPageLoad(driver);
				pIT_InputPage_Obj.setSalary("50,000");
				PIT_InputPage.selectGivenSalary("Net");
				pIT_InputPage_Obj.enterAdditionalCompensation("aaaa:5000800000009:No:No;:50000:Yes:Yes;:50000:No:Yes;");
				pIT_InputPage_Obj.clickViewResultsBtn();
				verifyElementText(PIT_InputPage.getAdditionalCompensationLValueMsgElement(),Numeric_Field_Length_MSG,"Multiple rows of AdditionalCompensation Value = More than 12 digit", test);
				verifyElementText(PIT_InputPage.getAdditionalCompensationLabelMsgElement(), Mandatory_Field_MSG,"AdditionalCompensation Label = Blank ", test);
				System.out.println("End of Test Run : 6");
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		
		} 
		finally
		{
			reports.flush();
			driver.quit();
		}
	
	}
	
	@AfterTest
	public void tearDown() {
		reports.flush();
		driver.quit();
		killBrowserExe(BROWSER_TYPE);

	}
	
}
