package regression.pit.tests;

import static driverfactory.Driver.checkFileDownloaded;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowWithURL;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;
import static pages.pit.PIT_ResultsPage.*;

import java.io.IOException;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import driverfactory.Driver;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import pages.pit.PIT_ResultsPage;
import utilities.DataReaderMap;
import utilities.InitTests;
import verify.SoftAssertions;

public class SC_007_NetToGross3rdCurrency extends InitTests{

	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	private ME_LoginPage mE_LoginPage = null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage = null;
	private ME_DashboardPage mE_DashboardPage = null;
	private PIT_HomePage pIT_HomePage = null;
	private PIT_InputPage pIT_InputPage_Obj=null;
	private PIT_ResultsPage pIT_ResultsPage_Obj=null;
	SC_007_NetToGross3rdCurrency(String appName) {
		super(appName);
	}

	//@BeforeMethod
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFOREMETHOD****************");
		System.out.println("Executing TC7");
		driverFact=new Driver();
		new SC_007_NetToGross3rdCurrency("PIT");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	
	
	@Test(enabled = true, dataProvider = "getDataFromExcel", dataProviderClass = DataReaderMap.class)
	public void netToGross3rdCurrency(Map<String, String> data) throws IOException {
		ExtentTest test=null;
		try {

			System.out.println(data.toString());
			test = reports.createTest(data.get("TestCaseName"));
			test.assignCategory("regression");
			
			if (data.get("RunMode").equalsIgnoreCase("Yes")) {
				beforeMethod();
				driver=driverFact.getEventDriver(webdriver,test);
				mE_LoginPage = new ME_LoginPage(driver);
				mE_ClientAccountSelectionPage = mE_LoginPage.doLogin();
				assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 15),
						"User logged in to the application & able to view Client-Account selection page ", test);
				mE_DashboardPage = mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),
						data.get("AccountCode").trim());
				if((System.getProperty("env")).equalsIgnoreCase("QA") )
				{
					assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
				}
				else
				{
					assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink_Prod, 5),"User is on Mobility Exchange Dashboard page ", test);
				}
				pIT_HomePage = mE_DashboardPage.accessPIT(driver);
				waitForPageLoad(driver);
				delay(4000);
				verifyElementTextContains(PIT_HomePage.beginCaclulationBtn, "Begin Your Calculation", test);
				pIT_InputPage_Obj=pIT_HomePage.accessInputPage(data.get("Country"),driver);
				//verifyElementTextContains(PIT_InputPage.viewResultsBtn, "View Results", test);
				
				//ON INPUT PAGE
				PIT_InputPage.selectTaxType(data.get("TaxType").trim());
				PIT_InputPage.selectTaxYear(data.get("TaxYear"));
				PIT_InputPage.selectGivenSalary(data.get("GivenSalary"));
				pIT_InputPage_Obj.setSalary(data.get("AnnualSalary"));
				if (!data.get("BonusType").equalsIgnoreCase("NA")) {
					PIT_InputPage.setBonus(data.get("BonusType"), data.get("BonusValue"));
				}
				PIT_InputPage.selectCurrency(data.get("Currency"));
				delay(3000);
				PIT_InputPage.selectExchangeRate(data.get("ExchangeRate"));
				PIT_InputPage.selectMarritalStatusDropdown(data.get("MarritalStatus"));

				if (!data.get("AdditionalCompensation").equalsIgnoreCase("NA")) {
					pIT_InputPage_Obj.enterAdditionalCompensation(data.get("AdditionalCompensation"));
				}
				if (data.get("IncludeSMA").equalsIgnoreCase("Yes")) {
					PIT_InputPage.selectSMAcheckbox();
				}
/*********************Special option section********************************************************/
				if(data.get("IncludeSS").equalsIgnoreCase("No")) {
					PIT_InputPage.clickIncludeSocialSecurity();
				}
				if(data.get("IncludeFA").equalsIgnoreCase("No")) {
					PIT_InputPage.clickIncludeFamilyAllowance();
				}
				
				pIT_ResultsPage_Obj=pIT_InputPage_Obj.clickViewResultsButton(driver);
				switchToWindowWithURL(driver,"SimpleResult");

				verifyElementTextContains(PIT_ResultsPage.headerLabel, "Personal Tax Calculation Results", test);
				verifyElementText(get2ndColumnElement("Exchange Rate"), data.get("ExpExchangeRate").trim(), "Exchange Rate", test);
				verifyElementContains(exchangeRateType, data.get("ExpExchangeRateType").trim(), "Exchange Rate Type", test);
				verifyElementText(get3rdColumnElement("Bonus"), data.get("ExpBonus").trim(), " Bonus ", test);
				// if AC is included 
				if(!data.get("AdditionalCompensation").equalsIgnoreCase("NA")) {
					assertTrue(driverFact.isElementExisting(driver, get1stColumnElement("Additional Compensation to include in Gross Up"), 15), "Additional Compensation to include in Gross Up", test);
					verifyElementText(get3rdColumnElement("Net Income (including taxable allowances)"), data.get("ExpNetIncome").trim(), " Net Income ", test);
				}
				else {
					
				verifyElementText(get3rdColumnElement("Net Income"), data.get("ExpNetIncome").trim(), " Net Income ", test);
			
				}
				
				verifyElementText(get3rdColumnElement("Total Personal Income Tax"), data.get("ExpTotalTax").trim(), " Total Tax ", test);
				verifyElementText(get3rdColumnElement("Gross Income"), data.get("ExpGrossIncome").trim(), " Net Income ", test);
				verifyElementText(get3rdColumnElement("Social Security Contributions"), data.get("ExpSS").trim(), " Social Security ", test);
				verifyElementText(get3rdColumnElement("Family Allowance"), data.get("ExpFA").trim(), " Family Allowance ", test);
				verifyElementText(get3rdColumnElement("Estimated Employer Contributions"), data.get("EstimatedEmployerContributions"), " Estimated Employer Contributions ", test);
				verifyElementText(PIT_ResultsPage.taxAsPercentageOfGrossIncome,
						data.get("TaxAsPercentageOfGrossIncome").trim(), "TaxAsPercentageOfGrossIncome", test);
				verifyElementText(PIT_ResultsPage.taxAndSocialSecurityAsPercentageofGrossIncome,
						data.get("TaxAndSocialSecurityAsPercentageofGrossIncome").trim(),
						"TaxAndSocialSecurityAsPercentageofGrossIncome", test);
				verifyElementText(PIT_ResultsPage.marginalRateTax, data.get("MarginalRateTax").trim(),
						"MarginalRateTax", test);
				verifyElementText(PIT_ResultsPage.marginalRateTaxAndSocialSecurity,
						data.get("MarginalRateTaxAndSocialSecurity").trim(), "Marginal Rate Tax And SocialSecurity",
						test);
				// File download Test
				if(data.get("Download").trim().equals("Yes")) {
					assertFalse(PIT_ResultsPage.exportBtn.isEnabled(), "Check By default Export button disabled ", test);
					PIT_ResultsPage.downloadPDF();
					String filePath= System.getProperty("user.home")+"\\Downloads";
					String fileName= "Mercer Simple Personal Income Tax Results.pdf";
				SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName, test), "Returns true if file is downloaded successfully", test);
				PIT_ResultsPage.downloadExcel();
				 fileName= "Mercer Simple Personal Income Tax Results.xlsx";
				 SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName,test), "Returns true if file is downloaded successfully", test);
				}
				
				
				
			} else {
				test.log(Status.SKIP, "Skipping the testcase because Run mode is NO");
				// throw new SkipException("Skipping the testcase because Run mode is NO");
			}

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);

		} finally {
			reports.flush();
			driver.quit();

		}

	}

	@AfterTest
	public void tearDown() {

		killBrowserExe(BROWSER_TYPE);
	}
}
	
	
	
	
	

