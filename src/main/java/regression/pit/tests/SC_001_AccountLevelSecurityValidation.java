package regression.pit.tests;


import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;

import static verify.SoftAssertions.*;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;

import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import utilities.DataReaderMap;
import utilities.InitTests;
import verify.SoftAssertions;
import verify.SoftAssertions.*;

public class SC_001_AccountLevelSecurityValidation extends InitTests {
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	
	public SC_001_AccountLevelSecurityValidation(String appName) {
		super(appName);
	} 
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFOREMETHOD****************");
		System.out.println("Executing TC1");
		driverFact=new Driver();
		new SC_001_AccountLevelSecurityValidation("PIT");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	
	@Test(priority = 1, enabled = true,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
	public void verifyAccountLevelSecurity(Map<String,String> data) throws Exception {
		ExtentTest test=null;
		try {
			test = reports.createTest(data.get("TestCaseName"));
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			if(data.get("RunMode").equalsIgnoreCase("Yes")) 
			{
			mE_LoginPage= new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
			assertTrue(Driver.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 15),"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),data.get("AccountCode").trim() );
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			else
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink_Prod, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			pIT_HomePage=mE_DashboardPage.accessPIT(driver);
			PIT_HomePage.clickOnBannerEmail();
			verifyElementTextContains(PIT_HomePage.clientName,data.get("Client").trim() ,test);
			verifyElementTextContains(PIT_HomePage.accountName,data.get("AccountCode").trim() ,test);
			if(data.get("TestCaseName").contains("ICS")) {
				assertTrue(PIT_HomePage.getCountryDropdownValues().contains("United States - US Average"), "Return true if ICS path account ="+data.get("AccountCode")  +"has Average cities", test);
				
			}
			if(data.get("TestCaseName").contains("GHRM")) {
				assertFalse(PIT_HomePage.getCountryDropdownValues().contains("United States - US Average"), "Return false if GHRM path account  ="+data.get("AccountCode")  +" does not have Average cities", test);
				
			}
							}
			
			} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			driver.quit();
		

		}
	}


@AfterTest
public void tearDown() {
	killBrowserExe(BROWSER_TYPE);
}
}
