package regression.pit.tests;


import static driverfactory.Driver.checkFileDownloaded;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;

import static verify.SoftAssertions.*;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import pages.pit.PIT_InputPage_ATC_TDCSection;
import pages.pit.PIT_ResultsPage;
import utilities.DataReaderMap;
import utilities.InitTests;
import verify.SoftAssertions;

public class SC_002_GrossToNetCalculation extends InitTests {
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	private PIT_InputPage pIT_InputPage = null;
	private PIT_InputPage_ATC_TDCSection pIT_InputPage_ATC_TDCSection=null;
	private PIT_ResultsPage pIT_ResultsPage=null;
	public SC_002_GrossToNetCalculation(String appName) {
		super(appName);
	} 
	
	public void beforeMethod() throws Exception
	{ 
		System.out.println("*****Inside BEFORE METHOD****************");
		System.out.println("Executing TC2");
		driverFact=new Driver();
		new SC_002_GrossToNetCalculation("PIT");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	
	@Test(priority = 1, enabled = true,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
	public void grossToNetCalculation(Map<String,String> data) throws Exception {
		ExtentTest test=null;
		try {
			System.out.println(data.toString());
			test = reports.createTest(data.get("TestCaseName"));
			test.assignCategory("regression");
			
			
			if(data.get("RunMode").equalsIgnoreCase("Yes")) 
			{
				beforeMethod();
				driver=driverFact.getEventDriver(webdriver,test);
				mE_LoginPage = new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
			assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 5),"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),data.get("AccountCode").trim() );
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			else
			{
				assertTrue(driverFact.isElementExisting(driver, ME_DashboardPage.PITLink_Prod, 5),"User is on Mobility Exchange Dashboard page ", test);
			}
			
			pIT_HomePage=mE_DashboardPage.accessPIT(driver);
			verifyElementTextContains(PIT_HomePage.beginCaclulationBtn, "Begin Your Calculation", test);
			pIT_InputPage=pIT_HomePage.accessInputPage(data.get("Country"),driver);
			waitForPageLoad(driver);
			delay(5000);
			//verifyElementTextContains(PIT_InputPage.viewResultsBtn, "View Results", test);
			assertTrue(driverFact.isElementExisting(driver, PIT_InputPage.salaryInput, 10),"User is on Input page ", test);
			PIT_InputPage.selectTaxType(data.get("TaxType"));
			PIT_InputPage.selectTaxYear(data.get("TaxYear"));
			delay(4000);
			pIT_InputPage.setSalary(data.get("AnnualSalary"));
			if (!data.get("BonusType").equalsIgnoreCase("NA")) {
				PIT_InputPage.setBonus(data.get("BonusType"), data.get("BonusValue"));
			}
			PIT_InputPage.selectCurrency(data.get("Currency"));
			PIT_InputPage.selectMarritalStatusDropdown(data.get("MarritalStatus"));
			

/*********************Special option section********************************************************/
			if (data.get("IncludeSMA").equalsIgnoreCase("Yes")) {
				PIT_InputPage.selectSMAcheckbox();
			}
			assertTrue(PIT_InputPage.includeFamilyAllowanceCheckBox.isSelected(), "Returns true if Include Family allowance is CHECKED by default", test);
			assertTrue(PIT_InputPage.includeFamilyAllowanceCheckBox.isSelected(), "Returns true if Include Social Security is CHECKED by default", test);
			if(data.get("IncludeSS").equalsIgnoreCase("No")) {
				PIT_InputPage.clickIncludeSocialSecurity();
			}
			if(data.get("IncludeFA").equalsIgnoreCase("No")) {
				PIT_InputPage.clickIncludeFamilyAllowance();
			}
			
			//initialize ATCTDC section 
			pIT_InputPage_ATC_TDCSection=pIT_InputPage.initATCTDCSectionPage(driver);
			//type = TaxDeductible or TaxableAllowance
			if(!data.get("TaxableAllowance").equals("NA")){
			pIT_InputPage_ATC_TDCSection.inputATC_TDCSection(data.get("TaxableAllowance"),"TaxableAllowance");
			}
			if(!data.get("TaxDeductibleContribution").equals("NA")) {
			pIT_InputPage_ATC_TDCSection.inputATC_TDCSection(data.get("TaxDeductibleContribution"),"TaxDeductible");
			}
			
			if(data.get("IncludeInSSC").equalsIgnoreCase("No")) {
				pIT_InputPage.selectIncludeinSocialSecurityCalculation_Dropdown("No");
			}
			
			
			pIT_ResultsPage=pIT_InputPage.clickViewResultsButton(driver);
			waitForPageLoad(driver);
			switchToWindowWithURL(driver,"SimpleResult");
			
				if (data.get("TaxType").trim().equals("Expatriate")) {
					verifyElementText(PIT_ResultsPage.countryMarketName, data.get("Country").trim() + " (Expatriate)",
							test);
				} else
			verifyElementText(PIT_ResultsPage.countryMarketName, data.get("Country").trim(), test);
			
			verifyElementTextContains(PIT_ResultsPage.headerLabel, "Personal Tax Calculation Results",test);
			verifyElementTextContains(PIT_ResultsPage.annualSalaryDetailsLabel, "(Gross)",test);
			verifyElementText(PIT_ResultsPage.netIncome, data.get("ExpNetIncome").trim(),"Net Income", test);
			verifyElementText(PIT_ResultsPage.totalIncomeTax, data.get("ExpTotalTax").trim(),"Total Income Tax", test);
			verifyElementText(PIT_ResultsPage.socialSecurity, data.get("ExpSS").trim(),"Social Security", test);
			verifyElementText(PIT_ResultsPage.familyAllowance, data.get("ExpFA").trim(),"Family Allowance", test);
			verifyElementText(PIT_ResultsPage.estimatedEmployerContributions, data.get("EstimatedEmployerContributions").trim(),"Estimated Employer Contribution",test);
			verifyElementText(PIT_ResultsPage.taxAsPercentageOfGrossIncome, data.get("TaxAsPercentageOfGrossIncome").trim(),"Tax As Percentage Of GrossIncome", test);
			verifyElementText(PIT_ResultsPage.taxAndSocialSecurityAsPercentageofGrossIncome	, data.get("TaxAndSocialSecurityAsPercentageofGrossIncome").trim(), "TaxAndSocialSecurityAsPercentageofGrossIncome",test);
			verifyElementText(PIT_ResultsPage.marginalRateTax, data.get("MarginalRateTax").trim(),"Marginal Rate Tax", test);
			verifyElementText(PIT_ResultsPage.marginalRateTaxAndSocialSecurity, data.get("MarginalRateTaxAndSocialSecurity").trim(),"Marginal Rate Tax And SocialSecurity", test);
						
			// File download Test
			if(data.get("Export").trim().equals("Yes")) {
				assertFalse(PIT_ResultsPage.exportBtn.isEnabled(), "Check By default Export button disabled ", test);
				PIT_ResultsPage.downloadPDF();
				String filePath= System.getProperty("user.home")+"\\Downloads";
				String fileName= "Mercer Simple Personal Income Tax Results.pdf";
			SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName,test), "Returns true if file is downloaded successfully", test);
			PIT_ResultsPage.downloadExcel();
			 fileName= "Mercer Simple Personal Income Tax Results.xlsx";
			 SoftAssertions.assertTrue(checkFileDownloaded(filePath, fileName,test), "Returns true if file is downloaded successfully", test);
			}
				
			}
			else {
				test.log(Status.SKIP, "Skipping the testcase because Run mode is NO");
			// throw new SkipException("Skipping the testcase because Run mode is NO");
			 }
			
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		
		} 
		finally
		{
			reports.flush();
			driver.quit();
			

		}
	}

	@AfterTest
	public void tearDown() {
		
		killBrowserExe(BROWSER_TYPE);
	}
}
