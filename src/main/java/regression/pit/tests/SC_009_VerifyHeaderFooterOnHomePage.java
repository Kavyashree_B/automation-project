package regression.pit.tests;

import static driverfactory.Driver.delay;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.waitForPageLoad;
import static pages.pit.PIT_HomePage.accountName;
import static pages.pit.PIT_HomePage.appTitle;
import static pages.pit.PIT_HomePage.bannerEmail;
import static pages.pit.PIT_HomePage.clientName;
import static pages.pit.PIT_HomePage.logoText;
import static pages.pit.PIT_HomePage.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyContains;
import static verify.SoftAssertions.verifyElementHyperLink;
import static verify.SoftAssertions.verifyElementLink;
import static verify.SoftAssertions.verifyElementListTextIgnoreCase;
import static verify.SoftAssertions.verifyElementTextContains;

import java.util.List;


import org.openqa.selenium.WebDriver;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import driverfactory.Driver;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import pages.pit.PIT_ResultsPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class SC_009_VerifyHeaderFooterOnHomePage extends InitTests {
	
	
	static Driver driverFact;
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	static String filePath= System.getProperty("user.home")+"\\Downloads";
	String expectedViewHelpFileName=null;
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	private PIT_InputPage pIT_InputPage=null;
	private PIT_ResultsPage pIT_ResultsPage=null;
	
	final String expectedTopNavBarLinks[]= {
			"ABOUT PERSONAL INCOME TAX",
			"OTHER RELATED SERVICES",
			"CONTACT MERCER"
			};
	final String expectedAboutPITLinks[]= {
			"Product Description",
			"Product Demonstration"
			};
	String expectedOtherRelatedServicesLinks[]= {
			"Mobility Services"
			};
	public SC_009_VerifyHeaderFooterOnHomePage(String appName) {
		super(appName);
	} 
	
	@BeforeClass
	public void beforeClass() throws Exception
	{
		driverFact=new Driver();
		new SC_009_VerifyHeaderFooterOnHomePage("PIT");
		System.out.println("Executing TC9");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	
	@Test( enabled = true)
	public void headerFooterOnHomePage() throws Exception {
		ExtentTest test=null;
		
		
		try {
			test = reports.createTest("SC_009_VerifyHeaderFooterOnHomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			mE_LoginPage= new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
			assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo,15),"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount("Mercer                                  ","97216906" );
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				waitForPageLoad(driver);
				delay(2000);
				verifyElementHyperLink(ME_DashboardPage.PITLink, test);
			}
			else
			{
				waitForPageLoad(driver);
				delay(2000);
				verifyElementHyperLink(ME_DashboardPage.PITLink_Prod, test);
			}
			pIT_HomePage=mE_DashboardPage.accessPIT(driver);
			
			verifyElementTextContains(bannerEmail,USERNAME,test);
			clickOnBannerEmail();
			verifyElementTextContains(clientName,"MERCER" ,test);
			verifyElementTextContains(accountName,"97216906" ,test);
		    verifyElementTextContains(appTitle,"PERSONAL INCOME TAX" ,test);	
			verifyElementTextContains(logoText,"Mobilize" ,test);
			verifyElementListTextIgnoreCase(topNavBarLinks, expectedTopNavBarLinks, test);
			//ABOUT PERSONAL INCOME TAX
			clickAboutPITLink();
			verifyElementListTextIgnoreCase(PIT_HomePage.aboutPITLinks, expectedAboutPITLinks, test);
			/*clickAboutPITLinks("Product Description");
			expectedViewHelpFileName="Personal Income Tax Solution.pdf";
			assertTrue(Driver.checkFileDownloaded(filePath, expectedViewHelpFileName,test),"Returns true if file is downloaded successfully --->"+expectedViewHelpFileName,test);*/
			//clickAboutPITLink();
			clickAboutPITLinks("Product Demonstration");
			driverFact.waitForPageLoad(driver);
			switchToWindowWithURL(driver, "CalculationDemo");
			verifyContains(driver.getCurrentUrl().trim(), "Home/CalculationDemo", "Verify iMercer Page is displayed ", test);
			driver.close();
			// go back to home page
			switchToWindowWithURL(driver, "PersonalIncomeTax");
			pIT_HomePage.clickOnContactMercer();
			verifyContains(driver.getCurrentUrl(), BASEURL+"support", "Verify Contact Mercer page is displayed ", test);
			driver.close();
			switchToWindowWithURL(driver, "PersonalIncomeTax");
			//OtherServices
			clickOnOtherServicesLink();
			verifyElementListTextIgnoreCase(otherRelatedServicesinks, expectedOtherRelatedServicesLinks, test);
			clickOtherRelatedServices("Mobility Services");
			driverFact.waitForPageLoad(driver);
			switchToWindowWithURL(driver, "imercer");
			verifyContains(driver.getTitle().trim(), "iMercer.com", "Verify iMercer Page is displayed ", test);
			switchToWindowWithURL(driver, "PersonalIncomeTax");
		
			//Footer
			verifyElementTextContains(PIT_HomePage.copyRightLabel, "© 2019 Mercer LLC, All Rights Reserved", test);
			String[] expectedSocialLinks= {"facebook","twitter","linkedin"};
			List<String> actualFooterSocialURLs=PIT_HomePage.getFooterSocialLinksURL();
			boolean b= false;
			for(String expectedSocialLink:expectedSocialLinks) 
			{
			  for(String actualFooterSocialURL:actualFooterSocialURLs)
			  	{
				   b= actualFooterSocialURL.contains(expectedSocialLink);
				  if(b) {
		            assertTrue(b, "Verify Footer contains link of "+expectedSocialLink , test);
		            PIT_HomePage.clickFooterSocialLinks(expectedSocialLink);
			    	switchToWindowWithURL(driver,expectedSocialLink);
			    	verifyContains(driver.getCurrentUrl(), expectedSocialLink, "Verifying "+expectedSocialLink+"link accessed successfully " , test);
					
			    	switchToWindowWithURL(driver,"PersonalIncomeTax");
					break;
				        } 
				 }
			  if(b==false)
			  assertTrue(b, "Footer does not contain Expected "+expectedSocialLink +" link & actual social URLs are = "+actualFooterSocialURLs, test);
			
			}
			
			//Return to ME
			verifyElementTextContains(returnToMobilityExchangeLink, "Return to Mobility Exchange", test);
			verifyElementLink(returnToMobilityExchangeLink, "Verifying Return to Mobility Exchange link", test);
			pIT_HomePage.clickReturnToMobilityExchange();
			verifyContains(driver.getCurrentUrl(), BASEURL+"Dashboard/MyTools", "Returned to Mobility Exchange Dashborad page", test);
		
		
			
		}
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			
			reports.flush();
			driver.quit();
			killBrowserExe(BROWSER_TYPE);

		}
	}

	
	
}
