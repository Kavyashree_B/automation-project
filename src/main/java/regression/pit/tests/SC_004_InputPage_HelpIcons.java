package regression.pit.tests;

import static driverfactory.Driver.delay;
//import static driverfactory.Driver.driver;
/*import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;*/
//import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.pit.ME_ClientAccountSelectionPage;
import pages.pit.ME_DashboardPage;
import pages.pit.ME_LoginPage;
import pages.pit.PIT_HomePage;
import pages.pit.PIT_InputPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class SC_004_InputPage_HelpIcons extends InitTests {
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage= null;
	private PIT_InputPage pIT_InputPage=null;
	String [] helpPopupContents= new String[2];
	public SC_004_InputPage_HelpIcons(String appName) {
		super(appName);
	} 
	
	@BeforeClass
	public void beforeClass() throws Exception
	{
		new SC_004_InputPage_HelpIcons("PIT");
		System.out.println("Executing TC4");
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

	}
	
	
	@Test(priority=1,enabled=true)
	public void verifyInputPageHelpIcons()  throws Exception {
		ExtentTest test=null;
		try {
			test = reports.createTest("SC_004_TC01_Input page- Help icons > Currency, TaxableAllowance, TaxDeductions, Taxable, BIK " );
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			System.out.println("BaseURL is: " + BASEURL);
			
			mE_LoginPage= new ME_LoginPage(driver);
			mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
			assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 5),"User logged in to the application & able to view Client-Account selection page ", test);
			mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount("Mercer                                  ","97216906" );
			if((System.getProperty("env")).equalsIgnoreCase("QA") )
			{
				waitForPageLoad(driver);
				delay(6000);
				verifyElementHyperLink(ME_DashboardPage.PITLink, test);
			}
			else
			{
				waitForPageLoad(driver);
				delay(2000);
				verifyElementHyperLink(ME_DashboardPage.PITLink_Prod, test);
			}
			pIT_HomePage=mE_DashboardPage.accessPIT(driver);
			waitForPageLoad(driver);
			delay(2000);
			verifyElementTextContains(PIT_HomePage.beginCaclulationBtn, "Begin Your Calculation", test);
			pIT_InputPage=pIT_HomePage.accessInputPage("India",driver);
			verifyElementTextContains(PIT_InputPage.viewResultsBtn, "View Results", test);
			waitForPageLoad(driver);
			delay(2000);
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("CurrencyHelp");
			
			verifyContains(helpPopupContents[0], "Currency", "Verify Currency help pop up header", test);
			verifyContains(helpPopupContents[1], "The chosen currency", "Verify Currency help pop up text", test);
			
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("taxAllowanceHelp");
			verifyContains(helpPopupContents[0], "Taxable Allowance", "Verify Taxable Allowance help pop up header", test);
			verifyContains(helpPopupContents[1], "To add a taxable allowance", "Verify Taxable Allowance help pop up text", test);
			
			
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("TaxDeductibleContributions");
			verifyContains(helpPopupContents[0], "Tax Deductible Contribution", "Verify TaxDeductibleContributions help pop up header", test);
			verifyContains(helpPopupContents[1], "tax deductible contribution", "Verify TaxDeductibleContributions help pop up text", test);
			
			PIT_InputPage.selectGivenSalary("Net");
			//taxable
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("TaxableHelp");
			verifyContains(helpPopupContents[0], "Taxable", "Verify checkboxes Taxable help pop up header", test);
			verifyContains(helpPopupContents[1], "Include in net income for gross up", "Verify TaxDeductibleContributions help pop up text", test);
			
			//BIK
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("BenefitofKindHelp");
			verifyContains(helpPopupContents[0], "Benefits In Kind", "Verify checkboxes Benefits In Kind help pop up header", test);
			verifyContains(helpPopupContents[1], "Non-cash element", "Verify Benefits In Kind help pop up text", test);
			// custom exchange rate
			PIT_InputPage.selectCurrency("USD");
			waitForPageLoad(driver);
			PIT_InputPage.selectExchangeRate("Custom:4.5");
			helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("CustomExchangeRate");
			verifyContains(helpPopupContents[0], "Custom Exchange Rate", "Verify Custom Exchange Rate help pop up header", test);
			verifyContains(helpPopupContents[1], "reference currency", "Verify Custom Exchange Rate help pop up text", test);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			//SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			

		}
		
	}
	@Test(priority=2,enabled=true)
	public void verifyInputPageDetailedTaxCalculationHelp() throws IOException {
		ExtentTest test=null;
		try {
			test = reports.createTest("SC_004_TC02_InputPage-Choose to see Detailed Tax Calculation Help" );
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			pIT_InputPage= new PIT_InputPage(driver);
			//Default behavior
			
		assertTrue(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify by Default Choose to see Detailed Tax Calculation is ENABLED", test);	
		assertFalse(driverFact.isElementExisting(driver, PIT_InputPage.chooseDetailedTaxCalcHelpBtn, 10), "Triangular Help Icon should NOT be displayed ", test);
		//With Bonus
		pIT_InputPage.setSalary("90000");
		PIT_InputPage.setBonus("Fixed Amount", "5000");
		PIT_InputPage.salaryLabel.click();//to take out focus 
		assertFalse(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify Choose to see Detailed Tax Calculation is DISABLED", test);	
		String [] helpPopupContents= new String[2];
		helpPopupContents=pIT_InputPage.getHelpPopUpContentsOf("ChoosetoseeDetailedReport");
		verifyContains(helpPopupContents[0], "Detailed Calculations", "Verify Detailed Calculations help pop up header", test);
		verifyContains(helpPopupContents[1], "The Detailed Calculation", "Verify The Detailed Calculation help pop up text", test);
		
		// With TA & TDC for GROSS
		PIT_InputPage.bonusInput.clear();
		pIT_InputPage.setSalary("60000");
		PIT_InputPage.selectGivenSalary("Gross");
		PIT_InputPage.salaryLabel.click();
		assertTrue(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify by Default Choose to see Detailed Tax Calculation is Emabled", test);	
		assertFalse(driverFact.isElementExisting(driver, PIT_InputPage.chooseDetailedTaxCalcHelpBtn, 10), "Triangular Help Icon should NOT be displayed ", test);
		PIT_InputPage.setTaxableAllowance("Fixed Amount", "6000");
		PIT_InputPage.salaryLabel.click();
		assertFalse(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify Choose to see Detailed Tax Calculation is DISABLED", test);
		assertTrue(driverFact.isElementExisting(driver, PIT_InputPage.chooseDetailedTaxCalcHelpBtn, 10), "Triangular Help Icon should NOT be displayed ", test);
		
		// with AC for NET
		PIT_InputPage.selectGivenSalary("Net");
		assertTrue(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify by Default Choose to see Detailed Tax Calculation is Emabled", test);	
		assertFalse(driverFact.isElementExisting(driver, PIT_InputPage.chooseDetailedTaxCalcHelpBtn, 10), "Triangular Help Icon should NOT be displayed ", test);
		pIT_InputPage.enterAdditionalCompensation("AC1:30000:Yes:No;");
		PIT_InputPage.salaryLabel.click();
		assertFalse(PIT_InputPage.chooseDetailedTaxCalcCheckBox.isEnabled(), "Verify Choose to see Detailed Tax Calculation is DISABLED", test);
		assertTrue(driverFact.isElementExisting(driver, PIT_InputPage.chooseDetailedTaxCalcHelpBtn, 10), "Triangular Help Icon should NOT be displayed ", test);
		
		}
		catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
		}
	} 
	
	@AfterTest
	public void tearDown() {
		driver.quit();
		killBrowserExe(BROWSER_TYPE);
	}
}
