package regression.pit.tests;


	

	
	import static driverfactory.Driver.delay;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
	
	import static verify.SoftAssertions.*;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
	import atu.testng.reports.logging.LogAs;
	import atu.testng.selenium.reports.CaptureScreen;
	import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
	import driverfactory.Driver;
	
	import pages.pit.ME_ClientAccountSelectionPage;
	import pages.pit.ME_DashboardPage;
	import pages.pit.ME_LoginPage;
	import pages.pit.PIT_HomePage;
import pages.pit.PIT_ResultsPage;
import static pages.pit.PIT_HomePage.*;
	import utilities.InitTests;
	import verify.SoftAssertions;

	public class SC_003_VerifyHomePage  extends InitTests {
		Driver driverFact=new Driver();
		WebDriver driver = null;
		WebDriver webdriver = null;
		static String filePath= System.getProperty("user.home")+"\\Downloads";
		String expectedViewHelpFileName=null;
		String fileName=null;
		private ME_LoginPage mE_LoginPage=null;
		private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
		private ME_DashboardPage mE_DashboardPage=null;
		private PIT_HomePage pIT_HomePage=null;
		
		public SC_003_VerifyHomePage(String appName) {
			super(appName);
		} 
		@BeforeClass
		public void beforeClass() throws Exception
		{
			new SC_003_VerifyHomePage("PIT");
			System.out.println("Executing TC3");
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

		}
		
		
		
				
		@Test(priority=1,enabled=true)
		public void verifyHomePageContents() throws Exception {
			ExtentTest test=null;
			try {
				test = reports.createTest("SC_003_VerifyHomePage contents" );
				test.assignCategory("regression");
				driver=driverFact.getEventDriver(webdriver,test);
				mE_LoginPage= new ME_LoginPage(driver);
				mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
				assertTrue(driverFact.isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo,15),"User logged in to the application & able to view Client-Account selection page ", test);
				mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount("Mercer                                  ","97216906" );
				if((System.getProperty("env")).equalsIgnoreCase("QA") )
				{
					waitForPageLoad(driver);
					delay(6000);
					verifyElementHyperLink(ME_DashboardPage.PITLink, test);
				}
				else
				{
					waitForPageLoad(driver);
					delay(2000);
					verifyElementHyperLink(ME_DashboardPage.PITLink_Prod, test);
				}
				
				pIT_HomePage=mE_DashboardPage.accessPIT(driver);
				
				verifyElementTextContains(h2_WelcomeHeader, "WELCOME to Personal Income Tax", test); 
				verifyElementTextContains(h2_text, "Mercer’s powerful, new income tax solution", test); 
				verifyElementTextContains(h2_text, "Mercer Global Mobility clients", test); 
				verifyElementTextContains(h2_text, "Contact your Mercer", test); 
				
				verifyElementTextContains(alertInfo, "With the recent launch of Mobility Exchange", test); 
				verifyElementTextContains(taxAlerts, "Tax Alerts", test);
				//waitForElementToDisplay(resources);
				verifyElementTextContains(resources, "Resources", test);  
				//verifyElementLink(taxAlertsLink, " In Tax Alerts section ", test);
				//verifyElementLink(resourcesLink, "In Resources section ", test);
				assertTrue(driverFact.isElementExisting(driver, PIT_HomePage.taxReportLocationDropdown, 5),"Tax report -Country / Market / City dropdown is displayed ", test);
				PIT_HomePage.runTaxReport("Brazil", "2017");
			
				// verify PDF file download 
				fileName= "tax_br_17.pdf";
			    assertTrue(Driver.checkFileDownloaded(filePath, fileName,test),"Returns true if file is downloaded successfully "+fileName,test);
			    PIT_HomePage.runTaxReport("China - Beijing", "2015");
			    fileName= "tax_cnbe15.pdf";
			    assertTrue(Driver.checkFileDownloaded(filePath, fileName,test),"Returns true if file is downloaded successfully "+fileName,test);
			    verifyElementTextContains(PIT_HomePage.returnToMobilityExchangeLink, "Return to Mobility Exchange", test);
				verifyElementLink(PIT_HomePage.returnToMobilityExchangeLink, "Verifying Return to Mobility Exchange link", test);
				pIT_HomePage.clickReturnToMobilityExchange();
				verifyContains(driver.getCurrentUrl(), BASEURL+"Dashboard/MyTools", "Returned to Mobility Exchange Dashborad page", test);
				
			    
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				softAssert.assertAll();

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				softAssert.assertAll();
			} 
			finally
			{
				reports.flush();
				

			}
			
		}
		
		
		@Test(priority=4,enabled=false)
		public void verifyReturnToMobilityExchange() throws Exception {
			ExtentTest test=null;
			
			try {
				test = reports.createTest("Verify return to mobility exchange link");
				test.assignCategory("regression");
				driver=driverFact.getEventDriver(webdriver,test);
				pIT_HomePage = new PIT_HomePage(driver);
				verifyElementTextContains(PIT_HomePage.returnToMobilityExchangeLink, "Return to Mobility Exchange", test);
				verifyElementLink(PIT_HomePage.returnToMobilityExchangeLink, "Verifying Return to Mobility Exchange link", test);
				pIT_HomePage.clickReturnToMobilityExchange();
				verifyContains(driver.getCurrentUrl(), BASEURL+"/Dashboard/MyTools", "Returned to Mobility Exchange Dashborad page", test);
				
				
				
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				softAssert.assertAll();

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();
			} 
			finally
			{
				reports.flush();
				

			}
			
		}
		
		
	
		
		
		
		@AfterTest
		public void tearDown() {
			driver.quit();
			killBrowserExe(BROWSER_TYPE);
		}
	}



