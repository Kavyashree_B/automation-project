package regression.careerview.tests;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.ManagerPage;

import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestVerifyManagerContent extends InitTests {


	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;


	public TestVerifyManagerContent(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestVerifyManagerContent careerViewTest = new TestVerifyManagerContent("CareerView");
		webdriver=driverFact.initWebDriver("https://careerview-stage.mercer.com/Demo2016/Managers/A18E28BD8AF041509417B18297F9ED90",BROWSER_TYPE,EXECUTION_ENV,"");
	}


	@Test(enabled = true, priority = 1)

	public void verifyManagerPage() throws Exception {
		try {
			test = reports.createTest("TC_30 - Verify content on Manager page");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);

			ManagerPage manager = new ManagerPage(driver);
			assertTrue(isElementExisting(driver, manager.managerHeader), "Header is present.", test);
			manager.clickdropdown();
			assertTrue(isElementExisting(driver, manager.english_dropdown),
					"English is present in the dropdown.", test);
			assertTrue(isElementExisting(driver, manager.spanish_dropdown),
					"Spanish is present in the dropdown.", test);

			manager.clickSpanish();
			verifyElementTextContains(manager.managerPage_content,
					"Un empleado solicitó una evaluación del gerente. Para completar la evaluación, revise cada una de las definiciones de competencia y aptitudes. Luego, seleccione la definición que se alinee mejor con su percepción de la aptitud del empleado. Después de que seleccione una aptitud, continúe a la próxima competencia. Para hacerlo, haga clic en el botón Next (Siguiente) ubicado en la esquina inferior derecha. Después de que complete la aptitud final, haga clic en el botón Submit (Enviar) para enviarle la evaluación al empleado.",
					test);
			verifyElementTextContains(manager.requested_employeeName, "Pavan QA11 ", test);
			verifyElementTextContains(manager.requested_employeeEmail, "gunashekar.sv@mercer.com", test);

			LocalDate localDate=LocalDate.now();
			Locale spanishLocale=new Locale("es","ES");
			Calendar today = Calendar.getInstance();  
			int d = today.get(Calendar.DATE);

			if(d<10){
				String dateInSpanish=localDate.format(DateTimeFormatter.ofPattern("MMMM d, yyyy",spanishLocale));
				System.out.println(""+dateInSpanish);
				verifyElementTextContains(manager.requested_Date, manager.convertToUpperCase(dateInSpanish) , test);
			}
			else{
				String dateInSpanish=localDate.format(DateTimeFormatter.ofPattern("MMMM dd, yyyy",spanishLocale));
				System.out.println(""+dateInSpanish);
				verifyElementTextContains(manager.requested_Date, manager.convertToUpperCase(dateInSpanish) , test);
			}

			manager.selectEnglish();
			manager.provideAssessment();
			assertTrue(isElementExisting(driver, manager.nextPage_button), "Nextpage button is present.", test);
			assertTrue(isElementExisting(driver, manager.nextButton_disabled), "Nextpage button is disabled.", test);
			manager.clickLevel1();
			assertTrue(isElementExisting(driver, manager.nextButton_enabled), "NextButton is enabled on selecting the radio button.", test);
			Driver.refreshpage();

			for(int i=0;i<5;i++) {
				manager.clickLevel1();
				manager.enterNotes();
				assertTrue(isElementExisting(driver, manager.coreCompetencyHeader_Manager), "Core Competency Header is present.", test);
				assertTrue(isElementExisting(driver, manager.notes_textbox), "Notes for competency is present.", test);
				assertTrue(isElementExisting(driver, manager.notesForCompetency_text), "Notes for competency is present.", test);

				manager.clickNext();
			}

			for(int i=0;i<2;i++) {
				manager.clickLevel1();
				manager.clickDevelopmentIdeas();
				assertTrue(isElementExisting(driver, manager.technicalCompetencyHeader_Manager), "Technical Competency Header is present.", test);
				assertTrue(isElementExisting(driver, manager.notes_textbox), "Notes for competency is present.", test);
				assertTrue(isElementExisting(driver, manager.notesForCompetency_text), "Notes for competency is present.", test);
				manager.clickNext();
			}

			manager.exitAndContinue();
			assertTrue(isElementExisting(driver, manager.managerHeader), "Header is present.", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
			driver.quit();
		}
	}

	@Test(enabled = true, priority = 2)

	public void verifyContinuityManagerPage() throws Exception {
		try {
			test = reports.createTest("TC_30_1 - Verify whether the user is able to continue where he left in the earlier session in the Manager's page");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			ManagerPage manager = new ManagerPage(driver);

			//switchToWindowWithURL("https://careerview-stage.mercer.com/Demo2016/Managers/A18E28BD8AF041509417B18297F9ED90");
			assertTrue(isElementExisting(driver, manager.managerHeader), "Header is present.", test);

			manager.clickContinue();
			manager.clickSubmit();
			assertTrue(isElementExisting(driver, manager.completed_button), "User is back to the Manager assessment page.", test);

			/////CODE FOR OUTLOOK ACCESS HAS TO BE WRITTEN HERE/////////////////////////
		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.quit();
		}
	}


}
