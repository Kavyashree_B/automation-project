package regression.careerview.tests;

import static driverfactory.Driver.delay;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.ComparePage;
import pages.careerview.CompareProcessPopUp;
import pages.careerview.DevelopmentIdeasPages;
import pages.careerview.HeaderPage;
import pages.careerview.HelpPage;
import pages.careerview.HomePage;
import pages.careerview.ITPage;
import pages.careerview.LoginPage;
import pages.careerview.ManagerAssessmentPage;
import pages.careerview.ManagerPage;
import pages.careerview.MyProfilePages;
import pages.careerview.OverviewPage;
import pages.careerview.StartExploringPages;
import pages.careerview.TechnicalCompetencyPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class VerifyCodingGrid extends InitTests{


	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public VerifyCodingGrid(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		VerifyCodingGrid careerView = new VerifyCodingGrid("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)

	public void verifyManagerAfterSubmit() throws Exception {
		try {
			test = reports.createTest("TC_31 - Verify whether manager can view manager assessment section after the  assessment submitted from manager dashboard");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage loginPage = new LoginPage(driver);
			HeaderPage header = new HeaderPage(driver);
			MyProfilePages myProfile = new MyProfilePages(driver);			
			ManagerAssessmentPage MAP = new ManagerAssessmentPage(driver);
			HomePage home = new HomePage(driver);

			/////////////////////////////////// TEMPORARY////////////////////////////////////////
			loginPage.loginToCareerView(USERNAME, PASSWORD);
			home.clickStartExploring();
			home.clickExploreWide();
			header.openProfile();
			myProfile.navigateToSideTab(myProfile.managerAssessmentLink);
			//////////////////////////////TEMPORARY//////////////////////////////////////

			assertTrue(isElementExisting(driver, MAP.available_button), "Button is changed to available.", test);
			assertTrue(isElementExisting(driver, MAP.newAssessmentPage), "Button is changed to available.", test);
			assertTrue(isElementExisting(driver, MAP.managerName), "Manager name is read only", test);
			assertTrue(isElementExisting(driver, MAP.managerEmail), "Manager Email is read only.", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 2)

	public void viewAssessmentFeedback() throws Exception {
		try {
			test = reports.createTest("TC_32 & TC_33 - To view manager assessment/feedback in Core & Technical competencies section");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);

			myProfile.navigateToSideTab(myProfile.CoreCompetenciesLink);
			for(int i=0;i<5;i++) {
				try {
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_core-competencies_"+i+"']/label[1]/span[1]"));
					verifyElementTextContains(ele, "Manager's Choice ", test);
				}
				catch(Exception e) {
					delay(1000);
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_core-competencies_"+i+"']/label[1]/span[1]"));
					verifyElementTextContains(ele, "Manager's Choice ", test);
				}
			}

			for(int i=0;i<5;i++) {
				try {
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_core-competencies_"+i+"']/label[1]/span[2]"));
					verifyElementTextContains(ele, "Manager Comments: Test Notes ", test);
				}
				catch(Exception e) {
					delay(1000);
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_core-competencies_"+i+"']/label[1]/span[2]"));
					verifyElementTextContains(ele, "Manager Comments: Test Notes ", test);
				}

			}

			myProfile.navigateToSideTab(myProfile.TechnicalCompetenciesLink);
			for(int i=0;i<2;i++) {
				try {
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_technical-competencies_"+i+"']/label[1]/span"));
					verifyElementTextContains(ele, "Manager's Choice ", test);
				}
				catch(Exception e) {
					delay(1000);
					WebElement ele = driver.findElement(By.xpath("//*[@id='user-competency_user_technical-competencies_"+i+"']/label[1]/span"));
					verifyElementTextContains(ele, "Manager's Choice ", test);
				}
			}


		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 3)

	public void viewDevelopmentIdeas() throws Exception {
		try {
			test = reports.createTest("TC_34 - To view whether development ideas are saved correctly.");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			DevelopmentIdeasPages developmentIdeas = new DevelopmentIdeasPages(driver);

			myProfile.navigateToSideTab(myProfile.developmentIdeas_Link);
			assertTrue(isElementExisting(driver, developmentIdeas.developmentIdeas_TC1), "Development Idea is updated correctly.", test);
			assertTrue(isElementExisting(driver, developmentIdeas.developmentIdeas_TC2), "Development Idea is updated correctly.", test);

		}catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 4)

	public void verifyRatingGrid() throws Exception {
		try {
			test = reports.createTest("TC_37 - Verify  Rating Grid when Manager selects level corresponding to competency type and User doesn't");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages mpp = new MyProfilePages(driver);
			CompareProcessPopUp compare = new CompareProcessPopUp(driver);

			mpp.viewComparisons();

			verifyElementTextContains(compare.roleSelected1, "Software Application Engineer III", test);
			verifyElementTextContains(compare.roleSelected2, "Programmer Analyst", test);
			verifyElementTextContains(compare.roleSelected3, "Programmer Analyst I", test);

			compare.navigateTo(compare.coreCompetencies);

			assertTrue(compare.ExpectedBlack_graphCC.size() >0, "Expected is displayed in black color when user / manager has not selected the level.", test); 
			assertTrue(compare.managerRed_graphCC.size() >0, "Manager is displayed in red color when user / manager has not selected the level.", test);

			compare.navigateTo(compare.technicalCompetencies);

			assertTrue(compare.ExpectedBlack_graphTC.size()>0, "Expected is displayed in black color when user / manager has not selected the level.", test);
			assertTrue(compare.ExpectedText.size()>0,  "Expected is present below the graph.", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 5)

	public void verifyUserSelectedRatingGrid() throws Exception {
		try {
			test = reports.createTest("TC_38 - Verify  Rating Gridwhen Manager & User have selected levels corresponding to competency types");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			CompareProcessPopUp compare = new CompareProcessPopUp(driver);
			compare.navigateTo(compare.technicalCompetencies);

			assertTrue(compare.youYellowCC.size()>0, " 'You' color is in Yellow color ,if user rating is equal to 'Expected'", test);
			assertTrue(compare.ExpectedBlack_graphCC.size() >0, "Expected is displayed in black color when user / manager has not selected the level.", test); 
			assertTrue(compare.managerRed_graphCC.size() >0, "Manager is displayed in red color when user / manager has not selected the level.", test);

			compare.navigateTo(compare.technicalCompetencies);

			assertTrue(isElementExisting(driver, compare.youGreenTC), "Label color is Green ,If Manager rating is greater than 'Expected'", test);
			assertTrue(isElementExisting(driver, compare.youGreenGridTC), "Label & Grids are in same  color", test);

			assertTrue(compare.ExpectedBlack_graphTC.size()>0, "Expected is displayed in black color when user / manager has not selected the level.", test);
			assertTrue(compare.ExpectedText.size()>0,  "Expected is present below the graph.", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
			driver.quit();
		}
	}
}
