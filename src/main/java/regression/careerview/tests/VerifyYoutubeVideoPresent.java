package regression.careerview.tests;

import static driverfactory.Driver.delay;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.CompareProcessPopUp;
import pages.careerview.HeaderPage;
import pages.careerview.HomePage;
import pages.careerview.LocateMePage;
import pages.careerview.LoginPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class VerifyYoutubeVideoPresent extends InitTests{

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public VerifyYoutubeVideoPresent(String appname) {
		super(appname);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		VerifyYoutubeVideoPresent careerViewTest = new VerifyYoutubeVideoPresent("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)

	public void generalUserYoutubeVideo() throws Exception {
		try {
			test = reports.createTest("TC_ 50 - Verify video playing in the application");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage loginPage = new LoginPage(driver);
			LocateMePage locateMe = new LocateMePage(driver);
			HomePage home = new HomePage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);

			loginPage.loginToCareerView(USERNAME, PASSWORD );
			home.clickStartExploring();
			home.clickExploreWide();

			HeaderPage header = new HeaderPage(driver);
			header.navigateViaHeader(header.locateMe_button);

			locateMe.clickHighlightedRole();

			compareProcessPopUp.navigateTo(compareProcessPopUp.dayInTheLife);

			//compare.checkYoutubeVideoPlaying();


		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			killBrowserExe("CHROME");
		}
	}
}