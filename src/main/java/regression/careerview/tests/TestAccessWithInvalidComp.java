
package regression.careerview.tests;

import static driverfactory.Driver.delay;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.OrgHeaderRegPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestAccessWithInvalidComp extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	public TestAccessWithInvalidComp(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestAccessWithInvalidComp access = new TestAccessWithInvalidComp("CareerView");
		webdriver=driverFact.initWebDriver("https://careerview-stage.mercer.com/abcd",BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)
	public void accessWithInvalidCompany() throws Exception {
		try {
			test = reports.createTest("TC 02 - Accessing url without company.");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			OrgHeaderRegPage orgHeader = new OrgHeaderRegPage(driver);
			orgHeader.waitForHeaderEle();
			verifyElementTextContains(orgHeader.HeaderEle, "Server Error", test);
			verifyElementTextContains(orgHeader.ForbiddenAccess, " 404 - File or directory not found.",
					test);
			verifyElementTextContains(orgHeader.PermissionMsg,
					"The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable.",
					test);
			// assertTrue(isElementExisting(driver,
			// OverviewPage.myProfile_button, 5),"User logged in to application", test);

		} 

		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}

		finally {
			reports.flush();
			driver.quit();
		}
	}


}
