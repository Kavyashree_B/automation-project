package regression.careerview.tests;

import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.fail;
import static verify.SoftAssertions.verifyContains;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.CoreCompetencyPage;
import pages.careerview.HeaderPage;
import pages.careerview.HelpPage;
import pages.careerview.HomePage;
import pages.careerview.ITPage;
import pages.careerview.LoginPage;
import pages.careerview.ManagerAssessmentPage;
import pages.careerview.MyProfilePages;
import pages.careerview.OverviewPage;
import pages.careerview.TechnicalCompetencyPage;
import utilities.InitTests;

public class TestVerifyHelpProfilePages extends InitTests{

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public TestVerifyHelpProfilePages(String appname) {
		super(appname);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestVerifyCompare careerViewTest = new TestVerifyCompare("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)

	public void verifyHelpPage() throws Exception {
		try {
			test = reports.createTest("TC_21 - Verifying HelpIcon in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			HeaderPage header = new HeaderPage(driver);
			HelpPage help = new HelpPage(driver);
			LoginPage loginPage = new LoginPage(driver);
			HomePage home = new HomePage(driver);

			loginPage.loginToCareerView(USERNAME, PASSWORD);
			home.clickStartExploring();
			home.clickExploreWide();

			header.navigateViaHeader(header.help_button);
			assertTrue(isElementExisting(driver, help.FAQ), "FAQ is present", test);
			assertTrue(isElementExisting(driver, help.Terms), "Terms is present", test);
			assertTrue(isElementExisting(driver, help.Tutorial), "Tutorial is present", test);

			assertTrue(isElementExisting(driver, help.GeneralLink), "General is present", test);
			assertTrue(isElementExisting(driver, help.Glossary), "Glossary is present", test);
			assertTrue(isElementExisting(driver, help.HowToUseTheSite), "How to use the site link is present", test);

			help.navigateTo(help.GeneralHeader);
			verifyElementTextContains(help.GeneralHeader, "General", test);
			assertTrue(isElementExisting(driver, help.FAQList), "List is present", test);

			help.navigateTo(help.Glossary);
			for(WebElement alphabets : help.alphabetsGlossary) {
				assertTrue(isElementExisting(driver, alphabets), "Alphabets are present", test);
			}

			help.clickS();
			assertTrue(help.glossarySContent1.isDisplayed(), "Content of alphabet S is displayed.", test);
			assertTrue(help.glossarySContent2.isDisplayed(), "Content of alphabet S is displayed.", test);

			help.navigateTo(help.HowToUseTheSite);
			assertTrue(isElementExisting(driver, help.howToUseThisSiteVideo1), "1st video is present.", test);
			assertTrue(isElementExisting(driver, help.howToUseThisSiteVideo2), "2nd video is present", test);

			assertTrue(isElementExisting(driver, help.zoomImg), "Zoom In image is present", test);
			assertTrue(isElementExisting(driver, help.locateImg), "Locate image is present", test);
			assertTrue(isElementExisting(driver, help.navigateImg), "Navigate image is present", test);
			assertTrue(isElementExisting(driver, help.compareImg), "Compare image is present", test);
			assertTrue(isElementExisting(driver, help.helpImg), "Help image is present", test);

			help.closeHelp();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 2)

	public void verifyProfile() throws Exception {
		try {
			test = reports.createTest("TC_22 - Verifying profile details in my Profile page");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			OverviewPage overview = new OverviewPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			assertTrue(isElementExisting(driver,overview.myProfile_header), "SignOut link is present.", test);

			myProfile.navigateToSideTab(myProfile.OverviewLink);
			assertTrue(isElementExisting(driver,overview.firstName_input), "First Name text box is present.", test);
			assertTrue(isElementExisting(driver,overview.lastName_input), "Last name textbox is present.", test);
			assertTrue(isElementExisting(driver,overview.email), "Email is present.", test);
			assertTrue(isElementExisting(driver,overview.about_input), "About is present.", test);
			assertTrue(isElementExisting(driver,overview.save_button), "Save button is present.", test);
			assertTrue(isElementExisting(driver,overview.uploadPhoto_button), "Upload photo button is present.", test);
			assertTrue(isElementExisting(driver,overview.yesBtn), "Yes button is present.", test);
			assertTrue(isElementExisting(driver,overview.noBtn), "No button is present.", test);


		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

		finally {
			reports.flush();
		}
	}
	@Test(enabled = true, priority = 3)

	public void verifyUpdateProfileData() throws Exception {
		try {
			test = reports.createTest("TC_23 - Verifying update profile data functionality");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			OverviewPage overview = new OverviewPage(driver);

			overview.editInfo("QA11","About: Checking test11");
			assertTrue(isElementExisting(driver,overview.savedInfoStyle_button),"Information saved successfully", test);
			overview.closeProfilepopup();

		}

		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 4)

	public void verifySavePostion() throws Exception {
		try {
			test = reports.createTest("TC_24 - Verify whether positions can be saved to a profile in HomePage");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			HeaderPage header = new HeaderPage(driver);
			ITPage itPage = new ITPage(driver);
			HomePage home = new HomePage(driver);

//			header.navigateViaHeader(header.home_button);
			home.clickIT();
			itPage.clickAnalyst();
			assertTrue(isElementExisting(driver, itPage.analystPlussign), " '+' sign is present.", test);

			itPage.addPositionToProfile();
			assertTrue(isElementExisting(driver, itPage.analystTickSign), " '_/' sign is present.", test);

			itPage.removePostionFromProfile();
			assertTrue(isElementExisting(driver, itPage.analystPlussign1), " '+' sign is present.", test);

			itPage.close_modal();

			assertTrue(isElementExisting(driver, itPage.analyst), "Modal was closed correctly", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 5)

	public void verifyEmployeesSelfRating() throws Exception {
		try {
			test = reports.createTest("TC_25 - Verify Employee Self Rating functionality.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			CoreCompetencyPage coreCompetency = new CoreCompetencyPage(driver);
			TechnicalCompetencyPage techCompetency = new TechnicalCompetencyPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.CoreCompetenciesLink);
			assertTrue(isElementExisting(driver, coreCompetency.coreCompetencyHeader), "Header is present.", test);

			for(int i=0;i<5;i++) {
				WebElement coreHeader = driver.findElement(By.id("user-competency_user_core-competencies_"+i+""));
				assertTrue(isElementExisting(driver, coreHeader), "Inner Header is present.", test);
			}

			coreCompetency.clickLevel1RadioButton();
			coreCompetency.saveCompetency();
			assertTrue(coreCompetency.coreCompetencyRadioButton1.isSelected(), "Level 1 Radiobutton was selected", test);

			myProfile.navigateToSideTab(myProfile.TechnicalCompetenciesLink);
			myProfile.navigateToSideTab(myProfile.CoreCompetenciesLink);
			assertTrue(coreCompetency.coreCompetencyRadioButton1.isSelected(), "Level 1 radio button is selected.", test);
			coreCompetency.clickLevel2RadioButton();
			coreCompetency.saveCompetency();

			myProfile.navigateToSideTab(myProfile.TechnicalCompetenciesLink);
			assertTrue(isElementExisting(driver, techCompetency.TechnicalCompetencyHeader), "Header is present.", test);

			for(int i=0;i<2;i++) {
				WebElement coreHeader = driver.findElement(By.id("user-competency_user_technical-competencies_"+i+""));
				assertTrue(isElementExisting(driver, coreHeader), "Inner Header is present.", test);
			}

			techCompetency.clickLevel1RadioButton();
			techCompetency.clickSave();
			assertTrue(techCompetency.technicalCompetencyRadioButton1.isSelected(), "Level 1 Radiobutton was selected", test);

			myProfile.navigateToSideTab(myProfile.CoreCompetenciesLink);
			myProfile.navigateToSideTab(myProfile.TechnicalCompetenciesLink);
			assertTrue(techCompetency.technicalCompetencyRadioButton1.isSelected(), "Level 1 radio button is selected.", test);
			techCompetency.clickLevel2RadioButton();
			techCompetency.clickSave();


		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}finally {
			reports.flush();
		}
	}


	@Test(enabled = true, priority = 6)

	public void verifyRequestAssessment() throws Exception {
		try {
			test = reports.createTest("TC_26 - Verify viewing Request Assessment To Manager.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			ManagerAssessmentPage managerAssessmentPage = new ManagerAssessmentPage(driver);

			myProfile.navigateToSideTab(myProfile.managerAssessmentLink);

			assertTrue(isElementExisting(driver, managerAssessmentPage.managerAssessmentHeader), "Header is present", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.requestAssessment_button), "Request Assessment button is present", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.managerEmail_input), "Email text box is present", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.managerName_input), "Manager name input is present", test);

			String managerName = managerAssessmentPage.managerName_input.getAttribute("readonly");
			assertTrue(managerName.equals("true"), "Manager name input is read only", test);

			String managerEmail = managerAssessmentPage.managerEmail_input.getAttribute("readonly");
			assertTrue(managerEmail.equals("true"), "Manager name input is read only", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.edit_button), "Edit button is present", test);
			assertTrue(managerAssessmentPage.edit_button.isEnabled(), "Edit button is enabled by default.", test);

		}catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 7)

	public void verifyValidRequestAssessment() throws Exception {
		try {
			test = reports.createTest("TC_27 - Verify Request Assessment To Manager when user/employee data is uploaded with valid ManagerEmail & Manager Name.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			ManagerAssessmentPage managerAssessmentPage = new ManagerAssessmentPage(driver);

			managerAssessmentPage.clickEditButton();

			assertTrue(managerAssessmentPage.managerEmail_input.isEnabled(), "Manager email input is editable", test);
			assertTrue(managerAssessmentPage.managerName_input.isEnabled(), "Manager name input is editable", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.save_button), "Save button is present", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.cancel_button), "Cancel button is present", test);
			//System.out.println(isElementExisting(driver, managerAssessmentPage.returnToDefaultManager_button));

			managerAssessmentPage.clickCancelButton();
			String managerName = managerAssessmentPage.managerName_input.getAttribute("readonly");
			//			System.out.println(managerName);
			assertTrue(managerName.equals("true"), "Manager name input is read only", test);
			String managerEmail = managerAssessmentPage.managerEmail_input.getAttribute("readonly");
			//			System.out.println(managerEmail);
			assertTrue(managerEmail.equals("true"), "Manager name input is read only", test);


			managerAssessmentPage.editManagerInfo("Test123","abc@mercer.com");
			verifyContains(managerAssessmentPage.getManageName(),"Test123","Name in Manager Assessment Page is correct.",test);
			verifyContains(managerAssessmentPage.getManageEmail(),"abc@mercer.com","Email in Manager Assessment page is correct.", test);

			managerAssessmentPage.clickEditButton();

			assertTrue(managerAssessmentPage.managerEmail_input.isEnabled(), "Manager email input is editable", test);
			assertTrue(managerAssessmentPage.managerName_input.isEnabled(), "Manager name input is editable", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.save_button), "Save button is present", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.cancel_button), "Cancel button is present", test);

			String alertText = managerAssessmentPage.returnToDefaultManager();

			Boolean bool = alertText.contains("Manager Name: Gunashekar SV");
			assertTrue(bool, "Alert box with a proper message is displayed.", test);
			Boolean bool1 = alertText.contains("Manager Email: gunashekar.sv@mercer.com");
			assertTrue(bool1, "Alert box with a proper message is displayed.", test);

			managerAssessmentPage.clickEditButton();
			verifyContains(managerAssessmentPage.getManageName(),"Gunashekar SV", "Name in Manager Assessment Page is correct.",test);
			verifyContains(managerAssessmentPage.getManageEmail(),"gunashekar.sv@mercer.com", "Email in Manager Assessment page is correct.", test);

			waitForElementToDisappear(By.xpath(managerAssessmentPage.defManager_xpath));
			assertTrue(true, "Return to default button is not present", test);
			managerAssessmentPage.editManagerInfoLast("Vidya", "vidya.dandinashivara@mercer.com");


		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 8)

	public void verifyrequestAssessmentFunctionality() throws Exception {
		try {
			test = reports.createTest("TC_29 - Verify Request Assessment To Manager.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			ManagerAssessmentPage managerAssessmentPage = new ManagerAssessmentPage(driver);

			managerAssessmentPage.requestAssesment();
			waitForElementToDisplay(managerAssessmentPage.requestSent_button);
			assertTrue(isElementExisting(driver, managerAssessmentPage.requestSent_button), "Request Sent Button is present.", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.managerName), "Manager name is read only", test);
			assertTrue(isElementExisting(driver, managerAssessmentPage.managerEmail), "Manager Email is read only.", test);

		} 

		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

		finally {
			reports.flush();
			driver.quit();
		}
	}

}
