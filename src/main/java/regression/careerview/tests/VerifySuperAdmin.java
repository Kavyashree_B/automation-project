package regression.careerview.tests;

import static driverfactory.Driver.*;

import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.*;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class VerifySuperAdmin extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public static String  dirPath = dir_path;

	public VerifySuperAdmin(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		VerifySuperAdmin careerView = new VerifySuperAdmin("CareerView");
		webdriver=driverFact.initWebDriver("https://careerview-stage.mercer.com/SuperAdmin",BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)

	public void superAdmin_AccessToApp() throws Exception {
		try {
			test = reports.createTest("TC_39 - Verify access to Super Admin site");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			RegisterPage register = new RegisterPage(driver);
			LoginPage login = new LoginPage(driver);
			SuperAdmin_LandingPage superAdmin = new SuperAdmin_LandingPage(driver);

			login.clickLogin();
			assertTrue(isElementExisting(driver, register.NoEmpIdMsg), "NoEmpID message was dispalyed when no emp id was entered.", test);
			assertTrue(isElementExisting(driver, register.noPasswordMsg), "NoPwd message was dispalyed when no emp id was entered.", test);

			login.loginToCareerView(USERNAME, PASSWORD);
			assertTrue(isElementExisting(driver, superAdmin.signOut_button), "Sign out button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.selectCompany_button), "Select Company button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.addNewClient_button), "Add new company button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.SuperAdmin_header), "Header is present.", test);
			for(WebElement company : superAdmin.companyList ) {
				assertTrue(isElementExisting(driver, company), "company is present.", test);
			}

		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 2)

	public void superAdmin_CreatingNewClient() throws Exception {
		try {
			test = reports.createTest("TC_40 - Verify creating new client functionality");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			SuperAdmin_LandingPage superAdmin = new SuperAdmin_LandingPage(driver);
			SuperAdmin_AddNewClient superAdminAdd = new SuperAdmin_AddNewClient(driver);

			superAdmin.clickAddNewClientButton();
			assertTrue(isElementExisting(driver, superAdminAdd.addNewClient_Header), "Header is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.adminName_input), "Admin name input is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.urlFrag_input), "UrlFragment input is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.adminEmail_input), "Admin Email is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.name_input), "Name input is present.", test);

			for(int i = 1; i< 4 ; i++) {
				WebElement checkbox = driver.findElement(By.xpath("//*[@id='form']/div[6]/div/div/div["+i+"]/input"));
				assertTrue(isElementExisting(driver, checkbox), "Checkbox is present.", test);
			}

			assertTrue(isElementExisting(driver, superAdminAdd.save_Button), "Save Button is present", test);
			assertTrue(isElementExisting(driver, superAdminAdd.cancel_Button), "Cancel button is present.", test);

			superAdminAdd.enterAllFields("Test123!@#","#$#@" ,"Test123" , "@#$#");
			waitForElementToDisappear(By.xpath(superAdminAdd.name_inputErrorXpath));
			assertTrue(true, "Name input accepts alpha numeric and special characters.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.urlFrag_inputError), "Url Fragment textbox does not accept special characters.", test);
			waitForElementToDisappear(By.xpath(superAdminAdd.adminName_inputErrorXpath));
			assertTrue(true, "Admin Name accepts alphanumeric characters.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.adminEmail_inputError), "Email format is invalid.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.checkbox_error), "Selection of checkbox is mandatory.", test);

			superAdminAdd.clickCancel();
			assertTrue(isElementExisting(driver, superAdmin.signOut_button), "Sign out button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.selectCompany_button), "Select Company button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.addNewClient_button), "Add new company button is present.", test);
			assertTrue(isElementExisting(driver, superAdmin.SuperAdmin_header), "Header is present.", test);

		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 3)

	public void superAdmin_EditClientDetails() throws Exception {
		try {
			test = reports.createTest("TC_41 - To create new company/client Details");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			SuperAdmin_LandingPage superAdmin = new SuperAdmin_LandingPage(driver);
			SuperAdmin_AddNewClient superAdminAdd = new SuperAdmin_AddNewClient(driver);
			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);

			superAdmin.selectCompany();
			assertTrue(superAdminAdd.name_input.getAttribute("value").equals("Demo2016"), "Name detail is present.", test);
			verifyElementTextContains(superAdminAdd.urlFrag, "http://careerview-stage.mercer.com/Demo2016", test);
			assertTrue(superAdminAdd.adminName_input.getAttribute("value").equals("Gunashekar SV"), "Admin Name is present.", test);
			assertTrue(superAdminAdd.adminEmail_input.getAttribute("value").equals("gunashekar.sv@mercer.com"), "Admin Email is present.", test);

			assertTrue(isElementExisting(driver, adminHeaderPage.details_button), "Details is present.", test);
			assertTrue(isElementExisting(driver, adminHeaderPage.analytics_button), "Analytics is present.", test);
			assertTrue(isElementExisting(driver, adminHeaderPage.import_button), "Import is present.", test);
			assertTrue(isElementExisting(driver, adminHeaderPage.adminTask_button), "Admin is present.", test);
			assertTrue(isElementExisting(driver, adminHeaderPage.users_button), "Users is present.", test);

		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 4)

	public void superAdminClientDetails() throws Exception {
		try {
			test = reports.createTest("TC_42 - 43 - To view/edit company/client Details");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			SuperAdmin_AddNewClient superAdminAdd = new SuperAdmin_AddNewClient(driver);

			assertTrue(superAdminAdd.name_input.getAttribute("value").equals("Demo2016"), "Name detail is present.", test);
			verifyElementTextContains(superAdminAdd.urlFrag, "http://careerview-stage.mercer.com/Demo2016", test);
			assertTrue(superAdminAdd.adminName_input.getAttribute("value").equals("Gunashekar SV"), "Admin Name is present.", test);
			assertTrue(superAdminAdd.adminEmail_input.getAttribute("value").equals("gunashekar.sv@mercer.com"), "Admin Email is present.", test);

			String attr_name = superAdminAdd.name_input.getAttribute("readonly");
			assertNull(attr_name, "Name field is editable.", test);

			String attr_Email = superAdminAdd.adminEmail_input.getAttribute("readonly");
			assertNull(attr_Email, "Email field is editable.", test);

			String attr_adminName = superAdminAdd.adminName_input.getAttribute("readonly");
			assertNull(attr_adminName, "Admin Name field is editable.", test);

			String attr_ipAddr = superAdminAdd.ipAddr_input.getAttribute("readonly");
			assertNull(attr_name, "IpAddress field is editable.", test);

			for(int i = 1; i< 4 ; i++) {

				WebElement checkbox = driver.findElement(By.xpath("//*[@id='form']/div[7]/div/div/div["+i+"]/input"));
				clickElement(checkbox);
				assertTrue(true, "Checkbox is editable.", test);
			}

			superAdminAdd.reset();

			for(int i = 1; i< 4 ; i++) {

				WebElement checkbox = driver.findElement(By.xpath("//*[@id='form']/div[7]/div/div/div["+i+"]/input"));
				assertTrue(checkbox.isSelected(), "Reset button is working correctly", test);
			}

		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 5)

	public void superAdmin_ClientDetails_CompanyLogo() throws Exception {
		try {
			test = reports.createTest("TC_44 - To verify whether user is able to edit the company logo.");
			test.assignCategory("regression");


			driver=driverFact.getEventDriver(webdriver,test);

			SuperAdmin_LandingPage superAdmin = new SuperAdmin_LandingPage(driver);
			SuperAdmin_AddNewClient superAdminAdd = new SuperAdmin_AddNewClient(driver);

			assertTrue(isElementExisting(driver, superAdminAdd.Logo_Text), "Logo header is present", test);
			assertTrue(isElementExisting(driver, superAdminAdd.chooseFile_button), "Choose File button is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.save_Button), "Save Button is present.", test);
			assertTrue(isElementExisting(driver, superAdminAdd.reset_button), "Reset Button is present.", test);

			String oldProfilePhoto =(superAdminAdd.Logo_img).getAttribute("style");
			//System.out.println(oldProfilePhoto);
			superAdminAdd.uploadPhoto();
			superAdminAdd.clickSave();
			String alertText = superAdminAdd.getAlertText();
			Boolean bool = alertText.equalsIgnoreCase("Company updated successfully");
			assertTrue(bool, "Alert box with a proper message is displayed.", test);
			waitForElementToDisplay(superAdminAdd.Logo_img);
			String newProfilePhoto =(superAdminAdd.Logo_img).getAttribute("style");
			//System.out.println(newProfilePhoto);
			assertFalse(newProfilePhoto.contentEquals(oldProfilePhoto), "Profile photo added successfully", test);


			superAdminAdd.revertTodefaultImage();
			//System.out.println(superAdminAdd.Logo_img.getAttribute("style"));
			Boolean defaultLogo = superAdminAdd.Logo_img.getAttribute("style").contains("width: 139px; height: 39px; background-image: url(\"https://careerview-stage.mercer.com/Demo2016/logo?");
			assertTrue(defaultLogo, "Logo image is present on the right side.", test);
			superAdminAdd.exitCompany();
			superAdmin.signOut();

			//System.out.println(superAdminAdd.loginLogo_img.getAttribute("style"));
			Boolean defaultLogo2 = superAdminAdd.loginLogo_img.getAttribute("style").contains("background-image: url(\"/SuperAdmin/Logo\"); width: 136px; height: 19px;");
			assertTrue(defaultLogo2, "Logo image is present on the right side.", test);


		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 6)

	public void superAdmin_AnalyticsTab() throws Exception {
		try {
			test = reports.createTest("TC_45 - To verify range selection functionality.");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login = new LoginPage(driver);
			SuperAdmin_LandingPage superAdmin = new SuperAdmin_LandingPage(driver);

			login.loginToCareerView(USERNAME, PASSWORD);
			superAdmin.selectCompany();

			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.analytics_button);

			AdminAnalyticsPage analyticsPage = new AdminAnalyticsPage(driver);
			assertTrue(isElementExisting(driver, analyticsPage.rangeSelection_dropdown), "User is navigated to analytics page", test);
			assertTrue(analyticsPage.checkDropdownOptions(), "Option is present in the dropdown.", test);
			analyticsPage.selectFromRangeDropdown();
			//System.out.println((analyticsPage.dateElement).size());

			assertTrue((analyticsPage.dateElement).size()>=28 && (analyticsPage.dateElement).size()<=31,"Monthly records are displayed", test);

			analyticsPage.selectFortnigtlyFromRangeDropdown();
			//System.out.println((analyticsPage.dateElement).size());

			assertTrue((analyticsPage.dateElement).size()>=4 && (analyticsPage.dateElement).size()<=16,"Fortnightly records are displayed", test);



		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 7)

	public void superAdmin_ImportTab() throws Exception {
		try {
			test = reports.createTest("TC_46 - To verify import tab and its functionality.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.import_button);

			AdminImportPage imports = new AdminImportPage(driver);
			assertTrue(isElementExisting(driver, imports.upload_import), "Upload button is present", test);
			assertTrue(isElementExisting(driver, imports.download_import), "Download button is present", test);
			assertTrue(imports.importHistory.size()>1, "Import History is present", test);

			imports.download();
			assertTrue(imports.isFileDownloaded(), "File Downloaded successfully.", test);

			assertTrue(isElementExisting(driver, imports.chooseFile_button), "Choose File button is present", test);
			assertTrue(isElementExisting(driver, imports.careerFramework_radiobutton), "CareerFramework radio button is present", test);
			assertTrue(isElementExisting(driver, imports.users_radiobutton), "Users radio button is present", test);

			imports.upload();
			assertTrue(isElementExisting(driver, imports.mandatoryMsg_buttonUpload), "Mandatory field message is displayed", test);
			assertTrue(isElementExisting(driver, imports.mandatoryMsg_radioUpload), "Mandatory field message is displayed", test);

			imports.clickCareerFramework();
			imports.upload();
			assertTrue(isElementExisting(driver, imports.mandatoryMsg_buttonUpload), "Mandatory field message is displayed", test);

			for(int i = 0; i <6; i++) {
				assertTrue(imports.importHistory_columnNames.get(i).getText().equals(imports.columnNamesArray[i]), "Column Names are present.", test);
			}

			//System.out.println(imports.importHistory_statusList.size());

			for( int i = 0; i < imports.importHistory_statusList.size(); i++)
			{
				if(imports.importHistory_statusList.get(i).getText().trim().equals("Completed") || imports.importHistory_statusList.get(i).getText().trim().equals("Failed")) 

				{
					assertTrue(true, "Status is either completed or failed.", test);
				}
			}

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 8)

	public void superAdmin_AdminTab() throws Exception {
		try {
			test = reports.createTest("TC_47 - To verify Admin tab and its functionality.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);


			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.adminTask_button);

			AdminTaskPage adminTaskPage = new AdminTaskPage(driver);
			assertTrue(isElementExisting(driver, adminTaskPage.adminTask_header), "User is navigated to admin task page", test);
			assertTrue(isElementExisting(driver, adminTaskPage.reset_button), "Reset button is present and User is navigated to admin task page", test);

			adminTaskPage.resetAndCancel();
			verifyContains(adminTaskPage.resetWithWrongUsername(), "Assessment request for EmployeeId = 9000 not found","Assessment not found.", test);
			verifyContains(adminTaskPage.resetWithCorrectUsername(), "Assessment request cleared", "Assessment cleared." ,test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 9)

	public void superAdmin_UsersTab() throws Exception {
		try {
			test = reports.createTest("TC_48 - To verify Users tab and its functionality.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.users_button);

			AdminUsers users = new AdminUsers(driver);

			assertTrue(isElementExisting(driver, users.search_textbox), "Search box is present.", test);
			assertTrue(isElementExisting(driver, users.search_button), "Search button is present.", test);
			waitForElementToDisappear(By.xpath(users.sixthEleInTable_xpath));
			assertTrue(true, "Only 5 records are displayed per page.", test);

			users.clickNext();
			users.clickPrevious();

			users.searchUser();
			assertTrue(isElementExisting(driver, users.searchResult_FirstName), "First Name is correct.", test);
			assertTrue(isElementExisting(driver, users.searchResult_LastName), "Last Name is correct.", test);


		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.close();
		}
	}

}
