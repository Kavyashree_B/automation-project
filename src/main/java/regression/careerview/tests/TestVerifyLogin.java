package regression.careerview.tests;

import static driverfactory.Driver.delay;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.LoginPage;
import pages.careerview.RegisterPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestVerifyLogin extends InitTests{


	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public TestVerifyLogin(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestVerifyLogin careerView = new TestVerifyLogin("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}

	@Test(enabled = true, priority = 1)

	public void verifyLoginPage() throws Exception {

		try {
			test = reports.createTest("TC_03 - Verifying Login page with valid company name");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.LoginCompanyLogo);

			//System.out.println(isElementExisting(driver, login.LoginCompanyLogo));
			assertTrue(isElementExisting(driver, login.LoginCompanyLogo),"CompanyLogo is present", test);
			verifyElementTextContains(login.Header1, "Welcome to Career View", test);
			verifyElementTextContains(login.Header2, "A tool to map your future.", test);
			verifyElementTextContains(login.LogIn_link, "Log In", test);
			verifyElementTextContains(login.Register_link, "Register", test);
			verifyElementTextContains(login.ForgotYourPassword, "Forgot your password?", test);
			verifyContains(login.username_input.getAttribute("placeholder"), "Employee ID", "Placeholder is verified." , test);
			verifyContains(login.password_input.getAttribute("placeholder"), "Password", "Placeholder is verified.",test);
			assertTrue(isElementExisting(driver, login.submit_button),"LogIn button is present", test);
			verifyContains(login.videoVerify.getAttribute("type"), "video/mp4","Video is pesent here", test);


		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}


	@Test(enabled = true, priority = 2)
	public void langChange() throws Exception {

		try {
			test = reports.createTest("TC_04 - Verifying Login page with Spanish content.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login = new LoginPage(driver);
			login.clickSpanish();
			waitForElementToDisplay(login.LoginCompanyLogo);
			//System.out.println(isElementExisting(driver, login.LoginCompanyLogo));
			verifyElementTextContains(login.Header1, "Bienvenido a Career View", test);
			verifyElementTextContains(login.Header2, "Una herramienta para trazar su futuro.", test);
			verifyElementTextContains(login.LogIn_link, "Iniciar sesión", test);
			verifyElementTextContains(login.Register_link, "Registrarse", test);
			verifyElementTextContains(login.ForgotYourPassword, "¿Olvidó la contraseña?", test);
			verifyContains(login.username_input.getAttribute("placeholder"), "ID de empleado","Placeholder is verified.", test);
			verifyContains(login.password_input.getAttribute("placeholder"), "Contraseña", "Placeholder is verified.",test);

			login.clickEnglish();

			verifyElementTextContains(login.Header1, "Welcome to Career View", test);
			verifyElementTextContains(login.Header2, "A tool to map your future.", test);
			verifyElementTextContains(login.LogIn_link, "Log In", test);
			verifyElementTextContains(login.Register_link, "Register", test);
			verifyElementTextContains(login.ForgotYourPassword, "Forgot your password?", test);
			verifyContains(login.username_input.getAttribute("placeholder"), "Employee ID","Placeholder is verified.", test);
			verifyContains(login.password_input.getAttribute("placeholder"), "Password", "Placeholder is verified.",test);
			//assertTrue(isElementExisting(driver, OverviewPage.myProfile_button, 5),"User logged in to application", test);
		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 3)
	public void loginWithInvalidCreds() throws Exception {

		try {
			test = reports.createTest("TC_05 - Logging in with Invalid Credentials.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login = new LoginPage(driver);
			login.loginWithInvalidCredentials("a123", "a123");
			verifyElementTextContains(login.invalidCredsMsg, "Invalid employee ID and/or password", test);

			login.loginWithNoCredentials();
			verifyElementTextContains(login.noCredsMsgUsername, "Employee Id field is required", test);
			verifyElementTextContains(login.noCredsMsgPwd, "Password field is required", test);

			login.loginWithEmployee_id("a123");
			verifyElementTextContains(login.noCredsMsgPwd, "Password field is required", test);
			//assertTrue(isElementExisting(driver, OverviewPage.myProfile_button, 5),"User logged in to application", test);
		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
			delay(2000);
		}
	}

	@Test(enabled = true, priority = 4)
	public void loginToCareerView() throws Exception {

		try {
			test = reports.createTest("TC_06 - Logging in to career view application");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			RegisterPage registerPage = new RegisterPage(driver);
			LoginPage loginPage = new LoginPage(driver);
			loginPage.clickForgotPwdLink();
			loginPage.clickSend();
			verifyElementTextContains(loginPage.noEmailMsg, "Email field is required", test);

			loginPage.enterInvalidEmail("abcd");
			verifyElementTextContains(loginPage.invalidEmailMsg,
					"The Email field is not a valid e-mail address.", test);

			loginPage.enterUnregisteredEmail("vidya.dandinashivara@mercer.com");
			delay(2000);

			verifyElementTextContains(loginPage.UnregisteredEmailMsg,
					"Invalid email address.Please confirm it or click the Register link.", test);

			//			/*******
			//			 * 
			//			 * 
			//			 * // loginPageLinks.enterValidEmail(); //
			//			 * verifyElementTextContains(WebElement, "An email has been sent
			//			 * to you in order to complete the reset password process.", test);
			//			 * 
			//			 * 
			//			 * DO NOT RUN THIS STEP as email will be triggered each time
			//			 *********/

			loginPage.clickLoginLink();
			assertTrue(isElementExisting(driver, loginPage.username_input), "Username field is present",
					test);
			assertTrue(isElementExisting(driver, loginPage.password_input), "Password field is present",
					test);

			loginPage.clickRegisterLink();

			assertTrue(isElementExisting(driver, loginPage.register.empId), "Username field is present",
					test);
			assertTrue(isElementExisting(driver, loginPage.register.password_input), "Password field is present",
					test);
			assertTrue(isElementExisting(driver, loginPage.register.LastName), "Lastname field is present", test);
			assertTrue(isElementExisting(driver, loginPage.register.FirstName), "Firstname field is present", test);
			assertTrue(isElementExisting(driver, loginPage.register.ConfirmPwd), "Confirm password field is present", test);

			loginPage.register.clickSubmit();
			verifyElementTextContains(loginPage.register.NoEmpIdMsg, "Employee Id field is required", test);
			verifyElementTextContains(loginPage.register.NoFirstNameMsg, "First Name field is required", test);
			verifyElementTextContains(loginPage.register.NoLastNameMsg, "Last Name field is required", test);

			loginPage.register.allFieldsExceptPwd("123" ,"abcd","efgh");

			verifyElementTextContains(loginPage.register.noPwdMsg,
					"Passwords must be at least eight (8) characters and include at least one numeric (0-9) and one punctuation mark, symbol and alternate case (upper & lower).",
					test);

		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
			driver.quit();
		}
	}


}
