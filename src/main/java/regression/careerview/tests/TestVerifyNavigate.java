package regression.careerview.tests;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;

import pages.careerview.HeaderPage;
import pages.careerview.HomePage;
import pages.careerview.ITPage;
import pages.careerview.LocateMePage;
import pages.careerview.LoginPage;
import pages.careerview.LogisticsPage;
import pages.careerview.MarketingPages;
import pages.careerview.MyProfilePages;
import pages.careerview.NavigateHomePage;
import pages.careerview.NavigatePopUpPage;
import pages.careerview.OverviewPage;
import pages.careerview.StartExploringPages;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestVerifyNavigate extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public TestVerifyNavigate(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestVerifyNavigate careerViewTest = new TestVerifyNavigate("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)
	public void loginToCareerView() throws Exception {
		try {
			test = reports.createTest("TC_08 - Logging in to career view application successfully");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage loginPage = new LoginPage(driver);
			OverviewPage overview = new OverviewPage(driver);
			HeaderPage header = new HeaderPage(driver);
			HomePage homePage = new HomePage(driver);

			loginPage.loginToCareerView(USERNAME, PASSWORD);

			waitForElementToDisplay(homePage.myProfile_button);
			assertTrue(isElementExisting(driver, homePage.myProfile_button),
					"User logged in to application", test);
			assertTrue(isElementExisting(driver, homePage.startExploring_button),
					"User logged in to application", test);

			homePage.clickStartExploring();
			StartExploringPages explore = new StartExploringPages(driver);
			assertTrue(isElementExisting(driver, homePage.exploreNarrow),
					"Explore Narrow button is present", test);
			assertTrue(isElementExisting(driver, homePage.exploreWide),
					"Explore Wide button is present.", test);


			homePage.clickExploreWide();

			assertTrue(isElementExisting(driver, header.zoomIn), "ZoomIn button is present.", test);
			assertTrue(isElementExisting(driver, header.zoomOut), "ZoomOut button is present.",
					test);
			assertTrue(isElementExisting(driver, header.locateMe_button), "Locate Me button is present.",
					test);
			assertTrue(isElementExisting(driver, header.navigate_button), "Navigate button is present.",
					test);
			assertTrue(isElementExisting(driver, header.compare_button), "Compare button is present.",
					test);
			assertTrue(isElementExisting(driver, header.help_button), "Help button is present.", test);

			header.clickSignout();
			loginPage.loginToCareerView(USERNAME, PASSWORD);
			waitForPageLoad(driver);
			homePage.clickStartExploring();
			homePage.clickExploreNarrow();
			header.zoomIn();
			waitForElementToDisplay(explore.appDev);
			verifyElementTextContains(explore.appDev, "Application Development", test);

			header.clickSignout();

			loginPage.loginToCareerView(USERNAME, PASSWORD);
			waitForPageLoad(driver);
			homePage.goTomyProfile();
			verifyElementTextContains(overview.myProfile_header, "My Profile", test);
			overview.closeProfilepopup();
			assertTrue(isElementExisting(driver, header.zoomIn), "ZoomIn button is present.", test);

		} 

		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		} 
		catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}

		finally {
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 2)
	public void verifyUserHomePage() throws Exception {
		try {
			test = reports.createTest("TC_08 - Verifying functionalities in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);


			StartExploringPages explore = new StartExploringPages(driver);
			LocateMePage locateMe = new LocateMePage(driver);
			HomePage home = new HomePage(driver);
			HeaderPage header = new HeaderPage(driver);

			assertTrue(isElementExisting(driver, header.zoomIn), "ZoomIn button is present.", test);
			assertTrue(isElementExisting(driver, header.zoomOut), "ZoomOut button is present.",
					test);
			assertTrue(isElementExisting(driver, header.locateMe_button), "Locate Me button is present.",
					test);
			assertTrue(isElementExisting(driver, header.navigate_button), "Navigate button is present.",
					test);
			assertTrue(isElementExisting(driver, header.compare_button), "Compare button is present.",
					test);
			assertTrue(isElementExisting(driver, header.help_button), "Help button is present.", test);

			assertTrue(isElementExisting(driver, home.marketing), "Marketing link is present.", test);
			assertTrue(isElementExisting(driver, home.Logistics), "Logistics link is present.", test);
			assertTrue(isElementExisting(driver, home.RnD), "RnD link is present.", test);
			assertTrue(isElementExisting(driver, home.IT), "IT link is present.", test);

			assertTrue(home.checkPresence(), "Blinking circle is present.", test);

			home.clickMarketing();
			MarketingPages marketing = new MarketingPages(driver);

			marketing.zoomOut();
			marketing.zoomOut();
			assertTrue(isElementExisting(driver, marketing.brandMgmt), "1st Marketing branch is present", test);
			assertTrue(isElementExisting(driver, marketing.marketComm), "2nd Marketing branch is present", test);
			assertTrue(isElementExisting(driver, marketing.marketResearch), "3rd Marketing branch is present", test);
			assertTrue(isElementExisting(driver, marketing.strategicMarketing), "4th Marketing branch is present", test);

			header.navigateViaHeader(header.home_button);
			assertTrue(isElementExisting(driver, header.zoomIn), "ZoomIn button is present.", test);
			assertTrue(isElementExisting(driver, header.zoomOutDisabled), "ZoomOut button is disabled.",
					test);

			header.zoomIn();
			waitForElementToDisplay(explore.appDev);
			assertTrue(isElementExisting(driver, locateMe.ownRole_hightlighted), "Position is displayed correctly.",
					test);
			assertTrue(isElementExisting(driver, header.zoomOut), "ZoomOut button is enabled.",
					test);

			header.zoomOut();
			assertTrue(isElementExisting(driver, explore.DbDevAdmin), "Page was zoomed out.",
					test);


		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}

		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 3)
	public void careerView_GeneralUserLocateMe () throws Exception {
		try {
			test = reports.createTest("TC_09 - Verifying LocateMe page in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LocateMePage locateMe = new LocateMePage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.navigateViaHeader(header.locateMe_button);
			assertTrue(isElementExisting(driver, locateMe.ownRole_hightlighted), "Position is displayed correctly.",test);
			assertTrue(isElementExisting(driver, locateMe.SideMinimap), "Mini map is displayed correctly.",test);
			assertTrue(isElementExisting(driver, locateMe.miniAD), "AD is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniDDA), "DDA is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniSI), "SI is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniQA), "QA is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniECOM), "ECOM is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniNetW), "NetW is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.miniSec), "Sec is present in mini map",test);

			header.zoomOut();
			header.zoomOut();
			header.zoomOut();

			assertTrue(isElementExisting(driver, locateMe.macroAD), "AD is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroDDA), "DDA is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroSI), "SI is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroQA), "QA is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroECOM), "ECOM is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroNetW), "NetW is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroSec), "Sec is present in the displayed structure",test);
			assertTrue(isElementExisting(driver, locateMe.macroTS), "TS is present in the displayed structure",test);
			scrollToElement(driver , locateMe.miniTS);
			assertTrue(isElementExisting(driver, locateMe.miniTS), "TS is present in mini map",test);
			assertTrue(isElementExisting(driver, locateMe.sideMiniMapArrow), "The side mini map arrow is present",test);

			locateMe.clickUpArrow();

			assertTrue(isElementExisting(driver, locateMe.ArrowDropdown_Logistics), "Logistics is displayed in the list when clicked on sideArrow",test);
			assertTrue(isElementExisting(driver, locateMe.Arrowdropdown_Marketing), "Marketing is displayed in the list when clicked on sideArrow",test);
			assertTrue(isElementExisting(driver, locateMe.ArrowDropdown_RnD), "RnD is displayed in the list when clicked on sideArrow",test);
			assertTrue(isElementExisting(driver, locateMe.ArrowDropdown_IT), "IT is displayed in the list when clicked on sideArrow",test);

			locateMe.clickLogistics_UpArrow();
			LogisticsPage logistics = new LogisticsPage(driver);
			header.zoomOut();
			header.zoomOut();
			assertTrue(isElementExisting(driver, logistics.mini_Materials_Warehousing), "Material Warehousing is present in the mini map",test);
			assertTrue(isElementExisting(driver, logistics.mini_Transportation_Distribution), " Transportation/Distribution is present in the mini map",test);
			assertTrue(isElementExisting(driver, logistics.miniLog_plan), "Planning is present in the mini map",test);
			assertTrue(isElementExisting(driver, logistics.miniLog_Oper), "Operations is present in the mini map",test);
			assertTrue(isElementExisting(driver, logistics.macro_Materials_Warehousing), "Material Warehousing is displayed in the department structure.",test);
			assertTrue(isElementExisting(driver, logistics.macro_Transportation_Distribution), "Transportation/Distribution is displayed in the department structure",test);
			assertTrue(isElementExisting(driver, logistics.macroLog_Planning), "Planning is displayed in the department structure",test);
			assertTrue(isElementExisting(driver, logistics.macroLog_Oper), "Operations is displayed in the department structure",test);

		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}

		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 4)
	public void verifyNavigatePage () throws Exception {
		try {
			test = reports.createTest("TC_10 - Verifying Navigate page in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			ITPage itPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.navigateViaHeader(header.navigate_button);
			assertTrue(isElementExisting(driver, navigatePopUp.navigatePopUp_modal),"Navigate pop up is visible", test);
			assertTrue(isElementExisting(driver, navigatePopUp.closeModal_button),"Close (X) symbol at top right corner is visible", test);
			assertTrue(isElementExisting(driver, navigatePopUp.start_button),"'Start' button at bottom right corner is visible", test);
			navigatePopUp.closePopup();

			header.navigateViaHeader(header.navigate_button);
			navigatePopUp.clickStart();

			assertTrue(isElementExisting(driver, navigateHomePage.userCurrentRole)," User Current Role is marked with red circle", test);
			verifyElementTextContains(navigateHomePage.defaultNumber, "1", test);
			assertTrue(isElementExisting(driver, navigateHomePage.careerPath),"Careerpath textbox is present.", test);
			assertTrue(navigateHomePage.textboxWatermark(), "Watermark is present.", test);
			assertTrue(isElementExisting(driver, navigateHomePage.saveButton),"Save button is present.", test);
			assertTrue(isElementExisting(driver, navigateHomePage.prevButton),"Previous button is present.", test);
			assertTrue(isElementExisting(driver, navigateHomePage.nextButton),"Next button is present.", test);

			itPage.selectRole(itPage.progAnalyst);
			navigateHomePage.clickSave();
			String alertText = navigateHomePage.getAlertText();
			Boolean bool = alertText.equalsIgnoreCase("You must enter a name for this path.");
			assertTrue(bool, "Alert box with a proper message is displayed.", test);

			itPage.selectRole(itPage.progAnalyst);
			navigateHomePage.enterValidName();
			String alertText2 = navigateHomePage.getAlertText();
			Boolean bool2 = alertText2.equalsIgnoreCase("You must select at least one role.");
			assertTrue(bool2, "Alert box with a proper message is displayed.", test);

			itPage.selectRole(itPage.progAnalyst);
			navigateHomePage.enterValidName();

			verifyElementTextContains(navigateHomePage.savedProfileMsg, "Saved to profile.",  test);
			verifyElementTextContains(navigateHomePage.makeAnotherbutton, "Make another", test);

			navigateHomePage.clickMakeAnother();
			assertTrue(isElementExisting(driver, navigateHomePage.userCurrentRole)," User Current Role is marked with red circle", test);

		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 5)
	public void verifySavedPath () throws Exception {
		try {
			test = reports.createTest("TC_11 - Verifying Saved paths in HomePage");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			ITPage ITPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			myProfile.deleteAllPaths();
			myProfile.closeProfilePopup();

			header.navigateViaHeader(header.navigate_button);
			navigatePopUp.clickStart();

			ITPage.selectRole(ITPage.progAnalyst);
			String userLastSelectedRole = ITPage.getRoleText(ITPage.progAnalystText);
			navigateHomePage.enterValidName();
			verifyElementTextContains(navigateHomePage.savedProfileMsg, "Saved to profile.",  test);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			assertTrue(isElementExisting(driver, myProfile.firstPath), "Path is displayed.", test);
			verifyElementTextContains(myProfile.userRole, myProfile.UserCurrentRole, test);
			assertTrue((myProfile.userSelectedRoleInPaths.getText()).equals(userLastSelectedRole), "Role Selected by user is correct", test);

			myProfile.closeProfilePopup();

		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}
	@Test(enabled = true, priority = 6)
	public void verifyEditPath () throws Exception {
		try {
			test = reports.createTest("TC_12 - Verifying Edit path functionality in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			ITPage ITPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			myProfile.selectUserPath();

			ITPage.editPath();
			String userSelectedRole = ITPage.getRoleText(ITPage.SRProgAnalystText);
			String alertText = navigateHomePage.SelectNewPath();
			assertTrue(alertText.contains("Please confirm to save it as a new path or override existing path."), "Alert box with a proper message is displayed.", test);
			verifyElementTextContains(navigateHomePage.savedProfileMsg, "Saved to profile.", test);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			assertTrue(userSelectedRole.equals(myProfile.userSelectedRoleInPaths.getText()), "Edited path is saved correctly.", test);

			System.out.println(myProfile.userSelectedRoleInPaths.getText());
			assertTrue((myProfile.pathName.getText()).equals("Test123"), "UserRole name was changed correctly.", test);
			myProfile.closeProfilePopup();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 7)
	public void verifyDeletePath () throws Exception {
		try {
			test = reports.createTest("TC_13 - Verifying Delete path functionality in HomePage");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			ITPage ITPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.navigateViaHeader(header.navigate_button);
			navigatePopUp.clickStart();

			ITPage.selectRole(ITPage.progAnalyst);
			String userLastSelectedRole = ITPage.getRoleText(ITPage.progAnalystText);
			navigateHomePage.enterValidName();	

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			myProfile.deletePathAndCancel();
			
			assertTrue(isElementExisting(driver, myProfile.userSelectedRoleInPaths2), "Path was not deleted.", test);
			myProfile.deletePath();
			assertTrue(isElementExisting(driver, myProfile.userSelectedRoleInPaths), "Path was deleted correctly.", test);

			myProfile.closeProfilePopup();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 8)
	public void verifyNavigate_SameFamily () throws Exception {
		try {
			test = reports.createTest("TC_14 - Verifying Navigation within the same department(Same Family)");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			ITPage ITPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);

			header.navigateViaHeader(header.navigate_button);
			navigatePopUp.clickStart();

			ITPage.clickBasicRoles();
			String text = ITPage.getRoleText(ITPage.SwAppEnggIIIText);
			ITPage.clickOtherRoles();

			//System.out.println(text);

			header.zoomIn();
			ITPage.scrollToElement(driver);
			verifyElementTextContains(ITPage.AppDevNumber, "2",  test);
			verifyElementTextContains(ITPage.progAnalystNumber, "3",  test);
			verifyElementTextContains(ITPage.JavaDevNumber, "4",  test);
			verifyElementTextContains(ITPage.SrProgAnalystNumber, "5",  test);
			verifyElementTextContains(ITPage.SwAppEnggIIINumber, "6",  test);

			navigateHomePage.enterValidName();
			verifyElementTextContains(navigateHomePage.savedProfileMsg, "Saved to profile.",  test);
			verifyElementTextContains(navigateHomePage.makeAnotherbutton, "Make another",  test);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);

			//System.out.println(myProfile.userSelectedRoleInPaths2.getText());

			assertTrue((myProfile.userSelectedRoleInPaths2.getText()).equals(text), "Role Selected by user is correct", test);
			myProfile.closeProfilePopup();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}


	@Test(enabled = true, priority = 9)
	public void verifyNavigateDifferentFamily () throws Exception {
		try {
			test = reports.createTest("TC_15 - Verifying Navigation within the different department(Different Family)");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			NavigateHomePage navigateHomePage = new NavigateHomePage(driver);
			LogisticsPage logisticsPage = new LogisticsPage(driver);
			ITPage itPage = new ITPage(driver);
			HeaderPage header = new HeaderPage(driver);


			header.navigateViaHeader(header.navigate_button);
			navigatePopUp.clickStart();

			itPage.clickOtherFamilyRoles();
			assertTrue(isElementExisting(driver, logisticsPage.macro_Materials_Warehousing), "Material Warehousing is displayed in the department structure.",test);
			assertTrue(isElementExisting(driver, logisticsPage.macro_Transportation_Distribution), "Transportation/Distribution is displayed in the department structure",test);
			assertTrue(isElementExisting(driver, logisticsPage.macroLog_Planning), "Planning is displayed in the department structure",test);
			assertTrue(isElementExisting(driver, logisticsPage.macroLog_Oper), "Operations is displayed in the department structure",test);

			logisticsPage.selectRoles();
			String text = itPage.getRoleText(logisticsPage.CordsMWText);	
			//System.out.println(text);
			verifyElementTextContains(logisticsPage.CordsMWNumber, "5",  test);
			navigateHomePage.enterValidName();
			verifyElementTextContains(navigateHomePage.savedProfileMsg, "Saved to profile.",  test);
			verifyElementTextContains(navigateHomePage.makeAnotherbutton, "Make another",  test);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.paths);
			//System.out.println(myProfile.userSelectedRoleInPaths.getText());
			assertTrue((myProfile.userSelectedRoleInPaths.getText()).equals(text), "Role Selected by user is correct", test);

		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
