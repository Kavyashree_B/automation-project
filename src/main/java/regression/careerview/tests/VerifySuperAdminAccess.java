package regression.careerview.tests;

import static driverfactory.Driver.*;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.AdminHeaderPage;
import pages.careerview.AdminPage;
import pages.careerview.LoginPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class VerifySuperAdminAccess extends InitTests{

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public VerifySuperAdminAccess(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		VerifySuperAdminAccess careerViewTest = new VerifySuperAdminAccess("CareerView");
			webdriver=driverFact.initWebDriver("https://careerview-stage.mercer.com/Demo2016/Admin",BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	

	@Test(enabled = true, priority = 1)

	public void companyAdminLogin() throws Exception {
		try {
			test = reports.createTest("TC_49 - Verify access to Super Admin site");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage loginPage = new LoginPage(driver);
			loginPage.loginToCareerView(USERNAME, PASSWORD);
			AdminHeaderPage adminHeader = new AdminHeaderPage(driver);
			waitForElementToDisappear(By.xpath(adminHeader.users_xpath));
			assertTrue(true, "Users tab is not present", test);
			
			
		}  catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}  finally {
			reports.flush();
			driver.quit();
		}
	}
	
}
