package regression.careerview.tests;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.How;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.ComparePage;
import pages.careerview.ComparePopUpPage;
import pages.careerview.CompareProcessPopUp;
import pages.careerview.CoreCompetencyPage;
import pages.careerview.HeaderPage;
import pages.careerview.HelpPage;
import pages.careerview.HomePage;
import pages.careerview.ITPage;
import pages.careerview.LocateMePage;
import pages.careerview.LoginPage;
import pages.careerview.LogisticsPage;
import pages.careerview.ManagerAssessmentPage;
import pages.careerview.MyProfilePages;
import pages.careerview.OverviewPage;
import pages.careerview.TechnicalCompetencyPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestVerifyCompare extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public TestVerifyCompare(String appname) {
		super(appname);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestVerifyCompare careerViewTest = new TestVerifyCompare("CareerView");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)

	public void verifyCompare() throws Exception {
		try {
			test = reports.createTest("TC_16 - Verifying Compare in HomePage");
			test.assignCategory("regression");


			//System.out.println("Logging in to application");
			driver=driverFact.getEventDriver(webdriver,test);
			//System.out.println("BaseURL is: " + BASEURL);


			LoginPage loginPage = new LoginPage(driver);
			LocateMePage locateMe = new LocateMePage(driver);
			MyProfilePages myProfile = new MyProfilePages(driver);

			ComparePopUpPage comparePopUp = new ComparePopUpPage(driver);
			ComparePage comparePage = new ComparePage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);
			HomePage home = new HomePage(driver);

			loginPage.loginToCareerView(USERNAME, PASSWORD);
			home.clickStartExploring();
			home.clickExploreWide();

			HeaderPage header = new HeaderPage(driver);
			header.openProfile();
			myProfile.navigateToSideTab(myProfile.comparisonsLink);

			for(WebElement comparison: myProfile.deleteComparison) {
				clickElementUsingJavaScript(driver, comparison);
				String alertText = compareProcessPopUp.getAlertTextForDeleteComparison();
				Boolean bool = alertText.equalsIgnoreCase("Are you sure you want to delete this comparison?");
				assertTrue(bool, "Alert box with a proper message is displayed.", test);
			}

			myProfile.closeProfilePopup();

			header.navigateViaHeader(header.compare_button);
			assertTrue(isElementExisting(driver, comparePopUp.startcompare_button),
					"compare pop up is visible", test);
			assertTrue(isElementExisting(driver, comparePopUp.closeModal_button),
					"compare pop up is visible", test);
			comparePopUp.closeModal();
			assertTrue(home.checkPresence(), "Blinking circle is present.", test);

			header.navigateViaHeader(header.compare_button);
			comparePopUp.enterComparePage();
			assertTrue(isElementExisting(driver, locateMe.ownRole_hightlighted),
					"Position is displayed correctly.", test);
			verifyElementTextContains(comparePage.comparePageMsg, "Select up to two roles to Compare.",
					test);
			assertTrue(isElementExisting(driver, comparePage.Xmark), "X (Close) Symbol is present.",
					test);

			comparePage.clickXmark();

			waitForElementToDisappear(By.xpath(comparePage.Xmark_xpath));
			assertTrue(true, "X (Close) Symbol is not present.",
					test);

			header.navigateViaHeader(header.compare_button);
			comparePopUp.enterComparePage();
			ITPage itPage = new ITPage(driver);
			comparePage.selectSingleRoleToCompare();
			String roleSelected = comparePage.getRoleText(itPage.progAnalyst);

			verifyElementTextContains(comparePage.selectedRoleMsg, "Selected:", test);
			assertTrue(isElementExisting(driver, comparePage.Xmark), "X (Close) Symbol is present.",
					test);
			assertTrue(roleSelected.equals(comparePage.selectedRole.getText()),
					"Selected Role is correct", test);

			comparePage.clickXmark();
			delay(2000);

			waitForElementToDisappear(By.xpath(comparePage.selRole_xpath));
			assertTrue(true, "Selected Element is no longer displayed.", test);

			header.navigateViaHeader(header.compare_button);
			comparePopUp.enterComparePage();
			comparePage.selectSingleRoleToCompare();
			comparePage.selectSingleRoleToCompare();
			delay(2000);
			waitForElementToDisappear(By.xpath(comparePage.selRole_xpath));
			assertTrue(true, "Selected Element is no longer displayed.", test);
			//System.out.println("Compare btn list: "+ comparePage.checkPresence(comparePage.compareBtnList));

			waitForElementToDisappear(By.xpath(comparePage.compareBtn_xpath));
			assertTrue(true, "Compare Button is no longer displayed.", test);

			assertTrue(isElementExisting(driver, locateMe.ownRole_hightlighted),
					"Position is displayed correctly.", test);

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 2)
	public void verifyCompareSameFamily() throws Exception {
		try {
			test = reports.createTest("TC_17 - Verifying Compare functionality within same family.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);


			ComparePage comparePage = new ComparePage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);
			ITPage itPage = new ITPage(driver);

			comparePage.compareWithinSameFamily();
			String roleComparePage = comparePage.getRoleText(itPage.progAnalystText, itPage.SwAppEnggIIIText , itPage.progAnalystIText);

			waitForElementToDisappear(By.xpath(comparePage.thirdElementToCompare));
			assertTrue(true,"Third Element cannot be selected.", test);
			comparePage.startCompare();
			assertTrue(isElementExisting(driver, compareProcessPopUp.sideModal), "Compare Pop up is opened.",test);

			waitForElementToDisplay(compareProcessPopUp.roleSelected1);
			String selRoles = comparePage.getRoleText(compareProcessPopUp.roleSelected2, compareProcessPopUp.roleSelected1, compareProcessPopUp.roleSelected3);
			/*verifyElementTextContains(compareProcessPopUp.roleSelected1, "Software Application Engineer III", test);
			verifyElementTextContains(compareProcessPopUp.roleSelected2, "Programmer Analyst", test);
			verifyElementTextContains(compareProcessPopUp.roleSelected3, "Programmer Analyst I", test);*/

			verifyContains(roleComparePage, selRoles,"All the roles are correctly displayed.", test);

			compareProcessPopUp.SaveComparison();
			waitForElementToDisplay(compareProcessPopUp.saveBtnAfterSaving);
			assertTrue(isElementExisting(driver, compareProcessPopUp.saveBtnAfterSaving),
					"Button text was changed to Saved and color turned to orange.", test);

			//-----------------------------------------------------------------------------------------------------------------------------------------------//			
			compareProcessPopUp.navigateTo(compareProcessPopUp.coreCompetencies);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected1);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 3 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 2 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 2 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 1 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 1 Expected Competency", test);
			//------------------------------------------------------------------------------------------------------------------------------------------------//
			compareProcessPopUp.navigateTo(compareProcessPopUp.technicalCompetencies);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected1);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 3 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 3 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 2 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 2 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 1 Expected Competency", test);

			// ===============================================================================================================================================
			compareProcessPopUp.navigateTo(compareProcessPopUp.pathsOthersHaveTaken);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected1);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Software Application Engineer III", test);
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			delay(3000);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				assertTrue((role.getText()).equals("Programmer Analyst"), "Role displayed is correct.", test);
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Programmer Analyst I", test);
			}
			// ====================================================================================================================================================

			// ADD CODE FOR DAY IN THE LIFE && OTHERS IN THIS ROLE

			compareProcessPopUp.navigateTo(compareProcessPopUp.dayInTheLife);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			String src = compareProcessPopUp.checkPresence();
			assertTrue(src.equals("https://www.youtube.com/embed/MoRayPASOWI"), "Youtube url is present", test);

			compareProcessPopUp.navigateTo(compareProcessPopUp.othersInThisRole);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.otherRolesToBeVerified) {
				assertTrue(isElementExisting(driver, role), "Roles are present", test);
			}
			// ==============================================================================================================================================
			assertTrue(isElementExisting(driver, compareProcessPopUp.compare_SaveBtn), "Save Button is present.",
					test);
			compareProcessPopUp.SaveComparison();
			String alertText1  = compareProcessPopUp.getAlertText();
			Boolean bool = alertText1.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool, "Alert box with a proper message is displayed.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected1);
			compareProcessPopUp.SaveComparison();
			String alertText2  = compareProcessPopUp.getAlertText();
			Boolean bool2 = alertText2.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool2, "Alert box with a proper message is displayed.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			compareProcessPopUp.SaveComparison();
			String alertText3  = compareProcessPopUp.getAlertText();
			Boolean bool3 = alertText3.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool3, "Alert box with a proper message is displayed.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			compareProcessPopUp.SaveComparison();
			String alertText4  = compareProcessPopUp.getAlertText();
			Boolean bool4 = alertText4.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool4, "Alert box with a proper message is displayed.", test);

			comparePage.close_modal();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 3)
	public void verifyComparDifferentFamily() throws Exception {
		try {
			test = reports.createTest("TC_18 - Verifying Compare functionality within Different family.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			LogisticsPage logistics = new LogisticsPage(driver);
			ComparePopUpPage comparePopUpPage = new ComparePopUpPage(driver);
			ComparePage comparePage = new ComparePage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);
			HeaderPage header = new HeaderPage(driver);
			ITPage itPage = new ITPage(driver);

			header.navigateViaHeader(header.compare_button);
			comparePopUpPage.enterComparePage();

			comparePage.clickRole(itPage.progAnalyst);
			String progAnalystText = comparePage.getRoleText(itPage.progAnalystText);
			String progAnalystIText = comparePage.getRoleText(itPage.progAnalystIText);
			comparePage.compareWithinDifferentFamily();
			String roleComparePage = comparePage.getRoleText(progAnalystText, logistics.AnalystMWText , progAnalystIText);
			waitForElementToDisappear(By.xpath(logistics.thirdElementToCompare));
			assertTrue(true,"Third Element cannot be selected.", test);
			comparePage.startCompare();
			assertTrue(isElementExisting(driver, compareProcessPopUp.sideModal), "Compare Pop up is opened.",
					test);

			waitForElementToDisplay(compareProcessPopUp.roleSelected4);
			String selRoles = comparePage.getRoleText(compareProcessPopUp.roleSelected2, compareProcessPopUp.roleSelected4, compareProcessPopUp.roleSelected3);

			verifyContains(roleComparePage, selRoles, "All selected roles are displayed correctly.", test);

			compareProcessPopUp.SaveComparison();
			waitForElementToDisplay(compareProcessPopUp.saveBtnAfterSaving);
			assertTrue(isElementExisting(driver, compareProcessPopUp.saveBtnAfterSaving),
					"Button text was changed to Saved and color turned to orange.", test);
			// =============================================================================================================================================
			compareProcessPopUp.navigateTo(compareProcessPopUp.coreCompetencies);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			assertTrue((compareProcessPopUp.coachesAndDevCC.getText()).equals("Level 2 Expected Competency"), "Text is present.", test);
			assertTrue((compareProcessPopUp.customerFocusCC.getText()).equals("Level 1 Expected Competency"), "Text is present.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 1 Expected Competency", test);
			// =========================================================================================================================================
			compareProcessPopUp.navigateTo(compareProcessPopUp.technicalCompetencies);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			waitForElementToDisplay(compareProcessPopUp.coachesAndDevTC);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 2 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 2 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			waitForElementToDisplay(compareProcessPopUp.coachesAndDevTC);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 1 Expected Competency", test);

			// ===============================================================================================================================================
			compareProcessPopUp.navigateTo(compareProcessPopUp.pathsOthersHaveTaken);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Analyst Materials / Warehousing", test);
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			delay(3000);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				///////////////////////////////////////////////////////////////////////////////////////////////
				assertTrue((role.getText()).equals("Programmer Analyst"), "Role displayed is correct.", test);
				/////////////////////////////////////////////////////////////////////////////////////////////////////
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Programmer Analyst I", test);
			}
			// ====================================================================================================================================================

			// ADD CODE FOR DAY IN THE LIFE && OTHERS IN THIS ROLE

			compareProcessPopUp.navigateTo(compareProcessPopUp.dayInTheLife);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			String src = compareProcessPopUp.checkPresence();
			assertTrue(src.equals("https://www.youtube.com/embed/MoRayPASOWI"), "Youtube url is present", test);

			compareProcessPopUp.navigateTo(compareProcessPopUp.othersInThisRole);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.otherRolesToBeVerified) {
				assertTrue(isElementExisting(driver, role), "Roles are present", test);
			}
			// ==============================================================================================================================================
			assertTrue(isElementExisting(driver, compareProcessPopUp.compare_SaveBtn), "Save Button is present.",
					test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			compareProcessPopUp.SaveComparison();
			String alertText1 = compareProcessPopUp.getAlertText();
			Boolean bool = alertText1.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool, "Alert box with a proper message is displayed.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			compareProcessPopUp.SaveComparison();
			String alertText2 =compareProcessPopUp.getAlertText();
			Boolean bool2 = alertText2.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool2, "Alert box with a proper message is displayed.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			compareProcessPopUp.SaveComparison();
			String alertText3 =compareProcessPopUp.getAlertText();
			Boolean bool3 = alertText3.equalsIgnoreCase("Comparision already saved.");
			assertTrue(bool3, "Alert box with a proper message is displayed.", test);

			comparePage.close_modal();

		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
		}
	}



	@Test(enabled = true, priority = 4)
	public void viewSavedComparison() throws Exception {
		try {
			test = reports.createTest("TC_19 - Verifying saved comparisons in the myProfile page.");
			test.assignCategory("regression");
			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			ComparePopUpPage cpp = new ComparePopUpPage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			myProfile.viewComparisons();
			//--------------------------------------------------------------------------------------------------------------------------------------------------------//
			compareProcessPopUp.navigateTo(compareProcessPopUp.coreCompetencies);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			assertTrue(isElementExisting(driver, compareProcessPopUp.roleSelected4), "Profile photos are present", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 2 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 1 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevCC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusCC, "Level 1 Expected Competency", test);

			//--------------------------------------------------------------------------------------------------------------------------------------------------------//
			compareProcessPopUp.navigateTo(compareProcessPopUp.technicalCompetencies);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			assertTrue(isElementExisting(driver, compareProcessPopUp.roleSelected4), "Profile photos are present", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 2 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 2 Expected Competency", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			verifyElementTextContains(compareProcessPopUp.coachesAndDevTC, "Level 1 Expected Competency", test);
			verifyElementTextContains(compareProcessPopUp.customerFocusTC, "Level 1 Expected Competency", test);

			//--------------------------------------------------------------------------------------------------------------------------------------------------------//
			compareProcessPopUp.navigateTo(compareProcessPopUp.pathsOthersHaveTaken);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Analyst Materials / Warehousing", test);
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				delay(2000);
				waitForElementToDisplay(role);
				assertTrue((role.getText()).equals("Programmer Analyst"), "Role displayed is correct.", test);
			}

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.pathsToBeVerified) {
				verifyElementTextContains(role, "Programmer Analyst I", test);
			}
			//--------------------------------------------------------------------------------------------------------------------------------------------------------//

			compareProcessPopUp.navigateTo(compareProcessPopUp.othersInThisRole);
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for (WebElement role : compareProcessPopUp.otherRolesToBeVerified) {
				assertTrue(isElementExisting(driver, role), "Roles are present", test);
			}
			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected2);
			verifyElementTextContains(compareProcessPopUp.progAnalystOthersInThisRoleMsg, "We're sorry, but all employees in this role have marked themselves as private.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected4);
			verifyElementTextContains(compareProcessPopUp.progAnalystOthersInThisRoleMsg, "We're sorry, but all employees in this role have marked themselves as private.", test);

			compareProcessPopUp.clickRole(compareProcessPopUp.roleSelected3);
			for(WebElement profilePhoto : compareProcessPopUp.OtherInThisRoleProfilePhoto) {
				assertTrue(isElementExisting(driver, profilePhoto), "Profile photos are present", test);
			}
			for(WebElement email : compareProcessPopUp.OtherInThisRoleEmail) {
				assertTrue(isElementExisting(driver, email), "Email links are present", test);
			}

			cpp.closeModal();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}finally {
			reports.flush();
		}
	}

	@Test(enabled = true, priority = 5)
	public void verifyDeleteSavedComparison() throws Exception {
		try {
			test = reports.createTest("TC_20 - Verifying delete comparison.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			MyProfilePages myProfile = new MyProfilePages(driver);
			ComparePopUpPage comparePopUpPage = new ComparePopUpPage(driver);
			ComparePage comparePage = new ComparePage(driver);
			CompareProcessPopUp compareProcessPopUp = new CompareProcessPopUp(driver);
			HeaderPage header = new HeaderPage(driver);

			header.openProfile();
			myProfile.navigateToSideTab(myProfile.comparisonsLink);
			for(WebElement comparison: myProfile.deleteComparison) {
				clickElementUsingJavaScript(driver, comparison);
				CompareProcessPopUp comparePopUp = new CompareProcessPopUp(driver);
				String alertText = comparePopUp.getAlertTextForDeleteComparison();
				Boolean bool = alertText.equalsIgnoreCase("Are you sure you want to delete this comparison?");
				assertTrue(bool, "Alert box with a proper message is displayed.", test);
			}

			assertTrue(isElementExisting(driver, compareProcessPopUp.noComparisonMessage), "All comparisons were deleted", test);
			myProfile.closeProfilePopup();

			header.navigateViaHeader(header.compare_button);
			comparePopUpPage.enterComparePage();
			comparePage.compareWithinSameFamily();
			waitForElementToDisappear(By.xpath(comparePage.thirdElementToCompare));
			assertTrue(true, "Third Element cannot be selected.", test);
			comparePage.startCompare();
			assertTrue(isElementExisting(driver, compareProcessPopUp.sideModal), "Compare Pop up is opened.",
					test);
			verifyElementTextContains(compareProcessPopUp.roleSelected1, "Software Application Engineer III", test);
			verifyElementTextContains(compareProcessPopUp.roleSelected2, "Programmer Analyst", test);
			verifyElementTextContains(compareProcessPopUp.roleSelected3, "Programmer Analyst I", test);

			compareProcessPopUp.SaveComparison();
			waitForElementToDisplay(compareProcessPopUp.saveBtnAfterSaving);
			assertTrue(isElementExisting(driver, compareProcessPopUp.saveBtnAfterSaving),
					"Button text was changed to Saved and color turned to orange.", test);

			compareProcessPopUp.closeComparePopUp();

		} catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

		finally {
			reports.flush();
			driver.quit();
		}
	}
}
