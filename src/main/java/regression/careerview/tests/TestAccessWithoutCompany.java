package regression.careerview.tests;

import static driverfactory.Driver.delay;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.OrgHeaderPage;
import utilities.InitTests;
import static verify.SoftAssertions.*;

public class TestAccessWithoutCompany extends InitTests {


	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	public TestAccessWithoutCompany(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeClass() throws Exception
	{
		TestAccessWithoutCompany access = new TestAccessWithoutCompany("CareerView");
		webdriver=driverFact.initWebDriver("https://careerview-stage.mercer.com/organization",BROWSER_TYPE,EXECUTION_ENV,"");

	}

	@Test(enabled = true, priority = 1)
	public void accessWithoutCompany() throws Exception {

		try {
			test = reports.createTest("TC_01 - Trying to directly access the application without logging in.");
			test.assignCategory("regression");

			driver=driverFact.getEventDriver(webdriver,test);

			OrgHeaderPage orgHeader = new OrgHeaderPage(driver);
			orgHeader.waitForHeaderEle();
			verifyElementTextContains(orgHeader.HeaderEle, "Server Error", test);
			verifyElementTextContains(orgHeader.ForbiddenAccess, "403 - Forbidden: Access is denied.", test);
			verifyElementTextContains(orgHeader.PermissionMsg, "You do not have permission to view this directory or page using the credentials that you supplied.", test);
			//assertTrue(isElementExisting(driver, OverviewPage.myProfile_button, 5),"User logged in to application", test);

		}
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
			driver.quit();
		}
	}

}
