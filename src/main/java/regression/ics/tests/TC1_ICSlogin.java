package regression.ics.tests;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToDisappear;

import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.fail;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementHyperLink;

import static verify.SoftAssertions.verifyEquals;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.ics.*;
import pages.mobilityportal.MytoolsPage;
import pages.imercer.HomePage;
import pages.imercer.PrivacyStatement;
import pages.ics.LoginPage;
import utilities.FileDownloader;
import utilities.InitTests;

public class TC1_ICSlogin extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;
	String browser = InitTests.BROWSER_TYPE;
	String balSheetFileName="balancesheet.xls";
	ICSHomePage home;
	MytoolsPage mytool;
	public TC1_ICSlogin(String appname) {
		super(appname);
	}
	@BeforeClass
	public void beforeClass() throws Exception {
		
		TC1_ICSlogin obj = new TC1_ICSlogin("ICS");
		
	}

	@Test(enabled = true, priority = 1)
	public void verifyLogin() throws Exception {

		try {
			test = reports.createTest("verifyLogin");
			test.assignCategory("Regression");
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			home=new ICSHomePage(driver);
			waitForElementToDisplay(home.welcomeTxt);
			verifyElementTextContains(home.welcomeTxt, "Welcome", test);
			verifyElementTextContains(home.headerpage.compTab, "Compensation Tables", test);
			verifyElementTextContains(home.headerpage.calcTabLnk, "Calculators", test);
			verifyElementTextContains(home.headerpage.prodTab, "Products", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			
			reports.flush();

		}

	}

	@Test(enabled = true, priority = 2)
	public void verifyAccessOneTimeTable() throws Exception {

		try {
			test = reports.createTest("verifyAccessOneTimeTable");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			home.headerpage.navigateToCompLnk("One-Time Tables");
			CompensationTablePage compPage = new CompensationTablePage(driver);
			waitForElementToDisplay(compPage.oneTimeTablesTab);
			verifyElementTextContains(compPage.oneTimeTablesTab, "One-Time Tables", test);
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessOneTimeTable failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessOneTimeTable failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	
	@Test(enabled = true, priority = 3)
	public void verifyAccessQuickCalc() throws Exception {

		try {
			test = reports.createTest("verifyAccessQuickCalc");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			
			home.headerpage.navigateToCalMenu("Quick Calc");
			CalculatorPage calcpage = new CalculatorPage(driver);
			waitForElementToDisplay(calcpage.quickCalcTab);
			verifyElementTextContains(calcpage.quickCalcTab, "Quick Calc", test);
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessQuickCalc failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessQuickCalc failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 4)
	public void verifyAccessHomeCountryData() throws Exception {

		try {
			test = reports.createTest("verifyAccessHomeCountryData");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			home.headerpage.navigateToProdMenu("Housing & Education Reports");
			Products products = new Products(driver);
			waitForElementToDisplay(products.housingEduTab);
			verifyElementTextContains(products.housingEduTab, "Housing", test);
			verifyElementTextContains(products.housingEduTab, "Education Reports", test);
		
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessHomeCountryData failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAccessHomeCountryData failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}
	
	@Test(enabled = true, priority = 5)
	public void verifyHome() throws Exception {

		try {
			test = reports.createTest("verifyHome");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			home.headerpage.navigateToHome();
			ICSHomePage icshome=new ICSHomePage(driver);
			verifyElementTextContains(icshome.headerpage.accLnk, "My Account", test);
			verifyElementTextContains(icshome.headerpage.adminLnk, "Admin", test);
			verifyElementTextContains(icshome.headerpage.contactLnk, "Contact", test);
			verifyElementTextContains(icshome.headerpage.logoutLnk, "Logout", test);
			verifyElementTextContains(icshome.upcomingEventsTxt, "Upcoming Events", test);
			verifyElementTextContains(icshome.welcomeTxt, "Welcome", test);
			verifyElementTextContains(icshome.productsTxt, "Products", test);
			verifyElementTextContains(icshome.knowledgCenter, "Knowledge Center", test);
			verifyElementHyperLink(icshome.currEventsLnk,test);
			verifyElementHyperLink(icshome.balSheetLnk,test);
			verifyElementHyperLink(icshome.newCalculatoLnk,test);
			verifyElementHyperLink(icshome.webcastsLnk,test);
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyHome", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyHome", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void verifyNavigationBar() throws Exception {

		try {
			test = reports.createTest("verifyNavigationBar");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			ICSHomePage icshome=new ICSHomePage(driver);
			verifyElementTextContains(icshome.headerpage.homeTabLnk, "Home", test);
			verifyElementTextContains(icshome.headerpage.compTab, "Compensation Tables", test);
			verifyElementTextContains(icshome.headerpage.calcTabLnk ,"Calculators", test);
			verifyElementTextContains(icshome.headerpage.prodTab, "Products", test);
			verifyElementTextContains(icshome.headerpage.locTab, "Data by Location", test);
			verifyElementTextContains(icshome.headerpage.subscriptionTxt, "Subscription", test);
			verifyElementTextContains(icshome.productsTxt, "Products", test);
			verifyElementTextContains(icshome.knowledgCenter, "Knowledge Center", test);
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyNavigationBar", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyNavigationBar", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}

	@Test(enabled = true, priority = 7)
	public void verifyFooterLinks() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyFooterLinks");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			String icsWindow=driver.getWindowHandle();
			waitForElementToDisplay(icshome.footerPage.mercerLLTxt);
			verifyElementTextContains(icshome.footerPage.mercerLLTxt, "Mercer LLC, All Rights Reserved", test);
			icshome.footerPage.clickOnTermsLnk();
			
			switchToWindowByTitle("iMercer.com",driver);
			HomePage imercerHome=new HomePage(driver);
			waitForElementToDisplay(imercerHome.imercerWelcomTxt);
			verifyElementTextContains(imercerHome.imercerWelcomTxt, "Welcome to imercer.com", test);
			driver.close();
			switchToWindow(icsWindow,driver);
			
			icshome.footerPage.clickOnPrivacyLnk();
			switchToWindowByTitle("Privacy Statement",driver);
			PrivacyStatement privacy=new PrivacyStatement(driver);
			waitForElementToDisplay(privacy.privacyStateTxt);
			verifyElementTextContains(privacy.privacyStateTxt, "PRIVACY STATEMENT", test);
			driver.close();
			switchToWindow(icsWindow,driver);
			
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyFooterLinks", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyFooterLinks", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	@Test(enabled = true, priority = 8)
	public void verifyCompensationMenu() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyCompensationMenu");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			hoverOverElement(icshome.headerpage.compTab,driver);
			waitForElementToDisplay(icshome.headerpage.addTableLnk);	
			verifyElementTextContains(icshome.headerpage.currTableLnk, "Current Tables", test);
			verifyElementTextContains(icshome.headerpage.inactiveTableLnk, "Inactive Tables", test);
			verifyElementTextContains(icshome.headerpage.oneTimeTableLnk, "One-Time Tables", test);
			verifyElementTextContains(icshome.headerpage.addTableLnk, "Add Table", test);
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCompensationMenu", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCompensationMenu", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	@Test(enabled = true, priority = 9)
	public void verifyCurrentTables() throws Exception {

		try {
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyCurrentTables");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);

			icshome.headerpage.navigateToHome();
			icshome.headerpage.navigateToCompLnk("Current Tables");
			CurrentTables currTables=new CurrentTables(driver);
			
			waitForElementToDisplay(currTables.groupManagBtn);	
			verifyElementTextContains(currTables.groupManagBtn, "Group Table Manager", test);
			verifyElementTextContains(currTables.percentageBtn, "Percent Change Report", test);
			verifyElementTextContains(currTables.updateSelTabBtn, "Update Selected", test);
			verifyElementTextContains(currTables.exportBtn, "Export", test);
			verifyElementTextContains(currTables.printBtn, "Print", test);
			currTables.showTables();
			waitForElementToDisplay(currTables.selectAllTxt);
			verifyElementTextContains(currTables.tableHeaders.get(2), "Table", test);
			verifyElementTextContains(currTables.tableHeaders.get(2), "Details", test);
			verifyElementTextContains(currTables.tableHeaders.get(3), "Last", test);
			verifyElementTextContains(currTables.tableHeaders.get(3), "Update", test);
			verifyElementTextContains(currTables.tableHeaders.get(4), "Location", test);
			verifyElementTextContains(currTables.tableHeaders.get(5), "Host Country", test);
			verifyElementTextContains(currTables.tableHeaders.get(6), "Host", test);
			verifyElementTextContains(currTables.tableHeaders.get(6), "Location", test);
			verifyElementTextContains(currTables.tableHeaders.get(7), "Price Type", test);
			verifyElementTextContains(currTables.tableHeaders.get(8), "Index", test);
			verifyElementTextContains(currTables.tableHeaders.get(9), "Report", test);
			verifyElementTextContains(currTables.tableHeaders.get(10), "Price Type", test);
			verifyElementTextContains(currTables.tableHeaders.get(11), "Latest", test);
			verifyElementTextContains(currTables.tableHeaders.get(11), "Index", test);
			verifyElementTextContains(currTables.tableHeaders.get(12), "Spendable", test);
			verifyElementTextContains(currTables.tableHeaders.get(12), "Factor", test);
			verifyElementTextContains(currTables.tableHeaders.get(13), "Exchange", test);
			verifyElementTextContains(currTables.tableHeaders.get(14), "Reason For", test);
			verifyElementTextContains(currTables.tableHeaders.get(14), "Last Change", test);
			icshome.headerpage.navigateToHome();
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCurrentTables", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCurrentTables", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	@Test(enabled = true, priority = 10)
	public void verifyOneTimeTables() throws Exception {

		try {
			
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyOneTimeTables");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToCompLnk("One-Time Tables");
			OneTimeTables onetimeTables=new OneTimeTables(driver);
			waitForElementToDisplay(onetimeTables.showTables);
			onetimeTables.showTables();
			waitForElementToDisplay(onetimeTables.fromDate);	
			waitForElementToDisplay(onetimeTables.toDate);

			verifyElementTextContains(onetimeTables.exportBtn, "Export", test);
			verifyElementTextContains(onetimeTables.printBtn, "Print", test);
		
			verifyElementTextContains(onetimeTables.tableHeaders.get(0), "Table", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(0), "Details", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(1), "Order Date", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(2), "Location", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(3), "Country", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(4), "Location", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(5), "Price Type", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(6), "Index", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(7), "Report", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(8), "Home", test);
			verifyElementTextContains(onetimeTables.tableHeaders.get(8), "Price Type", test);
			onetimeTables.clickTableInfo();
			waitForElementToDisplay(onetimeTables.historyLnk);
			waitForElementToDisplay(onetimeTables.historyHeader);
			verifyElementTextContains(onetimeTables.historyHeaders.get(0), "View", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(1), "Generation", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(2), "Table", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(2), "Date", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(3), "Table", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(4), "Index", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(5), "Exchange", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(6), "Spendable", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(7), "Points", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(8), "Price", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(9), "Reason For", test);
			verifyElementTextContains(onetimeTables.historyHeaders.get(9), "Last Change", test);
			icshome.headerpage.navigateToHome();
			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyOneTimeTables", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyOneTimeTables", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	
	@Test(enabled = true, priority = 11)
	public void verifyQuickCalc() throws Exception {

		try {
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyQuickCalc");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToCalMenu("Quick Calc");
			QuickCalcPage quickTables=new QuickCalcPage(driver);
			waitForElementToDisplay(quickTables.showTables);
			quickTables.showTables();
			waitForElementToDisplay(quickTables.tabInfoIconLnk);	

			verifyElementTextContains(quickTables.tableHeaders.get(0), "Click to proceed", test);
			verifyElementTextContains(quickTables.tableHeaders.get(1), "One Time Tables", test);
			verifyElementTextContains(quickTables.tableHeaders.get(2), "Home", test);
			verifyElementTextContains(quickTables.tableHeaders.get(2), "Location", test);
			verifyElementTextContains(quickTables.tableHeaders.get(3), "Host Country", test);
			verifyElementTextContains(quickTables.tableHeaders.get(4), "Host", test);
			verifyElementTextContains(quickTables.tableHeaders.get(4), "Location", test);
			verifyElementTextContains(quickTables.tableHeaders.get(5), "Price Type", test);
			verifyElementTextContains(quickTables.tableHeaders.get(6), "Index", test);
			verifyElementTextContains(quickTables.tableHeaders.get(7), "Report", test);
			verifyElementTextContains(quickTables.tableHeaders.get(8), "Home", test);
			verifyElementTextContains(quickTables.tableHeaders.get(8), "Price Type", test);
			quickTables.clickTableInfo();
			quickTables.doCalculate();
			waitForElementToDisplay(quickTables.calcResultTxt);	
			verifyElementTextContains(quickTables.calcResultTxt, "CALCULATED RESULTS", test);
			quickTables.clickDone();
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyQuickCalc", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyQuickCalc", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	@Test(enabled = true, priority = 12)
	public void verifyBalanceSheetCalc() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyBalanceSheetCalc");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToCalMenu("Balance Sheet");
			BalanceSheet balSheet=new BalanceSheet(driver);
			waitForElementToDisplay(balSheet.showTables);
			balSheet.showTables();
			waitForElementToDisplay(balSheet.tabInfoIconLnk);	
			verifyElementTextContains(balSheet.tableHeaders.get(0), "Click to proceed", test);
			verifyElementTextContains(balSheet.tableHeaders.get(1), "     Tables", test);
			verifyElementTextContains(balSheet.tableHeaders.get(2), "Home", test);
			verifyElementTextContains(balSheet.tableHeaders.get(2), "Location", test);
			verifyElementTextContains(balSheet.tableHeaders.get(3), "Host Country", test);
			verifyElementTextContains(balSheet.tableHeaders.get(4), "Host", test);
			verifyElementTextContains(balSheet.tableHeaders.get(4), "Location", test);
			verifyElementTextContains(balSheet.tableHeaders.get(5), "Price Type", test);
			verifyElementTextContains(balSheet.tableHeaders.get(6), "Index", test);
			verifyElementTextContains(balSheet.tableHeaders.get(7), "Report", test);
			verifyElementTextContains(balSheet.tableHeaders.get(8), "Home", test);
			verifyElementTextContains(balSheet.tableHeaders.get(8), "Price Type", test);
			balSheet.clickTableInfo();
			
			waitForElementToDisplay(balSheet.calculateButton);	
			verifyElementTextContains(balSheet.generalInfo, "General Information", test);
			verifyElementTextContains(balSheet.taxInfo, "Tax Information", test);
			verifyElementTextContains(balSheet.housingInfo, "Housing Information", test);
			verifyElementTextContains(balSheet.standardPremiums, "Standard Premiums", test); 
			verifyElementTextContains(balSheet.standardPremiums, "Allowances", test);
			verifyElementTextContains(balSheet.standardPremiums, "Incentives", test);
			
			balSheet.doCalculate();
			waitForElementToDisplay(balSheet.balSheetSchedulTxt);	
			verifyElementTextContains(balSheet.balSheetSchedulTxt, "BALANCE SHEET SCHEDULE", test);
			verifyElementTextContains(balSheet.doneBtn, "Done", test);
			verifyElementTextContains(balSheet.modifyButton, "Modify", test);
			verifyElementTextContains(balSheet.exportBtn, "Export", test);
			verifyElementTextContains(balSheet.printBtn, "Print", test);
			balSheet.clickOnExport();
			Thread.sleep(5000);
			verifyEquals(FileDownloader.getExistanceStatus(balSheetFileName), true,"File "+balSheetFileName+" download success", test);
			verifyEquals(FileDownloader.getDeleteStatus(balSheetFileName), true,"File "+balSheetFileName+" delete success", test);
			balSheet.clickDone();
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyBalanceSheetCalc", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyBalanceSheetCalc", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	
	@Test(enabled = true, priority = 13)
	public void verifySTPM() throws Exception {

		try {
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifySTPM");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToCalMenu("Short Term Per Diem");
			PerDiemPage perDiem=new PerDiemPage(driver);
			waitForElementToDisplay(perDiem.currPerDiemLocTab);
			verifyElementTextContains(perDiem.currPerDiemLocTab, "Current Per Diem Locations", test);
			verifyElementTextContains(perDiem.addperDiemLocTab, "Add Per Diem Locations", test);
			verifyElementTextContains(perDiem.createNewPerDiemTab, "Create New Per Diem Template", test);
			perDiem.currPerDiemLocTab.click();
			waitForElementToDisplay(perDiem.runPerDiem);
			waitForElementToDisplay(perDiem.selectAllTxt);

			//verifyEquals(perDiem.runPerDiem.getAttribute("title"), "Run Per Diem Report", test);
			verifyElementTextContains(perDiem.selectAllTxt, "Select", test);
			verifyElementTextContains(perDiem.selectAllTxt, "All", test);

			verifyElementTextContains(perDiem.hostCountryCol, "Host Country", test);
			verifyElementTextContains(perDiem.hostLocCol, "Location", test);

			verifyElementTextContains(perDiem.orderDateCol, "Order Date", test);
			verifyElementTextContains(perDiem.priceDateCol, "Pricing Date", test);
			verifyElementTextContains(perDiem.purchaseUpdateCol, "Purchase Updates", test);
			perDiem.navigateToTab("Add Per Diem Locations");
			waitForElementToDisplay(perDiem.checkBoxAddPerDiemTab);
			perDiem.navigateToTab("Create New Per Diem Template");
			verifyElementTextContains(perDiem.shortName, "Short Name", test);



			
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifySTPM", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifySTPM", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	
	@Test(enabled = true, priority = 14)
	public void verifyCalcHousing() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyCalcHousing");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToCalMenu("Host Housing");
			HostHousingPage housepage=new HostHousingPage(driver);
			waitForElementToDisplay(housepage.selCountry);
			housepage.calculate("Australia", "Melbourne", "", "Expensive", "House", "Furnished", "");
			
			verifyElementTextContains(housepage.propertyDescript, "Property Description", test);
			
			verifyElementTextContains(housepage.printBtn, "Print", test);
			verifyElementTextContains(housepage.doneBtn, "Done", test);
			housepage.clickOnDone();
			housepage.clickOnPurchaseLoc();
			Products product=new Products(driver);
			waitForElementToDisplay(product.purchasedProdTxt);

			verifyElementTextContains(product.housingEduTab, "Education Reports", test);

			verifyElementTextContains(product.housingEduTab, "Housing", test);

			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCalcHousing", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCalcHousing", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	
	@Test(enabled = true, priority = 15)
	public void verifyProducts() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyProducts");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			hoverOverElement(icshome.headerpage.prodTab, driver);	
			waitForElementToDisplay(icshome.headerpage.homeCountrySubMenuLnk);

			verifyElementTextContains(icshome.headerpage.homeCountrySubMenuLnk, "Home Country/Market Data & Tax Profiles", test);
			verifyElementTextContains(icshome.headerpage.utilityReportsSubMenuLnk, "Utility Reports", test);
			verifyElementTextContains(icshome.headerpage.housingAndEducationSubMenuLnk, "Housing & Education Reports", test);
			verifyElementTextContains(icshome.headerpage.locationEvalSubMenuLnk, "Location Evaluation Reports (Hardship)", test);
			verifyElementTextContains(icshome.headerpage.customReportsShowlSubMenuLnk, "Custom Reports", test);
			verifyElementTextContains(icshome.headerpage.mercerPassportSubMenuLnk, "MercerPassport", test);
			icshome.headerpage.navigateToHome();
			icshome.headerpage.navigateToProdMenu("Home Country/Market Data & Tax Profiles");

			Products products=new Products(driver);
			waitForElementToDisplay(products.purchasedProdTxt);

			verifyElementTextContains(products.selectedTab, "Home Country/Market Data", test);
			verifyElementTextContains(products.selectedTab, "Tax Profiles", test);

			verifyElementTextContains(products.purchasedProdTxt, "Purchased Products", test);
			verifyElementTextContains(products.availForPurchaseTxt, "Available for Purchase", test);
			icshome.headerpage.navigateToHome();

			icshome.headerpage.navigateToProdMenu("Utility Reports");
			waitForElementToDisplay(products.utilityRepPurchaseTxt);
			verifyElementTextContains(products.utilityRepPurchaseTxt, "Purchased Products", test);
			verifyElementTextContains(products.utilityRepAvailTxt, "Available for Purchase", test);
			icshome.headerpage.navigateToHome();
			icshome.headerpage.navigateToProdMenu("Housing & Education Reports");
			waitForElementToDisplay(products.houseEduRepPurchaseTxt);
			verifyElementTextContains(products.houseEduRepPurchaseTxt, "Purchased Products", test);
			verifyElementTextContains(products.houseEduRepAvailTxt, "Available for Purchase", test);

			icshome.headerpage.navigateToHome();
			icshome.headerpage.navigateToProdMenu("Location Evaluation Reports (Hardship)");

			waitForElementToDisplay(products.mercerRecommLocPremBtn);
			
			verifyElementTextContains(products.locHardAvailTxt, "Purchased Products", test);
			icshome.headerpage.navigateToHome();


			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyProducts", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyProducts", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	
	@Test(enabled = true, priority = 16)
	public void verifyPassport() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			test = reports.createTest("verifyPassport");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToProdMenu("MercerPassport");
			Products products=new Products(driver);

			waitForElementToDisplay(products.mercerPassportInfoTxt);

			verifyElementTextContains(products.mercerPassportInfoTxt, "Mercer Passport Information", test);
			verifyElementTextContains(products.downloadBrochureLnk, "Download Brochure", test);

			

			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyPassport", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyPassport", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();
		}
	}
	
	
	@Test(enabled = true, priority = 17)
	public void verifyDataByLocation() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

			test = reports.createTest("verifyDataByLocation");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			hoverOverElement(icshome.headerpage.dataByLocTab, driver);	
			waitForElementToDisplay(icshome.headerpage.hostLocSubMenulnk);
			verifyElementTextContains(icshome.headerpage.hostLocSubMenulnk, "Host Locations", test);
			verifyElementTextContains(icshome.headerpage.homeLocationSubMenulnk, "Home Locations", test);

			icshome.headerpage.navigateToDataMenu("Host Locations");
			DataByLocationPage dataPage=new DataByLocationPage(driver);
			waitForElementToDisplay(dataPage.hardShipTrollTxt);

			verifyElementTextContains(dataPage.locationResourcetxt, "Location Resources", test);
			verifyElementTextContains(dataPage.hardShipTrollTxt, "Hardship recommendations, housing costs, retail outlets used to survey goods & services prices, and data publication schedules are available for the following locations", test);
			clickElement(dataPage.locationDetailLnk);
			waitForElementToDisplay(dataPage.selectedTab);
			verifyElementTextContains(dataPage.selectedTab, "Resources", test);

			verifyElementTextContains(dataPage.tablestab, "Tables", test);
			verifyElementTextContains(dataPage.localmapTab, "Local Map", test);
			
			waitForElementToDisplay(dataPage.availProdResourceTab);
			verifyElementTextContains(dataPage.availProdResourceTab, "Available Products", test);
			verifyElementTextContains(dataPage.otherInfoResourceTab, "Other Information", test);

			clickElement(dataPage.tablestab);
			waitForElementToDisplay(dataPage.excelButton);
			verifyElementTextContains(dataPage.excelButton, "Export", test);
			waitForElementToDisplay(dataPage.tablesData);

			clickElement(dataPage.localmapTab);
			waitForElementToDisappear(dataPage.ajaxLoader);
			waitForElementToDisplay(dataPage.tablesData);

			//verifyElementTextContains(dataPage.searchByCity, "Search by city", test);

			
			icshome.headerpage.navigateToHome();
			icshome.headerpage.navigateToDataMenu("Home Locations");
			waitForElementToDisplay(dataPage.hardShipTrollTxt);
			verifyElementTextContains(dataPage.hardShipTrollTxt, "Home Country/Market Data and Tax Profiles, and Utility Reports are available for the following locations", test);
			clickElement(dataPage.locationDetailLnk);

			waitForElementToDisplay(dataPage.selectedTab);
			verifyElementTextContains(dataPage.selectedTab, "Resources", test);

			verifyElementTextContains(dataPage.tablestab, "Tables", test);
			verifyElementTextContains(dataPage.localmapTab, "Local Map", test);
			
			waitForElementToDisplay(dataPage.availProdResourceTab);
			verifyElementTextContains(dataPage.availProdResourceTab, "Available Products", test);
			verifyElementTextContains(dataPage.otherInfoResourceTab, "Other Information", test);
			icshome.headerpage.navigateToMenu("Logout");
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyDataByLocation", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyDataByLocation", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	

	@Test(enabled = true, priority = 18)
	public void verifyMyAccount() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

			test = reports.createTest("verifyMyAccount");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToMenu("My Account");
			MyAccountPage myAccount=new MyAccountPage(driver);
			waitForElementToDisplay(myAccount.myPrefTab);
			verifyElementTextContains(myAccount.selectedTab, "My Contact Information", test);
			verifyElementTextContains(myAccount.myPrefTab, "My Preferences", test);
			clickElement(myAccount.myPrefTab);
			waitForElementToDisplay(myAccount.eventNotifyTab);
			verifyElementTextContains(myAccount.eventNotifyTab, "Event Notification", test);

			waitForElementToDisplay(myAccount.mercerCompTableCheck);
			boolean expCheck;
			if(myAccount.mercerCompTableCheck.isSelected())
				expCheck=false;
			else
				expCheck=true;
			clickElement(myAccount.mercerCompTableCheck);
			clickElement(myAccount.submitBtn);
			waitForElementToDisplay(myAccount.mercerCompTableCheck);
			Thread.sleep(4000);
			verifyEquals(myAccount.mercerCompTableCheck.isSelected(), expCheck,"", test);
			clickElement(myAccount.contactInfoTab);
			waitForElementToDisplay(myAccount.emailTxt);
			verifyElementTextContains(myAccount.userInfoTxt, "User Information", test);
			verifyElementTextContains(myAccount.contactInfoTxt, "Contact Information", test);
			verifyElementTextContains(myAccount.addressTxt, "Address", test);
			verifyElementTextContains(myAccount.emailTxt, "@mercer.com", test);


			clickElement(myAccount.okBtn);

			myAccount.headerpage.navigateToMenu("Logout");
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyMyAccount", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyMyAccount", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();
			

		}
	}
	


	
	@Test(enabled = true, priority = 19)
	public void verifyContactTab() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

			test = reports.createTest("verifyContactTab");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToMenu("Contact");
			ContactPage contact=new ContactPage(driver);
			waitForElementToDisplay(contact.selectedTab);
			verifyElementTextContains(contact.selectedTab, "My Consultant", test);
			verifyElementTextContains(contact.contactTxt, "Contact", test);
			

			verifyElementTextContains(contact.consultantTxt, "Consultant", test);

			contact.headerpage.navigateToMenu("Logout");
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyContactTab", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyContactTab", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();
			

		}
	}
	

	@Test(enabled = true, priority = 20)
	public void verifyAdminTab() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

			test = reports.createTest("verifyAdminTab");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			hoverOverElement(icshome.headerpage.adminLnk,driver);
			waitForElementToDisplay(icshome.headerpage.companyAccounts);

			verifyElementHyperLink(icshome.headerpage.manageSubscriptions,test); 
			
			verifyElementHyperLink(icshome.headerpage.orderHistory,test); 

			verifyElementHyperLink(icshome.headerpage.contactsAndRoles,test); 

			verifyElementHyperLink(icshome.headerpage.productsMaintenance,test); 

			verifyElementHyperLink(icshome.headerpage.manageMaps,test); 

			verifyElementHyperLink(icshome.headerpage.manageLogging,test); 

			clickElement(icshome.headerpage.companyAccounts);
			CompanyAccountsPage company=new CompanyAccountsPage(driver);
			waitForElementToDisplay(company.namesWithM);
			clickElement(company.namesWithM);
			waitForElementToDisplay(company.resultsWithMEle);

			for(WebElement companyName:company.resultsWithM)
			{
				verifyEquals(companyName.getText().startsWith("M"),true,"",test);
			}

			icshome.headerpage.navigateToAdminMenu("Manage Subscriptions");
			ManageSubscripPage manageSubscriptPage=new ManageSubscripPage(driver);
			waitForElementToDisplay(manageSubscriptPage.selectedTab);
			verifyElementTextContains(manageSubscriptPage.subscriptionsTab, "Subscriptions", test);

			clickElement(manageSubscriptPage.viewLnk);

			waitForElementToDisplay(manageSubscriptPage.subscriptNameTxt);
			verifyElementTextContains(manageSubscriptPage.subscriptNameTxt, "Subscription", test);
			verifyElementTextContains(manageSubscriptPage.subscriptNameTxt, "Name", test);
			verifyElementTextContains(manageSubscriptPage.subscriptDetailTxt, "Details", test);
			
			icshome.headerpage.navigateToAdminMenu("Order History");
			OrderHistory orderHistory=new OrderHistory(driver);
			
			waitForElementToDisplay(orderHistory.selectedTab);
			verifyElementTextContains(orderHistory.selectedTab, "Table Orders", test);
			verifyElementTextContains(orderHistory.productOrderTab, "Product Orders", test);
			waitForElementToDisplay(orderHistory.selTablesClientName);

			orderHistory.selectTablesClient("9651789");

			clickElement(orderHistory.tableOrdersShowBtn);
			waitForElementToDisplay(orderHistory.dataTabClientName);
			
			clickElement(orderHistory.productOrderTab);
			waitForElementToDisplay(orderHistory.selClientName);

			orderHistory.selectClient("9651789");
			clickElement(orderHistory.productOrdersShowBtn);
			waitForElementToDisplay(orderHistory.prodClientName);
			verifyElementTextContains(orderHistory.prodClientName, "Blackstone Group", test);
			
			icshome.headerpage.navigateToAdminMenu("Contacts And Roles");
			ContactsAndRolesPage contactRoles=new ContactsAndRolesPage(driver);
			waitForElementToDisplay(contactRoles.selectedTab);
			verifyElementTextContains(contactRoles.selectedTab, "Client Contacts", test);
			verifyElementTextContains(contactRoles.deleteClinetTab, "Deleted Client Contacts", test);

			verifyElementTextContains(contactRoles.invalidClientTab, "Invalid Client Contacts", test);

			verifyElementTextContains(contactRoles.roleAdminTab, "Role Administration", test);
			clickElement(contactRoles.roleAdminTab);
			waitForElementToDisplay(contactRoles.roleDesc);
			verifyElementTextContains(contactRoles.roleDesc, "Description", test);
			
			contactRoles.headerpage.navigateToMenu("Logout");
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAdminTab", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyAdminTab", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();
			

		}
	}
	
	@Test(enabled = true, priority = 21)
	public void verifyLogout() throws Exception {

		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");

			test = reports.createTest("verifyLogout");
			test.assignCategory("Regression");
			driver = driverFact.getEventDriver(webdriver, test);
			LoginPage login = new LoginPage(driver);
			login.login(USERNAME, PASSWORD);
			ICSHomePage icshome=new ICSHomePage(driver);
			icshome.headerpage.navigateToMenu("Logout");
			waitForElementToDisplay(login.signIn);
			} 

		 catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyLogout", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyLogout", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();
			driver.close();
			

		}
	}
	




	@AfterSuite
	public void killDriver() {

		killBrowserExe(BROWSER_TYPE);

	}

}
