<!DOCTYPE html>
<%@page import="utilities.MyExtentReports"%>
<%@page import="java.io.File"%>

<html lang="en">
<head>
<script type="text/javascript">
	function selectAllReg() {
		var items = document.getElementsByName('RegTestCases');
		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}

	function selectAllSauce() {
		var items = document.getElementsByName('SaucelabTests');

		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}
	function selectAllSmoke() {
		var items = document.getElementsByName('SmokeTestCases');
		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}

	function selectAllRest() {
		var items = document.getElementsByName('RestAPITests');
		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}
	function selectAllSOAP() {
		var items = document.getElementsByName('SOAPTests');
		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}
	function selectAllGrid() {
		var items = document.getElementsByName('GridTests');
		for (var i = 0; i < items.length; i++) {
			if (items[i].type == 'checkbox' && items[i].checked == false)
				items[i].checked = true;
			else if (items[i].type == 'checkbox' && items[i].checked == true)
				items[i].checked = false;

		}
	}
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Creative One Page Parallax Template">
<meta name="keywords"
	content="Creative, Onepage, Parallax, HTML5, Bootstrap, Popular, custom, personal, portfolio" />
<meta name="author" content="">
<title>Mercer Test Automation Framework</title>
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/prettyPhoto.css" rel="stylesheet">
<link href="./css/font-awesome.min.css" rel="stylesheet">
<link href="./css/animate.css" rel="stylesheet">
<link href="./css/main.css" rel="stylesheet">
<link href="./css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]-->
<link rel="shortcut icon" href="Jsp/images/ico/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<style>
#LockOn {
	display: block;
	visibility: hidden;
	position: absolute;
	z-index: 999;
	top: 0px;
	left: 0px;
	width: 105%;
	height: 105%;
	background-color: white;
	vertical-align: bottom;
	padding-top: 20%;
	filter: alpha(opacity = 75);
	opacity: 0.75;
	font-size: large;
	color: blue;
	font-style: italic;
	font-weight: 400;
	background-image: url("images/loader.gif");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
}

html, body {
	max-width: 100%;
	overflow-x: hidden;
}
</style>
</head>
<!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner">
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
			</div>
		</div>
	</div>
	<!--/.preloader-->
	<div id="LockOn"></div>
	<!--/.Loading Image-->

	<header id="navigation">
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>

						<!-- <img style="position: absolute; top: 16px; right: 86px;"
							src="./images/LTI_Lets_solve.png" alt="logo">  --><img
							src="./images/mercer.png" alt="logo">

					</h1>

					<!-- 					<h3 style="color: #069">CTB Applications</32>
 -->
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="scroll active"><a href="#navigation"><b>Home</b></a></li>
						<li class="scroll"><a href="#smoke"><b>Smoke</b></a></li>

						<li class="scroll"><a href="#regression"><b>Regression</b></a></li>

						<li class="scroll"><a href="#RestAPI"><b>REST
									WebServices</b></a></li>
						<li class="scroll"><a href="#SOAP"><b>SOAP
									WebServices</b></a></li>

						<li class="scroll"><a href="#saucelabs"><b>Sauce Labs</b></a></li>
						<li class="scroll"><a href="#seleniumgrid"><b>Selenium
									Grid</b></a></li>

						<li class="scroll"><a
							href="./ATU Reports/Results/ConsolidatedPage.html"
							target="_blank"><b>ATU Reports</b></a></li>
						<%@ page import="java.util.ArrayList"%>
						<%@ page import="utilities.DateUtils"%>

						<%
							/* String time = DateUtils.getCurrTimeStamp();
							MyExtentReports.timeStamp = time;
							 File folder = new File("C:\\Users\\testlab1\\uiframework\\src\\main\\WebContent\\extentReports");
							String links[]=folder.list(); */

							out.println("<li class='scroll'><a href='reports.jsp'><b>ExtentReports</b></a></li>");
							/* out.println("<li class='scroll'><a href='./extentReports/" + links[0]
									+ "'target='_blank'><b>Extent Reports</b></a></li>"); */
							/* out.println("<li class='scroll'><a href='./extentReports/" + time
									+ ".html' target='_blank'><b>Extent Reports</b></a></li>"); */
						%>
						<!-- 												<li class="scroll"><a href="#services"><b>Services</b></a></li>
 -->
					</ul>
				</div>
			</div>
		</div>
		<!--/navbar-->
	</header>
	<!--/#navigation-->

	<section id="home">
		<div class="home-pattern"></div>
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#main-carousel" data-slide-to="1"></li>
				<li data-target="#main-carousel" data-slide-to="2"></li>
			</ol>
			<!--/.carousel-indicators-->
			<div class="carousel-inner">
				<div class="item active"
					style="background-image: url(./images/slider/slider1.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInDown">Live Testing</h2>
							<p class="animated bounceInUp">Manual and Automated Tests on
								100's of Real devices & Browsers</p>
							<a class="btn btn-default slider-btn animated fadeIn" href="#">Get
								Started</a>
						</div>
					</div>
				</div>
				<div class="item"
					style="background-image: url(./images/slider/slider4.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInDown">Visual
								Verification</h2>
							<p class="animated bounceInUp">Automated Screenshots &
								Comparision</p>
							<a class="btn btn-default slider-btn animated fadeIn" href="#">Get
								Started</a>
						</div>
					</div>
				</div>
				<div class="item"
					style="background-image: url(./images/slider/slider3.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInRight">E2E Testing</h2>
							<p class="animated bounceInLeft">Covers all functional and
								Non-functional aspects</p>
							<a class="btn btn-default slider-btn animated bounceInUp"
								href="#">Get Started</a>
						</div>
					</div>
				</div>
			</div>
			<!--/.carousel-inner-->

			<a class="carousel-left member-carousel-control hidden-xs"
				href="#main-carousel" data-slide="prev"><i
				class="fa fa-angle-left"></i></a> <a
				class="carousel-right member-carousel-control hidden-xs"
				href="#main-carousel" data-slide="next"><i
				class="fa fa-angle-right"></i></a>
		</div>
	</section>
	<!--/#home-->

	<!-- -------------------------------------------------------------------------------------------------------------------- -->
	<section id="services" class="parallax-section">
		<div class="container">
			<div class="row text-center">
				<div class="col-sm-8 col-sm-offset-2">
					<h2 class="title-one">Services</h2>
					<p>Here we have listed the list of services that are provided
						by this framework; SauceLabs- a third party cloud service provider
						whcih allows to run the scripts in different browser; Jenkins-
						CI/CD tool allows you to schedule your jobs; Reports-See all your
						test reports here</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="our-service">
						<div class="services row">
							<div class="col-sm-4">
								<div class="single-service">
									<a href="http://saucelabs.com/" target="_blank"> <i
										class="fa fa-th"></i></a>
									<h2>SauceLabs</h2>
									<p>See all test cases running in cloud based environment
										called saucelabs with various verions of Browser and with
										various devices</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="single-service">
									<a href="http://localhost:8082" target="_blank"> <i
										class="fa fa-html5"></i>
										<h2>Jenkins dashboard</h2></a>
									<p>See all Jobs configured in continous integration tool
										called jenkins and run your jobs and schedule your jobs as
										Daily run or weekly run</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="single-service">
									<a href="./ATU Reports/index.html" target="_blank"> <i
										class="fa fa-users"></i></a>
									<h2>Reports</h2>
									<p>See all previous reports in buildwise manner, ATU
										Reports are used for Reporting which provides you GUI Based
										reports with screenshots error</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#service-->
	<!-- ---------------------------------------------------------------------------------------------------------------- -->
	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase = ExcelReader.ReadDriverSuiteExcel("Regression");
	%>
	<section id="regression">

		<div class="row text-center clearfix">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="contact-heading">
					<h2 class="title-one">Regression Suite</h2>
					<p>Regression Testing is defined as a type of software testing
						to confirm that a recent program or code change has not adversely
						affected existing features.</p>
				</div>
			</div>
		</div>

		<!-- <div class="row">
				<div class="col-sm-12">
					<div class="services row"> -->
		<div id="contact-form-section">
			<!-- 							<div class="status alert alert-success" style="display: none"></div> -->
			<form action="regression" id="contact-form" class="contact"
				name="contact-form">
				<label><h4>Select Browser&nbsp;</h4></label> <select
					name="browserName" id="browser">


					<option value="chrome">Chrome</option>
					<option value="IE">Internet Explorer</option>
					<option value="FF">Firefox</option>
					<option value="phantomjs">Headless</option>

				</select> </label> <!-- <label>
					<h4>Select Environment&nbsp;</h4>
				</label> <select name="browserName" id="browser">


					<option value="QA">QA</option>
					<option value="Staging">Staging</option>
					<option value="Production">Production</option>
					<option value="UAT">UAT</option>
					<option value="CIT">CIT</option>
					<option value="CSO">CSO</option>


				</select> </label> -->
				<div class="form-group">

					<h3>Select Applications To Run:</h3>

					<div
						style="overflow: scroll; height: 250px; width: 100%; overflow: auto">
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllReg()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < Testcase.size(); i++) {

									//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
									out.println("<tr><td><h4><input type='checkbox' name='RegTestCases' value='" + Testcase.get(i) + "'>"
											+ "  " + Testcase.get(i) + "</h4></td>");

								}
							%>
						</table>
						<button type="submit" class="btn btn-primary" name='submit'
							onClick="$('#LockOn').css('visibility', 'visible');">Run
							Test</button>
					</div>


				</div>
			</form>
		</div>

	</section>
	<!-- Regression-->
	<!-- -------------------------------------------------------------------------------------------------------- -->
	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase1 = ExcelReader.ReadDriverSuiteExcel("Smoke");
	%>
	<section id="smoke" class="parallax-section">

		<div class="row text-center">
			<div class="col-sm-8 col-sm-offset-2">
				<h2 class="title-one">Smoke Test</h2>
				<p>Smoke Testing, also known as "Build Verification Testing", is
					a type of software testing that comprises of a non-exhaustive set
					of tests that aim at ensuring that the most important functions
					work. The results of this testing is used to decide if a build is
					stable enough to proceed with further testing.</p>
			</div>
		</div>






		<div id="contact-form-section">



			<form action="smoke" id="contact-form" class="contact"
				name="contact-form">
				<label><h4>Select Browser&nbsp;</h4></label> <select
					name="browserName" id="browser">

					<option value="chrome">Chrome</option>
					<option value="IE">Internet Explorer</option>
					<option value="FF">Firefox</option>
					<option value="phantomjs">Headless</option>
					<!-- <option value = "Safari">Safari</option>
               <option value = "Edge">Edge</option> -->

				</select>
				<!-- <label><h4>&nbsp;&nbsp;Execution Environment&nbsp;</h4></label>
							
							<select name="env" id = "env">
							
               <option  value = "local">Local</option>
               <option value = "grid">Selenium Grid</option>
               <option value = "saucelabs">SauceLabs</option>
        
             </select> -->
				<div class="form-group">

					<h3>Select Applications To Run:</h3>


					<div
						style="overflow: scroll; height: 250px; width: 100%; overflow: auto">
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllSmoke()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < Testcase1.size(); i++) {

									//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
									out.println("<tr><td><h4><input type='checkbox' name='SmokeTestCases' value='" + Testcase1.get(i) + "'>"
											+ "  " + Testcase1.get(i) + "</h4></td>");

								}
							%>
						</table>

						<button type="submit" class="btn btn-primary" name='submit'
							onClick="$('#LockOn').css('visibility', 'visible');">Run
							Test</button>
					</div>
				</div>
			</form>

		</div>


	</section>
	<!--/#Smoke-->



	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase2 = ExcelReader.ReadDriverSuiteExcel("RestAPI");
	%>

	<section id="RestAPI" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">REST WebServices Tests</h2>
						<p>Web Services Testing is testing of Web services and its
							Protocols like SOAP & REST. To test a Webservice you can Test
							ManuallyCreate your own Automation Code Use an off-the shelf
							automation tool like SoapUI.</p>
					</div>
				</div>
			</div>

			<!-- 	<div class="container">
				<div class="our-service">
					<div class="services row"> -->
			<div id="contact-form-section">
				<!-- 							<div class="status alert alert-success" style="display: none"></div>
 -->
				<form action="rest" id="contact-form" class="contact"
					name="contact-form">
					<div class="form-group">
						<h3>Select the testcases to run</h3>
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllRest()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < Testcase2.size(); i++) {
									//loop to move the next checkboxes testcases to next row is if condition meet
									if (i % 6 == 0) {
										out.println("<tr/>");
									}
									out.println("<td><h4><input type='checkbox' name='RestAPITests' value='" + Testcase2.get(i) + "'>"
											+ "  " + Testcase2.get(i) + "&nbsp; &nbsp;" + "</h4></td>");

								}
							%>
							<div class="form-group">
								<tr>
									<td><button type="submit" class="btn btn-primary"
											name='submit'
											onClick="$('#LockOn').css('visibility', 'visible');">Run
											Test</button>
								</tr>
							</div>
						</table>
					</div>
				</form>
			</div>
		</div>
		<!-- 	</div>
			</div>
		</div> -->
	</section>

	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> soaptests = ExcelReader.ReadDriverSuiteExcel("SOAPWebservice");
	%>

	<section id="SOAP" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">SOAP WebServices Tests</h2>
						<p>Web Services Testing is testing of Web services and its
							Protocols like SOAP & REST. To test a Webservice you can Test
							ManuallyCreate your own Automation Code Use an off-the shelf
							automation tool like SoapUI.</p>
					</div>
				</div>
			</div>

			<!-- 		<div class="container">
				<div class="our-service">
					<div class="services row"> -->
			<div id="contact-form-section">
				<div class="status alert alert-success" style="display: none"></div>
				<form action="soap" id="contact-form" class="contact"
					name="contact-form">
					<div class="form-group">
						<h3>Select the testcases to run</h3>
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllSOAP()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < soaptests.size(); i++) {
									//loop to move the next checkboxes testcases to next row is if condition meet
									if (i % 5 == 0) {
										out.println("<tr/>");
									}
									out.println("<td><h4><input type='checkbox' name='SOAPTests' value='" + soaptests.get(i) + "'>" + "  "
											+ soaptests.get(i) + "&nbsp; &nbsp;" + "</h4></td>");
								}
							%>
							<div class="form-group">
								<tr>
									<td><button type="submit" class="btn btn-primary"
											name='submit'
											onClick="$('#LockOn').css('visibility', 'visible');">Run
											Test</button>
								</tr>
							</div>
						</table>
					</div>
				</form>
			</div>
		</div>
		<!-- </div>
			</div>
		</div> -->
	</section>


	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase3 = ExcelReader.ReadDriverSuiteExcel("SauceLabs");
	%>

	<section id="saucelabs" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Sauce Labs</h2>
						<p>See all test cases running in cloud based environment
							called Sauce Labs with various versions of Browser and with
							various devices</p>
					</div>
				</div>
			</div>

			<!-- 	<div class="container">
				<div class="our-service">
					<div class="services row"> -->
			<div id="contact-form-section">
				<div class="status alert alert-success" style="display: none"></div>
				<form action="saucelabs" id="contact-form" class="contact"
					name="contact-form">
					<label><h4>Select Browser&nbsp;</h4></label> <select
						name="browserName" id="browser">


						<option value="chrome">Chrome</option>
						<option value="IE">Internet Explorer</option>
						<option value="FF">Firefox</option>
						<option value="Safari">Safari</option>
						<option value="Edge">Edge</option>
						<option value="Edge">Android Emulator</option>
						<option value="Edge">IOS Simulator</option>

					</select></label>
					<div class="form-group">
						<h3>Select the testcases to run</h3>
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllSauce()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < Testcase3.size(); i++) {
									if (i % 6 == 0) {
										out.println("<tr/>");
									}

									out.println("<td><h4><input type='checkbox' name='SaucelabTests' value='" + Testcase3.get(i) + "'>"
											+ "  " + Testcase3.get(i) + "&nbsp; &nbsp;" + "</h4></td>");

								}
							%>
							<div class="form-group">
								<tr>
									<td><button type="submit" class="btn btn-primary"
											name='submit'
											onClick="$('#LockOn').css('visibility', 'visible');">Run
											Test</button>
								</tr>
							</div>
						</table>
					</div>
				</form>
			</div>
		</div>
		<!-- </div>
			</div>
		</div> -->
	</section>


	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase4 = ExcelReader.ReadDriverSuiteExcel("SeleniumGrid");
	%>

	<section id="seleniumgrid" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">SeleniumGrid</h2>
						<p>Selenium Grid is a part of the Selenium Suite that
							specializes in running multiple tests across different browsers,
							operating systems, and machines in parallel.</p>
					</div>
				</div>
			</div>

			<!-- 	<div class="container">
				<div class="our-service">
					<div class="services row"> -->
			<div id="contact-form-section">
				<div class="status alert alert-success" style="display: none"></div>
				<form action="seleniumgrid" id="contact-form" class="contact"
					name="contact-form">
					<label><h4>Select Browser&nbsp;</h4></label> <select
						name="browserName" id="browser">


						<option value="chrome">Chrome</option>
						<option value="IE">Internet Explorer</option>
						<option value="FF">Firefox</option>

					</select></label>
					<div class="form-group">
						<h3>Select the testcases to run</h3>
						<table>
							<tr>
								<td><h4>
										<input type=checkbox onclick='selectAllGrid()'
											value="Select All" />Select All
									</h4></td>
							</tr>

							<%
								for (int i = 0; i < Testcase4.size(); i++) {
									if (i % 5 == 0) {
										out.println("<tr/>");
									}
									out.println("<td><h4><input type='checkbox' name='GridTests' value='" + Testcase4.get(i) + "'>" + "  "
											+ Testcase4.get(i) + "&nbsp; &nbsp;" + "</h4></td>");

								}
							%>
							<div class="form-group">
								<tr>
									<td><button type="submit" class="btn btn-primary"
											name='submit'
											onClick="$('#LockOn').css('visibility', 'visible');">Run
											Test</button>
								</tr>
							</div>
						</table>
					</div>
				</form>
			</div>
		</div>
		<!-- 	</div>
			</div>
		</div> -->
	</section>



	<!-- -------------------------------------------------------------------------------------------------------- -->

	<!--/#portfolio-->
	<!-- --------------------------------------------------------------------------------------------------------------------- -->
	<!-- ------------------------------------------------------------------------------------------------------ -->
	<footer id="footer">
		<div class="container">
			<div class="text-center">
				<p>
					Copyright &copy; 2019 - <a href="https://www.mercer.com/">Mercer</a>
					| All Rights Reserved
				</p>
			</div>
		</div>
	</footer>
	<!--/#footer-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/jquery.parallax.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>