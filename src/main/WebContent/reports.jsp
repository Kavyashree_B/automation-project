<!DOCTYPE html>
<%@page import="utilities.MyExtentReports"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<html lang="en">
<head>
<title>Execution Summary</title>

<link rel="stylesheet" type="text/css"
	href="./HTML_Design_Files/CSS/design.css" />
<link rel="stylesheet" type="text/css"
	href="./HTML_Design_Files/CSS/jquery.jqplot.css" />
<link rel="Stylesheet" type="text/css"
	href="./HTML_Design_Files/CSS/jquery-ui.min.css" />

<script type="text/javascript"
	src="./HTML_Design_Files/JS/jquery.min.js"></script>
<script type="text/javascript"
	src="./HTML_Design_Files/JS/jquery.jqplot.min.js"></script>
<!--[if lt IE 9]>
        <script type="text/javascript" src="./HTML_Design_Files/JS/excanvas.js"></script>
        <![endif]-->


<script type="text/javascript"
	src="./HTML_Design_Files/JS/jqplot.barRenderer.min.js"></script>
<script type="text/javascript"
	src="./HTML_Design_Files/JS/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript"
	src="./HTML_Design_Files/JS/jqplot.pointLabels.min.js"></script>

<script type="text/javascript"
	src="./HTML_Design_Files/JS/jqplot.highlighter.min.js"></script>

<script type="text/javascript" src="./Results/barChart.js"></script>
<script type="text/javascript" src="./Results/lineChart.js"></script>
<script type="text/javascript"
	src="./HTML_Design_Files/JS/jquery-ui.min.js"></script>

<script type="text/javascript">
            $(document).ready(function() {
                $("#tabs").tabs();

                $('#tabs').bind('tabsshow', function(event, ui) {
/*                     if (ui.index === 1 && plot1._drawCount === 0) {
 */                        plot1.replot();
 							plot2.replot();
                    
                });
            });
        </script>
</head>

<body>
	<table id="mainTable">
		<tr id="header">
			<td id="logo">
				<div style="padding-left: 20px; float: left">

					<img src="./HTML_Design_Files/morneau.png" alt="Logo" height="50"
						width="140" />
				</div> <br />
			</td>
			<td align="center" id="headertext"><b
				style="padding-left: 70px; float: left">Automation Testing
					Reports</b>
				<div style="padding-right: 20px; float: right">
					<img src="./HTML_Design_Files/LTI_Lets_solve.png" height="50"
						width="90" />
				</div></td>
		</tr>


		<tr id="container">
			<td id="menu">
				<ul>


					<li class="menuStyle"><b>Reports</b></li>

					<%
					ClassLoader loader = this.getClass().getClassLoader();
					Properties props = new Properties();
					InputStream input = loader.getResourceAsStream("config/testdata.properties");
					props.load(input);
					String dir_path = props.getProperty("userdir");

					System.out.println("jsp--dir " + dir_path);

					File folder = new File(dir_path + "\\src\\main\\WebContent\\extentReports");
					String links[] = folder.list();
					String links1[]=new String[links.length];
					List<String> time=new ArrayList<String>();
					List<Date> timeStamp=new ArrayList<Date>();
					for (int i = 0; i < links.length; i++)
						if (links[i].contains(".html"))
						{
							links1[i]=links[i].replace("hh", "");
							links1[i]=links1[i].replace("mm", "");
							links1[i]=links1[i].replace("ss", "");
							links1[i]=links1[i].replace(".html", "");
							 SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");  
						     timeStamp.add(sdf.parse(links1[i]));
							
						}
				int j=timeStamp.size();
					Collections.sort(timeStamp,Collections.reverseOrder());
					for(Date reportDate:timeStamp)
					{
						 SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");  
						 String date=sdf.format(reportDate);
					out.println("<li class='menuStyle'><a href='./extentReports/" + date+".html" + "'target='_blank'><b>"+date+ "</b></a></li>");
					 //out.println("<li class='menuStyle'><a href='./ATU Reports/Results/Run_"+j+"/ATU_CompleteSuiteRecording.mov' target='_blank'><b>Video</b></a></li>");
					 j--;

					}
					
					
					
					
					%>

				</ul>
			</td>




			<!-- <td id="content">

                    <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">Line Chart</a></li>
                            <li><a href="#tabs-2">Bar Chart</a></li>      
                        </ul>
                        <div id="tabs-1" style="text-align: left;">
                            <p class="info" style="text-align: center;color: black;font-size: 14px">
                                The following Line chart demonstrates the number of Passed, Failed and Skipped Test Cases
                                in last 10 Runs
                            </p>                            
                            <div id="line" style="height: 270px;  width: 85%; margin-top: 20px;color:black;"></div>

                        </div>
                        <div id="tabs-2" style="text-align: left;">
                            <p class="info" style="text-align: center;color: black;font-size: 14px">
                                The following Bar chart demonstrates the number of Passed, Failed and Skipped Test Cases
                                in last 10 Runs
                            </p>
                            <div id="bar" style="margin-top:20px; margin-left:20px; width:85%; height:300px;color:black;"></div>
                        </div>    
                    </div> 
                </td> -->
		</tr>
		<tr id="footer">
			<td colspan="2">Best Viewed in &nbsp; <a
				href="http://www.mozilla.org/en-US/firefox/new/">Firefox</a> &nbsp;
				<a href="http://www.apple.com/in/safari/">Safari</a>&nbsp; <a
				href="http://www.google.com/chrome/">Chrome</a>&nbsp; <a
				href="http://windows.microsoft.com/en-IN/internet-explorer/download-ie">IE
					9 & Above</a>&nbsp; &nbsp; Extent Report Version: v3.1.5 &nbsp; Reports
				by: <a href="http://extentreports.com//">Extent Reports</a>
			</td>
		</tr>
	</table>
	<!-- -------------------------------------------------------------------------------------------------------- -->

	<!--/#portfolio-->
	<!-- --------------------------------------------------------------------------------------------------------------------- -->
	<!-- ------------------------------------------------------------------------------------------------------ -->
	<footer id="footer">
		<div class="container">
			<div class="text-center">
				<p>
					Copyright &copy; 2019 - <a href="https://www.mercer.com/">Mercer</a>
					| All Rights Reserved
				</p>
			</div>
		</div>
	</footer>
	<!--/#footer-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/jquery.parallax.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>